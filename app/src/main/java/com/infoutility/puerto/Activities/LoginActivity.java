package com.infoutility.puerto.Activities;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;

import com.google.common.collect.ImmutableMap;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.infoutility.puerto.Api.ResourceHandler;
import com.infoutility.puerto.Api.Status;
import com.infoutility.puerto.Keys.Key;
import com.infoutility.puerto.MainActivity;
import com.infoutility.puerto.Models.Handheld;
import com.infoutility.puerto.Models.Login;
import com.infoutility.puerto.R;
import com.infoutility.puerto.Utils.AlertUtil;
import com.infoutility.puerto.Utils.PreferencesManager;
import com.infoutility.puerto.Utils.Tools;

import java.util.List;

import pub.devrel.easypermissions.AppSettingsDialog;
import pub.devrel.easypermissions.EasyPermissions;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener, com.infoutility.puerto.Interfaces.Response.OnResponse, EasyPermissions.PermissionCallbacks {

    public static String IPADDRESS = "";
    private Intent homeActivity;

//    view
    private TextInputEditText tietUsername;
    private TextInputEditText tietPassword;
    private Button btnLogin;
    private Spinner spModule;

    private Key.modules selectedModule;

    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        PreferencesManager.initializeInstance(this);

        String ipAddress = PreferencesManager.getInstance().getString(Key.sharedPreferences.ipAddress, null);
        if(ipAddress == null){
            PreferencesManager.getInstance().setString(Key.sharedPreferences.ipAddress, Key.api.baseUrl);
            ipAddress = Key.api.baseUrl;
        }
        IPADDRESS = ipAddress;


        tietUsername = findViewById(R.id.al_tiet_username);
        tietPassword = findViewById(R.id.al_tiet_password);
        spModule = findViewById(R.id.al_sp_module);
        btnLogin = findViewById(R.id.al_btn_login);


        spModule.setAdapter(new ArrayAdapter<>(this, R.layout.support_simple_spinner_dropdown_item, Key.modules.values()));
        btnLogin.setOnClickListener(this);
        ((Button)findViewById(R.id.al_btn_ip)).setOnClickListener(this);

        doPermissionRequest();

        homeActivity = new Intent(LoginActivity.this, MainActivity.class);

        if (PreferencesManager.getInstance().getBoolean(Key.sharedPreferences.isLogged, false))
            goHome();
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.al_btn_login:
                doLogin();
                break;
            case R.id.al_btn_ip:
                Intent ip = new Intent(LoginActivity.this, ChangeIPActivity.class);
                startActivity(ip);
                break;
        }
    }


    private void setDeviceImei(){
        if (PreferencesManager.getInstance().getString(Key.sharedPreferences.imei, null) == null)
                PreferencesManager.getInstance().setString(Key.sharedPreferences.imei, Tools.getDeviceImei(this));
    }

    private void doPermissionRequest(){
        String[] perms = {
                Manifest.permission.INTERNET,
                Manifest.permission.ACCESS_WIFI_STATE,
                Manifest.permission.READ_PHONE_STATE
        };


        if (!EasyPermissions.hasPermissions(this, perms)) {
            EasyPermissions.requestPermissions(this, getString(R.string.all_msg_permissions), Key.requestCode.PERMISSIONS, perms);
        }
    }


    private void doLogin(){
        selectedModule = (Key.modules) spModule.getSelectedItem();

        if (selectedModule.id() != Key.modules.SELECT.id()
            ){
            if (validate()){
                initProgressDialog("Iniciando Sesion...");
                ResourceHandler.doLogin(this, Key.requestCode.GET_LOGIN, tietUsername.getText().toString(), tietPassword.getText().toString());
            }

        } else {
            AlertUtil.showAlertOk(getResources().getString(R.string.all_text_warning), getResources().getString(R.string.al_error_valid_option), this, new AlertUtil.OnOkListener() {
                @Override
                public void onOk() {
                    //do nothing
                }
            }).show();
        }
    }

    private void goHome(){
        homeActivity.putExtra(Key.extra.SELECTED_MODULE, PreferencesManager.getInstance().getInt(Key.sharedPreferences.previousSelectedModelue, Key.modules.SELECT.id()));
        startActivity(homeActivity);
        finish();
    }


    private boolean validate() {
        if (tietUsername.getText() == null || tietUsername.getText().toString().trim().equals("")){
            tietUsername.setError(getResources().getString(R.string.al_error_login_username));
          return false;
        }

        if (tietPassword.getText() == null || tietPassword.getText().toString().trim().equals("")){
            tietPassword.setError(getResources().getString(R.string.al_error_login_password));
            return false;
        }

        return true;
    }

    private void doHandhelVerify(){
        String imei = PreferencesManager.getInstance().getString(Key.sharedPreferences.imei, null);
        if (imei != null){
            ResourceHandler.getHandhelds(this, Key.requestCode.GET_HANDHELD, ImmutableMap.of(Key.api.paramHandheldId, imei));
        } else {
            AlertUtil.showAlertOk(getString(R.string.all_text_warning), getString(R.string.all_msg_imei_error), this, new AlertUtil.OnOkListener() {
                @Override
                public void onOk() {

                }
            }).show();
        }

    }


    private void initProgressDialog(String msg){
        progressDialog = new ProgressDialog(this);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage(msg);
        progressDialog.setCancelable(false);
        progressDialog.show();
    }


    @Override
    public void onResponse(int requestCode, Object o, Object r) {
        switch (requestCode){
            case Key.requestCode.GET_LOGIN:
                progressDialog.dismiss();
                if (o instanceof JsonObject){
                    JsonObject response = (JsonObject) o;
                    Login login = new Gson().fromJson(response.getAsJsonArray(Key.jsonResponse.data).get(0).toString(), Login.class);
                    if (login.getGrabador() != -1 && login.getGrabador() != 0){
                        PreferencesManager.getInstance().setInt(Key.sharedPreferences.recorder, login.getGrabador());
                        PreferencesManager.getInstance().setInt(Key.sharedPreferences.previousSelectedModelue, selectedModule.id());
                        PreferencesManager.getInstance().setString(Key.sharedPreferences.userName, login.getUsuario());
                        PreferencesManager.getInstance().setInt(Key.sharedPreferences.userRole, login.getRol());
                        doHandhelVerify();

                    } else {
                        AlertUtil.showAlertOk(getResources().getString(R.string.all_text_warning), getResources().getString(R.string.al_error_login), this, new AlertUtil.OnOkListener() {
                            @Override
                            public void onOk() {
                                //do nothing
                            }
                        }).show();
                    }

                } else if (o instanceof Status) {
                    Status status = (Status) o;
                    AlertUtil.showAlertOk(getResources().getString(R.string.all_text_warning), status.getMessage(), this, new AlertUtil.OnOkListener() {
                        @Override
                        public void onOk() {
                            //do nothing
                        }
                    }).show();
                }
                break;
            case Key.requestCode.GET_HANDHELD:
                if (o instanceof JsonObject){
                    JsonObject response = (JsonObject) o;
                    if (response.getAsJsonArray(Key.jsonResponse.data).size() > 0){
                        Handheld handheld = new Gson().fromJson(response.getAsJsonArray(Key.jsonResponse.data).get(0).toString(), Handheld.class);

                        if (handheld.getEstado().equals("H")){
                            PreferencesManager.getInstance().setString(Key.sharedPreferences.handheld, handheld.getHandheld_id());
                            PreferencesManager.getInstance().setBoolean(Key.sharedPreferences.isLogged, true);

                            goHome();

                        } else {
                            AlertUtil.showAlertOk(getResources().getString(R.string.all_text_warning),
                                    getResources().getString(R.string.all_msg_handheld_error_status),
                                    this,
                                    new AlertUtil.OnOkListener() {
                                        @Override
                                        public void onOk() {
                                            //do nothing
                                        }
                                    }).show();
                        }
                    } else {
                        AlertUtil.showAlertOk(getResources().getString(R.string.all_text_warning),
                                getResources().getString(R.string.all_msg_handheld_error_configuration),
                                this,
                                new AlertUtil.OnOkListener() {
                                    @Override
                                    public void onOk() {
                                        //do nothing
                                    }
                                }).show();
                    }
                } else if (o instanceof Status) {
                    Status status = (Status) o;
                    AlertUtil.showAlertOk(getResources().getString(R.string.all_text_warning), status.getMessage(), this, new AlertUtil.OnOkListener() {
                        @Override
                        public void onOk() {
                            //do nothing
                        }
                    }).show();
                }
                break;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        EasyPermissions.requestPermissions(this, "Todos los permisos son necesarios para ejecutar esta aplicación.", requestCode, permissions);
    }


    @Override
    public void onPermissionsGranted(int requestCode, @NonNull List<String> perms) {
        setDeviceImei();
    }

    @Override
    public void onPermissionsDenied(int requestCode, @NonNull List<String> perms) {
        if (EasyPermissions.somePermissionPermanentlyDenied(this, perms)) {
            new AppSettingsDialog.Builder(this).build().show();
        }
    }
}
