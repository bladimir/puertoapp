package com.infoutility.puerto.Activities;

import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.infoutility.puerto.Keys.Key;
import com.infoutility.puerto.R;
import com.infoutility.puerto.Utils.PreferencesManager;

public class ChangeIPActivity extends AppCompatActivity implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        PreferencesManager.initializeInstance(this);
        setContentView(R.layout.activity_change_ip);
        ((Button)findViewById(R.id.aci_btn_success)).setOnClickListener(this);
        String ipAddress = PreferencesManager.getInstance().getString(Key.sharedPreferences.ipAddress, "");
        ((TextInputEditText)findViewById(R.id.aci_tiet_number)).setText(ipAddress);
    }


    private void setValue(){
        String info = ((TextInputEditText)findViewById(R.id.aci_tiet_number)).getText().toString();
        String key = ((TextInputEditText)findViewById(R.id.aci_tiet_key)).getText().toString();
        if(key.equals(Key.api.keyBaseUrl)){
            PreferencesManager.getInstance().setString(Key.sharedPreferences.ipAddress, info);
            ((TextView)findViewById(R.id.aci_txt_msj)).setText("Almacenado completamente, por favor cerrar la aplicacion y volverla a abrir para aplicar cambios");
        }else{
            ((TextView)findViewById(R.id.aci_txt_msj)).setText("La clave no es la correcta");
        }
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.aci_btn_success:
                setValue();
                break;
        }
    }
}
