package com.infoutility.puerto.Utils;

import android.os.Bundle;
import android.os.Parcelable;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Iterator;

import com.infoutility.puerto.Keys.Key;

public class JsonBundle {

    public static Bundle createBundleString(String obj) throws JSONException {
        JSONObject jsonObject = new JSONObject(obj);
        return createBundle(jsonObject);
    }

    public static Bundle createArrayBundleString(String obj) throws JSONException {
        JSONArray jsonArray = new JSONArray(obj);
        return createBundleArray(jsonArray);
    }

    public static JSONArray createArrayOfAnyBundle(ArrayList innerArray) throws JSONException {

        JSONArray bElements = new JSONArray();


        JSONObject bInnerData;

        for (int j = 0; j < innerArray.size(); j++) {
            if (innerArray.get(j) instanceof Bundle) {
                bInnerData = createJsonFromBundle((Bundle)innerArray.get(j));
                bElements.put(bInnerData);
            } else{
                bElements.put(innerArray.get(j));
            }
        }

        if (bElements.length() > 0) {
            return bElements;
        }
        return new JSONArray();
    }


    /*public static void mostrarerror(String titulo, String contenido, Activity activity, int contentFragment){
        DialogError dialogError = new DialogError();
        dialogError.setTextos(titulo, contenido);
        ((FrameLayout)activity.findViewById(contentFragment)).setVisibility(View.VISIBLE);
        FragmentTransaction fragmentTransaction = activity.getFragmentManager().beginTransaction();
        fragmentTransaction.replace(contentFragment, dialogError);
        fragmentTransaction.commit();
    }

    public static void mostraFaltaArticulo(String titulo, String contenido, Activity activity, int contentFragment){
        DialogFaltaerror dialogFaltaerror = new DialogFaltaerror();
        dialogFaltaerror.setTextos(titulo, contenido);
        ((FrameLayout) activity.findViewById(contentFragment)).setVisibility(View.VISIBLE);
        FragmentTransaction fragmentTransaction = activity.getFragmentManager().beginTransaction();
        fragmentTransaction.replace(contentFragment, dialogFaltaerror);
        fragmentTransaction.commit();
    }*/


    public static ArrayList createArrayOfAny(JSONArray innerArray) throws JSONException {

        ArrayList<Bundle> bElements = new ArrayList<>();
        ArrayList<String> sElements = new ArrayList<>();
        ArrayList<Integer> iElements = new ArrayList<>();
        ArrayList<Double> dElements = new ArrayList<>();
        ArrayList oElements = new ArrayList<>();


        Bundle bInnerData;

        for (int j = 0; j < innerArray.length(); j++) {
            if (innerArray.get(j) instanceof JSONObject) {
                bInnerData = createBundle(innerArray.getJSONObject(j));
                bElements.add(bInnerData);
            } else if (innerArray.get(j) instanceof String) {
                sElements.add(innerArray.getString(j));
            } else if (innerArray.get(j) instanceof Integer) {
                iElements.add(innerArray.getInt(j));
            } else if (innerArray.get(j) instanceof Double) {
                dElements.add(innerArray.getDouble(j));
            } else {
                oElements.add(innerArray.get(j));
            }
        }

        if (bElements.size() > 0) {
            return bElements;
        }

        if (sElements.size() > 0) {
            return sElements;
        }

        if (iElements.size() > 0) {
            return iElements;
        }
        if (dElements.size() > 0) {
            return dElements;
        }
        if (oElements.size() > 0) {
            return oElements;
        }
        return new ArrayList();
    }

    public static Bundle createBundle(JSONObject obj) throws JSONException {

        Bundle data = new Bundle();
        Iterator<String> iter = obj.keys();

        while (iter.hasNext()) {
            String key = iter.next();

            if (obj.get(key) instanceof String) {
                data.putString(key, obj.getString(key));
            } else if (obj.get(key) instanceof Integer) {
                data.putInt(key, obj.getInt(key));
            } else if (obj.get(key) instanceof Long) {
                data.putLong(key, obj.getLong(key));
            } else if (obj.get(key) instanceof Double) {
                data.putDouble(key, obj.getDouble(key));
            } else if (obj.get(key) instanceof Boolean) {
                data.putBoolean(key, obj.getBoolean(key));
            } else if (obj.get(key) instanceof JSONObject) {
                data.putBundle(key, createBundle(obj.getJSONObject(key)));
            } else if (obj.get(key) instanceof JSONArray) {

                JSONArray innerArray = obj.getJSONArray(key);

                if (innerArray != null && innerArray.length() > 0) {

                    ArrayList<Bundle> bElements = new ArrayList<>();
                    ArrayList<String> sElements = new ArrayList<>();
                    ArrayList<Integer> iElements = new ArrayList<>();

                    ArrayList aElements = new ArrayList<>();
                    Bundle bInnerData;

                    for (int j = 0; j < innerArray.length(); j++) {

                        if (innerArray.get(j) instanceof JSONObject) {
                            bInnerData = createBundle(innerArray.getJSONObject(j));
                            bElements.add(bInnerData);
                        } else if (innerArray.get(j) instanceof String) {
                            sElements.add(innerArray.getString(j));
                        } else if (innerArray.get(j) instanceof Integer) {
                            iElements.add(innerArray.getInt(j));
                        } else if (innerArray.get(j) instanceof Long) {
                            sElements.add(String.valueOf(innerArray.getLong(j)));
                        } else if (innerArray.get(j) instanceof Double) {
                            sElements.add(String.valueOf(innerArray.getDouble(j)));
                        } else if (innerArray.get(j) instanceof JSONArray) {
                            aElements.add(createArrayOfAny((JSONArray) innerArray.get(j)));
                        }
                    }
                    if (bElements.size() > 0) {
                        data.putParcelableArrayList(key, bElements);
                    }

                    if (sElements.size() > 0) {
                        data.putStringArrayList(key, sElements);
                    }
                    if (iElements.size() > 0) {
                        data.putIntegerArrayList(key, iElements);
                    }

                    if (aElements.size() > 0) {
                        data.putParcelableArrayList(key, aElements);
                    }
                }
            }
        }

        if (data.isEmpty()) {
            return null;
        }

        return data;
    }



    public static Bundle createBundleArray(JSONArray objArray) throws JSONException {

        Bundle data = new Bundle();

        if (objArray != null && objArray.length() > 0) {

            ArrayList<Bundle> bElements = new ArrayList<>();
            Bundle bInnerData;


            for (int j = 0; j < objArray.length(); j++) {

                if (objArray.get(j) instanceof JSONObject) {
                    bInnerData = createBundle(objArray.getJSONObject(j));
                    bElements.add(bInnerData);
                }
            }
            if (bElements.size() > 0) {
                data.putParcelableArrayList(Key.bundle.bundleArray, bElements);
            }

        }


        if (data.isEmpty()) {
            return null;
        }

        return data;
    }

    public static JSONObject createJsonFromBundle(Bundle data) throws JSONException {
        JSONObject obj = new JSONObject();

        for (String key : data.keySet()) {
            if (data.get(key) instanceof String) {
                obj.put(key, data.getString(key));
            }
            if (data.get(key) instanceof Integer) {
                obj.put(key, data.getInt(key));
            }
            if (data.get(key) instanceof Double) {
                obj.put(key, data.getDouble(key));
            }
            if (data.get(key) instanceof Boolean) {
                obj.put(key, data.getBoolean(key));
            }
            if (data.get(key) instanceof Bundle) {
                obj.put(key, createJsonFromBundle((Bundle) data.get(key)));
            }


            if (data.get(key) instanceof ArrayList) {
                ArrayList<Parcelable> hoy = data.getParcelableArrayList(key);
                JSONArray array = new JSONArray();
                for (Object uno : hoy) {
                    if (uno instanceof Bundle) {
                        array.put(createJsonFromBundle((Bundle) uno));
                    }else if(uno instanceof String){
                        array.put(uno);
                    }else if(uno instanceof Integer){
                        array.put(uno);
                    }else if(uno instanceof ArrayList){
                        array.put(createArrayOfAnyBundle((ArrayList)uno));
                    }
                }
                if (array.length() > 0) {
                    obj.put(key, array);
                }
            }
        }
        if (data.isEmpty()) {
            return null;
        }

        return obj;
    }


}
