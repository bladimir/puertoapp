package com.infoutility.puerto.Utils;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;

import com.infoutility.puerto.R;

public class AlertUtil {
    public interface OnOkListener
    {
        void onOk();
    }

    public interface OnCancelListener
    {
        void onCancel();
    }

    public static AlertDialog.Builder showAlertOk(String title, String message, Context context, final OnOkListener onOkListener){
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(title);
        builder.setMessage(message);

        builder.setPositiveButton(R.string.all_text_accept, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                onOkListener.onOk();
            }
        });

        builder.setCancelable(false);
        return builder;
    }

    public static AlertDialog.Builder showAlertConfirmation(String title, String message, Context context, String positiveText, final OnOkListener onOkListener, final OnCancelListener onCancelListener){
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(title);
        builder.setMessage(message);

        builder.setPositiveButton(positiveText, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                onOkListener.onOk();
            }
        });

        builder.setNegativeButton(R.string.all_text_cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                onCancelListener.onCancel();
            }
        });
        return builder;
    }
}
