package com.infoutility.puerto.Utils;

import android.content.Context;
import android.content.SharedPreferences;

import com.infoutility.puerto.Keys.Key;

import java.util.Map;

public class PreferencesManager {

    private static PreferencesManager sInstance;
    private final SharedPreferences mPref;

    private PreferencesManager(Context context) {
        mPref = context.getSharedPreferences(Key.sharedPreferences.name, Context.MODE_PRIVATE);
    }

    public static synchronized void initializeInstance(Context context) {
        if (sInstance == null) {
            sInstance = new PreferencesManager(context);
        }
    }

    public static synchronized PreferencesManager getInstance() {
        if (sInstance == null) {
            throw new IllegalStateException(PreferencesManager.class.getSimpleName() +
                    " is not initialized, call initializeInstance(..) method first.");
        }
        return sInstance;
    }

    public void invalidateSession(){
//        mPref.edit().clear().apply();
        Map<String, ?> prefs = mPref.getAll();
        for (Map.Entry<String, ?> prefToReset : prefs.entrySet()){
            if (!prefToReset.getKey().equals(Key.sharedPreferences.imei) && !prefToReset.getKey().equals(Key.sharedPreferences.ipAddress))
                mPref.edit().remove(prefToReset.getKey()).apply();
        }
    }

    public void setString(String key, String value) {
        mPref.edit()
                .putString(key, value)
                .apply();
    }

    public String getString(String key, String defValue) {
        return mPref.getString(key, defValue);
    }


    public void setLong(String key, long value) {
        mPref.edit()
                .putLong(key, value)
                .apply();
    }

    public long getLong(String key, long defValue) {
        return mPref.getLong(key, defValue);
    }


    public void setInt(String key, int value) {
        mPref.edit()
                .putInt(key, value)
                .apply();
    }

    public int getInt(String key, int defValue) {
        return mPref.getInt(key, defValue);
    }

    public void setBoolean(String key, boolean value) {
        mPref.edit()
                .putBoolean(key, value)
                .apply();
    }

    public boolean getBoolean(String key, boolean defValue) {
        return mPref.getBoolean(key, defValue);
    }

}
