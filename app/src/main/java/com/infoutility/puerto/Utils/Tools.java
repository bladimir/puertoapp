package com.infoutility.puerto.Utils;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.telephony.TelephonyManager;
import android.view.View;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.infoutility.puerto.Adapters.TextAdapter;
import com.infoutility.puerto.Keys.Key;
import com.infoutility.puerto.MainActivity;
import com.infoutility.puerto.Models.ContainerCondition;
import com.infoutility.puerto.Models.ContainerMeasure;
import com.infoutility.puerto.Models.ContainerType;
import com.infoutility.puerto.Models.Crane;
import com.infoutility.puerto.Models.DelayType;
import com.infoutility.puerto.Models.LoadType;
import com.infoutility.puerto.Models.Machine;
import com.infoutility.puerto.Models.MoveType;
import com.infoutility.puerto.Models.PinboxType;
import com.infoutility.puerto.Models.ShiftType;
import com.infoutility.puerto.Models.TypeRelocation;
import com.infoutility.puerto.Models.Yard;
import com.infoutility.puerto.R;

import org.parceler.Parcels;

import java.lang.reflect.Type;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import static android.content.Context.TELEPHONY_SERVICE;

public class Tools {


    public static <T> List<T> getListFromJson(String jsonArray, Class<T> clazz) {
        Type typeOfT = TypeToken.getParameterized(List.class, clazz).getType();
        return new Gson().fromJson(jsonArray, typeOfT);
    }

    @SuppressWarnings("unchecked")
    public static <T> List<TextAdapter> fromJson(String jsonArray, Class<T> clazz) {
        Type typeOfT = TypeToken.getParameterized(List.class, clazz).getType();
        List<TextAdapter> spList = new ArrayList<>();

        if (ShiftType.class.equals(clazz)) {
            for (ShiftType member : (List<ShiftType>) new Gson().fromJson(jsonArray, typeOfT)) {
                spList.add(new TextAdapter(String.valueOf(member.getId_tipo_turno()), member.getNombre()));
            }
        } else if (ContainerCondition.class.equals(clazz)) {
            for (ContainerCondition member : (List<ContainerCondition>) new Gson().fromJson(jsonArray, typeOfT)) {
                spList.add(new TextAdapter(String.valueOf(member.getCodigo()), member.getDescripcion()));
            }
        } else if (MoveType.class.equals(clazz)) {
            for (MoveType member : (List<MoveType>) new Gson().fromJson(jsonArray, typeOfT)) {
                spList.add(new TextAdapter(String.valueOf(member.getId_movimiento()), member.getDescripcion()));
            }
        } else if (Crane.class.equals(clazz)) {
            for (Crane member : (List<Crane>) new Gson().fromJson(jsonArray, typeOfT)) {
                spList.add(new TextAdapter(String.valueOf(member.getCodigo()), member.getNombre()));
            }
        } else if (ContainerType.class.equals(clazz)) {
            for (ContainerType member : (List<ContainerType>) new Gson().fromJson(jsonArray, typeOfT)) {
                spList.add(new TextAdapter(String.valueOf(member.getCodigo()), member.getDescripcion()));
            }
        } else if (ContainerMeasure.class.equals(clazz)) {
            for (ContainerMeasure member : (List<ContainerMeasure>) new Gson().fromJson(jsonArray, typeOfT)) {
                spList.add(new TextAdapter(String.valueOf(0), String.valueOf(member.getMedida())));
            }
        } else if (LoadType.class.equals(clazz)) {
            for (LoadType member : (List<LoadType>) new Gson().fromJson(jsonArray, typeOfT)) {
                spList.add(new TextAdapter(String.valueOf(member.getCodigo()), String.valueOf(member.getDescripcion())));
            }
        } else if (DelayType.class.equals(clazz)) {
            for (DelayType member : (List<DelayType>) new Gson().fromJson(jsonArray, typeOfT)) {
                spList.add(new TextAdapter(String.valueOf(member.getDemora()), String.valueOf(member.getDemora()) + " - " + member.getNombre_demora(), member.getDescripcion()));
            }
        } else if (PinboxType.class.equals(clazz)) {
            for (PinboxType member : (List<PinboxType>) new Gson().fromJson(jsonArray, typeOfT)) {
                spList.add(new TextAdapter(String.valueOf(member.getCodigo()), member.getDescripcion()));
            }
        }  else if (Yard.class.equals(clazz)) {
            for (Yard member : (List<Yard>) new Gson().fromJson(jsonArray, typeOfT)) {
                spList.add(new TextAdapter(String.valueOf(member.getPatio()), member.getNombre()));
            }
        }  else if (Machine.class.equals(clazz)) {
            for (Machine member : (List<Machine>) new Gson().fromJson(jsonArray, typeOfT)) {
                spList.add(new TextAdapter(String.valueOf(member.getMaquina()), String.valueOf(member.getMaquina()), member.getDescripcion()));
            }
        }  else if (TypeRelocation.class.equals(clazz)) {
            for (TypeRelocation member : (List<TypeRelocation>) new Gson().fromJson(jsonArray, typeOfT)) {
                spList.add(new TextAdapter(String.valueOf(member.getCod_movimiento()), String.valueOf(member.getDescripcion())));
            }
        }
        return spList;
    }


    public static void setFragment(FragmentManager fragmentManager, int contentFragment, Fragment replace, String tag) {
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out);
        fragmentTransaction.replace(contentFragment, replace);
        fragmentTransaction.addToBackStack(tag);
        fragmentTransaction.commit();
    }


    public static void showDialogFragment(FragmentManager fragmentManager, DialogFragment dialogFragment, String tag) {
        dialogFragment.show(fragmentManager, tag);
    }

    public static String formatDate(Date date, String format) {
        SimpleDateFormat sdf = new SimpleDateFormat(format, Locale.getDefault());
        return sdf.format(date);
    }

    public static Date toDate(String strDate, String format){
        SimpleDateFormat sdf = new SimpleDateFormat(format, Locale.getDefault());
        try {
            return sdf.parse(strDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String formatStrDate(String strDate, String format){
        return formatDate(toDate(strDate, format), format);
    }

    public static <T> Bundle fromObject(String key, T t) {
        Bundle bundle = new Bundle();
        bundle.putParcelable(key, Parcels.wrap(t));
        return bundle;
    }

    public static <T> T fromBundle(String key, Bundle b) {
        return Parcels.unwrap(b.getParcelable(key));
    }


    public static void setFragmentLoading(Context context, boolean visible) {
        if (context instanceof MainActivity) {
            ((MainActivity) context).findViewById(R.id.fl_main_content_fragment_loading).setVisibility(visible ? View.VISIBLE : View.GONE);
        }
    }


    public static String getDeviceMacAddress(Context context) {
        WifiManager wifiManager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
        WifiInfo wifiInfo = wifiManager.getConnectionInfo();
        if (wifiInfo != null)
            return wifiInfo.getMacAddress();
        return null;
    }

    public static String getDeviceAndroidId(Context context) {
        return Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);
    }

    public static String getDeviceImei(Context context) {
        TelephonyManager TelephonyMgr = (TelephonyManager) context.getSystemService(TELEPHONY_SERVICE);
        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
            return null;
        }
        return TelephonyMgr.getDeviceId();
    }
}
