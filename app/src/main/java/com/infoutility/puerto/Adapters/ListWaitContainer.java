package com.infoutility.puerto.Adapters;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.infoutility.puerto.Interfaces.Response;
import com.infoutility.puerto.Keys.Key;
import com.infoutility.puerto.Models.ContainerPendingList;
import com.infoutility.puerto.R;

import java.util.ArrayList;
import java.util.List;

public class ListWaitContainer extends RecyclerView.Adapter<ListWaitContainer.ViewHolder> {

    private List<ContainerPendingList> listWait;
    private Response.OnResponse onResponse;

    public ListWaitContainer(List<ContainerPendingList> listWait, Response.OnResponse onResponse) {
        this.listWait = listWait;
        this.onResponse = onResponse;
    }

    @NonNull
    @Override
    public ListWaitContainer.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        CardView cardView = (CardView) LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.cardview_list_wait, viewGroup, false);
        return new ListWaitContainer.ViewHolder(cardView, onResponse, this.listWait);
    }

    @Override
    public void onBindViewHolder(@NonNull ListWaitContainer.ViewHolder viewHolder, int i) {
        CardView cardView = viewHolder.cardView;
        ContainerPendingList containerPendingList = listWait.get(i);
        String typeMov = containerPendingList.getTipo_movimiento();
        String titleMov = "";
        if(typeMov.equals("R")) titleMov = "Recepcion";
        if(typeMov.equals("I")) titleMov = "Importacion";
        if(typeMov.equals("D")) titleMov = "Despacho";
        if(typeMov.equals("E")) titleMov = "Exportacion";

        ((TextView)cardView.findViewById(R.id.clw_txt_mov)).setText(titleMov);
        ((TextView)cardView.findViewById(R.id.clw_txt_ref)).setText("Referencia: " + containerPendingList.getReferencia());
        ((TextView)cardView.findViewById(R.id.clw_txt_number)).setText("Numero: " + containerPendingList.getNumero_identifiacion());
        ((TextView)cardView.findViewById(R.id.clw_txt_pref)).setText("Prefijo" + containerPendingList.getPrefijo());
    }

    @Override
    public int getItemCount() {
        return (listWait!=null)? listWait.size() : 0;
    }

    protected static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        private CardView cardView;
        private Response.OnResponse onResponse;
        private List<ContainerPendingList> listWait;

        public ViewHolder(CardView v, Response.OnResponse onResponse, List<ContainerPendingList> listWait){
            super(v);
            cardView = v;
            this.listWait = listWait;
            this.onResponse = onResponse;
            cardView.setOnClickListener(this);
            //((Button)cardView.findViewById(R.id.clrr_btn_select)).setOnClickListener(this);
        }


        @Override
        public void onClick(View view) {
            Log.i("ContainerYardFragment", getAdapterPosition()+"");
            this.onResponse.onResponse(Key.requestCode.RESPONSE_CARD, getAdapterPosition(), null);

        }
    }
}
