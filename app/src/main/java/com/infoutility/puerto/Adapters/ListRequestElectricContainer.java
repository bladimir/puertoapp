package com.infoutility.puerto.Adapters;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.infoutility.puerto.Interfaces.Response;
import com.infoutility.puerto.Keys.Key;
import com.infoutility.puerto.R;
import com.infoutility.puerto.Utils.Tools;

import java.util.ArrayList;

public class ListRequestElectricContainer extends RecyclerView.Adapter<ListRequestElectricContainer.ViewHolder>{
    private ArrayList<Bundle> list;
    private Response.OnResponse onResponse;


    public ListRequestElectricContainer(ArrayList<Bundle> list, Response.OnResponse onResponse) {
        this.list = list;
        this.onResponse = onResponse;
    }

    @NonNull
    @Override
    public ListRequestElectricContainer.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        CardView cardView = (CardView) LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.cardview_list_electronic_contanier, viewGroup, false);
        return new ListRequestElectricContainer.ViewHolder(cardView, onResponse, this.list);
    }

    @Override
    public void onBindViewHolder(@NonNull ListRequestElectricContainer.ViewHolder viewHolder, int i) {
        CardView cardView = viewHolder.cardView;
        Bundle bundle = list.get(i);
        String tempDate = bundle.getString("fecha_desconeccion", null);
        String dateT = "----";
        if(tempDate !=null){
            dateT = "Fecha desconexion: " + Tools.formatStrDate( tempDate, Key.datetimePattern.YYYYMMDD_HHMMSS_DASH_SEPARATOR);
        }
        ((TextView)cardView.findViewById(R.id.clec_txt_desc)).setText(dateT);
        tempDate = bundle.getString("fecha_coneccion", null);
        dateT = "----";
        if(tempDate !=null){
            dateT = "Fecha conexion: " + Tools.formatStrDate( tempDate, Key.datetimePattern.YYYYMMDD_HHMMSS_DASH_SEPARATOR);
        }
        ((TextView)cardView.findViewById(R.id.clec_txt_conect)).setText(dateT);
        ((TextView)cardView.findViewById(R.id.clec_txt_number)).setText("Num.:" + bundle.getString("numero_de_identificacion", ""));
        ((TextView)cardView.findViewById(R.id.clec_txt_pref)).setText("Pref.:" + bundle.getString("prefijo", ""));
        ((TextView)cardView.findViewById(R.id.clec_txt_remark)).setText(bundle.getString("observacion", ""));
    }

    @Override
    public int getItemCount() {
        return (list!=null)? list.size() : 0;
    }


    protected static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        private CardView cardView;
        private Response.OnResponse onResponse;
        private ArrayList<Bundle> listStudent;

        public ViewHolder(CardView v, Response.OnResponse onResponse, ArrayList<Bundle> listStudent){
            super(v);
            cardView = v;
            this.listStudent = listStudent;
            this.onResponse = onResponse;
        }


        @Override
        public void onClick(View view) {
            //Log.i("ContainerYardFragment", getAdapterPosition()+"");
            //this.onResponse.onResponse(Key.requestCode.RESPONSE_CARD, getAdapterPosition(), null);

        }
    }


}
