package com.infoutility.puerto.Adapters;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.infoutility.puerto.Interfaces.Response;
import com.infoutility.puerto.Keys.Key;
import com.infoutility.puerto.Models.MeterUser;
import com.infoutility.puerto.R;
import com.infoutility.puerto.Realm.Model.ElectricModel;

import java.util.ArrayList;
import java.util.List;

public class ListSearchPowerMeter extends RecyclerView.Adapter<ListSearchPowerMeter.ViewHolder>{

    private ArrayList<ElectricModel> list;
    private Response.OnResponse onResponse;

    public ListSearchPowerMeter(ArrayList<ElectricModel> list, Response.OnResponse onResponse) {
        this.list = list;
        this.onResponse = onResponse;
    }

    @NonNull
    @Override
    public ListSearchPowerMeter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        CardView cardView = (CardView) LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.cardview_list_search_power, viewGroup, false);
        return new ListSearchPowerMeter.ViewHolder(cardView, onResponse, this.list);
    }

    @Override
    public void onBindViewHolder(@NonNull ListSearchPowerMeter.ViewHolder viewHolder, int i) {
        CardView cardView = viewHolder.cardView;
        ElectricModel meterUser = this.list.get(i);
        ((TextView)cardView.findViewById(R.id.clsp_txt_name)).setText(String.valueOf(meterUser.getMus_lectura()));
        ((TextView)cardView.findViewById(R.id.clsp_txt_cod)).setText("Codigo: " + meterUser.getUel_codigo());
        ((TextView)cardView.findViewById(R.id.clsp_txt_num)).setText("Contador: " + meterUser.getUel_contador());
        ((TextView)cardView.findViewById(R.id.clsp_txt_tar_cod)).setText("Tar cogigo: " + meterUser.getUel_tar_codigo());
        String allText = "Nombre: " + meterUser.getUel_nombre() + "\n" +
                "Direccion: " + meterUser.getUel_direccion() + "\n" +
                //"Tipo: " + meterUser.get + "\n" +
                //"Estatus: " + meterUser.getUel_estatus() + "\n" +
                "Tarifa Quetzal: " + meterUser.getTar_tarifa_quetzal()+ "\n" +
                "Tafifa Dolar: " + meterUser.getTar_tarifa_dolar()+ "\n" +
                "Numero Tarjeta: " + meterUser.getUel_numero_tarjeta()+ "\n"
                //"Observaciones: " + meterUser.getUel_observaciones() + "\n" +
                //"Descripcion: " + meterUser.getTar_descripcion()
                ;
        ((TextView)cardView.findViewById(R.id.clsp_txt_remark)).setText(allText);
    }

    @Override
    public int getItemCount() {
        return (list!=null)? list.size() : 0;
    }

    protected static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        private CardView cardView;
        private Response.OnResponse onResponse;
        private ArrayList<ElectricModel> listStudent;

        public ViewHolder(CardView v, Response.OnResponse onResponse, ArrayList<ElectricModel> listStudent){
            super(v);
            cardView = v;
            this.listStudent = listStudent;
            this.onResponse = onResponse;
            ((Button)cardView.findViewById(R.id.clsp_btn_next)).setOnClickListener(this);
        }


        @Override
        public void onClick(View view) {
            Log.i("ContainerYardFragment", getAdapterPosition()+"");
            this.onResponse.onResponse(Key.requestCode.RESPONSE_CARD, getAdapterPosition(), null);

        }
    }



}
