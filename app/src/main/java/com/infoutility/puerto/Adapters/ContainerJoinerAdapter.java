package com.infoutility.puerto.Adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;

import com.google.common.collect.ImmutableMap;
import com.infoutility.puerto.Adapters.ViewHolders.ContainerJoinedViewHolder;
import com.infoutility.puerto.Interfaces.Response;
import com.infoutility.puerto.Keys.Key;
import com.infoutility.puerto.Models.ContainerJoined;
import com.infoutility.puerto.R;
import com.infoutility.puerto.Utils.AlertUtil;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class ContainerJoinerAdapter extends RecyclerView.Adapter<ContainerJoinedViewHolder> implements Filterable {

    private Context mContext;
    private List<ContainerJoined> list;
    private List<ContainerJoined> listFiltered;
    private Response.OnResponse onResponse;
    private boolean isJoined;

    public ContainerJoinerAdapter(Context mContext, List<ContainerJoined> list, Response.OnResponse onResponse, boolean isJoined) {
        this.mContext = mContext;
        this.list = list;
        this.listFiltered = list;
        this.onResponse = onResponse;
        this.isJoined = isJoined;
    }

    @NonNull
    @Override
    public ContainerJoinedViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.cardview_container_join, parent, false);

        return new ContainerJoinedViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ContainerJoinedViewHolder containerJoinedViewHolder, int i) {
        final ContainerJoined containerJoined = listFiltered.get(i);

        if (isJoined){
            containerJoinedViewHolder.tvIdJoin.setText(mContext.getResources().getString(R.string.ccj_title_id_join, String.valueOf(containerJoined.getId_atado())));
            containerJoinedViewHolder.tvId.setText("Atados: \n\n" + containerJoined.getRelated());
            containerJoinedViewHolder.tvPrefix.setVisibility(View.GONE);
            containerJoinedViewHolder.tvTrip.setVisibility(View.GONE);
            containerJoinedViewHolder.tvMove.setVisibility(View.GONE);
            containerJoinedViewHolder.btnUnjoin.setVisibility(View.GONE);
            containerJoinedViewHolder.btnSelect.setVisibility(View.VISIBLE);
        } else {
            containerJoinedViewHolder.tvIdJoin.setText(mContext.getResources().getString(R.string.ccj_title_id_join, String.valueOf(containerJoined.getId_atado())));
            containerJoinedViewHolder.tvPrefix.setText(mContext.getResources().getString(R.string.ccj_title_prefix, containerJoined.getPrefijo()));
            containerJoinedViewHolder.tvId.setText(mContext.getResources().getString(R.string.ccj_title_id, containerJoined.getNumero_de_identificacion()));
            containerJoinedViewHolder.tvTrip.setText(mContext.getResources().getString(R.string.ccj_title_trip, String.valueOf(containerJoined.getViaje_no())));
            containerJoinedViewHolder.tvMove.setText(mContext.getResources().getString(R.string.ccj_title_move, containerJoined.getTipo_de_movimiento() == 1 ? "Descarga" : "Carga"));
            containerJoinedViewHolder.btnUnjoin.setVisibility(View.VISIBLE);
            containerJoinedViewHolder.btnSelect.setVisibility(View.GONE);
        }



        containerJoinedViewHolder.btnUnjoin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertUtil.showAlertConfirmation(mContext.getString(R.string.all_text_warning),
                        mContext.getString(R.string.all_msg_container_unjoin),
                        mContext,
                        mContext.getString(R.string.all_text_unjoin),
                        new AlertUtil.OnOkListener() {
                            @Override
                            public void onOk() {
                                onResponse.onResponse(Key.requestCode.CONTAINER_JOIN_UNJOINED, containerJoined, null);
                            }
                        }, new AlertUtil.OnCancelListener() {
                            @Override
                            public void onCancel() {
                                //do nothing
                            }
                        }).show();
            }
        });


        containerJoinedViewHolder.btnSelect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onResponse.onResponse(Key.requestCode.CONTAINER_JOIN_SELECTION, containerJoined, null);
            }
        });
    }

    @Override
    public int getItemCount() {
        return listFiltered.size();
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    listFiltered = list;
                } else {
                    List<ContainerJoined> filteredList = new ArrayList<>();
                    for (ContainerJoined s : list) {
                        if (s.getNumero_de_identificacion().toLowerCase().contains(charString.toLowerCase())) {
                            filteredList.add(s);
                        }
                    }
                    listFiltered = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = listFiltered;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                listFiltered = (ArrayList<ContainerJoined>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }
}
