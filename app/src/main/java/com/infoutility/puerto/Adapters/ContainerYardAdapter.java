package com.infoutility.puerto.Adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;

import com.infoutility.puerto.Adapters.ViewHolders.ContainerYardViewHolder;
import com.infoutility.puerto.Interfaces.Response;
import com.infoutility.puerto.Keys.Key;
import com.infoutility.puerto.Models.ContainerYard;
import com.infoutility.puerto.R;
import com.infoutility.puerto.Utils.AlertUtil;
import com.infoutility.puerto.Utils.PreferencesManager;

import java.util.ArrayList;
import java.util.List;

public class ContainerYardAdapter extends RecyclerView.Adapter<ContainerYardViewHolder> implements Filterable {

    private Context mContext;
    private List<ContainerYard> list;
    private List<ContainerYard> listFiltered;
    private Response.OnResponse onResponse;
    private int yardAction = 0;
    private boolean isExport;


    public ContainerYardAdapter(Context mContext, List<ContainerYard> list, Response.OnResponse onResponse) {
        this.mContext = mContext;
        this.list = list;
        this.listFiltered = list;
        this.onResponse = onResponse;
        PreferencesManager.initializeInstance(mContext);
        yardAction = PreferencesManager.getInstance().getInt(Key.sharedPreferences.yardAction, 0);
        isExport = yardAction == Key.yardAction.EXPORT.id();
    }

    @NonNull
    @Override
    public ContainerYardViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.cardview_yard_container, parent, false);

        return new ContainerYardViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ContainerYardViewHolder containerYardViewHolder, int i) {
        final ContainerYard containerYard = listFiltered.get(i);

        containerYardViewHolder.tvPrefix.setText(mContext.getString(R.string.cvcie_title_prefix, containerYard.getPrefijo()));
        containerYardViewHolder.tvOrder.setText(mContext.getString(R.string.cvcie_title_order, containerYard.getOrden()));
        containerYardViewHolder.tvId.setText(mContext.getString(R.string.cvcie_title_id, containerYard.getNumero_de_identificacion()));
        containerYardViewHolder.tvCof.setText(mContext.getString(R.string.cvcie_title_cof, containerYard.getC_o_f()));
        containerYardViewHolder.tvMeasure.setText(mContext.getString(R.string.cvcie_title_measure, String.valueOf(containerYard.getMedida())));
        containerYardViewHolder.tvCondition.setText(mContext.getString(R.string.cvcie_title_condition, containerYard.getCondicion()));
        containerYardViewHolder.tvMachine.setText(mContext.getString(R.string.cvcie_title_machine, containerYard.getMaquina()));
        containerYardViewHolder.tvDatetime.setText(mContext.getString(R.string.cvcie_title_datetime, isExport ? containerYard.getFecha_hora_ubica() : containerYard.getFecha_grabacion()));


        containerYardViewHolder.btnImport.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onResponse.onResponse(Key.requestCode.CONTAINER_YARD_IMPORT, containerYard, null);
            }
        });


        containerYardViewHolder.btnExport.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertUtil.showAlertConfirmation(mContext.getString(R.string.all_text_warning),
                        mContext.getString(R.string.all_msg_container_export),
                        mContext,
                        mContext.getString(R.string.btn_text_export),
                        new AlertUtil.OnOkListener() {
                            @Override
                            public void onOk() {
                                onResponse.onResponse(Key.requestCode.CONTAINER_YARD_EXPORT, containerYard, null);
                            }
                        }, new AlertUtil.OnCancelListener() {
                            @Override
                            public void onCancel() {
                                //do nothing
                            }
                        }).show();
            }
        });


        containerYardViewHolder.btnRevertImport.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onResponse.onResponse(Key.requestCode.CONTAINER_YARD_REVERT_IMPORT, containerYard, null);
            }
        });


        containerYardViewHolder.btnRevertExport.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onResponse.onResponse(Key.requestCode.CONTAINER_YARD_REVERT_EXPORT, containerYard, null);
            }
        });

        containerYardViewHolder.tvMachine.setVisibility(isExport ? View.VISIBLE : View.GONE);
        containerYardViewHolder.btnImport.setVisibility(isExport ? View.GONE : View.VISIBLE);
        containerYardViewHolder.btnExport.setVisibility(isExport ? View.VISIBLE : View.GONE);
        containerYardViewHolder.btnRevertImport.setVisibility(isExport ? View.GONE : View.VISIBLE);
        containerYardViewHolder.btnRevertExport.setVisibility(isExport ? View.VISIBLE : View.GONE);


    }

    @Override
    public int getItemCount() {
        return listFiltered.size();
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    listFiltered = list;
                } else {
                    List<ContainerYard> filteredList = new ArrayList<>();
                    for (ContainerYard s : list) {
                        if (s.getNumero_de_identificacion()
                                .toLowerCase().contains(charString.toLowerCase())) {
                            filteredList.add(s);
                        }
                    }
                    listFiltered = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = listFiltered;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                listFiltered = (ArrayList<ContainerYard>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }
}
