package com.infoutility.puerto.Adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;

import com.infoutility.puerto.Adapters.ViewHolders.PinboxViewHolder;
import com.infoutility.puerto.Interfaces.Response;
import com.infoutility.puerto.Keys.Key;
import com.infoutility.puerto.Models.Pinbox;
import com.infoutility.puerto.R;
import com.infoutility.puerto.Utils.AlertUtil;

import java.util.ArrayList;
import java.util.List;

public class PinboxAdapter extends RecyclerView.Adapter<PinboxViewHolder> implements Filterable {

    private Context mContext;
    private List<Pinbox> list;
    private List<Pinbox> listFiltered;
    private Response.OnResponse onResponse;

    public PinboxAdapter(Context mContext, List<Pinbox> list, Response.OnResponse onResponse) {
        this.mContext = mContext;
        this.list = list;
        this.listFiltered = list;
        this.onResponse = onResponse;
    }

    @NonNull
    @Override
    public PinboxViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.cardview_pinbox, parent, false);
        return new PinboxViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull PinboxViewHolder pinboxViewHolder, int i) {
        final Pinbox pinbox = listFiltered.get(i);
        pinboxViewHolder.tvMove.setText(mContext.getString(R.string.cvpb_title_move, String.valueOf(pinbox.getId_movimiento())));//Cambiar a Descripcion Catalogo
        pinboxViewHolder.tvCorrelative.setText(mContext.getString(R.string.cvpb_title_correlative, pinbox.getCorrelativo()));
        pinboxViewHolder.tvMeasure.setText(mContext.getString(R.string.cvpb_title_measure, pinbox.getMedida()));
        pinboxViewHolder.tvCrane.setText(mContext.getString(R.string.cvpb_title_crane, pinbox.getGrua()));
        pinboxViewHolder.tvDate.setText(mContext.getString(R.string.cvpb_title_date, pinbox.getFecha_grabacion()));

        pinboxViewHolder.btnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertUtil.showAlertConfirmation(mContext.getString(R.string.all_text_warning),
                        mContext.getString(R.string.all_msg_delete, "el", "pinbox"),
                        mContext,
                        mContext.getString(R.string.all_text_delete),
                        new AlertUtil.OnOkListener() {
                            @Override
                            public void onOk() {
                                onResponse.onResponse(Key.requestCode.PINBOX_DELETE, pinbox, null);
                            }
                        }, new AlertUtil.OnCancelListener() {
                            @Override
                            public void onCancel() {
                                //do nothing
                            }
                        }).show();
            }
        });

    }

    @Override
    public int getItemCount() {
        return listFiltered.size();
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    listFiltered = list;
                } else {
                    List<Pinbox> filteredList = new ArrayList<>();
                    for (Pinbox s : list) {
                        if (s.getCorrelativo() == Integer.valueOf(charString)) {
                            filteredList.add(s);
                        }
                    }
                    listFiltered = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = listFiltered;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                listFiltered = (ArrayList<Pinbox>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }
}
