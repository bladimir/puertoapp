package com.infoutility.puerto.Adapters;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.view.View;
import android.widget.TextView;

import com.infoutility.puerto.Interfaces.Response;
import com.infoutility.puerto.Keys.Key;
import com.infoutility.puerto.R;

import java.util.ArrayList;

public class BlockListContainerYard extends RecyclerView.Adapter<BlockListContainerYard.ViewHolder> {

    private ArrayList<Bundle> listBlock;
    private Response.OnResponse onResponse;
    private int typeShow;

    public BlockListContainerYard(ArrayList<Bundle> listBlock, Response.OnResponse onResponse, int typeShow){
        this.listBlock = listBlock;
        this.onResponse = onResponse;
        this.typeShow = typeShow;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        CardView cardView = (CardView) LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.cardview_block_list, viewGroup, false);
        return new BlockListContainerYard.ViewHolder(cardView, onResponse, this.listBlock, this.typeShow);
    }


    @Override
    public void onBindViewHolder(final ViewHolder viewHolder, int i) {
        //CardView cardView = viewHolder.cardView;
        Bundle bundle = listBlock.get(i);
        Log.i("ContainerYardFragment", String.valueOf(i));
        Log.i("ContainerYardFragment", bundle.toString());
        if(this.typeShow == Key.cardType.typeBlock){
            //((TextView)cardView.findViewById(R.id.cbl_text_title)).setText("Bloque " + bundle.getInt("bloque"));
            viewHolder.txtTitle.setText("Bloque " + bundle.getInt("bloque"));
        }else if(this.typeShow == Key.cardType.typeLevel){
            //((TextView)cardView.findViewById(R.id.cbl_text_title)).setText("Nivel " + bundle.getInt("nivel"));
            viewHolder.txtTitle.setText("Nivel " + bundle.getInt("nivel"));
        }else if(this.typeShow == Key.cardType.typeContainer){
            String datos = "F: " + bundle.getInt("fila_o_carril") + "/M:" +  bundle.getInt("modulo");


            //((TextView)cardView.findViewById(R.id.cbl_text_title)).setText(datos);

            if(bundle.getString("muerto").equals("S")){
                viewHolder.txtTitle.setText("X");
                viewHolder.txtTitle.setTextColor(ContextCompat.getColor(viewHolder.cardView.getContext(), R.color.white_solid));
                viewHolder.cardView.setBackgroundColor(ContextCompat.getColor(viewHolder.cardView.getContext(), R.color.black_solid));
            }else {
                viewHolder.txtTitle.setText(datos);
                String lleno = bundle.getString("numero_de_identificacion", "");
                if(lleno.equals("")){
                    //((TextView)cardView.findViewById(R.id.cbl_text_title)).setText("Vacio");
                }else{
                    //((TextView)cardView.findViewById(R.id.cbl_text_title)).setText("lleno");
                    viewHolder.cardView.setBackgroundColor(ContextCompat.getColor(viewHolder.cardView.getContext(), R.color.colorized_forest_green));
                }
            }


            if(bundle.getInt("selected", 0) == 1){
                viewHolder.cardView.setBackgroundColor(ContextCompat.getColor(viewHolder.cardView.getContext(), R.color.colorPrimary));
                //((CardView)cardView.findViewById(R.id.cardBlocks)).setCardBackgroundColor(ContextCompat.getColor(cardView.getContext(), R.color.colorPrimary));
                //((TextView)cardView.findViewById(R.id.cbl_text_title)).setTextColor(ContextCompat.getColor(cardView.getContext(), R.color.white_solid));
                viewHolder.txtTitle.setTextColor(ContextCompat.getColor(viewHolder.cardView.getContext(), R.color.white_solid));

            }/*else{
                txtTitle.setTextColor(ContextCompat.getColor(cardView.getContext(), R.color.black_solid));
                ((TextView)cardView.findViewById(R.id.cbl_text_title)).setTextColor(ContextCompat.getColor(cardView.getContext(), R.color.black_solid));
            }*/


        }



    }

    @Override
    public int getItemCount() {
        return (listBlock!=null)? listBlock.size() : 0;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }


    protected static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        public CardView cardView;
        private Response.OnResponse onResponse;
        private ArrayList<Bundle> listStudent;
        private int typeShow;
        public TextView txtTitle;

        public ViewHolder(CardView v, Response.OnResponse onResponse, ArrayList<Bundle> listStudent, int typeShow){
            super(v);
            cardView = v;
            this.listStudent = listStudent;
            this.onResponse = onResponse;
            this.typeShow = typeShow;
            cardView.setOnClickListener(this);
            int kk = getAdapterPosition();
            txtTitle = cardView.findViewById(R.id.cbl_text_title);
        }


        @Override
        public void onClick(View view) {
            Log.i("ContainerYardFragment", getAdapterPosition()+"");

            this.onResponse.onResponse(Key.requestCode.RESPONSE_CARD, getAdapterPosition(), this.typeShow);

        }
    }
}
