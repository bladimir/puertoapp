package com.infoutility.puerto.Adapters.ViewHolders;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.infoutility.puerto.R;

public class CurrentShiftViewHolder extends RecyclerView.ViewHolder  {

    public Button btnEnter;
    public Button btnDelete;
    public Button btnEdit;
    public TextView tvStart;
    public TextView tvEnd;
    public TextView tvType;

    public CurrentShiftViewHolder(@NonNull View itemView) {
        super(itemView);
        btnEnter = itemView.findViewById(R.id.ccs_btn_enter);
        btnDelete = itemView.findViewById(R.id.ccs_btn_delete);
        btnEdit = itemView.findViewById(R.id.ccs_btn_edit);
        tvStart = itemView.findViewById(R.id.ccs_tv_start);
        tvEnd = itemView.findViewById(R.id.ccs_tv_end);
        tvType = itemView.findViewById(R.id.ccs_tv_type);
    }
}
