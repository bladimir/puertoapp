package com.infoutility.puerto.Adapters.ViewHolders;

import android.support.annotation.NonNull;
import android.support.design.card.MaterialCardView;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.infoutility.puerto.R;

public class ContainerShipViewHolder extends RecyclerView.ViewHolder {

    public Button btnDetail;
    public Button btnNext;
    public TextView tvContainerShip;
    public TextView tvDate;
    public TextView tvSituation;
    public TextView tvShipSystem;
    public MaterialCardView cvContainerShip;


    public ContainerShipViewHolder(@NonNull View itemView) {
        super(itemView);
        btnDetail = itemView.findViewById(R.id.ccs_btn_detail);
        btnNext = itemView.findViewById(R.id.ccs_btn_next);
        tvContainerShip = itemView.findViewById(R.id.ccs_tv_container_ship);
        tvDate = itemView.findViewById(R.id.ccs_tv_date);
        tvSituation = itemView.findViewById(R.id.ccs_tv_situation);
        tvShipSystem = itemView.findViewById(R.id.ccs_tv_ship_system);
        cvContainerShip = itemView.findViewById(R.id.ccs_cv_container_ship);
    }
}
