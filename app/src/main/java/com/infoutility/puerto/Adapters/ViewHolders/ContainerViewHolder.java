package com.infoutility.puerto.Adapters.ViewHolders;

import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.infoutility.puerto.R;

public class ContainerViewHolder extends RecyclerView.ViewHolder {

    public TextView tvOrder;
    public TextView tvPrefix;
    public TextView tvId;
    public TextView tvMeasure;
    public TextView tvType;
    public Button btnReserve;
    public Button btnInfo;
    public Button btnSelect;
    public Button btnConfirm;
    public CardView cvContainer;

    public ContainerViewHolder(@NonNull View itemView) {
        super(itemView);
        tvOrder = itemView.findViewById(R.id.cc_tv_order);
        tvPrefix = itemView.findViewById(R.id.cc_tv_prefix);
        tvId = itemView.findViewById(R.id.cc_tv_id);
        tvMeasure = itemView.findViewById(R.id.cc_tv_measure);
        tvType = itemView.findViewById(R.id.cc_tv_type);
        btnReserve = itemView.findViewById(R.id.cc_btn_reserve);
        btnInfo = itemView.findViewById(R.id.cc_btn_info);
        btnSelect = itemView.findViewById(R.id.cc_btn_select);
        btnConfirm = itemView.findViewById(R.id.cc_btn_confirm);
        cvContainer = itemView.findViewById(R.id.cc_cv_container);
    }
}
