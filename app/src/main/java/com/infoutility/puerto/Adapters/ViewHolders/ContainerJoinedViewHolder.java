package com.infoutility.puerto.Adapters.ViewHolders;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.infoutility.puerto.R;

public class ContainerJoinedViewHolder extends RecyclerView.ViewHolder {

    public TextView tvIdJoin;
    public TextView tvPrefix;
    public TextView tvId;
    public TextView tvTrip;
    public TextView tvMove;
    public Button btnUnjoin;
    public Button btnSelect;

    public ContainerJoinedViewHolder(@NonNull View itemView) {
        super(itemView);
        tvIdJoin = itemView.findViewById(R.id.ccj_tv_id_join);
        tvPrefix = itemView.findViewById(R.id.ccj_tv_prefix);
        tvId = itemView.findViewById(R.id.ccj_tv_id);
        tvTrip = itemView.findViewById(R.id.ccj_tv_trip);
        tvMove = itemView.findViewById(R.id.ccj_tv_move);
        btnUnjoin = itemView.findViewById(R.id.ccj_btn_unjoin);
        btnSelect = itemView.findViewById(R.id.ccj_btn_select);
    }
}
