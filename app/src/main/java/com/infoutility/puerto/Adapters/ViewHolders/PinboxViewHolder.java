package com.infoutility.puerto.Adapters.ViewHolders;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.infoutility.puerto.R;

public class PinboxViewHolder extends RecyclerView.ViewHolder {
    public TextView tvMove;
    public TextView tvCorrelative;
    public TextView tvMeasure;
    public TextView tvCrane;
    public TextView tvDate;
    public Button btnDelete;

    public PinboxViewHolder(@NonNull View itemView) {
        super(itemView);
        tvMove = itemView.findViewById(R.id.cvpb_tv_move);
        tvCorrelative = itemView.findViewById(R.id.cvpb_tv_correlative);
        tvMeasure = itemView.findViewById(R.id.cvpb_tv_measure);
        tvCrane = itemView.findViewById(R.id.cvpb_tv_crane);
        tvDate = itemView.findViewById(R.id.cvpb_tv_date);
        btnDelete = itemView.findViewById(R.id.cvpb_btn_delete);
    }
}
