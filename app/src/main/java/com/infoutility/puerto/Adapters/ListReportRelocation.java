package com.infoutility.puerto.Adapters;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.infoutility.puerto.Interfaces.Response;
import com.infoutility.puerto.Keys.Key;
import com.infoutility.puerto.R;
import com.infoutility.puerto.Utils.Tools;

import java.util.ArrayList;

public class ListReportRelocation extends RecyclerView.Adapter<ListReportRelocation.ViewHolder> {
    private ArrayList<Bundle> listReport;
    private Response.OnResponse onResponse;
    private int type;

    public ListReportRelocation(ArrayList<Bundle> listReport, Response.OnResponse onResponse, int type) {
        this.listReport = listReport;
        this.onResponse = onResponse;
        this.type = type;
    }

    @NonNull
    @Override
    public ListReportRelocation.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        CardView cardView = (CardView) LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.cardview_list_report_relocation, viewGroup, false);
        return new ListReportRelocation.ViewHolder(cardView, onResponse, this.listReport);
    }

    @Override
    public void onBindViewHolder(@NonNull ListReportRelocation.ViewHolder viewHolder, int i) {
        CardView cardView = viewHolder.cardView;
        Bundle bundle = listReport.get(i);
        if(this.type == 2){
            ((TextView)cardView.findViewById(R.id.clrr_txt_ship)).setText("Buque: " + bundle.getString("viaje", ""));
            String tempDate = bundle.getString("inicio", null);
            String dateInit = "----";
            if(tempDate !=null){
                dateInit = "Inicio: " + Tools.formatStrDate( tempDate, Key.datetimePattern.YYYYMMDD_HHMMSS_DASH_SEPARATOR);
            }
            ((TextView)cardView.findViewById(R.id.clrr_txt_init_date)).setText(dateInit);
            tempDate = bundle.getString("fin", null);
            if(tempDate != null){
                dateInit = "Fin: " +  Tools.formatStrDate( tempDate, Key.datetimePattern.YYYYMMDD_HHMMSS_DASH_SEPARATOR);
            }
            ((TextView)cardView.findViewById(R.id.clrr_txt_finish_date)).setText(dateInit);
            ((TextView)cardView.findViewById(R.id.clrr_txt_year)).setText("US Solicitante: " + bundle.getInt("us_solicitante", 0));
            ((TextView)cardView.findViewById(R.id.clrr_txt_number)).setText("Movimeintos: " + bundle.getInt("cantida_mov", 0));
        }else{
            ((TextView)cardView.findViewById(R.id.clrr_txt_ship)).setVisibility(View.GONE);
            ((TextView)cardView.findViewById(R.id.clrr_txt_ship)).setVisibility(View.GONE);
            ((TextView)cardView.findViewById(R.id.clrr_txt_ship)).setVisibility(View.GONE);
            ((TextView)cardView.findViewById(R.id.clrr_txt_init_date)).setVisibility(View.GONE);
            ((TextView)cardView.findViewById(R.id.clrr_txt_finish_date)).setVisibility(View.GONE);

            ((TextView)cardView.findViewById(R.id.clrr_txt_year)).setText("Año: " + bundle.getInt("ano", 0));
            ((TextView)cardView.findViewById(R.id.clrr_txt_number)).setText("Numero: " + bundle.getString("num_reporte", ""));
        }
        String date = "Fecha: " + Tools.formatStrDate( bundle.getString("fecha", ""), Key.datetimePattern.YYYYMMDD_HHMMSS_DASH_SEPARATOR);
        ((TextView)cardView.findViewById(R.id.clrr_txt_date)).setText(date);
        ((TextView)cardView.findViewById(R.id.clrr_txt_remark)).setText("Observaciones: " + bundle.getString("observaciones", ""));

    }

    @Override
    public int getItemCount() {
        return (listReport!=null)? listReport.size() : 0;
    }


    protected static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        private CardView cardView;
        private Response.OnResponse onResponse;
        private ArrayList<Bundle> listStudent;

        public ViewHolder(CardView v, Response.OnResponse onResponse, ArrayList<Bundle> listStudent){
            super(v);
            cardView = v;
            this.listStudent = listStudent;
            this.onResponse = onResponse;
            ((Button)cardView.findViewById(R.id.clrr_btn_select)).setOnClickListener(this);
        }


        @Override
        public void onClick(View view) {
            Log.i("ContainerYardFragment", getAdapterPosition()+"");
            this.onResponse.onResponse(Key.requestCode.RESPONSE_CARD, getAdapterPosition(), null);

        }
    }

}
