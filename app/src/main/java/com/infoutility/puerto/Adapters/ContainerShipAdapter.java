package com.infoutility.puerto.Adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;

import com.infoutility.puerto.Adapters.ViewHolders.ContainerShipViewHolder;
import com.infoutility.puerto.Interfaces.Response;
import com.infoutility.puerto.Keys.Key;
import com.infoutility.puerto.Models.ContainerShip;
import com.infoutility.puerto.R;

import java.util.ArrayList;
import java.util.List;

public class ContainerShipAdapter extends RecyclerView.Adapter<ContainerShipViewHolder> implements Filterable {

    private Context mContext;
    private List<ContainerShip> list;
    private List<ContainerShip> listFiltered;
    private Response.OnResponse onResponse;

    public ContainerShipAdapter(Context mContext, List<ContainerShip> list, Response.OnResponse onResponse) {
        this.mContext = mContext;
        this.list = list;
        this.listFiltered = list;
        this.onResponse = onResponse;
    }

    @NonNull
    @Override
    public ContainerShipViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.cardview_container_ship, parent, false);

        return new ContainerShipViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ContainerShipViewHolder containerShipViewHolder, int i) {
        final ContainerShip containerShip = listFiltered.get(i);
        containerShipViewHolder.tvContainerShip.setText(mContext.getResources().getString(R.string.ccs_title_container_ship, containerShip.getBarco()));
        containerShipViewHolder.tvDate.setText(mContext.getResources().getString(R.string.ccs_title_date, containerShip.getFecha_hora() == null ? "-" : "\n" + containerShip.getFecha_hora()));
        containerShipViewHolder.tvSituation.setText(mContext.getResources().getString(R.string.ccs_title_ship_situation, containerShip.getSituaciondescripcion()));
        containerShipViewHolder.tvShipSystem.setText(mContext.getResources().getString(R.string.ccs_title_trip, containerShip.getViaje_empornac()));

        if(containerShip.getSituaciondescripcion().equals(Key.containerSituation.ZARPADO.name()))
            containerShipViewHolder.cvContainerShip.setCardBackgroundColor(mContext.getColor(R.color.colorized_black_rose));
        else if(containerShip.getSituaciondescripcion().equals(Key.containerSituation.ATRAQUE.name()))
            containerShipViewHolder.cvContainerShip.setCardBackgroundColor(mContext.getColor(R.color.colorized_chilean_fire));
        else if(containerShip.getSituaciondescripcion().equals(Key.containerSituation.ANUNCIO.name()))
            containerShipViewHolder.cvContainerShip.setCardBackgroundColor(mContext.getColor(R.color.colorized_forest_green));
        else if(containerShip.getSituaciondescripcion().equals(Key.containerSituation.FONDEADO.name()))
            containerShipViewHolder.cvContainerShip.setCardBackgroundColor(mContext.getColor(R.color.colorized_deep_sea_green));
        else if(containerShip.getSituaciondescripcion().equals(Key.containerSituation.CONFIRMACION.name()))
            containerShipViewHolder.cvContainerShip.setCardBackgroundColor(mContext.getColor(R.color.colorized_lucky_point));

        containerShipViewHolder.btnDetail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onResponse.onResponse(Key.requestCode.CONTAINER_SHIP_DETAIL, containerShip, null);
            }
        });


        containerShipViewHolder.btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onResponse.onResponse(Key.requestCode.CONTAINER_SHIFT, containerShip, null);
            }
        });
    }

    @Override
    public int getItemCount() {
        return listFiltered.size();
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    listFiltered = list;
                } else {
                    List<ContainerShip> filteredList = new ArrayList<>();
                    for (ContainerShip s : list) {
                        if (s.getSituaciondescripcion().toLowerCase().contains(charString.toLowerCase())) {
                            filteredList.add(s);
                        }
                    }
                    listFiltered = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = listFiltered;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                listFiltered = (ArrayList<ContainerShip>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }
}
