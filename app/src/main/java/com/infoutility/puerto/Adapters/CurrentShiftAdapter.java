package com.infoutility.puerto.Adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;

import com.infoutility.puerto.Adapters.ViewHolders.CurrentShiftViewHolder;
import com.infoutility.puerto.Interfaces.Response;
import com.infoutility.puerto.Keys.Key;
import com.infoutility.puerto.Models.Shift;
import com.infoutility.puerto.R;
import com.infoutility.puerto.Utils.AlertUtil;

import java.util.ArrayList;
import java.util.List;

public class CurrentShiftAdapter extends RecyclerView.Adapter<CurrentShiftViewHolder> implements Filterable {

    private Context mContext;
    private List<Shift> list;
    private List<Shift> listFiltered;
    private Response.OnResponse onResponse;

    public CurrentShiftAdapter(Context mContext, List<Shift> list, Response.OnResponse onResponse) {
        this.mContext = mContext;
        this.list = list;
        this.listFiltered = list;
        this.onResponse = onResponse;
    }

    @NonNull
    @Override
    public CurrentShiftViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.cardview_current_shift, parent, false);

        return new CurrentShiftViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull CurrentShiftViewHolder currentShiftViewHolder, int i) {
        final Shift shift = listFiltered.get(i);


        currentShiftViewHolder.tvStart.setText(mContext.getResources().getString(R.string.ccsh_title_start, shift.getFecha_hora_inicio()));
        currentShiftViewHolder.tvEnd.setText(mContext.getResources().getString(R.string.ccsh_title_end, shift.getFecha_hora_final()));
        currentShiftViewHolder.tvType.setText(mContext
                .getResources().getString(R.string.ccsh_title_type,
                        shift.getId_tipo_turno().equals("1") ? "ORDINARIO"
                                : shift.getId_tipo_turno().equals("2") ? "EXTRA"
                                : shift.getId_tipo_turno().equals("3") ?"DOBLE ORDINARIO"
                                : "-"));//Pasar a Catalogo

        currentShiftViewHolder.btnEnter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onResponse.onResponse(Key.requestCode.CONTAINER_OPERATIONS, shift, null);
            }
        });

        currentShiftViewHolder.btnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onResponse.onResponse(Key.requestCode.SHIFT_DELETE, shift, null);
            }
        });

        currentShiftViewHolder.btnEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onResponse.onResponse(Key.requestCode.SHIFT_EDIT, shift, null);
//                AlertUtil.showAlertConfirmation(mContext.getString(R.string.all_text_warning), "El turno seleccionado se actualizara con la informacion ingresada, desea actualizar?", mContext, "Actualizar", new AlertUtil.OnOkListener() {
//                    @Override
//                    public void onOk() {
//                        onResponse.onResponse(Key.requestCode.SHIFT_EDIT, shift, null);
//                    }
//                }, new AlertUtil.OnCancelListener() {
//                    @Override
//                    public void onCancel() {
//
//                    }
//                }).setCancelable(false)
//                        .show();
            }
        });
    }

    @Override
    public int getItemCount() {
        return listFiltered.size();
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    listFiltered = list;
                } else {
                    List<Shift> filteredList = new ArrayList<>();
                    for (Shift s : list) {
                        if (s.getId_cuadrilla().toLowerCase().contains(charString.toLowerCase())) {
                            filteredList.add(s);
                        }
                    }
                    listFiltered = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = listFiltered;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                listFiltered = (ArrayList<Shift>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }
}
