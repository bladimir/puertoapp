package com.infoutility.puerto.Adapters;

public class TextAdapter {
    private String id;
    private String label;
    private String data;

    public TextAdapter(String id, String label) {
        this.id = id;
        this.label = label;
    }

    public TextAdapter(String id, String label, String data) {
        this.id = id;
        this.label = label;
        this.data = data;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }
}
