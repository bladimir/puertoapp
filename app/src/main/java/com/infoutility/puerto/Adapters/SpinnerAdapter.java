package com.infoutility.puerto.Adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.gson.Gson;
import com.infoutility.puerto.R;
import com.infoutility.puerto.Utils.Tools;

import java.util.List;

public class SpinnerAdapter extends ArrayAdapter<TextAdapter> {

    private Context context;
    private List<TextAdapter> list;
    private static Gson gson = new Gson();

    private SpinnerAdapter(@NonNull Context context, int resource, @NonNull List<TextAdapter> objects) {
        super(context, resource, objects);
        this.context = context;
        this.list = objects;
    }

    public static <T> SpinnerAdapter createArrayAdapter(Context context, String strJson, Class<T> tClass) {
        return new SpinnerAdapter(context, R.layout.support_simple_spinner_dropdown_item, Tools.fromJson(strJson, tClass));
    }

    public static <T> SpinnerAdapter createArrayAdapter(Context context, List<TextAdapter> lst) {
        return new SpinnerAdapter(context, R.layout.support_simple_spinner_dropdown_item, lst);
    }


    public static void selectSpinnerItemByValue(Spinner spinner, String value){
        SpinnerAdapter spinnerAdapter = (SpinnerAdapter) spinner.getAdapter();
        for (int position = 0; position < spinnerAdapter.getCount(); position++){
            if (spinnerAdapter.getItem(position).getLabel().equals(value)){
                spinner.setSelection(position);
                return;
            }

        }
    }

    public static void selectSpinnerItemById(Spinner spinner, String id){
        SpinnerAdapter spinnerAdapter = (SpinnerAdapter) spinner.getAdapter();
        for (int position = 0; position < spinnerAdapter.getCount(); position++){
            TextAdapter ta =spinnerAdapter.getItem(position);
            if (ta != null && ta.getId().equals(id)){
                spinner.setSelection(position);
                return;
            }

        }
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Nullable
    @Override
    public TextAdapter getItem(int position) {
        return list.get(position);
    }


    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        TextView label = (TextView) super.getView(position, convertView, parent);
        label.setText(list.get(position).getLabel());
        return label;
    }

    @Override
    public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        TextView label = (TextView) super.getView(position, convertView, parent);
        label.setText(list.get(position).getLabel());
        return label;
    }
}
