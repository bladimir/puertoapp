package com.infoutility.puerto.Adapters.ViewHolders;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.infoutility.puerto.R;

public class ContainerYardViewHolder extends RecyclerView.ViewHolder {

    public TextView tvPrefix;
    public TextView tvOrder;
    public TextView tvId;
    public TextView tvCof;
    public TextView tvMeasure;
    public TextView tvCondition;
    public TextView tvMachine;
    public TextView tvDatetime;
    public Button btnImport;
    public Button btnExport;
    public Button btnRevertImport;
    public Button btnRevertExport;

    public ContainerYardViewHolder(@NonNull View itemView) {
        super(itemView);
        tvPrefix = itemView.findViewById(R.id.cvcie_tv_prefix);
        tvOrder = itemView.findViewById(R.id.cvcie_tv_order);
        tvId = itemView.findViewById(R.id.cvcie_tv_id);
        tvCof = itemView.findViewById(R.id.cvcie_tv_cof);
        tvMeasure = itemView.findViewById(R.id.cvcie_tv_measure);
        tvCondition = itemView.findViewById(R.id.cvcie_tv_condition);
        tvMachine = itemView.findViewById(R.id.cvcie_tv_machine);
        tvDatetime = itemView.findViewById(R.id.cvcie_tv_datetime);
        btnImport = itemView.findViewById(R.id.cvcie_btn_import);
        btnExport = itemView.findViewById(R.id.cvcie_btn_export);
        btnRevertImport = itemView.findViewById(R.id.cvcie_btn_revert_import);
        btnRevertExport = itemView.findViewById(R.id.cvcie_btn_revert_export);
    }
}
