package com.infoutility.puerto.Adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;

import com.google.common.base.Predicate;
import com.google.common.collect.Iterables;
import com.infoutility.puerto.Adapters.ViewHolders.ContainerViewHolder;
import com.infoutility.puerto.Events.Events;
import com.infoutility.puerto.Interfaces.Response;
import com.infoutility.puerto.Keys.Key;
import com.infoutility.puerto.Models.Container;
import com.infoutility.puerto.Models.ContainerJoined;
import com.infoutility.puerto.R;
import com.infoutility.puerto.Events.GlobalBus;
import com.infoutility.puerto.Utils.AlertUtil;

import org.checkerframework.checker.nullness.compatqual.NullableDecl;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.List;

public class ContainerAdapter extends RecyclerView.Adapter<ContainerViewHolder> implements Filterable {

    private Context mContext;
    private List<Container> list;
    private List<Container> listFiltered;
    private Response.OnResponse onResponse;
    private boolean isJoinStarted;
    private ContainerViewHolder containerViewHolder;
    private final SparseBooleanArray selectedJoined =new SparseBooleanArray();
    private List<ContainerJoined> listJoined;

    public ContainerAdapter(Context mContext, List<Container> list, Response.OnResponse onResponse, List<ContainerJoined> listJoined) {
        this.mContext = mContext;
        this.list = list;
        this.listFiltered = list;
        this.onResponse = onResponse;
        this.listJoined = listJoined;
    }

    @NonNull
    @Override
    public ContainerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.cardview_container, parent, false);

        return new ContainerViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final ContainerViewHolder containerViewHolder, int i) {
        final Container container = listFiltered.get(i);
        final int position = i;
        this.containerViewHolder = containerViewHolder;
        containerViewHolder.tvOrder.setText(mContext.getResources().getString(R.string.cc_title_order, container.getOrden()));
        containerViewHolder.tvPrefix.setText(mContext.getResources().getString(R.string.cc_title_prefix, container.getPrefijo()));
        containerViewHolder.tvId.setText(mContext.getResources().getString(R.string.cc_title_id, container.getNumero_de_identificacion()));
        containerViewHolder.tvMeasure.setText(mContext.getResources().getString(R.string.cc_title_measure, String.valueOf(container.getMedida())));
        containerViewHolder.tvType.setText(mContext.getResources()
                .getString(R.string.cc_title_type,
                container.getTipo_de_movimiento() == 1 ? "Descarga"
                        : container.getTipo_de_movimiento() == 2 ? "Carga"
                        : "-"));//Pasar a Catalogo

        boolean existJoined = Iterables.any(listJoined, new Predicate<ContainerJoined>() {
            @Override
            public boolean apply(@NullableDecl ContainerJoined input) {
                if (input != null)
                    return input.getNumero_de_identificacion().equals(container.getNumero_de_identificacion());
                return false;
            }
        });

        if (container.getConfirmado() != null){
            containerViewHolder.btnSelect.setVisibility(View.GONE);
            containerViewHolder.btnReserve.setVisibility(View.GONE);
            if (container.getConfirmado().equals("C") || container.getConfirmado().equals("SI")){
                containerViewHolder.btnConfirm.setVisibility(View.GONE);
                containerViewHolder.cvContainer.setCardBackgroundColor(mContext.getColor(R.color.card_background));
            } else {
                containerViewHolder.btnConfirm.setVisibility(View.VISIBLE);
                containerViewHolder.cvContainer.setCardBackgroundColor(mContext.getColor(R.color.card_background_selected));
            }
        } else {
            containerViewHolder.btnConfirm.setVisibility(View.VISIBLE);
            containerViewHolder.btnReserve.setVisibility(View.VISIBLE);
            containerViewHolder.btnSelect.setVisibility(View.VISIBLE);
            containerViewHolder.cvContainer.setCardBackgroundColor(mContext.getColor(R.color.card_background));
        }

        if (isJoinStarted){
//            containerViewHolder.btnSelect.setVisibility(View.VISIBLE);
            if (existJoined){
                containerViewHolder.btnSelect.setVisibility(View.GONE);
            } else {
                containerViewHolder.btnSelect.setVisibility(View.VISIBLE);
            }

            if (selectedJoined.get(position)){
                containerViewHolder.cvContainer.setCardBackgroundColor(mContext.getColor(R.color.carmine));
            } else {
                containerViewHolder.cvContainer.setCardBackgroundColor(mContext.getColor(R.color.card_background));
            }
        } else {
            selectedJoined.clear();
            containerViewHolder.btnSelect.setVisibility(View.GONE);
            containerViewHolder.cvContainer.setCardBackgroundColor(mContext.getColor(R.color.card_background));
        }


        containerViewHolder.btnReserve.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                containerViewHolder.cvContainer.setCardBackgroundColor(mContext.getColor(R.color.card_background_selected));
//                containerViewHolder.btnReserve.setText(mContext.getString(R.string.btn_text_confirm));
//                Collections.swap(listFiltered, position, 0);
//                notifyItemMoved(position, 0);
                onResponse.onResponse(Key.requestCode.CONTAINER_RESERVED, container, null);
            }
        });

        containerViewHolder.btnConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onResponse.onResponse(Key.requestCode.CONTAINER_CONFIRMED, container, null);
//                AlertUtil.showAlertConfirmation(mContext.getString(R.string.all_text_warning),
//                        mContext.getString(R.string.all_msg_container_confirm),
//                        mContext,
//                        mContext.getString(R.string.all_text_confirm),
//                        new AlertUtil.OnOkListener() {
//                            @Override
//                            public void onOk() {
//                                onResponse.onResponse(Key.requestCode.CONTAINER_CONFIRMED, container, null);
//                            }
//                        }, new AlertUtil.OnCancelListener() {
//                            @Override
//                            public void onCancel() {
//                                //do nothing
//                            }
//                        }).show();
            }
        });

        containerViewHolder.btnSelect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectedJoined.put(position, true);
                onResponse.onResponse(Key.requestCode.CONTAINER_JOIN, container, null);
                notifyDataSetChanged();
            }
        });

        containerViewHolder.btnInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onResponse.onResponse(Key.requestCode.CONTAINER_DETAIL, container, null);
            }
        });
    }

    @Override
    public int getItemCount() {
        return listFiltered.size();
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    listFiltered = list;
                } else {
                    List<Container> filteredList = new ArrayList<>();
                    for (Container c : list) {
                        if (c.getNumero_de_identificacion().toLowerCase().contains(charString.toLowerCase())) {
                            filteredList.add(c);
                        }
                    }
                    listFiltered = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = listFiltered;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                listFiltered = (ArrayList<Container>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }

    public boolean isJoinStarted() {
        return isJoinStarted;
    }

    public void setJoinStarted(boolean joinStarted) {
        isJoinStarted = joinStarted;
    }

    @Subscribe
    public void onMessageEvent(Events.FragmentAdapterMessage event) {
//        if (event.isNotifyChange()){
//            this.containerViewHolder.cvContainer.setCardBackgroundColor(mContext.getColor(R.color.carmine));
//            this.containerViewHolder.btnSelect.setVisibility(View.GONE);
//        }

    }

    @Override
    public void onAttachedToRecyclerView(@NonNull RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
        GlobalBus.getBus().register(this);
    }

    @Override
    public void onDetachedFromRecyclerView(@NonNull RecyclerView recyclerView) {
        super.onDetachedFromRecyclerView(recyclerView);
        GlobalBus.getBus().unregister(this);
    }
}
