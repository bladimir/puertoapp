package com.infoutility.puerto.Adapters.ViewHolders;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.infoutility.puerto.R;

public class DelayViewHolder extends RecyclerView.ViewHolder {
    public TextView tvDelay;
    public TextView tvDateTimeStart;
    public TextView tvDateTimeEnd;
    public TextView tvMove;
    public TextView tvLoad;
    public Button btnDelete;
    
    public DelayViewHolder(@NonNull View itemView) {
        super(itemView);
        tvDelay = itemView.findViewById(R.id.cvd_tv_delay);
        tvDateTimeStart = itemView.findViewById(R.id.cvd_tv_datetime_start);
        tvDateTimeEnd = itemView.findViewById(R.id.cvd_tv_datetime_end);
        tvMove = itemView.findViewById(R.id.cvd_tv_move);
        tvLoad = itemView.findViewById(R.id.cvd_tv_load);
        btnDelete = itemView.findViewById(R.id.cvd_btn_delete);
    }
}
