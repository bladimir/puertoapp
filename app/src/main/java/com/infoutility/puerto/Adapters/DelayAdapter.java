package com.infoutility.puerto.Adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;

import com.google.common.base.Predicate;
import com.google.common.collect.Iterables;
import com.infoutility.puerto.Adapters.ViewHolders.DelayViewHolder;
import com.infoutility.puerto.Interfaces.Response;
import com.infoutility.puerto.Keys.Key;
import com.infoutility.puerto.Models.Delay;
import com.infoutility.puerto.Models.DelayType;
import com.infoutility.puerto.R;
import com.infoutility.puerto.Utils.AlertUtil;
import com.infoutility.puerto.Utils.PreferencesManager;
import com.infoutility.puerto.Utils.Tools;

import org.checkerframework.checker.nullness.compatqual.NullableDecl;

import java.util.ArrayList;
import java.util.List;

public class DelayAdapter extends RecyclerView.Adapter<DelayViewHolder> implements Filterable {
    private Context mContext;
    private List<Delay> list;
    private List<DelayType> listType = new ArrayList<>();
    private List<Delay> listFiltered;
    private Response.OnResponse onResponse;

    public DelayAdapter(Context mContext, List<Delay> list, Response.OnResponse onResponse) {
        this.mContext = mContext;
        this.list = list;
        this.listFiltered = list;
        this.onResponse = onResponse;
        PreferencesManager.initializeInstance(mContext);
    }

    public void loadCatalog(){
        String lst = PreferencesManager.getInstance().getString(Key.catalog.lstDelayType, null);
        if (lst != null){
            listType = Tools.getListFromJson(lst, DelayType.class);
        }

    }


    @NonNull
    @Override
    public DelayViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.cardview_delay, parent, false);
        return new DelayViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull DelayViewHolder delayViewHolder, int i) {
        final Delay delay = listFiltered.get(i);
        String strDelay = "-";
        if (!listType.isEmpty()){
            DelayType dt = Iterables.find(listType, new Predicate<DelayType>() {
                @Override
                public boolean apply(@NullableDecl DelayType input) {
                    if (input != null)
                        return input.getDemora() == delay.getDemora();
                    return false;
                }
            });

            if (dt != null)
                strDelay = dt.getNombre_demora();
        }


        delayViewHolder.tvDelay.setText(mContext.getString(R.string.cvd_title_delay, strDelay));
        delayViewHolder.tvDateTimeStart.setText(mContext.getString(R.string.cvd_title_datetime_start, delay.getInicio()));
        delayViewHolder.tvDateTimeEnd.setText(mContext.getString(R.string.cvd_title_datetime_end, delay.getFin()));
        delayViewHolder.tvMove.setText(mContext.getString(R.string.cvd_title_move, delay.getTipo_movimiento().equals("2") ? "Carga" : "Descarga"));
        delayViewHolder.tvLoad.setText(mContext.getString(R.string.cvd_title_load, delay.getTipo_carga().equals("C") ? "CONTENEDOR" : delay.getTipo_carga().equals("M") ? "MERCADERIA" : "-"));

        delayViewHolder.btnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertUtil.showAlertConfirmation(mContext.getString(R.string.all_text_warning),
                        mContext.getString(R.string.all_msg_delete, "la", "demora"),
                        mContext,
                        mContext.getString(R.string.all_text_delete),
                        new AlertUtil.OnOkListener() {
                            @Override
                            public void onOk() {
                                onResponse.onResponse(Key.requestCode.DELAY_DELETE, delay, null);
                            }
                        }, new AlertUtil.OnCancelListener() {
                            @Override
                            public void onCancel() {
                                //do nothing
                            }
                        }).show();
            }
        });

    }

    @Override
    public int getItemCount() {
        return listFiltered.size();
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    listFiltered = list;
                } else {
                    List<Delay> filteredList = new ArrayList<>();
                    for (Delay s : list) {
                        if (s.getDemora() == Integer.valueOf(charString)) {
                            filteredList.add(s);
                        }
                    }
                    listFiltered = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = listFiltered;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                listFiltered = (ArrayList<Delay>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }
}
