package com.infoutility.puerto.Keys;

import java.util.concurrent.TimeUnit;

public interface Key {

    interface api {
        String baseUrl = "http://200.6.239.37:7001/Empornac/webapi/";
        String keyBaseUrl = "PuertoAppPlay19";
//        String baseUrl = "http://40.69.148.29:8080/Empornac/webapi/";
        String shiftType = "Tturno";
        String containerType = "Clase_Contenedor";
        String pinboxType = "Tpinbox";
        String moveType = "Tipo_Movimiento";
        String delayTypesDetail = "TipoDemoraDetalle";
        String sequence = "Secuencias";
        String activities = "Actividades";
        String containerCondition = "Condicion_Contenedor";
        String crane = "gruas";
        String handheld = "Handheld";
        String containerMeasures = "Medida_Contenedor";
        String workMode = "modalidadLaboral";
        String loadType = "TipoCarga";
        String delay = "Demora";
        String trip = "viaje";
        String login = "login";
        String shift = "turnoxcheque";
        String container = "contenedor";
        String containerJoined = "atados";
        String pinbox = "pinbox";
        String delayType = "TDemora";
        String containerYard = "contenedorpatio";
        String machine = "Maquina";
        String yard = "patio";
        String exportImport = "export_import";
        String listBlock = "Listado_bloque";
        String listLevelForBlock = "Listado_Niveles";
        String listContainerForLevel = "Listado_Modulo";
        String listReportRelocation = "ReporteReubicacion";
        String listReportRelocationInt = "ReporteMovimientoInterno";
        String listRelocationContainer = "detallereubicacion";
        String InfoRelocation = "detallereubicacion";
        String reception = "Recepcion";
        String dispatch = "Despacho";
        String meterUser = "Buscar_usuario";
        String meter = "Lector";
        String consumptionHistory = "Historial_Consumo";
        String rate = "Tarifageneric";
        String listTypeRelocation = "tiporeubicacion";
        String infoRequest = "recepcion_refrigerado";
        String infoRequestExist = "recepcion_refrigerado_contenedor";
        String requestColed = "recepcion_refrigerado_contenedor";
        String infoRequestDispatch = "despacho_refrigerado";
        String requestColedDispatch = "despacho_refrigerado";
        String listSearchPowerMeter = "Buscar_usuario";
        String revertImport = "RevertirImportacion";
        String yardMap = "MapaPatio";
        String revertExport = "RevertirExportacion";
        String containerPending = "contenedor_pendiente";
        String usersOffline = "BuscarUsuarioSabana";
        String containerConfirmed = "ListadoConfirmados";


        //Parameters
        // - container
        String paramTrip = "viaje";
        String paramWorkMode = "tipo";
        String paramPrefix = "prefijo";
        String paramHandheldId = "id";
        String paramRecorder = "grabador";
        String paramShift = "id_turno";
        String paramMoveType = "tipo_movimiento";
        String paramMoveTypeAlt = "tipo_Movimiento";
        String paramType = "tipo";
        String paramConfirmed = "confirmado";
        String paramIdentification = "identificacion";
        String paramYarn = "patio";
        String paramBlock = "bloque";
        String paramLevel = "nivel";
        String paramOfice = "oficial";
        String paramLast = "ultimas";
        String paramNumReport = "NUM_REPORTE";
        String year = "ANIO";
        String paramId = "id";
        String paramAuthReception = "AUTORIZACION_RECEPCION";
        String paramAuthDispatch = "AUTORIZACION_DESPACHO";
        String paramMeter = "Contador";
        String paramUserCode = "CodigoUsuario";
        String paramYear = "anio";
        String paramRequestUpp = "RECEPCION";
        String paramPrefijo = "PREFIJO";
        String paramNumberIDentify = "NUMERO_DE_IDENTIFICACION";
        String paramToken = "FICHA";
        String paramUserPending = "USUARIO_GRABADOR";
        String paramQuantity = "cantidad";

    }


    interface httpStatusCode {
        //        Informational
        interface Continue {
            int code = 100;
            String message = "The initial part of a request has been received and has not yet been rejected by the server.";
        }


        //        Success
        interface success {
            int code = 200;
            String message = "The request has succeeded.";
        }

        //        Redirection
        interface multipleChoice {
            int code = 300;
            String message = "The request has more than one possible response.";
        }

        //        Cliente Error
        interface badRequest {
            int code = 400;
            String message = "The server could not understand the request due to invalid syntax.";
        }

        interface unauthorized {
            int code = 401;
            String message = "The request has not been applied because it lacks valid authentication credentials for the target resource.";
        }

        interface forbidden {
            int code = 403;
            String message = "The server understood the request but refuses to authorize it.";
        }

        interface notFound {
            int code = 404;
            String message = "The origin server did not find a current representation for the target resource or is not willing to disclose that one exists.";
        }

        //        Server Error
        interface internalServerError {
            int code = 500;
            String message = "The server encountered an unexpected condition that prevented it from fulfilling the request.";
        }

        //Others
        interface noInternet {
            int code = -1000;
            String message = "No internet connection found.";
        }

        interface unknown {
            int code = -1001;
            String message = "An unknown error occurred.";
        }
    }

    interface statusCode {
        //        Informational
        interface Continue {
            int code = 100;
            String message = "The initial part of a request has been received and has not yet been rejected by the server.";
        }


        //        Success
        interface success {
            int code = 200;
            String message = "The request has succeeded.";
        }

        //        Redirection
        interface multipleChoice {
            int code = 300;
            String message = "The request has more than one possible response.";
        }

        //        Cliente Error
        interface badRequest {
            int code = 400;
            String message = "El servidor no pudo entender la solicitud debido a una sintaxis no válida.";
        }

        interface unauthorized {
            int code = 401;
            String message = "La solicitud requiere de credenciales de autenticación válidas para el recurso.";
        }

        interface forbidden {
            int code = 403;
            String message = "El servidor entendió la solicitud pero se niega a autorizarla.";
        }

        interface notFound {
            int code = 404;
            String message = "Recurso no encontrado.";
        }

        //        Server Error
        interface internalServerError {
            int code = 500;
            String message = "El servidor encontró un error interno o una configuración incorrecta y no pudo completar su solicitud.";
        }

        //Others
        interface noInternet {
            int code = -1000;
            String message = "No se pudo establecer conexión con el servidor.";
        }

        interface unknown {
            int code = -1001;
            String message = "Un error desconocido ocurrió.";
        }
    }

    interface requestCode {
        // Navigation
        int CONTAINER_SHIP_DETAIL = 2000;
        int CONTAINER_OPERATIONS = 2001;
        int DIALOG_DATETIME = 2002;
        int CONTAINER_SHIFT = 2003;
        int SHIFT_DELETE = 2004;
        int CONTAINER_JOIN = 2005;
        int CONTAINER_SHIP_DETAIL_CLOSED = 2006;
        int CONTAINER_JOIN_NOTIFY_START = 2007;
        int CONTAINER_JOIN_NOTIFY_END = 2008;
        int CONTAINER_RESERVED = 2009;
        int CONTAINER_CONFIRMED = 2010;
        int CONTAINER_DETAIL = 2011;
        int PINBOX_DELETE = 2012;
        int DELAY_DELETE = 2013;
        int CONTAINER_YARD_IMPORT = 2014;
        int CONTAINER_YARD_EXPORT = 2015;
        int CONTAINER_YARD_IMPORT_CLOSED = 2015;
        int CONTAINER_JOIN_UNJOINED = 2016;
        int CONTAINER_JOIN_SELECTION = 2017;
        int CONTAINER_YARD_REVERT_IMPORT = 2018;
        int CONTAINER_YARD_REVERT_EXPORT = 2019;
        int SHIFT_EDIT = 2020;

        int RESPONSE_CARD = 2501;
        int RESPONSE_DIALOG = 2502;
        int RESPONSE_REALM = 2503;


        // API
        int GET_SHIFT_TYPE = 3000;
        int GET_CONTAINER_TYPE = 3001;
        int GET_PINBOX_TYPE = 3002;
        int GET_MOVE_TYPE = 3003;
        int GET_DELAY_TYPE_DETAIL = 3004;
        int GET_SEQUENCE = 3005;
        int GET_ACTIVITIES = 3006;
        int GET_CONTAINER_CONDITION = 3007;
        int GET_CRANE = 3008;
        int GET_HANDHELD = 3009;
        int GET_CONTAINER_MEASURES = 3010;
        int GET_WORK_MODE = 3011;
        int GET_LOAD_TYPE = 3012;
        int GET_DELAY = 3013;
        int GET_TRIP = 3014;
        int GET_LOGIN = 3015;
        int GET_SHIFT = 3016;
        int GET_CONTAINER = 3017;
        int GET_CONTAINER_JOINED = 3018;
        int POST_SHIFT = 3019;
        int DELETE_SHIFT = 3020;
        int POST_CONTAINER_JOINED = 3021;
        int DELETE_CONTAINER_JOINED = 3022;
        int PUT_CONTAINER_RESERVED = 3023;
        int POST_CONTAINER = 3024;
        int PUT_CONTAINER_CONFIRMED = 3025;
        int GET_PINBOX= 3026;
        int POST_PINBOX= 3027;
        int PUT_PINBOX= 3028;
        int DELETE_PINBOX= 3029;
        int GET_DELAY_TYPE = 3030;
        int POST_DELAY = 3031;
        int DELETE_DELAY = 3032;
        int GET_CONTAINER_YARD = 3033;
        int GET_MACHINE = 3034;
        int GET_YARD = 3035;
        int POST_CONTAINER_YARD = 3036;
        int PUT_CONTAINER_YARD = 3037;


        int GET_LIST_BLOCK = 3501;
        int GET_LIST_LEVELS = 3502;
        int GET_LIST_MODULES = 3503;
        int GET_LIST_REPORT_RELOCATION = 3504;
        int POST_REPORT_RELOCATION = 3505;
        int GET_INFO_RELOCATION = 3506;
        int RESPONSE_SET_POSITION = 3507;
        int GET_TYPE_RELOCATION = 3508;
        int GET_INFO_REQUEST = 3509;
        int GET_INFO_REQUEST_EXIST = 3510;
        int GET_REQUEST_COLED = 3511;
        int GET_LIST_DISPATCH = 3512;
        int GET_LIST_TEMPS = 3513;
        int GET_LIST_POWER_METER_SEARCH = 3514;
        int GET_LIST_HISTORY_ELECTRONIC = 3515;


        int GET_CONTAINER_JOINED_BY_ID = 3038;
        int GET_RECEPTION = 3039;
        int POST_RECEPTION = 3040;
        int GET_DISPATCH = 3041;
        int POST_DISPATCH = 3042;
        int GET_METER_USER = 3043;
        int GET_METER = 3044;
        int GET_METER_HISTORY_CONSUMPTION = 3045;
        int POST_METER_HISTORY_CONSUMPTION = 3046;
        int GET_METER_RATE = 3047;

        int POST_CONTAINER_YARD_REVERT_IMPORT = 3048;
        int POST_CONTAINER_YARD_REVERT_EXPORT = 3049;

        int PUT_DISPATCH = 3050;
        int PUT_RECEPTION = 3051;
        int POST_YARD_MAP = 3052;


        int GET_CONTAINER_PENDING = 3053;
        int POST_CONTAINER_PENDING = 3054;
        int DELETE_CONTAINER_PENDING = 3055;

        int GET_CONTAINER_CONFIRMED = 3056;

        //Generals
        int PERMISSIONS = 4000;
    }


    interface jsonResponse {
        String data = "datos";
    }

    interface cardType {
        int typeBlock = 1;
        int typeLevel = 2;
        int typeContainer = 3;
    }

    interface extra {
        String SELECTED_MODULE = "SELECTED_MODULE";
        String CONTAINER_SHIP_DETAIL = "CONTAINER_SHIP_DETAIL";
        String CONTAINER_SHIP_NAVIGATE = "CONTAINER_SHIP_NAVIGATE";
        String WORK_SHIFT_NAVIGATE = "WORK_SHIFT_NAVIGATE";
        String CONTAINER_DETAIL = "CONTAINER_DETAIL";
        String CONTAINER_CONFIRM = "CONTAINER_CONFIRM";
        String CONTAINER_IMPORT = "CONTAINER_IMPORT";
        String METER_RECEIPT = "METER_RECEIPT";
        String YARD_CONTAINER_REVERT_EXPORT = "YARD_CONTAINER_REVERT_EXPORT";
        String YARD_CONTAINER_REVERT_DISPATCH = "YARD_CONTAINER_REVERT_DISPATCH";
    }

    interface bundle{
        String bundleArray = "array";
    }


    interface datetimePattern {
        String DDMMYY_SLASH_SEPARATOR = "dd/MM/yy";                     /*16/10/19*/
        String DDMMYY_SLASH_MONTH_NAME = "dd MMM yyyy";                 /*16 Oct 2019*/
        String YYYYMMDD_DASH_SEPARATOR = "yyyy-MM-dd";                  /*2019-10-16*/
        String DDMMYY_HHMM_AM_PM_DASH_SEPARATOR = "dd-MM-yyyy h:mm a";  /*16-10-2019 1:11 AM*/
        String DDMMYYYY_HHMMSS_DASH_SEPARATOR = "dd-MM-yyyy HH:mm:ss";  /*16-10-2019 01:11:28*/
        String DDMMYYYY_HHMMSS_SLASH_SEPARATOR = "dd/MM/yy HH:mm:ss";   /*16/10/2019 01:11:28*/
        String YYYYMMDD_HHMMSS_DASH_SEPARATOR = "yyyy-MM-dd HH:mm:ss";  /*2019-10-16 01:11:28*/
    }
    
    enum modules {
        SELECT(-1,"SELECCIONE"),
        CONTAINER_LOAD(0,"CARGA Y DESCARGA DE CONTENEDORES"),
        CONTAINER_YARD(1,"MANEJO DE PATIO DE CONTENEDORES"),
        ELECTRIC_POWER_CONNECTION(2,"CONEXIÓN Y DESCONEXIÓN DE ELÉCTRICOS"),
        ELECTRIC_POWER_METER(3,"LECTURA DE CONTADORES");

        private int id;
        private String description;

        modules(int id, String description) {
            this.id = id;
            this.description = description;
        }

        public int id() {
            return id;
        }

        public String description() {
            return description;
        }

        @Override
        public String toString() {
            return description;
        }
    }

    enum httpStatusCodes {
        CONTINUE(100, "The initial part of a request has been received and has not yet been rejected by the server."),
        SUCCESS(200, "The request has succeeded."),
        MULTIPLE_CHOICE(300, "The request has more than one possible response."),
        BAD_REQUEST(400, "The server could not understand the request due to invalid syntax."),
        UNAUTHORIZED(401, "The request has not been applied because it lacks valid authentication credentials for the target resource."),
        FORBIDDEN(403, "The server understood the request but refuses to authorize it."),
        NOT_FOUND(404, "The origin server did not find a current representation for the target resource or is not willing to disclose that one exists."),
        INTERNAL_SERVER_ERROR(500, "The server encountered an unexpected condition that prevented it from fulfilling the request."),
        NO_INTERNET(-1000, "No internet connection found."),
        UNKNOWN(-1001, "An unknown error occurred.");

        private int code;
        private String message;

        httpStatusCodes(int code, String message) {
            this.code = code;
            this.message = message;
        }

        public int code() {
            return code;
        }

        public String message() {
            return message;
        }

        @Override
        public String toString() {
            return message;
        }
    }

    interface sharedPreferences {
        String name = "PREFERENCES_MANAGER";

        String recorder = "recorder";
        String workMode = "workMode"; //Carga 2  y Descarga 1
        String trip = "trip";
        String currentShift = "currentShift";
        String isLogged = "isLogged";
        String previousSelectedModelue = "previousSelectedModelue";
        String imei = "imei";
        String handheld = "handheld";
        String userName = "userName";
        String userRole = "userRole";
        String confirmedContainer ="confirmedContainer";
        String yardAction ="yardAction";
        String reportRelocation ="reportRelocation";
        String numReport = "num_reporte";
        String yard ="yard";
        String meter = "meter";
        String tripName = "tripName";
        String ipAddress = "ipAddress";
    }


    enum workMode {
        LOAD(2,"CARGA"),
        UNLOAD(1,"DESCARGA");

        private int id;
        private String description;

        workMode(int id, String description) {
            this.id = id;
            this.description = description;
        }

        public int id() {
            return id;
        }

        public String description() {
            return description;
        }

        @Override
        public String toString() {
            return description;
        }
    }


    enum confirmedContainer {
        CONFIRMED(1,"CONFIRMADO"),
        NO_CONFIRMED(0,"NO_CONFIRMADO");

        private int id;
        private String description;

        confirmedContainer(int id, String description) {
            this.id = id;
            this.description = description;
        }

        public int id() {
            return id;
        }

        public String description() {
            return description;
        }

        @Override
        public String toString() {
            return description;
        }
    }

    enum yardAction {
        IMPORT(1,"IMPORT"),
        EXPORT(2,"EXPORT");

        private int id;
        private String description;

        yardAction(int id, String description) {
            this.id = id;
            this.description = description;
        }

        public int id() {
            return id;
        }

        public String description() {
            return description;
        }

        @Override
        public String toString() {
            return description;
        }
    }

    enum containerTemperatureCondition {
        REEFER(null,"REEFER"),
        SECO("S","SECO");

        private String id;
        private String description;

        containerTemperatureCondition(String id, String description) {
            this.id = id;
            this.description = description;
        }

        public String id() {
            return id;
        }

        public String description() {
            return description;
        }

        @Override
        public String toString() {
            return description;
        }
    }


    enum containerSituation {
        ALL(0,"TODOS"),
        ZARPADO(1,"ZARPADO"),
        ATRAQUE(2, "ATRAQUE"),
        ANUNCIO(3, "ANUNCIO"),
        FONDEADO(4, "FONDEADO"),
        CONFIRMACION(5, "CONFIRMACION");

        private int id;
        private String description;

        containerSituation(int id, String description) {
            this.id = id;
            this.description = description;
        }

        public int id() {
            return id;
        }

        public String description() {
            return description;
        }

        @Override
        public String toString() {
            return description;
        }
    }


    interface catalog {
        String lstDelayType = "lstDelayType";
    }

    enum containerOperationFilter {
        ALL(0,"TODOS"),
        ORDER(1,"ORDEN"),
        MISSING(2, "FALTANTES"),
        JOINED(3, "ATADOS");

        private int id;
        private String description;

        containerOperationFilter(int id, String description) {
            this.id = id;
            this.description = description;
        }

        public int id() {
            return id;
        }

        public String description() {
            return description;
        }

        @Override
        public String toString() {
            return description;
        }
    }

    interface timer {
        long delay = 1000 * 60 * 16;
    }
}
