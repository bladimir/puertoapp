package com.infoutility.puerto.DialogFragments;

import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;

import com.google.common.collect.Lists;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.infoutility.puerto.Adapters.SpinnerAdapter;
import com.infoutility.puerto.Adapters.TextAdapter;
import com.infoutility.puerto.Api.ResourceHandler;
import com.infoutility.puerto.Interfaces.Response;
import com.infoutility.puerto.Keys.Key;
import com.infoutility.puerto.Models.ContainerCondition;
import com.infoutility.puerto.Models.ContainerMeasure;
import com.infoutility.puerto.Models.ContainerType;
import com.infoutility.puerto.Models.Crane;
import com.infoutility.puerto.Models.MoveType;
import com.infoutility.puerto.Models.Request.Container;
import com.infoutility.puerto.Models.Response.BaseResponse;
import com.infoutility.puerto.R;
import com.infoutility.puerto.Utils.AlertUtil;
import com.infoutility.puerto.Utils.PreferencesManager;
import com.infoutility.puerto.Utils.Tools;

import org.parceler.Parcels;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class AddContainerDialogFragment extends DialogFragment implements View.OnClickListener, Response.OnResponse {

    public final static String TAG = AddContainerDialogFragment.class.getSimpleName();


//    View
    private Button btnAdd;
    private Button btnCancel;
    private Button btnConfirm;
    private Spinner spCondition;
    private Spinner spMove;
    private Spinner spCrane;
    private Spinner spCof;
    private Spinner spMeasure;
    private Spinner spReefer;
    private TextInputEditText tietOrder;
    private TextInputEditText tietPrefix;
    private TextInputEditText tietNumber;
    private TextInputEditText tietWeight;
    private TextInputEditText tietDescription;

    private static Response.OnResponse onResponse;

    private long trip = 0;
    private int recorder = 0;
    private int workMode = 0;
    private long currentShift = 0;
    private String handheld = null;

    private com.infoutility.puerto.Models.Container confirmedContainer;

    private boolean isConfirmation;

    public static AddContainerDialogFragment newInstance(Response.OnResponse response){
        onResponse = response;
        return new AddContainerDialogFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null)
            confirmedContainer = Parcels.unwrap(getArguments().getParcelable(Key.extra.CONTAINER_CONFIRM));
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = requireActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.dialog_fragment_add_container, null);

        isConfirmation = (confirmedContainer != null);
        PreferencesManager.initializeInstance(getContext());

        trip = PreferencesManager.getInstance().getLong(Key.sharedPreferences.trip, 0L);
        recorder = PreferencesManager.getInstance().getInt(Key.sharedPreferences.recorder, 0);
        workMode = PreferencesManager.getInstance().getInt(Key.sharedPreferences.workMode, 0);
        currentShift = PreferencesManager.getInstance().getLong(Key.sharedPreferences.currentShift, 0L);
        handheld = PreferencesManager.getInstance().getString(Key.sharedPreferences.handheld, null);

        ResourceHandler.listContainerConditions(this, Key.requestCode.GET_CONTAINER_CONDITION);
        ResourceHandler.listMovementTypes(this, Key.requestCode.GET_MOVE_TYPE);
        ResourceHandler.listCranes(this, Key.requestCode.GET_CRANE);
        ResourceHandler.listContainerTypes(this, Key.requestCode.GET_CONTAINER_TYPE);
        ResourceHandler.listContainerMeasures(this, Key.requestCode.GET_CONTAINER_MEASURES);

        btnAdd = view.findViewById(R.id.dfac_btn_add);
        btnCancel = view.findViewById(R.id.dfac_btn_cancel);
        btnConfirm = view.findViewById(R.id.dfac_btn_confirm);
        spCondition = view.findViewById(R.id.dfac_sp_condition);
        spMove = view.findViewById(R.id.dfac_sp_move);
        spCrane = view.findViewById(R.id.dfac_sp_crane);
        spCof = view.findViewById(R.id.dfac_sp_cof);
        spReefer = view.findViewById(R.id.dfac_sp_reefer);
        spMeasure = view.findViewById(R.id.dfac_sp_measure);
        tietOrder = view.findViewById(R.id.dfac_tiet_order);
        tietPrefix = view.findViewById(R.id.dfac_tiet_prefix);
        tietNumber = view.findViewById(R.id.dfac_tiet_number);
        tietWeight = view.findViewById(R.id.dfac_tiet_weight);
        tietDescription = view.findViewById(R.id.dfac_tiet_description);


        if (isConfirmation)
            loadConfirmedContainer();

        btnCancel.setOnClickListener(this);
        btnAdd.setOnClickListener(this);
        btnConfirm.setOnClickListener(this);

        spReefer.setAdapter(new ArrayAdapter<>(getActivity(), R.layout.support_simple_spinner_dropdown_item, Key.containerTemperatureCondition.values()));

        builder.setView(view)
                .setTitle(isConfirmation ? R.string.dfac_title_confirm_container : R.string.dfac_title_add_container);

        return builder.create();
    }


    private void loadConfirmedContainer(){
        //Render
        btnAdd.setVisibility(View.GONE);
        btnConfirm.setVisibility(View.VISIBLE);
        //ReadOnly
        tietOrder.setEnabled(false);
        tietPrefix.setEnabled(false);
        tietNumber.setEnabled(false);

        tietOrder.setText(String.valueOf(confirmedContainer.getOrden()));
        tietPrefix.setText(String.valueOf(confirmedContainer.getPrefijo()));
        tietNumber.setText(String.valueOf(confirmedContainer.getNumero_de_identificacion()));
        tietWeight.setText(String.valueOf(confirmedContainer.getPeso_manifestado()));



    }


    private String validate(){
        if (handheld == null)
            return "Handheld no valido,";
        if (currentShift == 0)
            return "No existe turno.";
        if (recorder == 0)
            return "No existe recorder al cual asginar turno.";
        if (workMode == 0)
            return "Debe seleccionar una opcion de trabajo";
        if (trip == 0)
            return "No existe viaje.";
        if (tietOrder.getText() == null)
            return "Debe Ingresar orden";
        if (tietPrefix.getText() == null)
            return "Debe Ingresar prefijo";
        if (tietNumber.getText() == null)
            return "Debe Ingresar numero";
        if (tietWeight.getText() == null)
            return "Debe Ingresar peso";
        if (tietDescription.getText() == null)
            return "Debe Ingresar descripcion";
        return null;
    }

    private void addContainer(){
        String validationMessage = validate();
        if (validationMessage == null){
            Container containerRequest = new Container();

            containerRequest.setVIAJE_NO(String.valueOf(trip));
//            containerRequest.setTIPO_DE_MOVIMIENTO(Integer.valueOf(((TextAdapter)spMove.getSelectedItem()).getId()));
            containerRequest.setTIPO_DE_MOVIMIENTO(workMode);
            containerRequest.setORDEN(Integer.valueOf(tietOrder.getText().toString()));
            containerRequest.setPREFIJO(tietPrefix.getText().toString());
            containerRequest.setNUMERO_DE_IDENTIFICACION(tietNumber.getText().toString());
            containerRequest.setCONDICION(((TextAdapter)spCondition.getSelectedItem()).getId());
            containerRequest.setPESO_MANIFESTADO(Double.valueOf(tietWeight.getText().toString()));
            containerRequest.setPESO_BASCULA(0);//??
            containerRequest.setMAQUINA(0);//??
            containerRequest.setMEDIDA(Integer.valueOf(((TextAdapter)spMeasure.getSelectedItem()).getLabel()));
            containerRequest.setC_O_F(((TextAdapter)spCof.getSelectedItem()).getId());
            containerRequest.setTIPO_CONTENEDOR("C");
            containerRequest.setTARA(0);//??
            containerRequest.setCONFIRMADO("R");//??
            containerRequest.setSEQ_REG_CONTE(0);//??
            containerRequest.setGRABADOR(recorder);
            containerRequest.setFECHA_GRABACION(Tools.formatDate(new Date(), Key.datetimePattern.YYYYMMDD_HHMMSS_DASH_SEPARATOR));
            containerRequest.setHANDHELD_ID(handheld);
            containerRequest.setID_TURNO(currentShift);
            containerRequest.setVIAJE_TURNO(String.valueOf(trip));
            containerRequest.setOBSERVACION_CHEQUE(tietDescription.getText().toString());
//            containerRequest.setREFER_SECO_OPERANDO(((Key.containerTemperatureCondition)spReefer.getSelectedItem()).id());//?
            containerRequest.setREFER_SECO_OPERANDO("S");
//            containerRequest.setID_MOVIMIENTO(workMode);
            containerRequest.setID_MOVIMIENTO(Integer.valueOf(((TextAdapter)spMove.getSelectedItem()).getId()));
            containerRequest.setOPERADOR(0);//??
            String s = new Gson().toJson(containerRequest);
            ResourceHandler.postContainer(this, Key.requestCode.POST_CONTAINER, containerRequest);
        } else {
            AlertUtil.showAlertOk(getString(R.string.all_text_warning), validationMessage, getContext(), new AlertUtil.OnOkListener() {
                @Override
                public void onOk() {
                    // do nothing
                }
            }).show();
        }
    }


    private void doConfirmed(){
        com.infoutility.puerto.Models.Request.Container ContainerConfirmed = new com.infoutility.puerto.Models.Request.Container();

        ContainerConfirmed.setVIAJE_NO(String.valueOf(confirmedContainer.getViaje_no()));
//        ContainerConfirmed.setTIPO_DE_MOVIMIENTO(Integer.valueOf(((TextAdapter)spMove.getSelectedItem()).getId()));
        ContainerConfirmed.setTIPO_DE_MOVIMIENTO(workMode);
        ContainerConfirmed.setORDEN(confirmedContainer.getOrden());
        ContainerConfirmed.setPREFIJO(confirmedContainer.getPrefijo());
        ContainerConfirmed.setNUMERO_DE_IDENTIFICACION(confirmedContainer.getNumero_de_identificacion());
        ContainerConfirmed.setCONDICION(((TextAdapter)spCondition.getSelectedItem()).getId());
        ContainerConfirmed.setPESO_MANIFESTADO(Double.valueOf(tietWeight.getText().toString()));
        ContainerConfirmed.setPESO_BASCULA(0);//??
        ContainerConfirmed.setMAQUINA(0);//??
        ContainerConfirmed.setMEDIDA(Integer.valueOf(((TextAdapter)spMeasure.getSelectedItem()).getLabel()));
        ContainerConfirmed.setC_O_F(((TextAdapter)spCof.getSelectedItem()).getId());
        ContainerConfirmed.setTIPO_CONTENEDOR("C");
        ContainerConfirmed.setTARA(0);//??
        ContainerConfirmed.setCONFIRMADO("C");
        ContainerConfirmed.setSEQ_REG_CONTE(0);//??
        ContainerConfirmed.setGRABADOR(recorder);
        ContainerConfirmed.setFECHA_GRABACION(Tools.formatDate(new Date(), Key.datetimePattern.YYYYMMDD_HHMMSS_DASH_SEPARATOR));
        ContainerConfirmed.setHANDHELD_ID(handheld);
        ContainerConfirmed.setID_TURNO(currentShift);
        ContainerConfirmed.setVIAJE_TURNO(String.valueOf(trip));
        ContainerConfirmed.setOBSERVACION_CHEQUE(tietDescription.getText().toString());
//        ContainerConfirmed.setID_MOVIMIENTO(workMode);
        ContainerConfirmed.setID_MOVIMIENTO(Integer.valueOf(((TextAdapter)spMove.getSelectedItem()).getId()));
//        ContainerConfirmed.setREFER_SECO_OPERANDO(((Key.containerTemperatureCondition)spReefer.getSelectedItem()).id());
        ContainerConfirmed.setREFER_SECO_OPERANDO("");
        ContainerConfirmed.setOPERADOR(0);//??
        ContainerConfirmed.setGRUA(((TextAdapter)spCrane.getSelectedItem()).getId());

        ResourceHandler.putContainer(this, Key.requestCode.PUT_CONTAINER_CONFIRMED, ContainerConfirmed);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.dfac_btn_add:
                addContainer();
                break;
            case R.id.dfac_btn_confirm:
                doConfirmed();
                break;
            case R.id.dfac_btn_cancel:
                this.dismiss();
                break;
        }

    }

    @Override
    public void onResponse(int requestCode, Object o, Object r) {

        switch (requestCode){
            case Key.requestCode.GET_CONTAINER_CONDITION:
                if (o instanceof JsonObject){
                    JsonObject response = (JsonObject) o;
                    SpinnerAdapter adapter = SpinnerAdapter.createArrayAdapter(getActivity(), response.getAsJsonArray(Key.jsonResponse.data).toString(), ContainerCondition.class);
                    spCondition.setAdapter(adapter);

                    if (isConfirmation)
                        SpinnerAdapter.selectSpinnerItemByValue(spCondition, confirmedContainer.getCondicion());
                }
                break;
            case Key.requestCode.GET_MOVE_TYPE:
                if (o instanceof JsonObject){
//                    JsonObject response = (JsonObject) o;
//                    SpinnerAdapter adapter = SpinnerAdapter.createArrayAdapter(getActivity(), response.getAsJsonArray(Key.jsonResponse.data).toString(), MoveType.class);
//                    spMove.setAdapter(adapter);

                    JsonObject response = (JsonObject) o;
                    List<MoveType> lstMove = Tools.getListFromJson(response.getAsJsonArray(Key.jsonResponse.data).toString(), MoveType.class);
                    List<TextAdapter> lstMoveFiltered = new ArrayList<>();
                    for (MoveType mt : lstMove) {
//                        if (mt.getTipo_movimiento().equals(String.valueOf(workMode)) && !mt.getId_movimiento().equals(String.valueOf(workMode)))
//                            lstMoveFiltered.add(new TextAdapter(mt.getId_movimiento(), mt.getDescripcion()));
                        if (mt.getTipo_movimiento().equals(String.valueOf(workMode)))
                            lstMoveFiltered.add(new TextAdapter(mt.getId_movimiento(), mt.getDescripcion()));
                    }
                    SpinnerAdapter adapter = SpinnerAdapter.createArrayAdapter(getActivity(), lstMoveFiltered);
                    spMove.setAdapter(adapter);

                    if (isConfirmation)
                        SpinnerAdapter.selectSpinnerItemById(spMove, String.valueOf(confirmedContainer.getTipo_de_movimiento()));
                }
                break;
            case Key.requestCode.GET_CRANE:
                if (o instanceof JsonObject){
                    JsonObject response = (JsonObject) o;
                    SpinnerAdapter adapter = SpinnerAdapter.createArrayAdapter(getActivity(), response.getAsJsonArray(Key.jsonResponse.data).toString(), Crane.class);
                    spCrane.setAdapter(adapter);
                }
                break;
            case Key.requestCode.GET_CONTAINER_TYPE:
                if (o instanceof JsonObject){
                    JsonObject response = (JsonObject) o;
                    SpinnerAdapter adapter = SpinnerAdapter.createArrayAdapter(getActivity(), response.getAsJsonArray(Key.jsonResponse.data).toString(), ContainerType.class);
                    spCof.setAdapter(adapter);
                    if (isConfirmation)
                        SpinnerAdapter.selectSpinnerItemById(spCof, String.valueOf(confirmedContainer.getC_o_f()));
                }
                break;
            case Key.requestCode.GET_CONTAINER_MEASURES:
                if (o instanceof JsonObject){
                    JsonObject response = (JsonObject) o;
                    SpinnerAdapter adapter = SpinnerAdapter.createArrayAdapter(getActivity(), response.getAsJsonArray(Key.jsonResponse.data).toString(), ContainerMeasure.class);
                    spMeasure.setAdapter(adapter);

                    if (isConfirmation)
                        SpinnerAdapter.selectSpinnerItemByValue(spMeasure, String.valueOf(confirmedContainer.getMedida()));
                }
                break;
            case Key.requestCode.POST_CONTAINER:
            case Key.requestCode.PUT_CONTAINER_CONFIRMED:
                if (o instanceof JsonObject){
                    JsonObject jObject = (JsonObject) o;
                    BaseResponse response = new Gson().fromJson(jObject.toString(), BaseResponse.class);

                    if (response.isResultado()){
                        onResponse.onResponse(Key.requestCode.CONTAINER_SHIP_DETAIL_CLOSED, isConfirmation, null);
                        dismiss();
//                        AlertUtil.showAlertOk(getString(R.string.all_text_success), response.getMessage(), getContext(), new AlertUtil.OnOkListener() {
//                            @Override
//                            public void onOk() {
//                                onResponse.onResponse(Key.requestCode.CONTAINER_SHIP_DETAIL_CLOSED, isConfirmation, null);
//                                dismiss();
//                            }
//                        }).setCancelable(false)
//                                .show();
                    } else {
                        AlertUtil.showAlertOk(getString(R.string.all_text_error), response.getMessage(), getContext(), new AlertUtil.OnOkListener() {
                            @Override
                            public void onOk() {
                                // do nothing
                            }
                        }).show();
                    }
                }
                break;
        }
    }
}
