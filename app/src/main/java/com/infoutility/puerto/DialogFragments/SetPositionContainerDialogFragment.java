package com.infoutility.puerto.DialogFragments;


import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.common.collect.ImmutableMap;
import com.google.gson.JsonObject;
import com.infoutility.puerto.Adapters.SpinnerAdapter;
import com.infoutility.puerto.Adapters.TextAdapter;
import com.infoutility.puerto.Api.ResourceHandler;
import com.infoutility.puerto.Interfaces.Response;
import com.infoutility.puerto.Keys.Key;
import com.infoutility.puerto.Models.Request.RelocationContainer;
import com.infoutility.puerto.Models.TypeRelocation;
import com.infoutility.puerto.Models.Yard;
import com.infoutility.puerto.R;
import com.infoutility.puerto.Utils.AlertUtil;
import com.infoutility.puerto.Utils.JsonBundle;

import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

/**
 * A simple {@link Fragment} subclass.
 */
public class SetPositionContainerDialogFragment extends DialogFragment implements View.OnClickListener, Response.OnResponse {

    public static final String TAG = SetPositionContainerDialogFragment.class.getSimpleName();
    private static Response.OnResponse onResponse;
    public static int TYPE_SIMPLE = 1;
    public static int TYPE_COMPLETE = 2;
    public static int typeSelected = TYPE_SIMPLE;

    private TextView tvMedida;
    private TextView tvCOF;
    private TextView tvCondicion;
    private TextView tvPatio;
    private TextView tvFila;
    private TextView tvModulo;
    private TextView tvNivel;
    //private Spinner spTypeRelocation;
    private ArrayList<Bundle> listMov;
    private int orden = 1;

    private static Bundle selected;
    private static Bundle selectedA;
    private static String numReportA;

    public static SetPositionContainerDialogFragment newInstance(Response.OnResponse response, int type, Bundle selectedB, Bundle selectedAB, String numReport){
        onResponse = response;
        typeSelected = type;
        selected = selectedB;
        selectedA = selectedAB;
        numReportA = numReport;
        return new SetPositionContainerDialogFragment();
    }

    public SetPositionContainerDialogFragment() {
        // Required empty public constructor
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = requireActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.dialog_fragment_set_position_container, null);

        builder.setView(view)
                .setTitle(R.string.dfcd_title_ReportRelocationIng);
        initView(view);
        return builder.create();
    }


    public void initView(View view){
        ((Button)view.findViewById(R.id.dfspc_btn_save)).setOnClickListener(this);
        ((Button)view.findViewById(R.id.dfspc_btn_cancel)).setOnClickListener(this);
        this.tvMedida = view.findViewById(R.id.dfspc_tiet_medida);
        this.tvCOF = view.findViewById(R.id.dfspc_tiet_cof);
        this.tvCondicion = view.findViewById(R.id.dfspc_tiet_condicion);
        this.tvPatio = view.findViewById(R.id.dfspc_tiet_patio);
        this.tvFila = view.findViewById(R.id.dfspc_tiet_fila);
        this.tvModulo = view.findViewById(R.id.dfspc_tiet_modulo);
        this.tvNivel = view.findViewById(R.id.dfspc_tiet_nivel);
        //this.spTypeRelocation = view.findViewById(R.id.dfspc_sp_type);

        if(selectedA != null && typeSelected == TYPE_SIMPLE){
            this.tvPatio.setText(selectedA.getString("patio", ""));
            this.tvPatio.setEnabled(false);
            this.tvFila.setText( String.valueOf(selectedA.getInt("fila_o_carril", 0)) );
            this.tvFila.setEnabled(false);
            this.tvModulo.setText(String.valueOf(selectedA.getInt("modulo", 0)));
            this.tvModulo.setEnabled(false);
            this.tvNivel.setText(String.valueOf(selectedA.getInt("nivel", 0)));
            this.tvNivel.setEnabled(false);
        }
        //getType();


    }

    private void getType(){
        ResourceHandler.getTypeRelocatio(this, Key.requestCode.GET_TYPE_RELOCATION);
    }

    private void getInfoDetalle(){
        SimpleDateFormat dateYear = new SimpleDateFormat("yyyy", Locale.getDefault());
        Date date = new Date();
        String year = dateYear.format(date);
        ResourceHandler.infoRelocation(this, Key.requestCode.GET_INFO_RELOCATION,
                ImmutableMap.of(Key.api.paramNumReport, numReportA, Key.api.year, year ));
    }

    private void saveData(){
        try{
            int medida = Integer.valueOf(tvMedida.getText().toString());
            String COF = tvCOF.getText().toString();
            String condicion = tvCondicion.getText().toString();
            String patioa = tvPatio.getText().toString();
            int filaa = Integer.valueOf(tvFila.getText().toString());
            int moduloa = Integer.valueOf(tvModulo.getText().toString());
            int nivela = Integer.valueOf(tvNivel.getText().toString());
            String numeroIden = selected.getString("numero_de_identificacion", "");
            String prefijo = selected.getString("prefijo", "");
            String patio = selected.getString("patio", "");
            int fila = selected.getInt("fila_o_carril", 0);
            int modulo = selected.getInt("modulo", 0);
            int nivel = selected.getInt("nivel", 0);
            //String type = ((TextAdapter)spTypeRelocation.getSelectedItem()).getId();


            RelocationContainer relocationContainer = new RelocationContainer(medida, COF, numeroIden, prefijo, patioa, filaa, moduloa,
                    nivela, condicion, patio, fila, modulo, nivel, this.orden, numReportA);
            ResourceHandler.postCreateRelocationContainer(this, Key.requestCode.POST_REPORT_RELOCATION, relocationContainer);
        }catch (Exception e){
            AlertUtil.showAlertOk(getString(R.string.all_text_warning), "La informacion ingresada no es valida, por favor revisar", getContext(), new AlertUtil.OnOkListener() {
                @Override
                public void onOk() {
//                                idJoined = Integer.valueOf(response.getMessage());
                }
            }).show();
        }





    }


    /*
    * RelocationContainer(int medida, String c_o_f, String numero_de_identificacion, String prefijo, String patio_a, int fila_a,
    * int modulo_a, int nivel_a, String condicion, String patio, int fila, int modulo, int nivel, int orden, String num_reporte)
    *
    * */

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.dfspc_btn_save:
                getInfoDetalle();
                break;
            case R.id.dfspc_btn_cancel:
                this.dismiss();
                break;
        }
    }

    @Override
    public void onResponse(int requestCode, Object o, Object r) {
        if(requestCode == Key.requestCode.GET_INFO_RELOCATION){
            Log.i(TAG, "TODO");
            Log.i(TAG, o.toString());

            try{

                JSONObject jsonObject = new JSONObject(o.toString());
                Bundle bundle = JsonBundle.createBundle(jsonObject);
                if(bundle != null){
                    this.listMov = bundle.getParcelableArrayList(Key.jsonResponse.data);
                    if(this.listMov.size() > 1){
                        Bundle bundle1 = this.listMov.get(0);
                        orden = bundle1.getInt("orden", 0) + 1;
                    }else{
                        orden = 0;
                    }
                }
                saveData();

            }catch (Exception e){
                Log.i(TAG, e.toString());
            }

        }else if(requestCode == Key.requestCode.GET_TYPE_RELOCATION){
            /*try{
                if (o instanceof JsonObject) {
                    JsonObject response = (JsonObject) o;
                    SpinnerAdapter adapter = SpinnerAdapter.createArrayAdapter(getActivity(), response.getAsJsonArray(Key.jsonResponse.data).toString(), TypeRelocation.class);
                    spTypeRelocation.setAdapter(adapter);
                }
            }catch (Exception e){

            }*/

        } else if(requestCode == Key.requestCode.POST_REPORT_RELOCATION){
            try {
                Log.i(TAG, "TODO");
                Log.i(TAG, o.toString());
                JSONObject jsonObject = new JSONObject(o.toString());
                Bundle bundle = JsonBundle.createBundle(jsonObject);
                if(bundle != null){
                    boolean result = bundle.getBoolean("Resultado", false);
                    if(result){

                        AlertUtil.showAlertOk(getString(R.string.all_text_success), "Contenedor movido con exito", getContext(), new AlertUtil.OnOkListener() {
                            @Override
                            public void onOk() {
//                                idJoined = Integer.valueOf(response.getMessage());
                            }
                        }).show();
                        this.dismiss();
                        onResponse.onResponse(Key.requestCode.RESPONSE_SET_POSITION, true, null);
                    }else{

                        String msg = bundle.getString("Message", "");
                        AlertUtil.showAlertOk(getString(R.string.all_text_warning), msg, getContext(), new AlertUtil.OnOkListener() {
                            @Override
                            public void onOk() {
//                                idJoined = Integer.valueOf(response.getMessage());
                            }
                        }).show();
                    }
                }else{
                    AlertUtil.showAlertOk(getString(R.string.all_text_warning), "Error en la conexion por favor intentar mas tarde", getContext(), new AlertUtil.OnOkListener() {
                        @Override
                        public void onOk() {
//                                idJoined = Integer.valueOf(response.getMessage());
                        }
                    }).show();
                }
            }catch (Exception e){
                AlertUtil.showAlertOk(getString(R.string.all_text_warning), "Error en la conexion por favor intentar mas tarde", getContext(), new AlertUtil.OnOkListener() {
                    @Override
                    public void onOk() {
//                                idJoined = Integer.valueOf(response.getMessage());
                    }
                }).show();
            }
        }
    }
}
