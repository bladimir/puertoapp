package com.infoutility.puerto.DialogFragments;


import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.google.gson.Gson;
import com.infoutility.puerto.Api.ResourceHandler;
import com.infoutility.puerto.Interfaces.Response;
import com.infoutility.puerto.Keys.Key;
import com.infoutility.puerto.Models.Request.ColedContainer;
import com.infoutility.puerto.R;
import com.infoutility.puerto.Utils.JsonBundle;
import com.infoutility.puerto.Utils.PreferencesManager;

import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * A simple {@link Fragment} subclass.
 */
public class ListRequestDialogFragment extends DialogFragment implements View.OnClickListener, Response.OnResponse  {

    public static final String TAG = ListRequestDialogFragment.class.getSimpleName();
    private static Response.OnResponse onResponse;
    private static Bundle bundleInfo;
    private TextInputEditText tietRemark;
    private String oficial;
    private String hanheld;

    public static ListRequestDialogFragment newInstance(Response.OnResponse response, Bundle bundle){
        onResponse = response;
        bundleInfo = bundle;
        return new ListRequestDialogFragment();
    }


    public ListRequestDialogFragment() {
        // Required empty public constructor
    }


    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = requireActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.dialog_fragment_list_request, null);

        builder.setView(view)
                .setTitle(R.string.dfcd_title_receipt_frecer);
        initView(view);
        return builder.create();
    }

    private void initView(View view){
        PreferencesManager.initializeInstance(getActivity());
        oficial = PreferencesManager.getInstance().getInt(Key.sharedPreferences.recorder, 0) + "";
        hanheld = PreferencesManager.getInstance().getString(Key.sharedPreferences.handheld, "");
        ((Button)view.findViewById(R.id.dflr_btn_desc)).setOnClickListener(this);
        ((Button)view.findViewById(R.id.dflr_btn_conec)).setOnClickListener(this);
        tietRemark = view.findViewById(R.id.dflr_tiet_remark);
    }

    private void saveType(int type){
        String recepcion = bundleInfo.getString("recepcion", "");
        String viaje = bundleInfo.getString("viaje_sistema", "");
        String dateGeneral = getDate();
        String fechaDes = "";
        String fechaGrab = dateGeneral;
        String remark = tietRemark.getText().toString();
        String typeConexion = "";
        ColedContainer coledContainer = null;
        if(type == 1){
            fechaDes = dateGeneral;
            typeConexion = "D";
            coledContainer = new ColedContainer(recepcion, viaje, fechaDes, oficial, fechaGrab,
                    hanheld, remark, "A", typeConexion, null, null, null, null, dateGeneral);
        }else{
            fechaDes = dateGeneral;
            typeConexion = "C";
            coledContainer = new ColedContainer(recepcion, viaje,null, null, null,
                    null, remark, "A", typeConexion, fechaDes, oficial, fechaGrab, hanheld, dateGeneral);
        }



        //ColedContainer coledContainer = new ColedContainer(fecha, convYear, remark, "2128",  supervisor);
        String hola = new Gson().toJson(coledContainer);
        Log.i(TAG, hola);
        ResourceHandler.postCreateRequertColed(this, Key.requestCode.GET_REQUEST_COLED, coledContainer);
    }

    /*public ColedContainer(String RECEPCION, String VIAJE_SISTEMA, String FECHA_DESCONECCION, String GRABADOR_DESCONECCION,
                          String FECHA_GRABA_DESCONECCION, String HANDHELD_ID_DESCONECCION, String OBSERVACION, String ESTATUS_CONECCION,
                          String CONEXION_O_DESCONEXION, String FECHA_CONECCION, String GRABADOR_CONECCION, String FECHA_GRABA_CONECCION,
                          String HANDHELD_ID_CONECCION) {
    * */

    private String getDate(){
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
        Date date = new Date();
        return dateFormat.format(date);
    }


    /*
    * public ColedContainer(String RECEPCION, String VIAJE_SISTEMA, String FECHA_DESCONECCION, String GRABADOR_DESCONECCION, String FECHA_GRABA_DESCONECCION, String HANDHELD_ID_DESCONECCION,
    * String OBSERVACION, String ESTATUS_CONECCION, String CONEXION_O_DESCONEXION) {*/

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.dflr_btn_conec:
                saveType(2);
                break;
            case R.id.dflr_btn_desc:
                saveType(1);
                break;
        }
    }

    @Override
    public void onResponse(int requestCode, Object o, Object r) {
        if(requestCode == Key.requestCode.GET_REQUEST_COLED){
            Log.i(TAG, o.toString());
            try {
                JSONObject jsonObject = new JSONObject(o.toString());
                Bundle bundle = JsonBundle.createBundle(jsonObject);

                if(bundle.getBoolean("Resultado", false)){
                    onResponse.onResponse(Key.requestCode.RESPONSE_DIALOG, true, bundle.getString("Message", ""));
                }else{
                    onResponse.onResponse(Key.requestCode.RESPONSE_DIALOG, false, null);
                }
                this.dismiss();
            }catch (Exception e){
                onResponse.onResponse(Key.requestCode.RESPONSE_DIALOG, false, null);
            }
        }
    }
}
