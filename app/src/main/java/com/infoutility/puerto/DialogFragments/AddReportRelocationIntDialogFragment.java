package com.infoutility.puerto.DialogFragments;

import android.app.Dialog;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.infoutility.puerto.Api.ResourceHandler;
import com.infoutility.puerto.Interfaces.Response;
import com.infoutility.puerto.Keys.Key;
import com.infoutility.puerto.Models.Request.ReportRelocation;
import com.infoutility.puerto.Models.Request.ReportRelocationInt;
import com.infoutility.puerto.R;
import com.infoutility.puerto.Utils.JsonBundle;
import com.infoutility.puerto.Utils.PreferencesManager;
import com.infoutility.puerto.Utils.Tools;

import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;


public class AddReportRelocationIntDialogFragment extends DialogFragment implements View.OnClickListener, Response.OnResponse {
    public static final String TAG = AddReportRelocationDialogFragment.class.getSimpleName();
    private static Response.OnResponse onResponse;
    private static final int CLEAR_DATE = 0;
    private static final int START_DATE = 1;
    private static final int END_DATE = 2;
    private int typeDate = CLEAR_DATE;


    private TextView tvDateTimeStart;
    private TextView tvDateTimeEnd;
    private TextView tvShip;
    private TextView tvSupervisor;
    private TextView tvRemark;


    private Date selectedTimeStart;
    private Date selectedTimeEnd;
    private String oficial;

    public static AddReportRelocationIntDialogFragment newInstance(Response.OnResponse response){
        onResponse = response;
        return new AddReportRelocationIntDialogFragment();
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = requireActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.dialog_fragment_add_report_relocation_int_dialog, null);

        builder.setView(view)
                .setTitle(R.string.dfcd_title_ReportRelocationIng);
        initView(view);
        return builder.create();
    }


    public void initView(View view){
        PreferencesManager.initializeInstance(getActivity());
        ((Button)view.findViewById(R.id.dfarrdi_btn_select)).setOnClickListener(this);
        ((ImageView)view.findViewById(R.id.dfarrd_iv_datetime_init)).setOnClickListener(this);
        ((ImageView)view.findViewById(R.id.dfarrd_iv_datetime_end)).setOnClickListener(this);
        tvDateTimeStart = view.findViewById(R.id.dfarrd_tiet_date_init);
        tvDateTimeEnd = view.findViewById(R.id.dfarrd_tiet_date_end);
        tvShip = view.findViewById(R.id.dfarrdi_tiet_ship);
        tvSupervisor = view.findViewById(R.id.dfarrd_tiet_supervisor);
        tvRemark = view.findViewById(R.id.dfarrd_tiet_remark);
        oficial = PreferencesManager.getInstance().getInt(Key.sharedPreferences.recorder, 0) + "";
        /*tietSupervisor = view.findViewById(R.id.dfarrd_tiet_supervisor);
        tietYear = view.findViewById(R.id.dfarrd_tiet_year);
        tietRemark = view.findViewById(R.id.dfarrd_tiet_remark);*/
    }

    private void createReport(){


        try {
            String supervisor = tvSupervisor.getText().toString();
            String remark = tvRemark.getText().toString();
            String ship = tvShip.getText().toString();
            //double convShip = Double.valueOf(ship);
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
            Date date = new Date();
            String fecha = dateFormat.format(date);
            String dateInit = dateFormat.format(selectedTimeStart);
            String dateFinish = dateFormat.format(selectedTimeEnd);
            ReportRelocationInt reportRelocationInt = new ReportRelocationInt(fecha, remark, oficial, supervisor, ship, dateInit, dateFinish);
            ResourceHandler.postCreateReportRelocationImt(this, Key.requestCode.POST_REPORT_RELOCATION, reportRelocationInt);
            //public ReportRelocationInt(String fecha, String observaciones, String oficial, String supervisor, double ship, String inicio, String fin)
        }catch (Exception e){

        }

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.dfarrdi_btn_select:
                this.createReport();
                break;
            case  R.id.dfarrd_iv_datetime_init:
                this.typeDate = START_DATE;
                Tools.showDialogFragment(getChildFragmentManager(), DateTimeDialogFragment.newInstance(this), DateTimeDialogFragment.TAG);
                break;

            case R.id.dfarrd_iv_datetime_end:
                this.typeDate = END_DATE;
                Tools.showDialogFragment(getChildFragmentManager(), DateTimeDialogFragment.newInstance(this), DateTimeDialogFragment.TAG);
                break;
        }
    }

    @Override
    public void onResponse(int requestCode, Object o, Object r) {
        if(requestCode == Key.requestCode.DIALOG_DATETIME){
            if(o instanceof Date){
                Date selectedDateTime = (Date)o;
                Log.i(TAG, selectedDateTime.toString());
                if(typeDate == START_DATE){
                    selectedTimeStart = selectedDateTime;
                    String strDateTimeStart = Tools.formatDate(selectedDateTime, Key.datetimePattern.YYYYMMDD_HHMMSS_DASH_SEPARATOR);
                    tvDateTimeStart.setText(strDateTimeStart);
                }else if(typeDate == END_DATE){
                    selectedTimeEnd = selectedDateTime;
                    String strDateTimeStart = Tools.formatDate(selectedDateTime, Key.datetimePattern.YYYYMMDD_HHMMSS_DASH_SEPARATOR);
                    tvDateTimeEnd.setText(strDateTimeStart);
                }
            }

        }else if(requestCode == Key.requestCode.POST_REPORT_RELOCATION){
            Log.i(TAG, o.toString());
            try {
                JSONObject jsonObject = new JSONObject(o.toString());
                Bundle bundle = JsonBundle.createBundle(jsonObject);

                if(bundle.getBoolean("Resultado", false)){
                    onResponse.onResponse(Key.requestCode.RESPONSE_DIALOG, true, bundle.getString("Message", ""));
                }else{
                    onResponse.onResponse(Key.requestCode.RESPONSE_DIALOG, false, null);
                }
                this.dismiss();
            }catch (Exception e){

            }

        }
    }
}
