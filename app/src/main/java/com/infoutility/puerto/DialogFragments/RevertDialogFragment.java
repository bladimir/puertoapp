package com.infoutility.puerto.DialogFragments;

import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.Spinner;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.infoutility.puerto.Adapters.SpinnerAdapter;
import com.infoutility.puerto.Adapters.TextAdapter;
import com.infoutility.puerto.Api.ResourceHandler;
import com.infoutility.puerto.Interfaces.Response;
import com.infoutility.puerto.Keys.Key;
import com.infoutility.puerto.Models.ContainerYard;
import com.infoutility.puerto.Models.Dispatch;
import com.infoutility.puerto.Models.Response.BaseResponse;
import com.infoutility.puerto.Models.Yard;
import com.infoutility.puerto.Models.YardMap;
import com.infoutility.puerto.R;
import com.infoutility.puerto.Utils.AlertUtil;
import com.infoutility.puerto.Utils.PreferencesManager;
import com.infoutility.puerto.Utils.Tools;

import org.parceler.Parcels;

import java.util.Date;

public class RevertDialogFragment extends DialogFragment implements View.OnClickListener, Response.OnResponse {
    public final static String TAG = RevertDialogFragment.class.getSimpleName();

    private TextInputEditText tietPrefix;
    private TextInputEditText tietId;
    private Spinner spYard;
    private TextInputEditText tietRow;
    private TextInputEditText tietModule;
    private TextInputEditText tietLevel;
    private Button btnCancel;
    private Button btnAccept;

    private static Response.OnResponse onResponse;
    private static int REVERT_DISPATCH = 1;
    private static int REVERT_EXPORT = 2;

    private static Dispatch dispatchFound;
    private static ContainerYard containerFound;

    private int recorder = 0;
    private int revertType = 0;

    public static RevertDialogFragment newInstance(Response.OnResponse response){
        onResponse = response;
        return new RevertDialogFragment();
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            dispatchFound = Parcels.unwrap(getArguments().getParcelable(Key.extra.YARD_CONTAINER_REVERT_DISPATCH));
            containerFound = Parcels.unwrap(getArguments().getParcelable(Key.extra.YARD_CONTAINER_REVERT_EXPORT));
        }

    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = requireActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.dialog_fragment_revert, null);

        PreferencesManager.initializeInstance(getContext());
        recorder = PreferencesManager.getInstance().getInt(Key.sharedPreferences.recorder, 0);

        if (dispatchFound != null){
            revertType = REVERT_DISPATCH;
        } else if (containerFound != null){
            revertType = REVERT_EXPORT;
        }

        ResourceHandler.listYards(this, Key.requestCode.GET_YARD);

        tietPrefix = view.findViewById(R.id.dfr_tiet_prefix);
        tietId = view.findViewById(R.id.dfr_tiet_id);
        spYard = view.findViewById(R.id.dfr_sp_yard);
        tietRow = view.findViewById(R.id.dfr_tiet_row);
        tietModule = view.findViewById(R.id.dfr_tiet_module);
        tietLevel = view.findViewById(R.id.dfr_tiet_level);
        btnCancel = view.findViewById(R.id.dfr_btn_cancel);
        btnAccept = view.findViewById(R.id.dfr_btn_accept);


        btnCancel.setOnClickListener(this);
        btnAccept.setOnClickListener(this);

        builder.setView(view)
                .setTitle("Revertir");

        return builder.create();
    }

    private String validate(){
        if (tietRow.getText() == null || tietRow.getText().toString().trim().equals(""))
            return "Debe ingresar fila";
        if (tietModule.getText() == null || tietModule.getText().toString().trim().equals(""))
            return "Debe ingresar modulo";
        if (tietLevel.getText() == null || tietLevel.getText().toString().trim().equals(""))
            return "Debe ingresar nivel";
        return null;
    }

    private void doVerificationRevert(){
//        if (dispatchFound != null){
//            YardMap yardMap = new YardMap();
//            yardMap.setPREFIJO(dispatchFound.getPrefijo());
//            yardMap.setNUMERO_DE_IDENTIFICACION(dispatchFound.getNumero_de_identificacion());
//            yardMap.setPATIO(((TextAdapter)spYard.getSelectedItem()).getId());
//            yardMap.setFILA_O_CARRIL(Integer.valueOf(tietRow.getText().toString()));
//            yardMap.setMODULO(Integer.valueOf(tietModule.getText().toString()));
//            yardMap.setNIVEL(Integer.valueOf(tietLevel.getText().toString()));
//            yardMap.setFECHA(Tools.formatDate(new Date(), Key.datetimePattern.YYYYMMDD_HHMMSS_DASH_SEPARATOR));
//            ResourceHandler.postYardMap(this, Key.requestCode.POST_YARD_MAP, yardMap);
//        } else {
//            AlertUtil.showAlertOk(getString(R.string.all_text_warning), "No se ha seleccionado despacho.", getContext(), new AlertUtil.OnOkListener() {
//                @Override
//                public void onOk() {
//                    // do nothing
//                }
//            }).show();
//        }
        YardMap yardMap = new YardMap();
        if (revertType == REVERT_DISPATCH){
            yardMap.setPREFIJO(dispatchFound.getPrefijo());
            yardMap.setNUMERO_DE_IDENTIFICACION(dispatchFound.getNumero_de_identificacion());
        } else if (revertType == REVERT_EXPORT){
            yardMap.setPREFIJO(containerFound.getPrefijo());
            yardMap.setNUMERO_DE_IDENTIFICACION(containerFound.getNumero_de_identificacion());
        }

        yardMap.setPATIO(((TextAdapter)spYard.getSelectedItem()).getId());
        yardMap.setFILA_O_CARRIL(Integer.valueOf(tietRow.getText().toString()));
        yardMap.setMODULO(Integer.valueOf(tietModule.getText().toString()));
        yardMap.setNIVEL(Integer.valueOf(tietLevel.getText().toString()));
        yardMap.setFECHA(Tools.formatDate(new Date(), Key.datetimePattern.YYYYMMDD_HHMMSS_DASH_SEPARATOR));
        ResourceHandler.postYardMap(this, Key.requestCode.POST_YARD_MAP, yardMap);
    }

    private void doDispathRevert(){

            Dispatch dispatchRequest = new Dispatch();
            Date now = new Date();

            dispatchRequest.setAutorizacion_despacho(dispatchFound.getAutorizacion_despacho());
            dispatchRequest.setFecha_hora_ubicador(Tools.formatDate(now, Key.datetimePattern.YYYYMMDD_HHMMSS_DASH_SEPARATOR));
            dispatchRequest.setFila_o_carril(Integer.valueOf(tietRow.getText().toString()));
            dispatchRequest.setGrabador_patio(recorder);
            dispatchRequest.setHora_grabacion_patio(Tools.formatDate(now, Key.datetimePattern.YYYYMMDD_HHMMSS_DASH_SEPARATOR));
            dispatchRequest.setMaquina(dispatchFound.getMaquina());
            dispatchRequest.setModulo(Integer.valueOf(tietModule.getText().toString()));
            dispatchRequest.setNivel(Integer.valueOf(tietLevel.getText().toString()));
            dispatchRequest.setNumero_de_identificacion(dispatchFound.getNumero_de_identificacion());
            dispatchRequest.setPatio(((TextAdapter)spYard.getSelectedItem()).getId());
            dispatchRequest.setPrefijo(dispatchFound.getPrefijo());
            String s = new Gson().toJson(dispatchRequest);
            ResourceHandler.putDispatch(this, Key.requestCode.PUT_DISPATCH, dispatchRequest);

    }

    private void doExportRevert(){
        com.infoutility.puerto.Models.Request.ContainerYard containerYardRequest = new com.infoutility.puerto.Models.Request.ContainerYard();
        containerYardRequest.setVIAJE_NO(containerFound.getViaje_no());
        containerYardRequest.setTIPO_DE_MOVIMIENTO(containerFound.getTipo_de_movimiento());
        containerYardRequest.setORDEN(containerFound.getOrden());
        containerYardRequest.setPREFIJO(containerFound.getPrefijo());
        containerYardRequest.setNUMERO_DE_IDENTIFICACION(containerFound.getNumero_de_identificacion());
        containerYardRequest.setPATIO(((TextAdapter)spYard.getSelectedItem()).getId());
        containerYardRequest.setFILA(Integer.valueOf(tietRow.getText().toString()));
        containerYardRequest.setMODULO(Integer.valueOf(tietModule.getText().toString()));
        containerYardRequest.setNIVEL(Integer.valueOf(tietLevel.getText().toString()));
        containerYardRequest.setMAQUINA(containerFound.getMaquina());
        containerYardRequest.setMEDIDA(containerFound.getMedida());
//        containerYardRequest.setGRABADOR_IMPOR_PATIO(Long.valueOf(containerFound.getGrabador_impor_patio()));
        containerYardRequest.setGRABADOR_IMPOR_PATIO(recorder);
        containerYardRequest.setFECHAHORA_IMPOR_PATIO(Tools.formatDate(new Date(), Key.datetimePattern.YYYYMMDD_HHMMSS_DASH_SEPARATOR));
String G = new Gson().toJson(containerYardRequest);
        ResourceHandler.postContainerYardRevertExport(this, Key.requestCode.POST_CONTAINER_YARD_REVERT_EXPORT, containerYardRequest);
    }

    private void doRevert(){
        AlertUtil.showAlertConfirmation(getString(R.string.all_text_warning), "Esta seguro de realizar la reversion?", getContext(), "Revertir", new AlertUtil.OnOkListener() {
            @Override
            public void onOk() {
                doVerificationRevert();
            }
        }, new AlertUtil.OnCancelListener() {
            @Override
            public void onCancel() {

            }
        }).setCancelable(false)
                .show();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.dfr_btn_accept:
                /*t: data, r:tipo reversion*/
//                onResponse.onResponse(Key.requestCode.RESPONSE_DIALOG, null, null);
                String validationMessage = validate();
                if (validationMessage == null){
                    if (revertType != 0){
                        doRevert();
                    } else {
                        AlertUtil.showAlertOk(getString(R.string.all_text_warning), (revertType == REVERT_DISPATCH ? "Despacho" :  "Contenedor") +" no valido", getContext(), new AlertUtil.OnOkListener() {
                            @Override
                            public void onOk() {
                                // do nothing
                            }
                        }).show();
                    }

                } else {
                    AlertUtil.showAlertOk(getString(R.string.all_text_warning), validationMessage, getContext(), new AlertUtil.OnOkListener() {
                        @Override
                        public void onOk() {
                            // do nothing
                        }
                    }).show();
                }
                break;
            case R.id.dfr_btn_cancel:
                dismiss();
                break;
            default:
                break;
        }
    }

    @Override
    public void onResponse(int requestCode, Object o, Object r) {
        switch (requestCode) {
            case Key.requestCode.GET_YARD:
                if (o instanceof JsonObject) {
                    JsonObject response = (JsonObject) o;
                    SpinnerAdapter adapter = SpinnerAdapter.createArrayAdapter(getActivity(), response.getAsJsonArray(Key.jsonResponse.data).toString(), Yard.class);
                    spYard.setAdapter(adapter);
                }
                break;
            case Key.requestCode.POST_YARD_MAP:
                if (o instanceof JsonObject){
                    JsonObject jObject = (JsonObject) o;
                    BaseResponse response = new Gson().fromJson(jObject.toString(), BaseResponse.class);

                    if (response.isResultado()){
                        if (revertType == REVERT_DISPATCH){
                            doDispathRevert();
                        } else if (revertType == REVERT_EXPORT){
                            doExportRevert();
                        } else {
                            AlertUtil.showAlertOk(getString(R.string.all_text_warning), "No se puede realizar la reversion.", getContext(), new AlertUtil.OnOkListener() {
                                @Override
                                public void onOk() {
                                    // do nothing
                                }
                            }).show();
                        }

                    } else {
                        AlertUtil.showAlertOk(getString(R.string.all_text_error), response.getMessage(), getContext(), new AlertUtil.OnOkListener() {
                            @Override
                            public void onOk() {
                                // do nothing
                            }
                        }).show();
                    }
                }
                break;
            case Key.requestCode.PUT_DISPATCH:
            case Key.requestCode.POST_CONTAINER_YARD_REVERT_EXPORT:
                if (o instanceof JsonObject){
                    JsonObject jObject = (JsonObject) o;
                    BaseResponse response = new Gson().fromJson(jObject.toString(), BaseResponse.class);

                    if (response.isResultado()){
                        onResponse.onResponse(Key.requestCode.RESPONSE_DIALOG, null, null);
                        AlertUtil.showAlertOk(getString(R.string.all_text_success), response.getMessage(), getContext(), new AlertUtil.OnOkListener() {
                            @Override
                            public void onOk() {
                                dismiss();
                            }
                        }).setCancelable(false)
                                .show();
                    } else {
                        AlertUtil.showAlertOk(getString(R.string.all_text_error), response.getMessage(), getContext(), new AlertUtil.OnOkListener() {
                            @Override
                            public void onOk() {
                                // do nothing
                            }
                        }).show();
                    }
                }
                break;
        }
    }
}
