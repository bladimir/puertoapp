package com.infoutility.puerto.DialogFragments;

import android.app.Dialog;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.google.common.collect.ImmutableMap;
import com.google.gson.JsonObject;
import com.infoutility.puerto.Api.ResourceHandler;
import com.infoutility.puerto.Api.Status;
import com.infoutility.puerto.Interfaces.Response;
import com.infoutility.puerto.Keys.Key;
import com.infoutility.puerto.Models.ConsumptionHistory;
import com.infoutility.puerto.Models.MeterReceipt;
import com.infoutility.puerto.R;
import com.infoutility.puerto.Utils.AlertUtil;
import com.infoutility.puerto.Utils.Tools;

import org.parceler.Parcels;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class MeterReceiptDialogFragment extends DialogFragment implements View.OnClickListener, Response.OnResponse {
    public static final String TAG = MeterReceiptDialogFragment.class.getSimpleName();



    private TextView tvBody;
    private TableLayout tlPrevious;
    private static Response.OnResponse onResponse;
    private MeterReceipt meterReceipt;

    public static MeterReceiptDialogFragment newInstance(Response.OnResponse response){
        onResponse = response;
        return new MeterReceiptDialogFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null)
            meterReceipt = Parcels.unwrap(getArguments().getParcelable(Key.extra.METER_RECEIPT));
    }


    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = requireActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.dialog_fragment_meter_receipt, null);

        tvBody = view.findViewById(R.id.dfmr_tv_body);
        tlPrevious = view.findViewById(R.id.dfmr_tl_previous);


        setData();

        builder.setView(view)
                .setTitle("Recibo");

        return builder.create();
    }


    private void setData(){
        if (meterReceipt != null){
            doPreviousConsumption(meterReceipt.getNoContador(), meterReceipt.getCodigo());

            String receiptBody
                    = String.format("No Contador: %s\n" +
                            "NIT: %s\n" +
                            "Nombre: %s\n\n\n" +
                            "Consumo Ant.: %s\n" +
                            "Lectura Ant.: %s\n" +
                            "LecturaActual: %s\n\n\n" +
                            "Consumo: %s   %s\n\n" +
                            "Total: %s",
                    meterReceipt.getNoContador(),
                    meterReceipt.getNit(), meterReceipt.getNombre(), meterReceipt.getConsumoAnterior() + " KW/hr", meterReceipt.getLecturaAnterior(),
                    meterReceipt.getLecturaActual(), meterReceipt.getConsumo() + " KW/hr", "Q." + meterReceipt.getTotal(), "Q." + meterReceipt.getTotal());

            tvBody.setText(receiptBody);
        } else {
            AlertUtil.showAlertOk(getString(R.string.all_text_warning), "No es posible generar el recibo", getContext(), new AlertUtil.OnOkListener() {
                @Override
                public void onOk() {
                    // do nothing
                }
            }).show();
        }
    }


    private void doPreviousConsumption(String meter, String code) {
        Date current = new Date();
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(current);
        String year = String.valueOf(calendar.get(Calendar.YEAR));

        ResourceHandler.getConsumptionHistory(this, Key.requestCode.GET_METER_HISTORY_CONSUMPTION,
                ImmutableMap.of(Key.api.paramMeter, meter,
                        Key.api.paramUserCode, code,
                        Key.api.paramYear, year));
    }


    private void populateTable(List<ConsumptionHistory> lst){

        TableRow row = new TableRow(getContext());
        row.addView(getCell("Mes", true));
        row.addView(getCell("Año", true));
        row.addView(getCell("Lectura", true));
        row.addView(getCell("Consumo", true));
        tlPrevious.addView(row, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));

        int limit = 1;
        if (lst.size() >= 3)
            limit = 3;
        else
            limit = lst.size();


        for (int i = 0; i < limit; i++) {

            row = new TableRow(getContext());
            row.addView(getCell(String.valueOf(lst.get(i).getMus_mes()), false));
            row.addView(getCell(String.valueOf(lst.get(i).getMus_anio()), false));
            row.addView(getCell(String.valueOf(lst.get(i).getMus_lectura()), false));
            row.addView(getCell(String.valueOf(lst.get(i).getMus_consumo()), false));
            tlPrevious.addView(row, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        }


    }

    private TextView getCell(String txt, boolean isHeader){
        TextView tv = new TextView(getContext());
        if (isHeader){
            tv.setTextSize(14);
            tv.setTypeface(Typeface.DEFAULT_BOLD);
            tv.setBackgroundColor(getContext().getColor(R.color.gray_chateau));
        }
        tv.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
        tv.setText(txt);

        return tv;
    }

    @Override
    public void onClick(View view) {

    }

    @Override
    public void onResponse(int requestCode, Object o, Object r) {
        switch (requestCode){
            case Key.requestCode.GET_METER_HISTORY_CONSUMPTION:
                if (o instanceof JsonObject) {
                    JsonObject response = (JsonObject) o;
                    List<ConsumptionHistory> lst = Tools.getListFromJson(response.getAsJsonArray(Key.jsonResponse.data).toString(), ConsumptionHistory.class);

                    if (lst.size() > 0){
                        populateTable(lst);
                    } else {
                        List<ConsumptionHistory> lstOffline = new ArrayList<>();
                        ConsumptionHistory consumptionHistory = new ConsumptionHistory();
                        consumptionHistory.setMus_anio(meterReceipt.getAnioAnterior());
                        consumptionHistory.setMus_mes(meterReceipt.getMesAnterior());
                        consumptionHistory.setMus_consumo(Integer.valueOf(meterReceipt.getConsumoAnterior()));
                        consumptionHistory.setMus_lectura(Long.valueOf(meterReceipt.getLecturaAnterior()));
                        lstOffline.add(consumptionHistory);
                        populateTable(lstOffline);

//                        AlertUtil.showAlertOk(getString(R.string.all_text_warning), "No existen resultados de consumos anterior", getContext(), new AlertUtil.OnOkListener() {
//                            @Override
//                            public void onOk() {
//                                // do nothing
//                            }
//                        }).show();
                    }
                } else if (o instanceof Status) {
                    Status status = (Status)o;
                    if (status.getCode() == Key.statusCode.noInternet.code){
                        List<ConsumptionHistory> lstOffline = new ArrayList<>();
                        ConsumptionHistory consumptionHistory = new ConsumptionHistory();
                        consumptionHistory.setMus_anio(meterReceipt.getAnioAnterior());
                        consumptionHistory.setMus_mes(meterReceipt.getMesAnterior());
                        consumptionHistory.setMus_consumo(Integer.valueOf(meterReceipt.getConsumoAnterior()));
                        consumptionHistory.setMus_lectura(Long.valueOf(meterReceipt.getLecturaAnterior()));
                        lstOffline.add(consumptionHistory);
                        populateTable(lstOffline);

                        AlertUtil.showAlertOk(getString(R.string.all_text_warning), "No hay conexion a internet se mostrar unicamente el ultimo consumo.", getContext(), new AlertUtil.OnOkListener() {
                            @Override
                            public void onOk() {
                                // do nothing
                            }
                        }).show();
                    } else {
                        AlertUtil.showAlertOk(getString(R.string.all_text_warning), "Error desconocido.", getContext(), new AlertUtil.OnOkListener() {
                            @Override
                            public void onOk() {
                                // do nothing
                            }
                        }).show();
                    }
                }
                break;
        }
    }
}
