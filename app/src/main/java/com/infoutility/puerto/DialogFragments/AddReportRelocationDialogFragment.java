package com.infoutility.puerto.DialogFragments;


import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.infoutility.puerto.Api.ResourceHandler;
import com.infoutility.puerto.Interfaces.Response;
import com.infoutility.puerto.Keys.Key;
import com.infoutility.puerto.Models.Request.ReportRelocation;
import com.infoutility.puerto.R;
import com.infoutility.puerto.Utils.JsonBundle;
import com.infoutility.puerto.Utils.PreferencesManager;

import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * A simple {@link Fragment} subclass.
 */
public class AddReportRelocationDialogFragment extends DialogFragment implements View.OnClickListener, Response.OnResponse {
    public static final String TAG = AddReportRelocationDialogFragment.class.getSimpleName();
    private static Response.OnResponse onResponse;
    private TextInputEditText tietSupervisor;
    private TextInputEditText tietYear;
    private TextInputEditText tietRemark;
    private String oficial;

    public AddReportRelocationDialogFragment() {
        // Required empty public constructor
    }

    public static AddReportRelocationDialogFragment newInstance(Response.OnResponse response){
        onResponse = response;
        return new AddReportRelocationDialogFragment();
    }



    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = requireActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.dialog_fragment_add_report_relocation_dialog, null);

        builder.setView(view)
                .setTitle(R.string.dfcd_title_ReportRelocation);
        initView(view);
        return builder.create();
    }



    public void initView(View view){
        PreferencesManager.initializeInstance(getActivity());
        ((Button)view.findViewById(R.id.dfarrd_btn_select)).setOnClickListener(this);
        tietSupervisor = view.findViewById(R.id.dfarrd_tiet_supervisor);
        tietYear = view.findViewById(R.id.dfarrd_tiet_year);
        tietRemark = view.findViewById(R.id.dfarrd_tiet_remark);
        oficial = PreferencesManager.getInstance().getInt(Key.sharedPreferences.recorder, 0) + "";
    }


    private void createReport(){

        String supervisor = tietSupervisor.getText().toString();
        String year = tietYear.getText().toString();
        int convYear = Integer.parseInt(year);
        String remark = tietRemark.getText().toString();

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
        Date date = new Date();
        String fecha = dateFormat.format(date);
        ReportRelocation reportRelocation = new ReportRelocation(fecha, convYear, remark, oficial,  supervisor);
        ResourceHandler.postCreateReportRelocation(this, Key.requestCode.POST_REPORT_RELOCATION, reportRelocation);

    }


    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.dfarrd_btn_select:
                this.createReport();
                break;
        }
    }

    @Override
    public void onResponse(int requestCode, Object o, Object r) {
        if(requestCode == Key.requestCode.POST_REPORT_RELOCATION){
            Log.i(TAG, o.toString());
            try {
                JSONObject jsonObject = new JSONObject(o.toString());
                Bundle bundle = JsonBundle.createBundle(jsonObject);

                if(bundle.getBoolean("Resultado", false)){
                    onResponse.onResponse(Key.requestCode.RESPONSE_DIALOG, true, bundle.getString("Message", ""));
                }else{
                    onResponse.onResponse(Key.requestCode.RESPONSE_DIALOG, false, null);
                }
                this.dismiss();
            }catch (Exception e){

            }

        }
    }
}
