package com.infoutility.puerto.DialogFragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.infoutility.puerto.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class ListSearchcoledDialogFragment extends Fragment {


    public ListSearchcoledDialogFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.dialog_fragment_list_searchcoled, container, false);
    }

}
