package com.infoutility.puerto.DialogFragments;

import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.infoutility.puerto.Adapters.SpinnerAdapter;
import com.infoutility.puerto.Adapters.TextAdapter;
import com.infoutility.puerto.Api.ResourceHandler;
import com.infoutility.puerto.Interfaces.Response;
import com.infoutility.puerto.Keys.Key;
import com.infoutility.puerto.Models.ContainerCondition;
import com.infoutility.puerto.Models.ContainerYard;
import com.infoutility.puerto.Models.Machine;
import com.infoutility.puerto.Models.Response.BaseResponse;
import com.infoutility.puerto.Models.Yard;
import com.infoutility.puerto.R;
import com.infoutility.puerto.Utils.AlertUtil;
import com.infoutility.puerto.Utils.PreferencesManager;
import com.infoutility.puerto.Utils.Tools;

import org.parceler.Parcels;

import java.util.Date;

public class ImportContainerDialogFragment extends DialogFragment implements View.OnClickListener, Response.OnResponse {
    public static final String TAG = ImportContainerDialogFragment.class.getSimpleName();

    private Spinner spYard;
    private TextInputEditText tietRow;
    private TextInputEditText tietModule;
    private TextInputEditText tietLevel;
    private Spinner spMachine;
    private Button btnImport;
    private Button btnCancel;
    private TextInputEditText tietMachine;
    private TextView tvMachine;

//    private long trip = 0;
    private int recorder = 0;
//    private int workMode = 0;
//    private String handheld = null;

    private ContainerYard containerYard;

    private static Response.OnResponse onResponse;

    public static ImportContainerDialogFragment newInstance(Response.OnResponse response){
        onResponse = response;
        return new ImportContainerDialogFragment();
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null)
            containerYard = Parcels.unwrap(getArguments().getParcelable(Key.extra.CONTAINER_IMPORT));
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = requireActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.dialog_fragment_import_container, null);

        PreferencesManager.initializeInstance(getContext());

        ResourceHandler.listMachines(this, Key.requestCode.GET_MACHINE);
        ResourceHandler.listYards(this, Key.requestCode.GET_YARD);

//        trip = PreferencesManager.getInstance().getLong(Key.sharedPreferences.trip, 0L);
        recorder = PreferencesManager.getInstance().getInt(Key.sharedPreferences.recorder, 0);
//        workMode = PreferencesManager.getInstance().getInt(Key.sharedPreferences.workMode, 0);
//        handheld = PreferencesManager.getInstance().getString(Key.sharedPreferences.handheld, null);

        spYard = view.findViewById(R.id.dfic_sp_yard);
        tietRow = view.findViewById(R.id.dfic_tiet_row);
        tietModule = view.findViewById(R.id.dfic_tiet_module);
        tietLevel = view.findViewById(R.id.dfic_tiet_level);
        spMachine = view.findViewById(R.id.dfic_sp_machine);
        btnImport = view.findViewById(R.id.dfic_btn_import);
        btnCancel = view.findViewById(R.id.dfic_btn_cancel);
        tietMachine = view.findViewById(R.id.dfic_tiet_machine);
        tvMachine = view.findViewById(R.id.dfic_tv_machine);

        spMachine.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                tvMachine.setText(((TextAdapter)adapterView.getItemAtPosition(i)).getData());
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        btnCancel.setOnClickListener(this);
        btnImport.setOnClickListener(this);

        builder.setView(view)
                .setTitle(R.string.fie_title_import);

        return builder.create();
    }



    private String validate(){
        if (containerYard == null)
            return "No se ha seleccionado contenedor,";
//        if (handheld == null)
//            return "Handheld no valido,";
        if (recorder == 0)
            return "No se pudo obtener grabador.";
//        if (workMode == 0)
//            return "Debe seleccionar una opcion de trabajo";
//        if (trip == 0)
//            return "No existe viaje.";
        if (tietRow.getText() == null || tietRow.getText().toString().trim().equals(""))
            return "Debe ingresar fila";
        if (tietModule.getText() == null || tietModule.getText().toString().trim().equals(""))
            return "Debe ingresar modulo";
        if (tietLevel.getText() == null || tietLevel.getText().toString().trim().equals(""))
            return "Debe ingresar nivel";
//        if (tietMachine.getText() == null || tietMachine.getText().toString().trim().equals(""))
//            return "Debe ingresar maquina";

        return null;
    }


    public void doImport(){
        String validate = validate();
        if (validate == null){
            com.infoutility.puerto.Models.Request.ContainerYard containerYardRequest = new com.infoutility.puerto.Models.Request.ContainerYard();
            containerYardRequest.setVIAJE_NO(containerYard.getViaje_no());
            containerYardRequest.setTIPO_DE_MOVIMIENTO(containerYard.getTipo_de_movimiento());
            containerYardRequest.setORDEN(containerYard.getOrden());
            containerYardRequest.setPREFIJO(containerYard.getPrefijo());
            containerYardRequest.setNUMERO_DE_IDENTIFICACION(containerYard.getNumero_de_identificacion());
            containerYardRequest.setPATIO(((TextAdapter)spYard.getSelectedItem()).getId());
            containerYardRequest.setFILA(Integer.valueOf(tietRow.getText().toString()));
            containerYardRequest.setMODULO(Integer.valueOf(tietModule.getText().toString()));
            containerYardRequest.setNIVEL(Integer.valueOf(tietLevel.getText().toString()));
            containerYardRequest.setMAQUINA(Integer.valueOf(((TextAdapter)spMachine.getSelectedItem()).getId()));
//            containerYardRequest.setMAQUINA(Integer.valueOf(tietMachine.getText().toString()));
            containerYardRequest.setMEDIDA(containerYard.getMedida());
            containerYardRequest.setGRABADOR_IMPOR_PATIO(recorder);
            containerYardRequest.setFECHAHORA_IMPOR_PATIO(Tools.formatDate(new Date(), Key.datetimePattern.YYYYMMDD_HHMMSS_DASH_SEPARATOR));

            ResourceHandler.postContainerYard(this, Key.requestCode.POST_CONTAINER_YARD, containerYardRequest);
        } else {
            AlertUtil.showAlertOk(getString(R.string.all_text_warning), validate, getContext(), new AlertUtil.OnOkListener() {
                @Override
                public void onOk() {

                }
            }).show();
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.dfic_btn_import:
                doImport();
                break;
            case R.id.dfic_btn_cancel:
                dismiss();
                break;
        }
    }

    @Override
    public void onResponse(int requestCode, Object o, Object r) {
        switch (requestCode) {
            case Key.requestCode.GET_YARD:
                if (o instanceof JsonObject) {
                    JsonObject response = (JsonObject) o;
                    SpinnerAdapter adapter = SpinnerAdapter.createArrayAdapter(getActivity(), response.getAsJsonArray(Key.jsonResponse.data).toString(), Yard.class);
                    spYard.setAdapter(adapter);
                }
                break;
            case Key.requestCode.GET_MACHINE:
                if (o instanceof JsonObject) {
                    JsonObject response = (JsonObject) o;
                    SpinnerAdapter adapter = SpinnerAdapter.createArrayAdapter(getActivity(), response.getAsJsonArray(Key.jsonResponse.data).toString(), Machine.class);
                    spMachine.setAdapter(adapter);
                }
                break;
            case Key.requestCode.POST_CONTAINER_YARD:
                if (o instanceof JsonObject){
                    JsonObject jObject = (JsonObject) o;
                    BaseResponse response = new Gson().fromJson(jObject.toString(), BaseResponse.class);

                    if (response.isResultado()){
                        onResponse.onResponse(Key.requestCode.RESPONSE_DIALOG, null, null);
                        AlertUtil.showAlertOk(getString(R.string.all_text_success), response.getMessage(), getContext(), new AlertUtil.OnOkListener() {
                            @Override
                            public void onOk() {
                                dismiss();
                            }
                        }).setCancelable(false)
                                .show();
                    } else {
                        AlertUtil.showAlertOk(getString(R.string.all_text_error), response.getMessage(), getContext(), new AlertUtil.OnOkListener() {
                            @Override
                            public void onOk() {
                                // do nothing
                            }
                        }).show();
                    }
                }
                break;
        }
    }
}
