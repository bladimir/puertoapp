package com.infoutility.puerto.DialogFragments;

import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.infoutility.puerto.Keys.Key;
import com.infoutility.puerto.Models.ContainerShip;
import com.infoutility.puerto.R;

import org.parceler.Parcels;

public class ContainerShipDetailDialogFragment extends DialogFragment implements View.OnClickListener {
    public static final String TAG = ContainerShipDetailDialogFragment.class.getSimpleName();

    private Button btnAccept;
    private TextView tvTripSystem;
    private TextView tvContainerShip;
    private TextView tvTripContainerShip;
    private TextView tvSituation;
    private TextView tvDate;
    private TextView tvShippingCompany;

    private Bundle bContainerShipInfo;

    public static ContainerShipDetailDialogFragment newInstance(){
        return new ContainerShipDetailDialogFragment();
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = requireActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.dialog_fragment_container_ship_detail, null);

        bContainerShipInfo = getArguments();

        tvTripSystem = view.findViewById(R.id.dfcsd_tv_trip_system);
        tvContainerShip = view.findViewById(R.id.dfcsd_tv_container_ship);
        tvTripContainerShip = view.findViewById(R.id.dfcsd_tv_trip_container_ship);
        tvSituation = view.findViewById(R.id.dfcsd_tv_situation);
        tvDate = view.findViewById(R.id.dfcsd_tv_date);
        tvShippingCompany = view.findViewById(R.id.dfcsd_tv_shipping_company);
        btnAccept = view.findViewById(R.id.dfcsd_btn_accept);

        btnAccept.setOnClickListener(this);

        builder.setView(view)
                .setTitle(R.string.dfcsd_title_container_ship_detail);

        setContainerDetailInfo();

        return builder.create();
    }

    private void setContainerDetailInfo(){
        if (bContainerShipInfo != null){
            ContainerShip containerShip = Parcels.unwrap(bContainerShipInfo.getParcelable(Key.extra.CONTAINER_SHIP_DETAIL));
            if (containerShip != null){
                tvTripSystem.setText(getResources().getString(R.string.dfcsd_title_trip_system, String.valueOf(containerShip.getViaje_empornac())));
                tvContainerShip.setText(getResources().getString(R.string.dfcsd_title_container_ship, containerShip.getBarco()));
                tvTripContainerShip.setText(getResources().getString(R.string.dfcsd_title_trip_container_ship, containerShip.getViaje_naviera()));
                tvSituation.setText(getResources().getString(R.string.dfcsd_title_situation, containerShip.getSituaciondescripcion()));
                tvDate.setText(getResources().getString(R.string.dfcsd_title_date, containerShip.getFecha_hora() != null ? containerShip.getFecha_hora() : "-"));
                tvShippingCompany.setText(getResources().getString(R.string.dfcsd_title_shipping_company, "-"));
            }
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.dfcsd_btn_accept:
                dismiss();
                break;
            default:
                break;
        }
    }
}
