package com.infoutility.puerto.DialogFragments;

import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.infoutility.puerto.Keys.Key;
import com.infoutility.puerto.Models.Container;
import com.infoutility.puerto.R;

import org.parceler.Parcels;

public class ContainerDetailDialogFragment extends DialogFragment implements View.OnClickListener {
    public static final String TAG = ContainerDetailDialogFragment.class.getSimpleName();

    private Button btnAccept;
    private TextView tvTrip;
    private TextView tvMoveType;
    private TextView tvCof;
    private TextView tvId;
    private TextView tvWeight;
    private TextView tvPrefix;
    private TextView tvOrder;
    private TextView tvConfirm;

    private Bundle bContainerInfo;

    public static ContainerDetailDialogFragment newInstance(){
        return new ContainerDetailDialogFragment();
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = requireActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.dialog_fragment_container_detail, null);

        bContainerInfo = getArguments();

        tvTrip = view.findViewById(R.id.dfcd_tv_trip);
        tvMoveType = view.findViewById(R.id.dfcd_tv_moveType);
        tvCof = view.findViewById(R.id.dfcd_tv_cof);
        tvId = view.findViewById(R.id.dfcd_tv_id);
        tvWeight = view.findViewById(R.id.dfcd_tv_weight);
        tvPrefix = view.findViewById(R.id.dfcd_tv_prefix);
        tvOrder = view.findViewById(R.id.dfcd_tv_order);
        tvConfirm = view.findViewById(R.id.dfcd_tv_confirm);
        btnAccept = view.findViewById(R.id.dfcd_btn_accept);

        btnAccept.setOnClickListener(this);

        builder.setView(view)
                .setTitle(R.string.dfcd_title_container_detail);

        setContainerDetailInfo();

        return builder.create();
    }


    private void setContainerDetailInfo(){
        if (bContainerInfo != null){
            Container container = Parcels.unwrap(bContainerInfo.getParcelable(Key.extra.CONTAINER_DETAIL));
            if (container != null){
                tvTrip.setText(getResources().getString(R.string.dfcd_title_trip, container.getViaje_no()));
                tvMoveType.setText(getResources().getString(R.string.dfcd_title_moveType, container.getTipo_de_movimiento()));
                tvCof.setText(getResources().getString(R.string.dfcd_title_cof, container.getC_o_f()));
                tvId.setText(getResources().getString(R.string.dfcd_title_id, container.getNumero_de_identificacion()));
                tvWeight.setText(getResources().getString(R.string.dfcd_title_weight, String.valueOf(container.getPeso_manifestado())));
                tvPrefix.setText(getResources().getString(R.string.dfcd_title_prefix, container.getPrefijo()));
                tvOrder.setText(getResources().getString(R.string.dfcd_title_order, container.getOrden()));
                tvConfirm.setText(getResources().getString(R.string.dfcd_title_confirm, container.getConfirmado() != null ? container.getConfirmado() : "-"));
            }
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.dfcd_btn_accept:
                dismiss();
                break;
            default:
                break;
        }
    }
}
