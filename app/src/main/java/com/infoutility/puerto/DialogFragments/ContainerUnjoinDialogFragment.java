package com.infoutility.puerto.DialogFragments;

import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.common.base.Function;
import com.google.common.base.Strings;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Multimap;
import com.google.common.collect.Multimaps;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.infoutility.puerto.Adapters.ContainerJoinerAdapter;
import com.infoutility.puerto.Api.ResourceHandler;
import com.infoutility.puerto.Interfaces.Response;
import com.infoutility.puerto.Keys.Key;
import com.infoutility.puerto.Models.ContainerJoined;
import com.infoutility.puerto.Models.Response.BaseResponse;
import com.infoutility.puerto.R;
import com.infoutility.puerto.Utils.AlertUtil;
import com.infoutility.puerto.Utils.PreferencesManager;
import com.infoutility.puerto.Utils.Tools;

import org.checkerframework.checker.nullness.compatqual.NullableDecl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

public class ContainerUnjoinDialogFragment extends DialogFragment implements View.OnClickListener, Response.OnResponse {
    public static final String TAG = ContainerUnjoinDialogFragment.class.getSimpleName();



    private LinearLayout llJoined;
    private RecyclerView rvJoined;
    private LinearLayout llJoinedRelated;
    private TextView tvContainerJoined;
    private RecyclerView rvJoinedRelated;
    private ContainerJoinerAdapter containerJoinerAdapter;
    private List<ContainerJoined> lstContainerJoined;
    private ContainerJoinerAdapter containerJoineRelatedAdapter;
    private List<ContainerJoined> lstContainerJoinedRelated;
    private Button btnBack;




    private long trip = 0;
    private int recorder = 0;
    private int workMode = 0;
    private long currentShift = 0;
    private String handheld = null;

    ContainerJoined containerJoinedSelected;

    private static Response.OnResponse onResponse;

    public static ContainerUnjoinDialogFragment newInstance(Response.OnResponse response){
        onResponse = response;
        return new ContainerUnjoinDialogFragment();
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = requireActivity().getLayoutInflater();
        View v = inflater.inflate(R.layout.dialog_fragment_container_unjoin, null);

        PreferencesManager.initializeInstance(getContext());

        trip = PreferencesManager.getInstance().getLong(Key.sharedPreferences.trip, 0L);
        recorder = PreferencesManager.getInstance().getInt(Key.sharedPreferences.recorder, 0);
        workMode = PreferencesManager.getInstance().getInt(Key.sharedPreferences.workMode, 0);
        currentShift = PreferencesManager.getInstance().getLong(Key.sharedPreferences.currentShift, 0L);
        handheld = PreferencesManager.getInstance().getString(Key.sharedPreferences.handheld, null);

        doDataLoad();

        rvJoined = v.findViewById(R.id.dfcu_rv_joined);
        rvJoinedRelated = v.findViewById(R.id.dfcu_rv_joined_related);
        llJoined = v.findViewById(R.id.dfcu_ll_joined);
        llJoinedRelated = v.findViewById(R.id.dfcu_ll_joined_related);
        tvContainerJoined = v.findViewById(R.id.dfcu_tv_container_joined);
        btnBack = v.findViewById(R.id.dfcu_btn_back);

        lstContainerJoined = new ArrayList<>();
        lstContainerJoinedRelated = new ArrayList<>();

        containerJoinerAdapter = new ContainerJoinerAdapter(getContext(), lstContainerJoined, this, true);
        containerJoineRelatedAdapter = new ContainerJoinerAdapter(getContext(), lstContainerJoinedRelated, this, false);

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext());
        rvJoined.setLayoutManager(mLayoutManager);
        rvJoined.setAdapter(containerJoinerAdapter);


        RecyclerView.LayoutManager mLayoutManagerR = new LinearLayoutManager(getContext());
        rvJoinedRelated.setLayoutManager(mLayoutManagerR);
        rvJoinedRelated.setAdapter(containerJoineRelatedAdapter);

        btnBack.setOnClickListener(this);

        toggleLayout(false);

        builder.setView(v)
                .setTitle(R.string.ccj_title_container_unjoined);
        builder.setCancelable(false);
        return builder.create();
    }

    private void doDataLoad() {
        ResourceHandler.listContainersJoined(this, Key.requestCode.GET_CONTAINER_JOINED,
                ImmutableMap.of(Key.api.paramTrip, String.valueOf(trip),
                        Key.api.paramWorkMode, String.valueOf(workMode)));
    }

//    private void doJoinedSelection(ContainerJoined containerJoined){
//        toggleLayout(true);
//        ResourceHandler.listContainersJoined(this, Key.requestCode.GET_CONTAINER_JOINED_BY_ID,
//                ImmutableMap.of(Key.api.paramTrip, String.valueOf(trip),
//                        Key.api.paramWorkMode, String.valueOf(workMode),
//                        Key.api.paramId, String.valueOf(containerJoined.getId_atado())));
//    }

    private void doJoinedSelection(){
        if (containerJoinedSelected != null){
            toggleLayout(true);
            ResourceHandler.listContainersJoined(this, Key.requestCode.GET_CONTAINER_JOINED_BY_ID,
                    ImmutableMap.of(Key.api.paramTrip, String.valueOf(trip),
                            Key.api.paramWorkMode, String.valueOf(workMode),
                            Key.api.paramId, String.valueOf(containerJoinedSelected.getId_atado())));
        }
    }

    private void doUnjoin(ContainerJoined containerJoined) {
        com.infoutility.puerto.Models.Request.ContainerJoined containerJoinedRequest = new com.infoutility.puerto.Models.Request.ContainerJoined();

        containerJoinedRequest.setID_ATADO(containerJoined.getId_atado());
        containerJoinedRequest.setVIAJE_NO(String.valueOf((containerJoined.getViaje_no())));
        containerJoinedRequest.setTIPO_DE_MOVIMIENTO(workMode);
        containerJoinedRequest.setPREFIJO(containerJoined.getPrefijo());
        containerJoinedRequest.setNUMERO_DE_IDENTIFICACION(containerJoined.getNumero_de_identificacion());
        containerJoinedRequest.setGRABADOR(recorder);
        containerJoinedRequest.setFECHA_GRABACION(Tools.formatDate(new Date(), Key.datetimePattern.YYYYMMDD_HHMMSS_DASH_SEPARATOR));
        containerJoinedRequest.setGRABADOR_ACTUALIZA(0);
        containerJoinedRequest.setFECHA_ACTUALIZA((Tools.formatDate(new Date(), Key.datetimePattern.YYYYMMDD_HHMMSS_DASH_SEPARATOR)));
        containerJoinedRequest.setHANDHELD_ID(handheld);
        containerJoinedRequest.setID_TURNO(currentShift);
        containerJoinedRequest.setHANDHELD_ID_ACTUALIZA("");


        ResourceHandler.deleteContainerJoined(this, Key.requestCode.DELETE_CONTAINER_JOINED, containerJoinedRequest);

    }

    private void toggleLayout(boolean selected){
        if (selected){
            llJoined.setVisibility(View.GONE);
            llJoinedRelated.setVisibility(View.VISIBLE);
        } else {
            llJoined.setVisibility(View.VISIBLE);
            llJoinedRelated.setVisibility(View.GONE);
        }

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.dfcu_btn_back:
                toggleLayout(false);
                break;
        }
    }
;
    @Override
    public void onResponse(int requestCode, Object o, Object r) {
        switch (requestCode){
            case Key.requestCode.GET_CONTAINER_JOINED:
                if (o instanceof JsonObject){
                    lstContainerJoined.clear();
                    JsonObject response = (JsonObject) o;
                    List<ContainerJoined> temp = Tools.getListFromJson(response.getAsJsonArray(Key.jsonResponse.data).toString(), ContainerJoined.class);
                    List<ContainerJoined> lstFiltered = new ArrayList<>();
                    Multimap<Integer, ContainerJoined> grouped = Multimaps.index(temp,
                            new Function<ContainerJoined, Integer>() {
                                @NullableDecl
                                @Override
                                public Integer apply(@NullableDecl ContainerJoined input) {
                                    if (input != null)
                                        return input.getId_atado();
                                    return -1;
                                }
                            });

                    Iterator<Integer> keyIterator = grouped.asMap().keySet().iterator();

                    while(keyIterator.hasNext()){
                        int key = keyIterator.next();
                        ContainerJoined filtered = new ContainerJoined();
                        String related = "Prefijo - No. Identificacion\n";
                        Collection<ContainerJoined> lst = grouped.get(key);
                        for (ContainerJoined c : lst){
                            related += c.getPrefijo() + " - " +c.getNumero_de_identificacion() + "\n";
                        }

                        filtered.setId_atado(key);
                        filtered.setRelated(related);
                        lstFiltered.add(filtered);
                    }


                    lstContainerJoined.addAll(lstFiltered);

                    containerJoinerAdapter.notifyDataSetChanged();
                }
                break;
            case Key.requestCode.CONTAINER_JOIN_SELECTION:
                if (o instanceof ContainerJoined){
//                    doJoinedSelection((ContainerJoined) o);
                    this.containerJoinedSelected = (ContainerJoined) o;
                    doJoinedSelection();
                }
                break;
            case Key.requestCode.GET_CONTAINER_JOINED_BY_ID:
                if (o instanceof JsonObject){
                    lstContainerJoinedRelated.clear();
                    JsonObject response = (JsonObject) o;
                    lstContainerJoinedRelated.addAll(Tools.getListFromJson(response.getAsJsonArray(Key.jsonResponse.data).toString(), ContainerJoined.class));
                    containerJoineRelatedAdapter.notifyDataSetChanged();
                }
                break;
            case Key.requestCode.CONTAINER_JOIN_UNJOINED:
                if (o instanceof ContainerJoined){
                    doUnjoin((ContainerJoined) o);
                }
                break;

            case Key.requestCode.DELETE_CONTAINER_JOINED:
                if (o instanceof JsonObject){
                    JsonObject jObject = (JsonObject) o;
                    BaseResponse response = new Gson().fromJson(jObject.toString(), BaseResponse.class);

                    if (response.isResultado()){
                        doJoinedSelection();
                    }

                    AlertUtil.showAlertOk(response.isResultado() ? getString(R.string.all_text_success) : getString(R.string.all_text_error),
                            response.isResultado() ? "Exito Eliminado" : "Fallo Eliminar", getContext(), new AlertUtil.OnOkListener() {
                                @Override
                                public void onOk() {

                                }
                            }).setCancelable(false)
                            .show();
                }
                break;
        }
    }
}
