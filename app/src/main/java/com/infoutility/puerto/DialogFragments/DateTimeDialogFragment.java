package com.infoutility.puerto.DialogFragments;

import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.TimePicker;

import com.infoutility.puerto.Interfaces.Response;
import com.infoutility.puerto.Keys.Key;
import com.infoutility.puerto.R;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class DateTimeDialogFragment extends DialogFragment implements View.OnClickListener {
    public final static String TAG = DateTimeDialogFragment.class.getSimpleName();

    private DatePicker dpDate;
    private TimePicker tpTime;
    private Button btnCancel;
    private Button btnAccept;
    private static Response.OnResponse onResponse;

    public static DateTimeDialogFragment newInstance(Response.OnResponse response){
        onResponse = response;
        return new DateTimeDialogFragment();
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = requireActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.dialog_fragment_datetime, null);


        dpDate = view.findViewById(R.id.dfdt_dp_date);
        tpTime = view.findViewById(R.id.dfdt_tp_time);
        btnAccept = view.findViewById(R.id.dfdt_btn_accept);
        btnCancel = view.findViewById(R.id.dfdt_btn_cancel);

        tpTime.setIs24HourView(true);

        btnAccept.setOnClickListener(this);
        btnCancel.setOnClickListener(this);


        builder.setView(view)
                .setTitle(R.string.dfdt_title_selection);

        return builder.create();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.dfdt_btn_accept:
                onResponse.onResponse(Key.requestCode.DIALOG_DATETIME, getSelectedDateTime(), null);
                dismiss();
                break;
            case R.id.dfdt_btn_cancel:
                dismiss();
                break;
            default:
                break;
        }
    }


    private Date getSelectedDateTime(){
        Calendar selected = new GregorianCalendar(dpDate.getYear(),
                dpDate.getMonth(),
                dpDate.getDayOfMonth(),
                tpTime.getHour(),
                tpTime.getMinute());
        return selected.getTime();
    }
}
