package com.infoutility.puerto.Interfaces;

public class Response {
    public interface OnResponse<T>{
        void onResponse(int requestCode,T t,T r);
    }
}
