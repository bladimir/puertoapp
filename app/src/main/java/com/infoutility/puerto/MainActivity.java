package com.infoutility.puerto;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.View;
import android.support.v4.view.GravityCompat;
import android.support.v7.app.ActionBarDrawerToggle;
import android.view.MenuItem;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;

import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.widget.TextView;

import com.infoutility.puerto.Activities.LoginActivity;
import com.infoutility.puerto.Fragments.ContainerColdManagerFragment;
import com.infoutility.puerto.Fragments.ContainerShipFragment;
import com.infoutility.puerto.Fragments.ContainerYardManagementFragment;
import com.infoutility.puerto.Fragments.DateTimeFragment;
import com.infoutility.puerto.Fragments.ElectricPowerMeterFragmente;
import com.infoutility.puerto.Fragments.ElectricPowerMeterSearchFragment;
import com.infoutility.puerto.Fragments.ElectricPowerMeterVerifyFragment;
import com.infoutility.puerto.Keys.Key;
import com.infoutility.puerto.Utils.AlertUtil;
import com.infoutility.puerto.Utils.PreferencesManager;
import com.infoutility.puerto.Utils.Tools;

import java.util.Locale;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private TextView tvUsername;
    private TextView tvData;
    private Fragment currentFragment;
    private String currentTAG;
    private int selectedModule =  Key.modules.SELECT.id();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close){
            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                Log.d("DRAWER", "ABIERTO");
                if (currentTAG != null && currentTAG.equals(ContainerShipFragment.TAG))
                    updateCurrentTripLabel();
            }

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
                Log.d("DRAWER", "CERRADO");
            }
        };
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        navigationView.setNavigationItemSelectedListener(this);

        View navHeader = navigationView.getHeaderView(0);
        tvUsername = navHeader.findViewById(R.id.nhm_tv_username);
        tvData = navHeader.findViewById(R.id.nhm_tv_session_time);

        loadNavHeader();

//        int selectedModule = getIntent().getIntExtra(Key.extra.SELECTED_MODULE, Key.modules.SELECT.id());
        selectedModule = getIntent().getIntExtra(Key.extra.SELECTED_MODULE, Key.modules.SELECT.id());

        this.setGlobalDateTime();

        if (selectedModule == Key.modules.CONTAINER_LOAD.id()){
            currentFragment = new ContainerShipFragment();
            currentTAG = ContainerShipFragment.TAG;
            Tools.setFragment(getSupportFragmentManager(), R.id.fl_main_content, currentFragment, currentTAG);
        } else if (selectedModule == Key.modules.CONTAINER_YARD.id()){
            currentFragment = new ContainerYardManagementFragment();
            currentTAG = ContainerYardManagementFragment.TAG;
            Tools.setFragment(getSupportFragmentManager(), R.id.fl_main_content, currentFragment, currentTAG);
        } else if (selectedModule == Key.modules.ELECTRIC_POWER_METER.id()){

            if (PreferencesManager.getInstance().getString(Key.sharedPreferences.meter, null) != null){
                /*currentFragment = new ElectricPowerMeterFragmente();
                currentTAG = ElectricPowerMeterFragmente.TAG;
                Tools.setFragment(getSupportFragmentManager(), R.id.fl_main_content, currentFragment, currentTAG);*/
                currentFragment = new ElectricPowerMeterSearchFragment();
                currentTAG = ElectricPowerMeterSearchFragment.TAG;
                Tools.setFragment(getSupportFragmentManager(), R.id.fl_main_content, currentFragment, currentTAG);
            } else {
                currentFragment = new ElectricPowerMeterVerifyFragment();
                currentTAG = ElectricPowerMeterVerifyFragment.TAG;
                Tools.setFragment(getSupportFragmentManager(), R.id.fl_main_content, currentFragment, currentTAG);
            }

        } else if (selectedModule == Key.modules.ELECTRIC_POWER_CONNECTION.id()){
            currentFragment = new ContainerColdManagerFragment();
            currentTAG = ContainerColdManagerFragment.TAG;
            Tools.setFragment(getSupportFragmentManager(), R.id.fl_main_content, currentFragment, currentTAG);

        }
    }

    private void updateCurrentTripLabel(){

        Long viaje = PreferencesManager.getInstance().getLong(Key.sharedPreferences.trip, 0L);
        int modo = PreferencesManager.getInstance().getInt(Key.sharedPreferences.workMode, 0);
        String wMode = "";
        if (modo != 0){
            wMode = modo == Key.workMode.LOAD.id() ? Key.workMode.LOAD.description() : Key.workMode.UNLOAD.description();
        }

        tvData.setText(String.format(Locale.getDefault(), "Viaje: %s\nBarco: %s\nModo: %s",
                (viaje == 0 ? "" : String.valueOf(viaje)),
                PreferencesManager.getInstance().getString(Key.sharedPreferences.tripName, ""),
                wMode));
    }

    private void menuMeterHomeValidation(){
        if (selectedModule == Key.modules.ELECTRIC_POWER_METER.id())
            if (PreferencesManager.getInstance().getString(Key.sharedPreferences.meter, null) != null){
                currentFragment = new ElectricPowerMeterFragmente();
                currentTAG = ElectricPowerMeterFragmente.TAG;
            }
    }

    private void loadNavHeader(){
        this.tvUsername
                .setText(String.format(Locale.getDefault(), "%d\n%s",
                        PreferencesManager.getInstance().getInt(Key.sharedPreferences.recorder, 0),
                        PreferencesManager.getInstance().getString(Key.sharedPreferences.userName, "")));
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else if(getSupportFragmentManager().getBackStackEntryCount() == 1) {
            AlertUtil.showAlertConfirmation(getString(R.string.all_text_warning), "Desea salir de la aplicacion?", this, "Salir", new AlertUtil.OnOkListener() {
                @Override
                public void onOk() {
                        moveTaskToBack(true);
                        finish();
                }
            }, new AlertUtil.OnCancelListener() {
                @Override
                public void onCancel() {

                }
            }).setCancelable(false)
                    .show();
        }  else if(getSupportFragmentManager().getBackStackEntryCount() > 1) {
            getSupportFragmentManager().popBackStack();
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_home) {
            menuMeterHomeValidation();
            Tools.setFragment(getSupportFragmentManager(), R.id.fl_main_content, currentFragment, currentTAG);
        } else if (id == R.id.nav_logout) {
            AlertUtil.showAlertConfirmation(getString(R.string.all_text_warning),
                    getString(R.string.all_msg_logout),
                    this, getString(R.string.all_text_logout),
                    new AlertUtil.OnOkListener() {
                        @Override
                        public void onOk() {
                            doLogout();
                        }
                    }, new AlertUtil.OnCancelListener() {
                        @Override
                        public void onCancel() {
                            //do nothing
                        }
                    }).show();
        }

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }


    private void doLogout(){
        PreferencesManager.getInstance().invalidateSession();
        startActivity(new Intent(MainActivity.this, LoginActivity.class));
        finish();
    }

    private void setGlobalDateTime(){
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.fl_datetime_content, new DateTimeFragment());
        fragmentTransaction.commit();
    }


}
