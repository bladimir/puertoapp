package com.infoutility.puerto.Models;

public class YardMap {
    private String PREFIJO;
    private String NUMERO_DE_IDENTIFICACION;
    private String PATIO;
    private int FILA_O_CARRIL;
    private int MODULO;
    private int NIVEL;
    private String FECHA;

    public YardMap(String PREFIJO, String NUMERO_DE_IDENTIFICACION, String PATIO, int FILA_O_CARRIL, int MODULO, int NIVEL, String FECHA) {
        this.PREFIJO = PREFIJO;
        this.NUMERO_DE_IDENTIFICACION = NUMERO_DE_IDENTIFICACION;
        this.PATIO = PATIO;
        this.FILA_O_CARRIL = FILA_O_CARRIL;
        this.MODULO = MODULO;
        this.NIVEL = NIVEL;
        this.FECHA = FECHA;
    }

    public YardMap() {
    }

    public String getPREFIJO() {
        return PREFIJO;
    }

    public void setPREFIJO(String PREFIJO) {
        this.PREFIJO = PREFIJO;
    }

    public String getNUMERO_DE_IDENTIFICACION() {
        return NUMERO_DE_IDENTIFICACION;
    }

    public void setNUMERO_DE_IDENTIFICACION(String NUMERO_DE_IDENTIFICACION) {
        this.NUMERO_DE_IDENTIFICACION = NUMERO_DE_IDENTIFICACION;
    }

    public String getPATIO() {
        return PATIO;
    }

    public void setPATIO(String PATIO) {
        this.PATIO = PATIO;
    }

    public int getFILA_O_CARRIL() {
        return FILA_O_CARRIL;
    }

    public void setFILA_O_CARRIL(int FILA_O_CARRIL) {
        this.FILA_O_CARRIL = FILA_O_CARRIL;
    }

    public int getMODULO() {
        return MODULO;
    }

    public void setMODULO(int MODULO) {
        this.MODULO = MODULO;
    }

    public int getNIVEL() {
        return NIVEL;
    }

    public void setNIVEL(int NIVEL) {
        this.NIVEL = NIVEL;
    }

    public String getFECHA() {
        return FECHA;
    }

    public void setFECHA(String FECHA) {
        this.FECHA = FECHA;
    }
}
