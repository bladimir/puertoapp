package com.infoutility.puerto.Models;

public class ContainerPending {
    private String REFERENCIA;
    private String TIPO_MOVIMIENTO;
    private String PREFIJO;
    private String NUMERO_IDENTIFIACION;
    private long USUARIO_GRABADOR;
    private String FECHA_GRABACION;
    private String PARAMETROS;

    public ContainerPending(String REFERENCIA, String TIPO_MOVIMIENTO, String PREFIJO, String NUMERO_IDENTIFIACION, long USUARIO_GRABADOR, String FECHA_GRABACION, String PARAMETROS) {
        this.REFERENCIA = REFERENCIA;
        this.TIPO_MOVIMIENTO = TIPO_MOVIMIENTO;
        this.PREFIJO = PREFIJO;
        this.NUMERO_IDENTIFIACION = NUMERO_IDENTIFIACION;
        this.USUARIO_GRABADOR = USUARIO_GRABADOR;
        this.FECHA_GRABACION = FECHA_GRABACION;
        this.PARAMETROS = PARAMETROS;
    }

    public ContainerPending() {
    }

    public String getREFERENCIA() {
        return REFERENCIA;
    }

    public void setREFERENCIA(String REFERENCIA) {
        this.REFERENCIA = REFERENCIA;
    }

    public String getTIPO_MOVIMIENTO() {
        return TIPO_MOVIMIENTO;
    }

    public void setTIPO_MOVIMIENTO(String TIPO_MOVIMIENTO) {
        this.TIPO_MOVIMIENTO = TIPO_MOVIMIENTO;
    }

    public String getPREFIJO() {
        return PREFIJO;
    }

    public void setPREFIJO(String PREFIJO) {
        this.PREFIJO = PREFIJO;
    }

    public String getNUMERO_IDENTIFIACION() {
        return NUMERO_IDENTIFIACION;
    }

    public void setNUMERO_IDENTIFIACION(String NUMERO_IDENTIFIACION) {
        this.NUMERO_IDENTIFIACION = NUMERO_IDENTIFIACION;
    }

    public long getUSUARIO_GRABADOR() {
        return USUARIO_GRABADOR;
    }

    public void setUSUARIO_GRABADOR(long USUARIO_GRABADOR) {
        this.USUARIO_GRABADOR = USUARIO_GRABADOR;
    }

    public String getFECHA_GRABACION() {
        return FECHA_GRABACION;
    }

    public void setFECHA_GRABACION(String FECHA_GRABACION) {
        this.FECHA_GRABACION = FECHA_GRABACION;
    }

    public String getPARAMETROS() {
        return PARAMETROS;
    }

    public void setPARAMETROS(String PARAMETROS) {
        this.PARAMETROS = PARAMETROS;
    }
}
