package com.infoutility.puerto.Models;

public class ConsumptionWrp {
    private int id;
    private Consumption consumption;

    public ConsumptionWrp(int id, Consumption consumption) {
        this.id = id;
        this.consumption = consumption;
    }

    public ConsumptionWrp() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Consumption getConsumption() {
        return consumption;
    }

    public void setConsumption(Consumption consumption) {
        this.consumption = consumption;
    }
}
