package com.infoutility.puerto.Models;

public class WorkMode {
    private int id_mod_laboral;
    private String nombre;

    public WorkMode(int id_mod_laboral, String nombre) {
        this.id_mod_laboral = id_mod_laboral;
        this.nombre = nombre;
    }

    public int getId_mod_laboral() {
        return id_mod_laboral;
    }

    public void setId_mod_laboral(int id_mod_laboral) {
        this.id_mod_laboral = id_mod_laboral;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
}
