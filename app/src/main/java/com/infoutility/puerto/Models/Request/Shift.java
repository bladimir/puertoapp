package com.infoutility.puerto.Models.Request;

public class Shift {
    private long ID_TURNO;
    private String FECHA_HORA_INICIO;
    private String FECHA_HORA_FINAL;
    private String OBSERVACIONES;
    private int GRABADOR;
    private String FECHA_GRABACION;
    private int ID_TIPO_TURNO;
    private String VIAJE_NO;
    private int TIPO_DE_MOVIMIENTO;
    private String HANDHELD_ID;
    private String ID_CUADRILLA = "1";
    private int ACTUALIZACION;

    public Shift(long ID_TURNO, String FECHA_HORA_INICIO, String FECHA_HORA_FINAL, String OBSERVACIONES, int GRABADOR, String FECHA_GRABACION, int ID_TIPO_TURNO, String VIAJE_NO, int TIPO_DE_MOVIMIENTO, String HANDHELD_ID, String ID_CUADRILLA, int ACTUALIZACION) {
        this.ID_TURNO = ID_TURNO;
        this.FECHA_HORA_INICIO = FECHA_HORA_INICIO;
        this.FECHA_HORA_FINAL = FECHA_HORA_FINAL;
        this.OBSERVACIONES = OBSERVACIONES;
        this.GRABADOR = GRABADOR;
        this.FECHA_GRABACION = FECHA_GRABACION;
        this.ID_TIPO_TURNO = ID_TIPO_TURNO;
        this.VIAJE_NO = VIAJE_NO;
        this.TIPO_DE_MOVIMIENTO = TIPO_DE_MOVIMIENTO;
        this.HANDHELD_ID = HANDHELD_ID;
        this.ID_CUADRILLA = ID_CUADRILLA;
        this.ACTUALIZACION = ACTUALIZACION;
    }

    public Shift() {
    }

    public long getID_TURNO() {
        return ID_TURNO;
    }

    public void setID_TURNO(long ID_TURNO) {
        this.ID_TURNO = ID_TURNO;
    }

    public String getFECHA_HORA_INICIO() {
        return FECHA_HORA_INICIO;
    }

    public void setFECHA_HORA_INICIO(String FECHA_HORA_INICIO) {
        this.FECHA_HORA_INICIO = FECHA_HORA_INICIO;
    }

    public String getFECHA_HORA_FINAL() {
        return FECHA_HORA_FINAL;
    }

    public void setFECHA_HORA_FINAL(String FECHA_HORA_FINAL) {
        this.FECHA_HORA_FINAL = FECHA_HORA_FINAL;
    }

    public String getOBSERVACIONES() {
        return OBSERVACIONES;
    }

    public void setOBSERVACIONES(String OBSERVACIONES) {
        this.OBSERVACIONES = OBSERVACIONES;
    }

    public int getGRABADOR() {
        return GRABADOR;
    }

    public void setGRABADOR(int GRABADOR) {
        this.GRABADOR = GRABADOR;
    }

    public String getFECHA_GRABACION() {
        return FECHA_GRABACION;
    }

    public void setFECHA_GRABACION(String FECHA_GRABACION) {
        this.FECHA_GRABACION = FECHA_GRABACION;
    }

    public int getID_TIPO_TURNO() {
        return ID_TIPO_TURNO;
    }

    public void setID_TIPO_TURNO(int ID_TIPO_TURNO) {
        this.ID_TIPO_TURNO = ID_TIPO_TURNO;
    }

    public String getVIAJE_NO() {
        return VIAJE_NO;
    }

    public void setVIAJE_NO(String VIAJE_NO) {
        this.VIAJE_NO = VIAJE_NO;
    }

    public int getTIPO_DE_MOVIMIENTO() {
        return TIPO_DE_MOVIMIENTO;
    }

    public void setTIPO_DE_MOVIMIENTO(int TIPO_DE_MOVIMIENTO) {
        this.TIPO_DE_MOVIMIENTO = TIPO_DE_MOVIMIENTO;
    }

    public String getHANDHELD_ID() {
        return HANDHELD_ID;
    }

    public void setHANDHELD_ID(String HANDHELD_ID) {
        this.HANDHELD_ID = HANDHELD_ID;
    }

    public String getID_CUADRILLA() {
        return ID_CUADRILLA;
    }

    public void setID_CUADRILLA(String ID_CUADRILLA) {
        this.ID_CUADRILLA = ID_CUADRILLA;
    }

    public int getACTUALIZACION() {
        return ACTUALIZACION;
    }

    public void setACTUALIZACION(int ACTUALIZACION) {
        this.ACTUALIZACION = ACTUALIZACION;
    }
}
