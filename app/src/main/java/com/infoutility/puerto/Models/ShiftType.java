package com.infoutility.puerto.Models;

public class ShiftType {
    private int id_tipo_turno;
    private String nombre;

    public int getId_tipo_turno() {
        return id_tipo_turno;
    }

    public void setId_tipo_turno(int id_tipo_turno) {
        this.id_tipo_turno = id_tipo_turno;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
}
