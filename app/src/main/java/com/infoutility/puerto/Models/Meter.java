package com.infoutility.puerto.Models;

public class Meter {
    private String tipo;
    private String ficha;
    private String nombre;

    public Meter(String tipo, String ficha, String nombre) {
        this.tipo = tipo;
        this.ficha = ficha;
        this.nombre = nombre;
    }

    public Meter() {
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getFicha() {
        return ficha;
    }

    public void setFicha(String ficha) {
        this.ficha = ficha;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
}
