package com.infoutility.puerto.Models.Request;

public class ContainerYard {
    private long VIAJE_NO;
    private int TIPO_DE_MOVIMIENTO;
    private int ORDEN;
    private String PREFIJO;
    private String NUMERO_DE_IDENTIFICACION;
    private String PATIO;
    private int FILA;
    private int MODULO;
    private int NIVEL;
    private long MAQUINA;
    private int MEDIDA;
    private long GRABADOR_IMPOR_PATIO;
    private String FECHAHORA_IMPOR_PATIO;

    public long getVIAJE_NO() {
        return VIAJE_NO;
    }

    public void setVIAJE_NO(long VIAJE_NO) {
        this.VIAJE_NO = VIAJE_NO;
    }

    public int getTIPO_DE_MOVIMIENTO() {
        return TIPO_DE_MOVIMIENTO;
    }

    public void setTIPO_DE_MOVIMIENTO(int TIPO_DE_MOVIMIENTO) {
        this.TIPO_DE_MOVIMIENTO = TIPO_DE_MOVIMIENTO;
    }

    public int getORDEN() {
        return ORDEN;
    }

    public void setORDEN(int ORDEN) {
        this.ORDEN = ORDEN;
    }

    public String getPREFIJO() {
        return PREFIJO;
    }

    public void setPREFIJO(String PREFIJO) {
        this.PREFIJO = PREFIJO;
    }

    public String getNUMERO_DE_IDENTIFICACION() {
        return NUMERO_DE_IDENTIFICACION;
    }

    public void setNUMERO_DE_IDENTIFICACION(String NUMERO_DE_IDENTIFICACION) {
        this.NUMERO_DE_IDENTIFICACION = NUMERO_DE_IDENTIFICACION;
    }

    public String getPATIO() {
        return PATIO;
    }

    public void setPATIO(String PATIO) {
        this.PATIO = PATIO;
    }

    public int getFILA() {
        return FILA;
    }

    public void setFILA(int FILA) {
        this.FILA = FILA;
    }

    public int getMODULO() {
        return MODULO;
    }

    public void setMODULO(int MODULO) {
        this.MODULO = MODULO;
    }

    public int getNIVEL() {
        return NIVEL;
    }

    public void setNIVEL(int NIVEL) {
        this.NIVEL = NIVEL;
    }

    public long getMAQUINA() {
        return MAQUINA;
    }

    public void setMAQUINA(long MAQUINA) {
        this.MAQUINA = MAQUINA;
    }

    public int getMEDIDA() {
        return MEDIDA;
    }

    public void setMEDIDA(int MEDIDA) {
        this.MEDIDA = MEDIDA;
    }

    public long getGRABADOR_IMPOR_PATIO() {
        return GRABADOR_IMPOR_PATIO;
    }

    public void setGRABADOR_IMPOR_PATIO(long GRABADOR_IMPOR_PATIO) {
        this.GRABADOR_IMPOR_PATIO = GRABADOR_IMPOR_PATIO;
    }

    public String getFECHAHORA_IMPOR_PATIO() {
        return FECHAHORA_IMPOR_PATIO;
    }

    public void setFECHAHORA_IMPOR_PATIO(String FECHAHORA_IMPOR_PATIO) {
        this.FECHAHORA_IMPOR_PATIO = FECHAHORA_IMPOR_PATIO;
    }

}
