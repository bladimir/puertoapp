package com.infoutility.puerto.Models.Request;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class RelocationContainer {


    int ano;
    int medida;
    String c_o_f;
    String numero_de_identificacion;
    String prefijo;
    String patio_a;
    int fila_a;
    int modulo_a;
    int nivel_a;
    String condicion;
    String fecha_hora_ubicador;
    String patio;
    int fila;
    int modulo;
    int nivel;
    int orden;
    String num_reporte;

    public RelocationContainer(int medida, String c_o_f, String numero_de_identificacion, String prefijo, String patio_a, int fila_a, int modulo_a, int nivel_a, String condicion, String patio, int fila, int modulo, int nivel, int orden, String num_reporte) {
        this.medida = medida;
        this.c_o_f = c_o_f;
        this.numero_de_identificacion = numero_de_identificacion;
        this.prefijo = prefijo;
        this.patio_a = patio_a;
        this.fila_a = fila_a;
        this.modulo_a = modulo_a;
        this.nivel_a = nivel_a;
        this.condicion = condicion;
        this.patio = patio;
        this.fila = fila;
        this.modulo = modulo;
        this.nivel = nivel;
        this.orden = orden;
        this.num_reporte = num_reporte;
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
        SimpleDateFormat dateYear = new SimpleDateFormat("yyyy", Locale.getDefault());
        Date date = new Date();
        String fecha = dateFormat.format(date);
        this.fecha_hora_ubicador = fecha;
        this.ano = Integer.valueOf(dateYear.format(date));

    }

    public int getAno() {
        return ano;
    }

    public void setAno(int ano) {
        this.ano = ano;
    }

    public int getMedida() {
        return medida;
    }

    public void setMedida(int medida) {
        this.medida = medida;
    }

    public String getC_o_f() {
        return c_o_f;
    }

    public void setC_o_f(String c_o_f) {
        this.c_o_f = c_o_f;
    }

    public String getNumero_de_identificacion() {
        return numero_de_identificacion;
    }

    public void setNumero_de_identificacion(String numero_de_identificacion) {
        this.numero_de_identificacion = numero_de_identificacion;
    }

    public String getPrefijo() {
        return prefijo;
    }

    public void setPrefijo(String prefijo) {
        this.prefijo = prefijo;
    }

    public String getPatio_a() {
        return patio_a;
    }

    public void setPatio_a(String patio_a) {
        this.patio_a = patio_a;
    }

    public int getFila_a() {
        return fila_a;
    }

    public void setFila_a(int fila_a) {
        this.fila_a = fila_a;
    }

    public int getModulo_a() {
        return modulo_a;
    }

    public void setModulo_a(int modulo_a) {
        this.modulo_a = modulo_a;
    }

    public int getNivel_a() {
        return nivel_a;
    }

    public void setNivel_a(int nivel_a) {
        this.nivel_a = nivel_a;
    }

    public String getCondicion() {
        return condicion;
    }

    public void setCondicion(String condicion) {
        this.condicion = condicion;
    }

    public String getFecha_hora_ubicador() {
        return fecha_hora_ubicador;
    }

    public void setFecha_hora_ubicador(String fecha_hora_ubicador) {
        this.fecha_hora_ubicador = fecha_hora_ubicador;
    }

    public String getPatio() {
        return patio;
    }

    public void setPatio(String patio) {
        this.patio = patio;
    }

    public int getFila() {
        return fila;
    }

    public void setFila(int fila) {
        this.fila = fila;
    }

    public int getModulo() {
        return modulo;
    }

    public void setModulo(int modulo) {
        this.modulo = modulo;
    }

    public int getNivel() {
        return nivel;
    }

    public void setNivel(int nivel) {
        this.nivel = nivel;
    }

    public int getOrden() {
        return orden;
    }

    public void setOrden(int orden) {
        this.orden = orden;
    }

    public String getNum_reporte() {
        return num_reporte;
    }

    public void setNum_reporte(String num_reporte) {
        this.num_reporte = num_reporte;
    }
}
