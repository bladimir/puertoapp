package com.infoutility.puerto.Models;

public class Activity {
    private int id_actividad;
    private String nombre;

    public Activity(int id_actividad, String nombre) {
        this.id_actividad = id_actividad;
        this.nombre = nombre;
    }

    public int getId_actividad() {
        return id_actividad;
    }

    public void setId_actividad(int id_actividad) {
        this.id_actividad = id_actividad;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
}
