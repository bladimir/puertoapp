package com.infoutility.puerto.Models;

public class MoveType {
    private String id_movimiento;
    private String descripcion;
    private String tipo_movimiento;

    public MoveType(String id_movimiento, String descripcion, String tipo_movimiento) {
        this.id_movimiento = id_movimiento;
        this.descripcion = descripcion;
        this.tipo_movimiento = tipo_movimiento;
    }

    public String getId_movimiento() {
        return id_movimiento;
    }

    public void setId_movimiento(String id_movimiento) {
        this.id_movimiento = id_movimiento;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getTipo_movimiento() {
        return tipo_movimiento;
    }

    public void setTipo_movimiento(String tipo_movimiento) {
        this.tipo_movimiento = tipo_movimiento;
    }
}
