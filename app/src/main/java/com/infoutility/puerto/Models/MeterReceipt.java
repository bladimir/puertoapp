package com.infoutility.puerto.Models;

import org.parceler.Parcel;

@Parcel(Parcel.Serialization.BEAN)
public class MeterReceipt {
    private String noContador;
    private String nit;
    private String nombre;
    private String consumoAnterior;
    private String lecturaAnterior;
    private String lecturaActual;
    private String consumo;
    private String total;
    private String codigo;
    private int anioAnterior;
    private int mesAnterior;

    public MeterReceipt(String noContador, String nit, String nombre, String consumoAnterior, String lecturaAnterior, String lecturaActual, String consumo, String total, String codigo, int anioAnterior, int mesAnterior) {
        this.noContador = noContador;
        this.nit = nit;
        this.nombre = nombre;
        this.consumoAnterior = consumoAnterior;
        this.lecturaAnterior = lecturaAnterior;
        this.lecturaActual = lecturaActual;
        this.consumo = consumo;
        this.total = total;
        this.codigo = codigo;
        this.anioAnterior = anioAnterior;
        this.mesAnterior = mesAnterior;
    }

    public MeterReceipt() {
    }

    public String getNoContador() {
        return noContador;
    }

    public void setNoContador(String noContador) {
        this.noContador = noContador;
    }

    public String getNit() {
        return nit;
    }

    public void setNit(String nit) {
        this.nit = nit;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getConsumoAnterior() {
        return consumoAnterior;
    }

    public void setConsumoAnterior(String consumoAnterior) {
        this.consumoAnterior = consumoAnterior;
    }

    public String getLecturaAnterior() {
        return lecturaAnterior;
    }

    public void setLecturaAnterior(String lecturaAnterior) {
        this.lecturaAnterior = lecturaAnterior;
    }

    public String getLecturaActual() {
        return lecturaActual;
    }

    public void setLecturaActual(String lecturaActual) {
        this.lecturaActual = lecturaActual;
    }

    public String getConsumo() {
        return consumo;
    }

    public void setConsumo(String consumo) {
        this.consumo = consumo;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public int getAnioAnterior() {
        return anioAnterior;
    }

    public void setAnioAnterior(int anioAnterior) {
        this.anioAnterior = anioAnterior;
    }

    public int getMesAnterior() {
        return mesAnterior;
    }

    public void setMesAnterior(int mesAnterior) {
        this.mesAnterior = mesAnterior;
    }
}
