package com.infoutility.puerto.Models;

public class ListConfirmed {
    private int trabajados;
    private String total;

    public int getTrabajados() {
        return trabajados;
    }

    public void setTrabajados(int trabajados) {
        this.trabajados = trabajados;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }
}
