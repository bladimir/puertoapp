package com.infoutility.puerto.Models;

import org.parceler.Parcel;

@Parcel(Parcel.Serialization.BEAN)
public class Container {
    private long viaje_no;
    private int atado;
    private int tipo_de_movimiento;
    private String c_o_f;
    private String numero_de_identificacion;
    private double peso_manifestado;
    private String prefijo;
    private String confirmado;
    private int orden;
    private String condicion;
    private int medida;

    public Container(long viaje_no, int atado, int tipo_de_movimiento, String c_o_f, String numero_de_identificacion, double peso_manifestado, String prefijo, String confirmado, int orden, String condicion, int medida) {
        this.viaje_no = viaje_no;
        this.atado = atado;
        this.tipo_de_movimiento = tipo_de_movimiento;
        this.c_o_f = c_o_f;
        this.numero_de_identificacion = numero_de_identificacion;
        this.peso_manifestado = peso_manifestado;
        this.prefijo = prefijo;
        this.confirmado = confirmado;
        this.orden = orden;
        this.condicion = condicion;
        this.medida = medida;
    }

    public Container() {
    }

    public long getViaje_no() {
        return viaje_no;
    }

    public void setViaje_no(long viaje_no) {
        this.viaje_no = viaje_no;
    }

    public int getTipo_de_movimiento() {
        return tipo_de_movimiento;
    }

    public void setTipo_de_movimiento(int tipo_de_movimiento) {
        this.tipo_de_movimiento = tipo_de_movimiento;
    }

    public String getC_o_f() {
        return c_o_f;
    }

    public void setC_o_f(String c_o_f) {
        this.c_o_f = c_o_f;
    }

    public String getNumero_de_identificacion() {
        return numero_de_identificacion;
    }

    public void setNumero_de_identificacion(String numero_de_identificacion) {
        this.numero_de_identificacion = numero_de_identificacion;
    }

    public double getPeso_manifestado() {
        return peso_manifestado;
    }

    public void setPeso_manifestado(double peso_manifestado) {
        this.peso_manifestado = peso_manifestado;
    }

    public String getPrefijo() {
        return prefijo;
    }

    public void setPrefijo(String prefijo) {
        this.prefijo = prefijo;
    }

    public String getConfirmado() {
        return confirmado;
    }

    public void setConfirmado(String confirmado) {
        this.confirmado = confirmado;
    }

    public int getOrden() {
        return orden;
    }

    public void setOrden(int orden) {
        this.orden = orden;
    }

    public String getCondicion() {
        return condicion;
    }

    public void setCondicion(String condicion) {
        this.condicion = condicion;
    }

    public int getAtado() {
        return atado;
    }

    public void setAtado(int atado) {
        this.atado = atado;
    }

    public int getMedida() {
        return medida;
    }

    public void setMedida(int medida) {
        this.medida = medida;
    }
}
