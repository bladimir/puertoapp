package com.infoutility.puerto.Models;

public class ConsumptionHistory {
    private String fecha_grabacion;
    private String grabador;
    private int mus_anio;
    private int mus_consumo;
    private String mus_lector;
    private long mus_lectura;
    private long mus_lectura_anterior;
    private int mus_mes;
    private String mus_observaciones;
    private long mus_uel_codigo;
    private long mus_uel_contador;

    public ConsumptionHistory(String fecha_grabacion, String grabador, int mus_anio, int mus_consumo, String mus_lector, long mus_lectura, long mus_lectura_anterior, int mus_mes, String mus_observaciones, long mus_uel_codigo, long mus_uel_contador) {
        this.fecha_grabacion = fecha_grabacion;
        this.grabador = grabador;
        this.mus_anio = mus_anio;
        this.mus_consumo = mus_consumo;
        this.mus_lector = mus_lector;
        this.mus_lectura = mus_lectura;
        this.mus_lectura_anterior = mus_lectura_anterior;
        this.mus_mes = mus_mes;
        this.mus_observaciones = mus_observaciones;
        this.mus_uel_codigo = mus_uel_codigo;
        this.mus_uel_contador = mus_uel_contador;
    }


    public ConsumptionHistory() {
    }

    public String getFecha_grabacion() {
        return fecha_grabacion;
    }

    public void setFecha_grabacion(String fecha_grabacion) {
        this.fecha_grabacion = fecha_grabacion;
    }

    public String getGrabador() {
        return grabador;
    }

    public void setGrabador(String grabador) {
        this.grabador = grabador;
    }

    public int getMus_anio() {
        return mus_anio;
    }

    public void setMus_anio(int mus_anio) {
        this.mus_anio = mus_anio;
    }

    public int getMus_consumo() {
        return mus_consumo;
    }

    public void setMus_consumo(int mus_consumo) {
        this.mus_consumo = mus_consumo;
    }

    public String getMus_lector() {
        return mus_lector;
    }

    public void setMus_lector(String mus_lector) {
        this.mus_lector = mus_lector;
    }

    public long getMus_lectura() {
        return mus_lectura;
    }

    public void setMus_lectura(long mus_lectura) {
        this.mus_lectura = mus_lectura;
    }

    public long getMus_lectura_anterior() {
        return mus_lectura_anterior;
    }

    public void setMus_lectura_anterior(long mus_lectura_anterior) {
        this.mus_lectura_anterior = mus_lectura_anterior;
    }

    public int getMus_mes() {
        return mus_mes;
    }

    public void setMus_mes(int mus_mes) {
        this.mus_mes = mus_mes;
    }

    public String getMus_observaciones() {
        return mus_observaciones;
    }

    public void setMus_observaciones(String mus_observaciones) {
        this.mus_observaciones = mus_observaciones;
    }

    public long getMus_uel_codigo() {
        return mus_uel_codigo;
    }

    public void setMus_uel_codigo(long mus_uel_codigo) {
        this.mus_uel_codigo = mus_uel_codigo;
    }

    public long getMus_uel_contador() {
        return mus_uel_contador;
    }

    public void setMus_uel_contador(long mus_uel_contador) {
        this.mus_uel_contador = mus_uel_contador;
    }
}
