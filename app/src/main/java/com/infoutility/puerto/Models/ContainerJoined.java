package com.infoutility.puerto.Models;

import java.util.List;

public class ContainerJoined {

    private int id_atado;
    private long viaje_no;
    private int tipo_de_movimiento;
    private String numero_de_identificacion;
    private String prefijo;
    //
    private String related;

    public ContainerJoined(int id_atado, long viaje_no, int tipo_de_movimiento, String numero_de_identificacion, String prefijo) {
        this.id_atado = id_atado;
        this.viaje_no = viaje_no;
        this.tipo_de_movimiento = tipo_de_movimiento;
        this.numero_de_identificacion = numero_de_identificacion;
        this.prefijo = prefijo;
    }

    public ContainerJoined() {
    }

    public int getId_atado() {
        return id_atado;
    }

    public void setId_atado(int id_atado) {
        this.id_atado = id_atado;
    }

    public long getViaje_no() {
        return viaje_no;
    }

    public void setViaje_no(long viaje_no) {
        this.viaje_no = viaje_no;
    }

    public int getTipo_de_movimiento() {
        return tipo_de_movimiento;
    }

    public void setTipo_de_movimiento(int tipo_de_movimiento) {
        this.tipo_de_movimiento = tipo_de_movimiento;
    }

    public String getNumero_de_identificacion() {
        return numero_de_identificacion;
    }

    public void setNumero_de_identificacion(String numero_de_identificacion) {
        this.numero_de_identificacion = numero_de_identificacion;
    }

    public String getPrefijo() {
        return prefijo;
    }

    public void setPrefijo(String prefijo) {
        this.prefijo = prefijo;
    }

    public String getRelated() {
        return related;
    }

    public void setRelated(String related) {
        this.related = related;
    }
}
