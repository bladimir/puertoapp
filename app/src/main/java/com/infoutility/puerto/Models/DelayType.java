package com.infoutility.puerto.Models;

public class DelayType {
    private String nombre_demora;
    private String descripcion;
    private int demora;
    private String tipo_demora;

    public DelayType(String nombre_demora, String descripcion, int demora, String tipo_demora) {
        this.nombre_demora = nombre_demora;
        this.descripcion = descripcion;
        this.demora = demora;
        this.tipo_demora = tipo_demora;
    }

    public String getNombre_demora() {
        return nombre_demora;
    }

    public void setNombre_demora(String nombre_demora) {
        this.nombre_demora = nombre_demora;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public int getDemora() {
        return demora;
    }

    public void setDemora(int demora) {
        this.demora = demora;
    }

    public String getTipo_demora() {
        return tipo_demora;
    }

    public void setTipo_demora(String tipo_demora) {
        this.tipo_demora = tipo_demora;
    }
}
