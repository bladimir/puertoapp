package com.infoutility.puerto.Models;

public class Machine {
    private String descripcion;
    private String tipodescripcion;
    private String estado;
    private String estatus;
    private String de_baja;
    private int tipo_de_maquina;
    private long maquina;

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getTipodescripcion() {
        return tipodescripcion;
    }

    public void setTipodescripcion(String tipodescripcion) {
        this.tipodescripcion = tipodescripcion;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getEstatus() {
        return estatus;
    }

    public void setEstatus(String estatus) {
        this.estatus = estatus;
    }

    public String getDe_baja() {
        return de_baja;
    }

    public void setDe_baja(String de_baja) {
        this.de_baja = de_baja;
    }

    public int getTipo_de_maquina() {
        return tipo_de_maquina;
    }

    public void setTipo_de_maquina(int tipo_de_maquina) {
        this.tipo_de_maquina = tipo_de_maquina;
    }

    public long getMaquina() {
        return maquina;
    }

    public void setMaquina(long maquina) {
        this.maquina = maquina;
    }
}
