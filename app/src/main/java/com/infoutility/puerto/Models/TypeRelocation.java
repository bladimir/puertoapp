package com.infoutility.puerto.Models;

public class TypeRelocation {

    String descripcion;
    String cod_movimiento;

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getCod_movimiento() {
        return cod_movimiento;
    }

    public void setCod_movimiento(String cod_movimiento) {
        this.cod_movimiento = cod_movimiento;
    }
}
