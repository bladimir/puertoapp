package com.infoutility.puerto.Models.Request;

public class Delay {
    private int num_rep_demora;
    private int demora;
    private int pers_persona;
    private String inicio;
    private long viaje;
    private String fin;
    private String grabador;
    private String handheld_id;
    private long turno;
    private String tipo_movimiento;
    private String tipo_carga;
    private String fecha_grabacion;
    private String DESCRIPCION;

    public int getNum_rep_demora() {
        return num_rep_demora;
    }

    public void setNum_rep_demora(int num_rep_demora) {
        this.num_rep_demora = num_rep_demora;
    }

    public int getDemora() {
        return demora;
    }

    public void setDemora(int demora) {
        this.demora = demora;
    }

    public int getPers_persona() {
        return pers_persona;
    }

    public void setPers_persona(int pers_persona) {
        this.pers_persona = pers_persona;
    }

    public String getInicio() {
        return inicio;
    }

    public void setInicio(String inicio) {
        this.inicio = inicio;
    }

    public long getViaje() {
        return viaje;
    }

    public void setViaje(long viaje) {
        this.viaje = viaje;
    }

    public String getFin() {
        return fin;
    }

    public void setFin(String fin) {
        this.fin = fin;
    }

    public String getGrabador() {
        return grabador;
    }

    public void setGrabador(String grabador) {
        this.grabador = grabador;
    }

    public String getHandheld_id() {
        return handheld_id;
    }

    public void setHandheld_id(String handheld_id) {
        this.handheld_id = handheld_id;
    }

    public long getTurno() {
        return turno;
    }

    public void setTurno(long turno) {
        this.turno = turno;
    }

    public String getTipo_movimiento() {
        return tipo_movimiento;
    }

    public void setTipo_movimiento(String tipo_movimiento) {
        this.tipo_movimiento = tipo_movimiento;
    }

    public String getTipo_carga() {
        return tipo_carga;
    }

    public void setTipo_carga(String tipo_carga) {
        this.tipo_carga = tipo_carga;
    }

    public String getFecha_grabacion() {
        return fecha_grabacion;
    }

    public void setFecha_grabacion(String fecha_grabacion) {
        this.fecha_grabacion = fecha_grabacion;
    }

    public String getDESCRIPCION() {
        return DESCRIPCION;
    }

    public void setDESCRIPCION(String DESCRIPCION) {
        this.DESCRIPCION = DESCRIPCION;
    }
}
