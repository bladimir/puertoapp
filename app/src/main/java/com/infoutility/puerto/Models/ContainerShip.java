package com.infoutility.puerto.Models;

import org.parceler.Parcel;

@Parcel(Parcel.Serialization.BEAN)
public class ContainerShip {
    private int situacion;
    private String fecha_hora;
    private String usuario;
    private long viaje_empornac;
    private String barco;
    private String viaje_naviera;
    private String situaciondescripcion;

    public int getSituacion() {
        return situacion;
    }

    public void setSituacion(int situacion) {
        this.situacion = situacion;
    }

    public String getFecha_hora() {
        return fecha_hora;
    }

    public void setFecha_hora(String fecha_hora) {
        this.fecha_hora = fecha_hora;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public long getViaje_empornac() {
        return viaje_empornac;
    }

    public void setViaje_empornac(long viaje_empornac) {
        this.viaje_empornac = viaje_empornac;
    }

    public String getBarco() {
        return barco;
    }

    public void setBarco(String barco) {
        this.barco = barco;
    }

    public String getViaje_naviera() {
        return viaje_naviera;
    }

    public void setViaje_naviera(String viaje_naviera) {
        this.viaje_naviera = viaje_naviera;
    }

    public String getSituaciondescripcion() {
        return situaciondescripcion;
    }

    public void setSituaciondescripcion(String situaciondescripcion) {
        this.situaciondescripcion = situaciondescripcion;
    }
}
