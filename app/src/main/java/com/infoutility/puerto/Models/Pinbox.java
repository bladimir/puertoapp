package com.infoutility.puerto.Models;

public class Pinbox {
    private long viaje_turno;
    private int medida;
    private String grua;
    private int correlativo;
    private String grabador;
    private String handheld_id;
    private int id_movimiento;
    private long viaje_empornac;
    private int tipo_pinbox;
    private int tipo_movimiento;
    private long id_turno;
    private String fecha_grabacion;

    public long getViaje_turno() {
        return viaje_turno;
    }

    public void setViaje_turno(long viaje_turno) {
        this.viaje_turno = viaje_turno;
    }

    public int getMedida() {
        return medida;
    }

    public void setMedida(int medida) {
        this.medida = medida;
    }

    public String getGrua() {
        return grua;
    }

    public void setGrua(String grua) {
        this.grua = grua;
    }

    public int getCorrelativo() {
        return correlativo;
    }

    public void setCorrelativo(int correlativo) {
        this.correlativo = correlativo;
    }

    public String getGrabador() {
        return grabador;
    }

    public void setGrabador(String grabador) {
        this.grabador = grabador;
    }

    public String getHandheld_id() {
        return handheld_id;
    }

    public void setHandheld_id(String handheld_id) {
        this.handheld_id = handheld_id;
    }

    public int getId_movimiento() {
        return id_movimiento;
    }

    public void setId_movimiento(int id_movimiento) {
        this.id_movimiento = id_movimiento;
    }

    public long getViaje_empornac() {
        return viaje_empornac;
    }

    public void setViaje_empornac(long viaje_empornac) {
        this.viaje_empornac = viaje_empornac;
    }

    public int getTipo_pinbox() {
        return tipo_pinbox;
    }

    public void setTipo_pinbox(int tipo_pinbox) {
        this.tipo_pinbox = tipo_pinbox;
    }

    public int getTipo_movimiento() {
        return tipo_movimiento;
    }

    public void setTipo_movimiento(int tipo_movimiento) {
        this.tipo_movimiento = tipo_movimiento;
    }

    public long getId_turno() {
        return id_turno;
    }

    public void setId_turno(long id_turno) {
        this.id_turno = id_turno;
    }

    public String getFecha_grabacion() {
        return fecha_grabacion;
    }

    public void setFecha_grabacion(String fecha_grabacion) {
        this.fecha_grabacion = fecha_grabacion;
    }
}
