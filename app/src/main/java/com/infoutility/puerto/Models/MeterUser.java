package com.infoutility.puerto.Models;

public class MeterUser {

    private String uel_direccion;
    private long uel_contador;
    private String tipo;
    private String uel_estatus;
    private long uel_codigo;
    private long uel_lectura;
    private String uel_observaciones;
    private String uel_nombre;
    private int colonia;
    private long uel_tar_codigo;
    private double tar_tarifa_quetzal;
    private String uel_numero_tarjeta;
    private String tar_descripcion;
    private double tar_tarifa_dolar;

    public MeterUser(String uel_direccion, long uel_contador, String tipo, String uel_estatus, long uel_codigo, long uel_lectura, String uel_observaciones, String uel_nombre, int colonia, long uel_tar_codigo, double tar_tarifa_quetzal, String uel_numero_tarjeta, String tar_descripcion, double tar_tarifa_dolar) {
        this.uel_direccion = uel_direccion;
        this.uel_contador = uel_contador;
        this.tipo = tipo;
        this.uel_estatus = uel_estatus;
        this.uel_codigo = uel_codigo;
        this.uel_lectura = uel_lectura;
        this.uel_observaciones = uel_observaciones;
        this.uel_nombre = uel_nombre;
        this.colonia = colonia;
        this.uel_tar_codigo = uel_tar_codigo;
        this.tar_tarifa_quetzal = tar_tarifa_quetzal;
        this.uel_numero_tarjeta = uel_numero_tarjeta;
        this.tar_descripcion = tar_descripcion;
        this.tar_tarifa_dolar = tar_tarifa_dolar;
    }

    public MeterUser() {
    }


    public String getUel_direccion() {
        return uel_direccion;
    }

    public void setUel_direccion(String uel_direccion) {
        this.uel_direccion = uel_direccion;
    }

    public long getUel_contador() {
        return uel_contador;
    }

    public void setUel_contador(long uel_contador) {
        this.uel_contador = uel_contador;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getUel_estatus() {
        return uel_estatus;
    }

    public void setUel_estatus(String uel_estatus) {
        this.uel_estatus = uel_estatus;
    }

    public long getUel_codigo() {
        return uel_codigo;
    }

    public void setUel_codigo(long uel_codigo) {
        this.uel_codigo = uel_codigo;
    }

    public long getUel_lectura() {
        return uel_lectura;
    }

    public void setUel_lectura(long uel_lectura) {
        this.uel_lectura = uel_lectura;
    }

    public String getUel_observaciones() {
        return uel_observaciones;
    }

    public void setUel_observaciones(String uel_observaciones) {
        this.uel_observaciones = uel_observaciones;
    }

    public String getUel_nombre() {
        return uel_nombre;
    }

    public void setUel_nombre(String uel_nombre) {
        this.uel_nombre = uel_nombre;
    }

    public int getColonia() {
        return colonia;
    }

    public void setColonia(int colonia) {
        this.colonia = colonia;
    }

    public long getUel_tar_codigo() {
        return uel_tar_codigo;
    }

    public void setUel_tar_codigo(long uel_tar_codigo) {
        this.uel_tar_codigo = uel_tar_codigo;
    }

    public double getTar_tarifa_quetzal() {
        return tar_tarifa_quetzal;
    }

    public void setTar_tarifa_quetzal(double tar_tarifa_quetzal) {
        this.tar_tarifa_quetzal = tar_tarifa_quetzal;
    }

    public String getUel_numero_tarjeta() {
        return uel_numero_tarjeta;
    }

    public void setUel_numero_tarjeta(String uel_numero_tarjeta) {
        this.uel_numero_tarjeta = uel_numero_tarjeta;
    }

    public String getTar_descripcion() {
        return tar_descripcion;
    }

    public void setTar_descripcion(String tar_descripcion) {
        this.tar_descripcion = tar_descripcion;
    }

    public double getTar_tarifa_dolar() {
        return tar_tarifa_dolar;
    }

    public void setTar_tarifa_dolar(double tar_tarifa_dolar) {
        this.tar_tarifa_dolar = tar_tarifa_dolar;
    }
}
