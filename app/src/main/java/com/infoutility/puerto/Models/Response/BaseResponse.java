package com.infoutility.puerto.Models.Response;

public class BaseResponse {
    private boolean Resultado;
    private String Message;

    public boolean isResultado() {
        return Resultado;
    }

    public void setResultado(boolean resultado) {
        Resultado = resultado;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String message) {
        Message = message;
    }
}
