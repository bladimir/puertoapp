package com.infoutility.puerto.Models;

public class ContainerMeasure {
    private int medida;

    public ContainerMeasure(int medida) {
        this.medida = medida;
    }

    public int getMedida() {
        return medida;
    }

    public void setMedida(int medida) {
        this.medida = medida;
    }
}
