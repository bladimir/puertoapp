package com.infoutility.puerto.Models;

import org.parceler.Parcel;

@Parcel(Parcel.Serialization.BEAN)
public class Dispatch {

    private long autorizacion_despacho;
    private String fecha_hora_ubicador;
    private int fila_o_carril;
    private long grabador_patio;
    private String hora_grabacion_patio;
    private long maquina;
    private int modulo;
    private int nivel;
    private String numero_de_identificacion;
    private String patio;
    private String prefijo;
    private String NUMERO_CHASIS;
    private String PREFIJO_CHASIS;
    private String placa;

    public Dispatch(long autorizacion_despacho, String fecha_hora_ubicador, int fila_o_carril, long grabador_patio, String hora_grabacion_patio, long maquina, int modulo, int nivel, String numero_de_identificacion, String patio, String prefijo, String NUMERO_CHASIS, String PREFIJO_CHASIS, String placa) {
        this.autorizacion_despacho = autorizacion_despacho;
        this.fecha_hora_ubicador = fecha_hora_ubicador;
        this.fila_o_carril = fila_o_carril;
        this.grabador_patio = grabador_patio;
        this.hora_grabacion_patio = hora_grabacion_patio;
        this.maquina = maquina;
        this.modulo = modulo;
        this.nivel = nivel;
        this.numero_de_identificacion = numero_de_identificacion;
        this.patio = patio;
        this.prefijo = prefijo;
        this.NUMERO_CHASIS = NUMERO_CHASIS;
        this.PREFIJO_CHASIS = PREFIJO_CHASIS;
        this.placa = placa;
    }

    public Dispatch() {
    }


    public long getAutorizacion_despacho() {
        return autorizacion_despacho;
    }

    public void setAutorizacion_despacho(long autorizacion_despacho) {
        this.autorizacion_despacho = autorizacion_despacho;
    }

    public String getFecha_hora_ubicador() {
        return fecha_hora_ubicador;
    }

    public void setFecha_hora_ubicador(String fecha_hora_ubicador) {
        this.fecha_hora_ubicador = fecha_hora_ubicador;
    }

    public int getFila_o_carril() {
        return fila_o_carril;
    }

    public void setFila_o_carril(int fila_o_carril) {
        this.fila_o_carril = fila_o_carril;
    }

    public long getGrabador_patio() {
        return grabador_patio;
    }

    public void setGrabador_patio(long grabador_patio) {
        this.grabador_patio = grabador_patio;
    }

    public String getHora_grabacion_patio() {
        return hora_grabacion_patio;
    }

    public void setHora_grabacion_patio(String hora_grabacion_patio) {
        this.hora_grabacion_patio = hora_grabacion_patio;
    }

    public long getMaquina() {
        return maquina;
    }

    public void setMaquina(long maquina) {
        this.maquina = maquina;
    }

    public int getModulo() {
        return modulo;
    }

    public void setModulo(int modulo) {
        this.modulo = modulo;
    }

    public int getNivel() {
        return nivel;
    }

    public void setNivel(int nivel) {
        this.nivel = nivel;
    }

    public String getNumero_de_identificacion() {
        return numero_de_identificacion;
    }

    public void setNumero_de_identificacion(String numero_de_identificacion) {
        this.numero_de_identificacion = numero_de_identificacion;
    }

    public String getPatio() {
        return patio;
    }

    public void setPatio(String patio) {
        this.patio = patio;
    }

    public String getPrefijo() {
        return prefijo;
    }

    public void setPrefijo(String prefijo) {
        this.prefijo = prefijo;
    }

    public String getNUMERO_CHASIS() {
        return NUMERO_CHASIS;
    }

    public void setNUMERO_CHASIS(String NUMERO_CHASIS) {
        this.NUMERO_CHASIS = NUMERO_CHASIS;
    }

    public String getPREFIJO_CHASIS() {
        return PREFIJO_CHASIS;
    }

    public void setPREFIJO_CHASIS(String PREFIJO_CHASIS) {
        this.PREFIJO_CHASIS = PREFIJO_CHASIS;
    }

    public String getPlaca() {
        return placa;
    }

    public void setPlaca(String placa) {
        this.placa = placa;
    }
}
