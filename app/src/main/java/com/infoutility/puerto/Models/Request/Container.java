package com.infoutility.puerto.Models.Request;

public class Container {
    private String VIAJE_NO;
    private int TIPO_DE_MOVIMIENTO;
    private int ORDEN;
    private String PREFIJO;
    private String NUMERO_DE_IDENTIFICACION;
    private String CONDICION;
    private double PESO_MANIFESTADO;
    private double PESO_BASCULA;
    private int MAQUINA;
    private int MEDIDA;
    private String C_O_F;
    private String TIPO_CONTENEDOR;
    private int TARA;
    private String GRUA;
    private String CONFIRMADO;
    private int SEQ_REG_CONTE;
    private int GRABADOR;
    private String FECHA_GRABACION;
    private String HANDHELD_ID;
    private long ID_TURNO;
    private String VIAJE_TURNO;
    private String OBSERVACION_CHEQUE;
    private String REFER_SECO_OPERANDO;
    private int ID_MOVIMIENTO;
    private int OPERADOR;

    public Container(String VIAJE_NO, int TIPO_DE_MOVIMIENTO, int ORDEN, String PREFIJO, String NUMERO_DE_IDENTIFICACION, String CONDICION, double PESO_MANIFESTADO, double PESO_BASCULA, int MAQUINA, int MEDIDA, String c_O_F, String TIPO_CONTENEDOR, int TARA, String GRUA, String CONFIRMADO, int SEQ_REG_CONTE, int GRABADOR, String FECHA_GRABACION, String HANDHELD_ID, long ID_TURNO, String VIAJE_TURNO, String OBSERVACION_CHEQUE, String REFER_SECO_OPERANDO, int ID_MOVIMIENTO, int OPERADOR) {
        this.VIAJE_NO = VIAJE_NO;
        this.TIPO_DE_MOVIMIENTO = TIPO_DE_MOVIMIENTO;
        this.ORDEN = ORDEN;
        this.PREFIJO = PREFIJO;
        this.NUMERO_DE_IDENTIFICACION = NUMERO_DE_IDENTIFICACION;
        this.CONDICION = CONDICION;
        this.PESO_MANIFESTADO = PESO_MANIFESTADO;
        this.PESO_BASCULA = PESO_BASCULA;
        this.MAQUINA = MAQUINA;
        this.MEDIDA = MEDIDA;
        C_O_F = c_O_F;
        this.TIPO_CONTENEDOR = TIPO_CONTENEDOR;
        this.TARA = TARA;
        this.GRUA = GRUA;
        this.CONFIRMADO = CONFIRMADO;
        this.SEQ_REG_CONTE = SEQ_REG_CONTE;
        this.GRABADOR = GRABADOR;
        this.FECHA_GRABACION = FECHA_GRABACION;
        this.HANDHELD_ID = HANDHELD_ID;
        this.ID_TURNO = ID_TURNO;
        this.VIAJE_TURNO = VIAJE_TURNO;
        this.OBSERVACION_CHEQUE = OBSERVACION_CHEQUE;
        this.REFER_SECO_OPERANDO = REFER_SECO_OPERANDO;
        this.ID_MOVIMIENTO = ID_MOVIMIENTO;
        this.OPERADOR = OPERADOR;
    }

    public Container() {
    }

    public String getVIAJE_NO() {
        return VIAJE_NO;
    }

    public void setVIAJE_NO(String VIAJE_NO) {
        this.VIAJE_NO = VIAJE_NO;
    }

    public int getTIPO_DE_MOVIMIENTO() {
        return TIPO_DE_MOVIMIENTO;
    }

    public void setTIPO_DE_MOVIMIENTO(int TIPO_DE_MOVIMIENTO) {
        this.TIPO_DE_MOVIMIENTO = TIPO_DE_MOVIMIENTO;
    }

    public int getORDEN() {
        return ORDEN;
    }

    public void setORDEN(int ORDEN) {
        this.ORDEN = ORDEN;
    }

    public String getPREFIJO() {
        return PREFIJO;
    }

    public void setPREFIJO(String PREFIJO) {
        this.PREFIJO = PREFIJO;
    }

    public String getNUMERO_DE_IDENTIFICACION() {
        return NUMERO_DE_IDENTIFICACION;
    }

    public void setNUMERO_DE_IDENTIFICACION(String NUMERO_DE_IDENTIFICACION) {
        this.NUMERO_DE_IDENTIFICACION = NUMERO_DE_IDENTIFICACION;
    }

    public String getCONDICION() {
        return CONDICION;
    }

    public void setCONDICION(String CONDICION) {
        this.CONDICION = CONDICION;
    }

    public double getPESO_MANIFESTADO() {
        return PESO_MANIFESTADO;
    }

    public void setPESO_MANIFESTADO(double PESO_MANIFESTADO) {
        this.PESO_MANIFESTADO = PESO_MANIFESTADO;
    }

    public double getPESO_BASCULA() {
        return PESO_BASCULA;
    }

    public void setPESO_BASCULA(double PESO_BASCULA) {
        this.PESO_BASCULA = PESO_BASCULA;
    }

    public int getMAQUINA() {
        return MAQUINA;
    }

    public void setMAQUINA(int MAQUINA) {
        this.MAQUINA = MAQUINA;
    }

    public int getMEDIDA() {
        return MEDIDA;
    }

    public void setMEDIDA(int MEDIDA) {
        this.MEDIDA = MEDIDA;
    }

    public String getC_O_F() {
        return C_O_F;
    }

    public void setC_O_F(String c_O_F) {
        C_O_F = c_O_F;
    }

    public String getTIPO_CONTENEDOR() {
        return TIPO_CONTENEDOR;
    }

    public void setTIPO_CONTENEDOR(String TIPO_CONTENEDOR) {
        this.TIPO_CONTENEDOR = TIPO_CONTENEDOR;
    }

    public int getTARA() {
        return TARA;
    }

    public void setTARA(int TARA) {
        this.TARA = TARA;
    }

    public String getCONFIRMADO() {
        return CONFIRMADO;
    }

    public void setCONFIRMADO(String CONFIRMADO) {
        this.CONFIRMADO = CONFIRMADO;
    }

    public int getSEQ_REG_CONTE() {
        return SEQ_REG_CONTE;
    }

    public void setSEQ_REG_CONTE(int SEQ_REG_CONTE) {
        this.SEQ_REG_CONTE = SEQ_REG_CONTE;
    }

    public int getGRABADOR() {
        return GRABADOR;
    }

    public void setGRABADOR(int GRABADOR) {
        this.GRABADOR = GRABADOR;
    }

    public String getFECHA_GRABACION() {
        return FECHA_GRABACION;
    }

    public void setFECHA_GRABACION(String FECHA_GRABACION) {
        this.FECHA_GRABACION = FECHA_GRABACION;
    }

    public String getHANDHELD_ID() {
        return HANDHELD_ID;
    }

    public void setHANDHELD_ID(String HANDHELD_ID) {
        this.HANDHELD_ID = HANDHELD_ID;
    }

    public long getID_TURNO() {
        return ID_TURNO;
    }

    public void setID_TURNO(long ID_TURNO) {
        this.ID_TURNO = ID_TURNO;
    }

    public String getVIAJE_TURNO() {
        return VIAJE_TURNO;
    }

    public void setVIAJE_TURNO(String VIAJE_TURNO) {
        this.VIAJE_TURNO = VIAJE_TURNO;
    }

    public String getOBSERVACION_CHEQUE() {
        return OBSERVACION_CHEQUE;
    }

    public void setOBSERVACION_CHEQUE(String OBSERVACION_CHEQUE) {
        this.OBSERVACION_CHEQUE = OBSERVACION_CHEQUE;
    }

    public String getREFER_SECO_OPERANDO() {
        return REFER_SECO_OPERANDO;
    }

    public void setREFER_SECO_OPERANDO(String REFER_SECO_OPERANDO) {
        this.REFER_SECO_OPERANDO = REFER_SECO_OPERANDO;
    }

    public int getID_MOVIMIENTO() {
        return ID_MOVIMIENTO;
    }

    public void setID_MOVIMIENTO(int ID_MOVIMIENTO) {
        this.ID_MOVIMIENTO = ID_MOVIMIENTO;
    }

    public int getOPERADOR() {
        return OPERADOR;
    }

    public void setOPERADOR(int OPERADOR) {
        this.OPERADOR = OPERADOR;
    }

    public String getGRUA() {
        return GRUA;
    }

    public void setGRUA(String GRUA) {
        this.GRUA = GRUA;
    }
}
