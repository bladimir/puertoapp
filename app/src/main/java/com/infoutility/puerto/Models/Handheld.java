package com.infoutility.puerto.Models;

public class Handheld {
    private String id_encriptado;
    private String fecha_habilitada;
    private String estado;
    private String handheld_id;

    public Handheld(String id_encriptado, String fecha_habilitada, String estado, String handheld_id) {
        this.id_encriptado = id_encriptado;
        this.fecha_habilitada = fecha_habilitada;
        this.estado = estado;
        this.handheld_id = handheld_id;
    }

    public String getId_encriptado() {
        return id_encriptado;
    }

    public void setId_encriptado(String id_encriptado) {
        this.id_encriptado = id_encriptado;
    }

    public String getFecha_habilitada() {
        return fecha_habilitada;
    }

    public void setFecha_habilitada(String fecha_habilitada) {
        this.fecha_habilitada = fecha_habilitada;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getHandheld_id() {
        return handheld_id;
    }

    public void setHandheld_id(String handheld_id) {
        this.handheld_id = handheld_id;
    }
}
