package com.infoutility.puerto.Models;

public class Reception {

    private long grabador_patio;
    private long usuario_de_servicio;
    private String estado_contenedor;
    private int medida;
    private String c_o_f;
    private String numero_de_identificacion;
    private String prefijo;
    private int fila_o_carril;
    private String fecha_recibe_doc_oficial_patio;
    private String viaje_barco;
    private String patio;
    private String tipo_contenedor;
    private String fecha_hora_recepcion;
    private long seq_reg_conte;
    private String fecha_hora_ubicador;
    private String selectivo_sat;
    private String fecha_entrega_doc_piloto;
    private String observaciones;
    private long maquina;
    private int modulo;
    private int nivel;
    private long autorizacion_recepcion;
    private String fecha_viaje_barco;
    private String refer_seco_operando;

    public Reception(long grabador_patio, long usuario_de_servicio, String estado_contenedor, int medida, String c_o_f, String numero_de_identificacion, String prefijo, int fila_o_carril, String fecha_recibe_doc_oficial_patio, String viaje_barco, String patio, String tipo_contenedor, String fecha_hora_recepcion, long seq_reg_conte, String fecha_hora_ubicador, String selectivo_sat, String fecha_entrega_doc_piloto, String observaciones, long maquina, int modulo, int nivel, long autorizacion_recepcion, String fecha_viaje_barco, String refer_seco_operando) {
        this.grabador_patio = grabador_patio;
        this.usuario_de_servicio = usuario_de_servicio;
        this.estado_contenedor = estado_contenedor;
        this.medida = medida;
        this.c_o_f = c_o_f;
        this.numero_de_identificacion = numero_de_identificacion;
        this.prefijo = prefijo;
        this.fila_o_carril = fila_o_carril;
        this.fecha_recibe_doc_oficial_patio = fecha_recibe_doc_oficial_patio;
        this.viaje_barco = viaje_barco;
        this.patio = patio;
        this.tipo_contenedor = tipo_contenedor;
        this.fecha_hora_recepcion = fecha_hora_recepcion;
        this.seq_reg_conte = seq_reg_conte;
        this.fecha_hora_ubicador = fecha_hora_ubicador;
        this.selectivo_sat = selectivo_sat;
        this.fecha_entrega_doc_piloto = fecha_entrega_doc_piloto;
        this.observaciones = observaciones;
        this.maquina = maquina;
        this.modulo = modulo;
        this.nivel = nivel;
        this.autorizacion_recepcion = autorizacion_recepcion;
        this.fecha_viaje_barco = fecha_viaje_barco;
        this.refer_seco_operando = refer_seco_operando;
    }

    public Reception() {
    }

    public long getGrabador_patio() {
        return grabador_patio;
    }

    public void setGrabador_patio(long grabador_patio) {
        this.grabador_patio = grabador_patio;
    }

    public long getUsuario_de_servicio() {
        return usuario_de_servicio;
    }

    public void setUsuario_de_servicio(long usuario_de_servicio) {
        this.usuario_de_servicio = usuario_de_servicio;
    }

    public String getEstado_contenedor() {
        return estado_contenedor;
    }

    public void setEstado_contenedor(String estado_contenedor) {
        this.estado_contenedor = estado_contenedor;
    }

    public int getMedida() {
        return medida;
    }

    public void setMedida(int medida) {
        this.medida = medida;
    }

    public String getC_o_f() {
        return c_o_f;
    }

    public void setC_o_f(String c_o_f) {
        this.c_o_f = c_o_f;
    }

    public String getNumero_de_identificacion() {
        return numero_de_identificacion;
    }

    public void setNumero_de_identificacion(String numero_de_identificacion) {
        this.numero_de_identificacion = numero_de_identificacion;
    }

    public String getPrefijo() {
        return prefijo;
    }

    public void setPrefijo(String prefijo) {
        this.prefijo = prefijo;
    }

    public int getFila_o_carril() {
        return fila_o_carril;
    }

    public void setFila_o_carril(int fila_o_carril) {
        this.fila_o_carril = fila_o_carril;
    }

    public String getFecha_recibe_doc_oficial_patio() {
        return fecha_recibe_doc_oficial_patio;
    }

    public void setFecha_recibe_doc_oficial_patio(String fecha_recibe_doc_oficial_patio) {
        this.fecha_recibe_doc_oficial_patio = fecha_recibe_doc_oficial_patio;
    }

    public String getViaje_barco() {
        return viaje_barco;
    }

    public void setViaje_barco(String viaje_barco) {
        this.viaje_barco = viaje_barco;
    }

    public String getPatio() {
        return patio;
    }

    public void setPatio(String patio) {
        this.patio = patio;
    }

    public String getTipo_contenedor() {
        return tipo_contenedor;
    }

    public void setTipo_contenedor(String tipo_contenedor) {
        this.tipo_contenedor = tipo_contenedor;
    }

    public String getFecha_hora_recepcion() {
        return fecha_hora_recepcion;
    }

    public void setFecha_hora_recepcion(String fecha_hora_recepcion) {
        this.fecha_hora_recepcion = fecha_hora_recepcion;
    }

    public long getSeq_reg_conte() {
        return seq_reg_conte;
    }

    public void setSeq_reg_conte(long seq_reg_conte) {
        this.seq_reg_conte = seq_reg_conte;
    }

    public String getFecha_hora_ubicador() {
        return fecha_hora_ubicador;
    }

    public void setFecha_hora_ubicador(String fecha_hora_ubicador) {
        this.fecha_hora_ubicador = fecha_hora_ubicador;
    }

    public String getSelectivo_sat() {
        return selectivo_sat;
    }

    public void setSelectivo_sat(String selectivo_sat) {
        this.selectivo_sat = selectivo_sat;
    }

    public String getFecha_entrega_doc_piloto() {
        return fecha_entrega_doc_piloto;
    }

    public void setFecha_entrega_doc_piloto(String fecha_entrega_doc_piloto) {
        this.fecha_entrega_doc_piloto = fecha_entrega_doc_piloto;
    }

    public String getObservaciones() {
        return observaciones;
    }

    public void setObservaciones(String observaciones) {
        this.observaciones = observaciones;
    }

    public long getMaquina() {
        return maquina;
    }

    public void setMaquina(long maquina) {
        this.maquina = maquina;
    }

    public int getModulo() {
        return modulo;
    }

    public void setModulo(int modulo) {
        this.modulo = modulo;
    }

    public int getNivel() {
        return nivel;
    }

    public void setNivel(int nivel) {
        this.nivel = nivel;
    }

    public long getAutorizacion_recepcion() {
        return autorizacion_recepcion;
    }

    public void setAutorizacion_recepcion(long autorizacion_recepcion) {
        this.autorizacion_recepcion = autorizacion_recepcion;
    }

    public String getFecha_viaje_barco() {
        return fecha_viaje_barco;
    }

    public void setFecha_viaje_barco(String fecha_viaje_barco) {
        this.fecha_viaje_barco = fecha_viaje_barco;
    }

    public String getRefer_seco_operando() {
        return refer_seco_operando;
    }

    public void setRefer_seco_operando(String refer_seco_operando) {
        this.refer_seco_operando = refer_seco_operando;
    }
}
