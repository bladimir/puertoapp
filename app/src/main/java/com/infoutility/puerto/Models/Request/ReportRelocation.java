package com.infoutility.puerto.Models.Request;

public class ReportRelocation {
    private String fecha;
    private int ano;
    private String observaciones;
    private String oficial;
    private int num_reporte = 0;
    private String supervisor;

    public ReportRelocation(String fecha, int ano, String observaciones, String oficial, String supervisor) {
        this.fecha = fecha;
        this.ano = ano;
        this.observaciones = observaciones;
        this.oficial = oficial;
        this.supervisor = supervisor;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public int getAno() {
        return ano;
    }

    public void setAno(int ano) {
        this.ano = ano;
    }

    public String getObservaciones() {
        return observaciones;
    }

    public void setObservaciones(String observaciones) {
        this.observaciones = observaciones;
    }

    public String getOficial() {
        return oficial;
    }

    public void setOficial(String oficial) {
        this.oficial = oficial;
    }

    public int getNum_reporte() {
        return num_reporte;
    }

    public void setNum_reporte(int num_reporte) {
        this.num_reporte = num_reporte;
    }

    public String getSupervisor() {
        return supervisor;
    }

    public void setSupervisor(String supervisor) {
        this.supervisor = supervisor;
    }
}
