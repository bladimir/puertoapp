package com.infoutility.puerto.Models.Request;

public class ColedContainerDispatch {

    String PREFIJO;
    String NUMERO_IDENTIFICACION;
    String VIAJE_SISTEMA;
    String FECHA_DESCONECCION;
    String GRABADOR_DESCONECCION;
    String FECHA_GRABA_DESCONECCION;
    String HANDHELD_ID_DESCONECCION;
    String OBSERVACION;
    String ESTATUS_CONECCION;
    String CONEXION_O_DESCONEXION;

    String FECHA_CONECCION;
    String GRABADOR_CONECCION;
    String FECHA_GRABA_CONECCION;
    String HANDHELD_ID_CONECCION;
    String FECHA_CREACION;

    public ColedContainerDispatch(String PREFIJO, String NUMERO_IDENTIFICACION, String VIAJE_SISTEMA, String FECHA_DESCONECCION,
                                  String GRABADOR_DESCONECCION, String FECHA_GRABA_DESCONECCION, String HANDHELD_ID_DESCONECCION,
                                  String OBSERVACION, String ESTATUS_CONECCION, String CONEXION_O_DESCONEXION, String FECHA_CONECCION,
                                  String GRABADOR_CONECCION, String FECHA_GRABA_CONECCION,
                                  String HANDHELD_ID_CONECCION, String FECHA_CREACION) {
        this.PREFIJO = PREFIJO;
        this.NUMERO_IDENTIFICACION = NUMERO_IDENTIFICACION;
        this.VIAJE_SISTEMA = VIAJE_SISTEMA;
        this.FECHA_DESCONECCION = FECHA_DESCONECCION;
        this.GRABADOR_DESCONECCION = GRABADOR_DESCONECCION;
        this.FECHA_GRABA_DESCONECCION = FECHA_GRABA_DESCONECCION;
        this.HANDHELD_ID_DESCONECCION = HANDHELD_ID_DESCONECCION;
        this.OBSERVACION = OBSERVACION;
        this.ESTATUS_CONECCION = ESTATUS_CONECCION;
        this.CONEXION_O_DESCONEXION = CONEXION_O_DESCONEXION;

        this.FECHA_CONECCION = FECHA_CONECCION;
        this.GRABADOR_CONECCION = GRABADOR_CONECCION;
        this.FECHA_GRABA_CONECCION = FECHA_GRABA_CONECCION;
        this.HANDHELD_ID_CONECCION = HANDHELD_ID_CONECCION;
        this.FECHA_CREACION = FECHA_CREACION;
    }

    public String getFECHA_CONECCION() {
        return FECHA_CONECCION;
    }

    public void setFECHA_CONECCION(String FECHA_CONECCION) {
        this.FECHA_CONECCION = FECHA_CONECCION;
    }

    public String getGRABADOR_CONECCION() {
        return GRABADOR_CONECCION;
    }

    public void setGRABADOR_CONECCION(String GRABADOR_CONECCION) {
        this.GRABADOR_CONECCION = GRABADOR_CONECCION;
    }

    public String getFECHA_GRABA_CONECCION() {
        return FECHA_GRABA_CONECCION;
    }

    public void setFECHA_GRABA_CONECCION(String FECHA_GRABA_CONECCION) {
        this.FECHA_GRABA_CONECCION = FECHA_GRABA_CONECCION;
    }

    public String getHANDHELD_ID_CONECCION() {
        return HANDHELD_ID_CONECCION;
    }

    public void setHANDHELD_ID_CONECCION(String HANDHELD_ID_CONECCION) {
        this.HANDHELD_ID_CONECCION = HANDHELD_ID_CONECCION;
    }

    public String getFECHA_CREACION() {
        return FECHA_CREACION;
    }

    public void setFECHA_CREACION(String FECHA_CREACION) {
        this.FECHA_CREACION = FECHA_CREACION;
    }

    public String getPREFIJO() {
        return PREFIJO;
    }

    public void setPREFIJO(String PREFIJO) {
        this.PREFIJO = PREFIJO;
    }

    public String getNUMERO_IDENTIFICACION() {
        return NUMERO_IDENTIFICACION;
    }

    public void setNUMERO_IDENTIFICACION(String NUMERO_IDENTIFICACION) {
        this.NUMERO_IDENTIFICACION = NUMERO_IDENTIFICACION;
    }

    public String getVIAJE_SISTEMA() {
        return VIAJE_SISTEMA;
    }

    public void setVIAJE_SISTEMA(String VIAJE_SISTEMA) {
        this.VIAJE_SISTEMA = VIAJE_SISTEMA;
    }

    public String getFECHA_DESCONECCION() {
        return FECHA_DESCONECCION;
    }

    public void setFECHA_DESCONECCION(String FECHA_DESCONECCION) {
        this.FECHA_DESCONECCION = FECHA_DESCONECCION;
    }

    public String getGRABADOR_DESCONECCION() {
        return GRABADOR_DESCONECCION;
    }

    public void setGRABADOR_DESCONECCION(String GRABADOR_DESCONECCION) {
        this.GRABADOR_DESCONECCION = GRABADOR_DESCONECCION;
    }

    public String getFECHA_GRABA_DESCONECCION() {
        return FECHA_GRABA_DESCONECCION;
    }

    public void setFECHA_GRABA_DESCONECCION(String FECHA_GRABA_DESCONECCION) {
        this.FECHA_GRABA_DESCONECCION = FECHA_GRABA_DESCONECCION;
    }

    public String getHANDHELD_ID_DESCONECCION() {
        return HANDHELD_ID_DESCONECCION;
    }

    public void setHANDHELD_ID_DESCONECCION(String HANDHELD_ID_DESCONECCION) {
        this.HANDHELD_ID_DESCONECCION = HANDHELD_ID_DESCONECCION;
    }

    public String getOBSERVACION() {
        return OBSERVACION;
    }

    public void setOBSERVACION(String OBSERVACION) {
        this.OBSERVACION = OBSERVACION;
    }

    public String getESTATUS_CONECCION() {
        return ESTATUS_CONECCION;
    }

    public void setESTATUS_CONECCION(String ESTATUS_CONECCION) {
        this.ESTATUS_CONECCION = ESTATUS_CONECCION;
    }

    public String getCONEXION_O_DESCONEXION() {
        return CONEXION_O_DESCONEXION;
    }

    public void setCONEXION_O_DESCONEXION(String CONEXION_O_DESCONEXION) {
        this.CONEXION_O_DESCONEXION = CONEXION_O_DESCONEXION;
    }
}
