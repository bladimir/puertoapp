package com.infoutility.puerto.Models.Request;

public class ReportRelocationInt {

    private String fecha;
    private String observaciones;
    private String oficial;
    private int num_reporte = 0;
    private String supervisor;
    private String viaje;
    private String inicio;
    private String fin;
    private int cantida_mov = 0;


    public ReportRelocationInt(String fecha, String observaciones, String oficial, String supervisor, String ship, String inicio, String fin) {
        this.fecha = fecha;
        this.observaciones = observaciones;
        this.oficial = oficial;
        this.supervisor = supervisor;
        this.viaje = ship;
        this.inicio = inicio;
        this.fin = fin;
        num_reporte = 0;
        cantida_mov = 0;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getObservaciones() {
        return observaciones;
    }

    public void setObservaciones(String observaciones) {
        this.observaciones = observaciones;
    }

    public String getOficial() {
        return oficial;
    }

    public void setOficial(String oficial) {
        this.oficial = oficial;
    }

    public int getNum_reporte() {
        return num_reporte;
    }

    public void setNum_reporte(int num_reporte) {
        this.num_reporte = num_reporte;
    }

    public String getSupervisor() {
        return supervisor;
    }

    public void setSupervisor(String supervisor) {
        this.supervisor = supervisor;
    }

    public String getShip() {
        return viaje;
    }

    public void setShip(String ship) {
        this.viaje = ship;
    }

    public String getInicio() {
        return inicio;
    }

    public void setInicio(String inicio) {
        this.inicio = inicio;
    }

    public String getFin() {
        return fin;
    }

    public void setFin(String fin) {
        this.fin = fin;
    }

    public int getCantida_mov() {
        return cantida_mov;
    }

    public void setCantida_mov(int cantida_mov) {
        this.cantida_mov = cantida_mov;
    }
}
