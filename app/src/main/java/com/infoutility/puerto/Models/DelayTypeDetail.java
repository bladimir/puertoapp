package com.infoutility.puerto.Models;

public class DelayTypeDetail {
    private String tipo_demora;
    private String descripcion;

    public DelayTypeDetail(String tipo_demora, String descripcion) {
        this.tipo_demora = tipo_demora;
        this.descripcion = descripcion;
    }

    public String getTipo_demora() {
        return tipo_demora;
    }

    public void setTipo_demora(String tipo_demora) {
        this.tipo_demora = tipo_demora;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
}
