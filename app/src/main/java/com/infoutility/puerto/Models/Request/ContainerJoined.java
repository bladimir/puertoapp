package com.infoutility.puerto.Models.Request;

public class ContainerJoined {
    private int ID_ATADO;
    private String VIAJE_NO;
    private int TIPO_DE_MOVIMIENTO;
    private String PREFIJO;
    private String NUMERO_DE_IDENTIFICACION;
    private int GRABADOR;
    private String FECHA_GRABACION;
    private int GRABADOR_ACTUALIZA;
    private String FECHA_ACTUALIZA;
    private String HANDHELD_ID;
    private long ID_TURNO;
    private String HANDHELD_ID_ACTUALIZA;

    public ContainerJoined(int ID_ATADO, String VIAJE_NO, int TIPO_DE_MOVIMIENTO, String PREFIJO, String NUMERO_DE_IDENTIFICACION, int GRABADOR, String FECHA_GRABACION, int GRABADOR_ACTUALIZA, String FECHA_ACTUALIZA, String HANDHELD_ID, long ID_TURNO, String HANDHELD_ID_ACTUALIZA) {
        this.ID_ATADO = ID_ATADO;
        this.VIAJE_NO = VIAJE_NO;
        this.TIPO_DE_MOVIMIENTO = TIPO_DE_MOVIMIENTO;
        this.PREFIJO = PREFIJO;
        this.NUMERO_DE_IDENTIFICACION = NUMERO_DE_IDENTIFICACION;
        this.GRABADOR = GRABADOR;
        this.FECHA_GRABACION = FECHA_GRABACION;
        this.GRABADOR_ACTUALIZA = GRABADOR_ACTUALIZA;
        this.FECHA_ACTUALIZA = FECHA_ACTUALIZA;
        this.HANDHELD_ID = HANDHELD_ID;
        this.ID_TURNO = ID_TURNO;
        this.HANDHELD_ID_ACTUALIZA = HANDHELD_ID_ACTUALIZA;
    }

    public ContainerJoined() {
    }

    public int getID_ATADO() {
        return ID_ATADO;
    }

    public void setID_ATADO(int ID_ATADO) {
        this.ID_ATADO = ID_ATADO;
    }

    public String getVIAJE_NO() {
        return VIAJE_NO;
    }

    public void setVIAJE_NO(String VIAJE_NO) {
        this.VIAJE_NO = VIAJE_NO;
    }

    public int getTIPO_DE_MOVIMIENTO() {
        return TIPO_DE_MOVIMIENTO;
    }

    public void setTIPO_DE_MOVIMIENTO(int TIPO_DE_MOVIMIENTO) {
        this.TIPO_DE_MOVIMIENTO = TIPO_DE_MOVIMIENTO;
    }

    public String getPREFIJO() {
        return PREFIJO;
    }

    public void setPREFIJO(String PREFIJO) {
        this.PREFIJO = PREFIJO;
    }

    public String getNUMERO_DE_IDENTIFICACION() {
        return NUMERO_DE_IDENTIFICACION;
    }

    public void setNUMERO_DE_IDENTIFICACION(String NUMERO_DE_IDENTIFICACION) {
        this.NUMERO_DE_IDENTIFICACION = NUMERO_DE_IDENTIFICACION;
    }

    public int getGRABADOR() {
        return GRABADOR;
    }

    public void setGRABADOR(int GRABADOR) {
        this.GRABADOR = GRABADOR;
    }

    public String getFECHA_GRABACION() {
        return FECHA_GRABACION;
    }

    public void setFECHA_GRABACION(String FECHA_GRABACION) {
        this.FECHA_GRABACION = FECHA_GRABACION;
    }

    public int getGRABADOR_ACTUALIZA() {
        return GRABADOR_ACTUALIZA;
    }

    public void setGRABADOR_ACTUALIZA(int GRABADOR_ACTUALIZA) {
        this.GRABADOR_ACTUALIZA = GRABADOR_ACTUALIZA;
    }

    public String getFECHA_ACTUALIZA() {
        return FECHA_ACTUALIZA;
    }

    public void setFECHA_ACTUALIZA(String FECHA_ACTUALIZA) {
        this.FECHA_ACTUALIZA = FECHA_ACTUALIZA;
    }

    public String getHANDHELD_ID() {
        return HANDHELD_ID;
    }

    public void setHANDHELD_ID(String HANDHELD_ID) {
        this.HANDHELD_ID = HANDHELD_ID;
    }

    public long getID_TURNO() {
        return ID_TURNO;
    }

    public void setID_TURNO(long ID_TURNO) {
        this.ID_TURNO = ID_TURNO;
    }

    public String getHANDHELD_ID_ACTUALIZA() {
        return HANDHELD_ID_ACTUALIZA;
    }

    public void setHANDHELD_ID_ACTUALIZA(String HANDHELD_ID_ACTUALIZA) {
        this.HANDHELD_ID_ACTUALIZA = HANDHELD_ID_ACTUALIZA;
    }
}
