package com.infoutility.puerto.Models;

public class Consumption {
    private String mus_uel_contador;
    private long mus_lectura;
    private long mus_uel_codigo;
    private long mus_consumo;
    private int mus_mes;
    private String mus_observaciones;
    private String mus_anio;
    private long grabador;
    private String mus_lector;
    private long mus_lectura_anterior;
    private String fecha_grabacion;
    private int mus_exceso;
    private double mus_total;

    public Consumption(String mus_uel_contador, long mus_lectura, long mus_uel_codigo, long mus_consumo, int mus_mes, String mus_observaciones, String mus_anio, long grabador, String mus_lector, long mus_lectura_anterior, String fecha_grabacion, int mus_exceso, double mus_total) {
        this.mus_uel_contador = mus_uel_contador;
        this.mus_lectura = mus_lectura;
        this.mus_uel_codigo = mus_uel_codigo;
        this.mus_consumo = mus_consumo;
        this.mus_mes = mus_mes;
        this.mus_observaciones = mus_observaciones;
        this.mus_anio = mus_anio;
        this.grabador = grabador;
        this.mus_lector = mus_lector;
        this.mus_lectura_anterior = mus_lectura_anterior;
        this.fecha_grabacion = fecha_grabacion;
        this.mus_exceso = mus_exceso;
        this.mus_total = mus_total;
    }


    public Consumption() {
    }

    public String getMus_uel_contador() {
        return mus_uel_contador;
    }

    public void setMus_uel_contador(String mus_uel_contador) {
        this.mus_uel_contador = mus_uel_contador;
    }

    public long getMus_lectura() {
        return mus_lectura;
    }

    public void setMus_lectura(long mus_lectura) {
        this.mus_lectura = mus_lectura;
    }

    public long getMus_uel_codigo() {
        return mus_uel_codigo;
    }

    public void setMus_uel_codigo(long mus_uel_codigo) {
        this.mus_uel_codigo = mus_uel_codigo;
    }

    public long getMus_consumo() {
        return mus_consumo;
    }

    public void setMus_consumo(long mus_consumo) {
        this.mus_consumo = mus_consumo;
    }

    public int getMus_mes() {
        return mus_mes;
    }

    public void setMus_mes(int mus_mes) {
        this.mus_mes = mus_mes;
    }

    public String getMus_observaciones() {
        return mus_observaciones;
    }

    public void setMus_observaciones(String mus_observaciones) {
        this.mus_observaciones = mus_observaciones;
    }

    public String getMus_anio() {
        return mus_anio;
    }

    public void setMus_anio(String mus_anio) {
        this.mus_anio = mus_anio;
    }

    public long getGrabador() {
        return grabador;
    }

    public void setGrabador(long grabador) {
        this.grabador = grabador;
    }

    public String getMus_lector() {
        return mus_lector;
    }

    public void setMus_lector(String mus_lector) {
        this.mus_lector = mus_lector;
    }

    public long getMus_lectura_anterior() {
        return mus_lectura_anterior;
    }

    public void setMus_lectura_anterior(long mus_lectura_anterior) {
        this.mus_lectura_anterior = mus_lectura_anterior;
    }

    public String getFecha_grabacion() {
        return fecha_grabacion;
    }

    public void setFecha_grabacion(String fecha_grabacion) {
        this.fecha_grabacion = fecha_grabacion;
    }

    public int getMus_exceso() {
        return mus_exceso;
    }

    public void setMus_exceso(int mus_exceso) {
        this.mus_exceso = mus_exceso;
    }

    public double getMus_total() {
        return mus_total;
    }

    public void setMus_total(double mus_total) {
        this.mus_total = mus_total;
    }
}
