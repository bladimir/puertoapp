package com.infoutility.puerto.Models;

import org.parceler.Parcel;

@Parcel(Parcel.Serialization.BEAN)
public class ContainerYard {
    private String c_o_f;
    private String condicion;
    private String fecha_hora_ubica;
    private String fechahora_impor_patio;
    private int fila;
    private String grabador_impor_patio;
    private int id_movimiento;
    private long maquina;
    private int medida;
    private int modulo;
    private int nivel;
    private String numero_de_identificacion;
    private long operador;
    private int orden;
    private String patio;
    private double peso_manifestado;
    private String prefijo;
    private int tara;
    private int tipo_de_movimiento;
    private long viaje_no;
    //
    private String confirmado;
    private String fecha_grabacion;
    private String grabador;
    private String handheld_id;
    private int id_turno;
    private String observacion_cheque;
    private double peso_bascula;
    private String refer_seco_operando;
    private long seq_reg_conte;
    private String tipo_contenedor;
    private long viaje_turno;

    public String getC_o_f() {
        return c_o_f;
    }

    public void setC_o_f(String c_o_f) {
        this.c_o_f = c_o_f;
    }

    public String getCondicion() {
        return condicion;
    }

    public void setCondicion(String condicion) {
        this.condicion = condicion;
    }

    public String getFecha_hora_ubica() {
        return fecha_hora_ubica;
    }

    public void setFecha_hora_ubica(String fecha_hora_ubica) {
        this.fecha_hora_ubica = fecha_hora_ubica;
    }

    public String getFechahora_impor_patio() {
        return fechahora_impor_patio;
    }

    public void setFechahora_impor_patio(String fechahora_impor_patio) {
        this.fechahora_impor_patio = fechahora_impor_patio;
    }

    public int getFila() {
        return fila;
    }

    public void setFila(int fila) {
        this.fila = fila;
    }

    public String getGrabador_impor_patio() {
        return grabador_impor_patio;
    }

    public void setGrabador_impor_patio(String grabador_impor_patio) {
        this.grabador_impor_patio = grabador_impor_patio;
    }

    public int getId_movimiento() {
        return id_movimiento;
    }

    public void setId_movimiento(int id_movimiento) {
        this.id_movimiento = id_movimiento;
    }

    public long getMaquina() {
        return maquina;
    }

    public void setMaquina(long maquina) {
        this.maquina = maquina;
    }

    public int getMedida() {
        return medida;
    }

    public void setMedida(int medida) {
        this.medida = medida;
    }

    public int getModulo() {
        return modulo;
    }

    public void setModulo(int modulo) {
        this.modulo = modulo;
    }

    public int getNivel() {
        return nivel;
    }

    public void setNivel(int nivel) {
        this.nivel = nivel;
    }

    public String getNumero_de_identificacion() {
        return numero_de_identificacion;
    }

    public void setNumero_de_identificacion(String numero_de_identificacion) {
        this.numero_de_identificacion = numero_de_identificacion;
    }

    public long getOperador() {
        return operador;
    }

    public void setOperador(long operador) {
        this.operador = operador;
    }

    public int getOrden() {
        return orden;
    }

    public void setOrden(int orden) {
        this.orden = orden;
    }

    public String getPatio() {
        return patio;
    }

    public void setPatio(String patio) {
        this.patio = patio;
    }

    public double getPeso_manifestado() {
        return peso_manifestado;
    }

    public void setPeso_manifestado(double peso_manifestado) {
        this.peso_manifestado = peso_manifestado;
    }

    public String getPrefijo() {
        return prefijo;
    }

    public void setPrefijo(String prefijo) {
        this.prefijo = prefijo;
    }

    public int getTara() {
        return tara;
    }

    public void setTara(int tara) {
        this.tara = tara;
    }

    public int getTipo_de_movimiento() {
        return tipo_de_movimiento;
    }

    public void setTipo_de_movimiento(int tipo_de_movimiento) {
        this.tipo_de_movimiento = tipo_de_movimiento;
    }

    public long getViaje_no() {
        return viaje_no;
    }

    public void setViaje_no(long viaje_no) {
        this.viaje_no = viaje_no;
    }

    public String getConfirmado() {
        return confirmado;
    }

    public void setConfirmado(String confirmado) {
        this.confirmado = confirmado;
    }

    public String getFecha_grabacion() {
        return fecha_grabacion;
    }

    public void setFecha_grabacion(String fecha_grabacion) {
        this.fecha_grabacion = fecha_grabacion;
    }

    public String getGrabador() {
        return grabador;
    }

    public void setGrabador(String grabador) {
        this.grabador = grabador;
    }

    public String getHandheld_id() {
        return handheld_id;
    }

    public void setHandheld_id(String handheld_id) {
        this.handheld_id = handheld_id;
    }

    public int getId_turno() {
        return id_turno;
    }

    public void setId_turno(int id_turno) {
        this.id_turno = id_turno;
    }

    public String getObservacion_cheque() {
        return observacion_cheque;
    }

    public void setObservacion_cheque(String observacion_cheque) {
        this.observacion_cheque = observacion_cheque;
    }

    public double getPeso_bascula() {
        return peso_bascula;
    }

    public void setPeso_bascula(double peso_bascula) {
        this.peso_bascula = peso_bascula;
    }

    public String getRefer_seco_operando() {
        return refer_seco_operando;
    }

    public void setRefer_seco_operando(String refer_seco_operando) {
        this.refer_seco_operando = refer_seco_operando;
    }

    public long getSeq_reg_conte() {
        return seq_reg_conte;
    }

    public void setSeq_reg_conte(long seq_reg_conte) {
        this.seq_reg_conte = seq_reg_conte;
    }

    public String getTipo_contenedor() {
        return tipo_contenedor;
    }

    public void setTipo_contenedor(String tipo_contenedor) {
        this.tipo_contenedor = tipo_contenedor;
    }

    public long getViaje_turno() {
        return viaje_turno;
    }

    public void setViaje_turno(long viaje_turno) {
        this.viaje_turno = viaje_turno;
    }
}
