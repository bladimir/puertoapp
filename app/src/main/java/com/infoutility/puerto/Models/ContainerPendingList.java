package com.infoutility.puerto.Models;

public class ContainerPendingList {
    private String numero_identifiacion;
    private String prefijo;
    private String tipo_movimiento;
    private String parametros;
    private long referencia;
    private String usuario_grabador;
    private String fecha_grabacion;

    public ContainerPendingList(String numero_identifiacion, String prefijo, String tipo_movimiento, String parametros, long referencia, String usuario_grabador, String fecha_grabacion) {
        this.numero_identifiacion = numero_identifiacion;
        this.prefijo = prefijo;
        this.tipo_movimiento = tipo_movimiento;
        this.parametros = parametros;
        this.referencia = referencia;
        this.usuario_grabador = usuario_grabador;
        this.fecha_grabacion = fecha_grabacion;
    }

    public ContainerPendingList() {
    }

    public String getNumero_identifiacion() {
        return numero_identifiacion;
    }

    public void setNumero_identifiacion(String numero_identifiacion) {
        this.numero_identifiacion = numero_identifiacion;
    }

    public String getPrefijo() {
        return prefijo;
    }

    public void setPrefijo(String prefijo) {
        this.prefijo = prefijo;
    }

    public String getTipo_movimiento() {
        return tipo_movimiento;
    }

    public void setTipo_movimiento(String tipo_movimiento) {
        this.tipo_movimiento = tipo_movimiento;
    }

    public String getParametros() {
        return parametros;
    }

    public void setParametros(String parametros) {
        this.parametros = parametros;
    }

    public long getReferencia() {
        return referencia;
    }

    public void setReferencia(long referencia) {
        this.referencia = referencia;
    }

    public String getUsuario_grabador() {
        return usuario_grabador;
    }

    public void setUsuario_grabador(String usuario_grabador) {
        this.usuario_grabador = usuario_grabador;
    }

    public String getFecha_grabacion() {
        return fecha_grabacion;
    }

    public void setFecha_grabacion(String fecha_grabacion) {
        this.fecha_grabacion = fecha_grabacion;
    }
}
