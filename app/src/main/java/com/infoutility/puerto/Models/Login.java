package com.infoutility.puerto.Models;

public class Login {
    private int grabador;
    private int rol;
    private String usuario;

    public int getGrabador() {
        return grabador;
    }

    public void setGrabador(int grabador) {
        this.grabador = grabador;
    }

    public int getRol() {
        return rol;
    }

    public void setRol(int rol) {
        this.rol = rol;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }
}
