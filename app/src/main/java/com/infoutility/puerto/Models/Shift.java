package com.infoutility.puerto.Models;

import org.parceler.Parcel;

@Parcel(Parcel.Serialization.BEAN)
public class Shift {
    private String fecha_hora_final;
    private long viaje_no;
    private String fecha_hora_inicio;
    private String tipo_de_movimiento;
    private String observaciones;
    private int grabador;
    private String handheld_id;
    private String id_tipo_turno;
    private String id_cuadrilla;
    private long id_turno;
    private String fecha_grabacion;

    public Shift(String fecha_hora_final, long viaje_no, String fecha_hora_inicio, String tipo_de_movimiento, String observaciones, int grabador, String handheld_id, String id_tipo_turno, String id_cuadrilla, long id_turno, String fecha_grabacion) {
        this.fecha_hora_final = fecha_hora_final;
        this.viaje_no = viaje_no;
        this.fecha_hora_inicio = fecha_hora_inicio;
        this.tipo_de_movimiento = tipo_de_movimiento;
        this.observaciones = observaciones;
        this.grabador = grabador;
        this.handheld_id = handheld_id;
        this.id_tipo_turno = id_tipo_turno;
        this.id_cuadrilla = id_cuadrilla;
        this.id_turno = id_turno;
        this.fecha_grabacion = fecha_grabacion;
    }

    public Shift() {
    }

    public String getFecha_hora_final() {
        return fecha_hora_final;
    }

    public void setFecha_hora_final(String fecha_hora_final) {
        this.fecha_hora_final = fecha_hora_final;
    }

    public long getViaje_no() {
        return viaje_no;
    }

    public void setViaje_no(long viaje_no) {
        this.viaje_no = viaje_no;
    }

    public String getFecha_hora_inicio() {
        return fecha_hora_inicio;
    }

    public void setFecha_hora_inicio(String fecha_hora_inicio) {
        this.fecha_hora_inicio = fecha_hora_inicio;
    }

    public String getTipo_de_movimiento() {
        return tipo_de_movimiento;
    }

    public void setTipo_de_movimiento(String tipo_de_movimiento) {
        this.tipo_de_movimiento = tipo_de_movimiento;
    }

    public String getObservaciones() {
        return observaciones;
    }

    public void setObservaciones(String observaciones) {
        this.observaciones = observaciones;
    }

    public int getGrabador() {
        return grabador;
    }

    public void setGrabador(int grabador) {
        this.grabador = grabador;
    }

    public String getHandheld_id() {
        return handheld_id;
    }

    public void setHandheld_id(String handheld_id) {
        this.handheld_id = handheld_id;
    }

    public String getId_tipo_turno() {
        return id_tipo_turno;
    }

    public void setId_tipo_turno(String id_tipo_turno) {
        this.id_tipo_turno = id_tipo_turno;
    }

    public String getId_cuadrilla() {
        return id_cuadrilla;
    }

    public void setId_cuadrilla(String id_cuadrilla) {
        this.id_cuadrilla = id_cuadrilla;
    }

    public long getId_turno() {
        return id_turno;
    }

    public void setId_turno(long id_turno) {
        this.id_turno = id_turno;
    }

    public String getFecha_grabacion() {
        return fecha_grabacion;
    }

    public void setFecha_grabacion(String fecha_grabacion) {
        this.fecha_grabacion = fecha_grabacion;
    }
}
