package com.infoutility.puerto.Models;

import java.util.List;

public class ContainerRelated {
    private int idAtado;
    private List<String> relacionados;

    public int getIdAtado() {
        return idAtado;
    }

    public void setIdAtado(int idAtado) {
        this.idAtado = idAtado;
    }

    public List<String> getRelacionados() {
        return relacionados;
    }

    public void setRelacionados(List<String> relacionados) {
        this.relacionados = relacionados;
    }
}
