package com.infoutility.puerto.Realm;

import com.infoutility.puerto.Realm.Model.ElectricModel;

import io.realm.Realm;
import io.realm.RealmResults;

public class DeleteModels {

    private static Realm realm = Realm.getDefaultInstance();;

    public static void deleteAll(){
        final RealmResults<ElectricModel> rElectric = realm.where(ElectricModel.class).findAll();
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                // remove single match
                rElectric.deleteAllFromRealm();
            }
        });
    }

}
