package com.infoutility.puerto.Realm.Model;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class ElectricModel extends RealmObject {

    private String uel_direccion;
    private long uel_contador;
    private long uel_tar_codigo;
    private int mus_lectura;
    private long uel_codigo;
    private int mus_consumo;
    private int mus_mes;
    private float tar_tarifa_quetzal;
    private int mus_anio;
    private String uel_numero_tarjeta;
    private float tar_tarifa_dolar;
    private String uel_nombre;


    public String getUel_direccion() {
        return uel_direccion;
    }

    public void setUel_direccion(String uel_direccion) {
        this.uel_direccion = uel_direccion;
    }

    public long getUel_contador() {
        return uel_contador;
    }

    public void setUel_contador(long uel_contador) {
        this.uel_contador = uel_contador;
    }

    public long getUel_tar_codigo() {
        return uel_tar_codigo;
    }

    public void setUel_tar_codigo(long uel_tar_codigo) {
        this.uel_tar_codigo = uel_tar_codigo;
    }

    public int getMus_lectura() {
        return mus_lectura;
    }

    public void setMus_lectura(int mus_lectura) {
        this.mus_lectura = mus_lectura;
    }

    public long getUel_codigo() {
        return uel_codigo;
    }

    public void setUel_codigo(long uel_codigo) {
        this.uel_codigo = uel_codigo;
    }

    public int getMus_consumo() {
        return mus_consumo;
    }

    public void setMus_consumo(int mus_consumo) {
        this.mus_consumo = mus_consumo;
    }

    public int getMus_mes() {
        return mus_mes;
    }

    public void setMus_mes(int mus_mes) {
        this.mus_mes = mus_mes;
    }

    public float getTar_tarifa_quetzal() {
        return tar_tarifa_quetzal;
    }

    public void setTar_tarifa_quetzal(float tar_tarifa_quetzal) {
        this.tar_tarifa_quetzal = tar_tarifa_quetzal;
    }

    public int getMus_anio() {
        return mus_anio;
    }

    public void setMus_anio(int mus_anio) {
        this.mus_anio = mus_anio;
    }

    public String getUel_numero_tarjeta() {
        return uel_numero_tarjeta;
    }

    public void setUel_numero_tarjeta(String uel_numero_tarjeta) {
        this.uel_numero_tarjeta = uel_numero_tarjeta;
    }

    public float getTar_tarifa_dolar() {
        return tar_tarifa_dolar;
    }

    public void setTar_tarifa_dolar(float tar_tarifa_dolar) {
        this.tar_tarifa_dolar = tar_tarifa_dolar;
    }

    public String getUel_nombre() {
        return uel_nombre;
    }

    public void setUel_nombre(String uel_nombre) {
        this.uel_nombre = uel_nombre;
    }
}
