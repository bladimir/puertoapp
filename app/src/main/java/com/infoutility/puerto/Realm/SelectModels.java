package com.infoutility.puerto.Realm;

import com.infoutility.puerto.Realm.Model.ElectricModel;
import com.infoutility.puerto.Realm.Model.SyncModel;

import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmResults;
import io.realm.Sort;

public class SelectModels {

    private static Realm realm = Realm.getDefaultInstance();;


    public static ArrayList<ElectricModel> selectAllElectronic(){
        RealmResults<ElectricModel> rElectric = realm.where(ElectricModel.class).findAll();
        ArrayList<ElectricModel> listElectronic = new ArrayList<>(rElectric);
        return listElectronic;
    }

    public static ArrayList<ElectricModel> selectFindUserElectronic(long id, String field){
        RealmResults<ElectricModel> rElectric = realm.where(ElectricModel.class).equalTo(field, id).findAll();
        ArrayList<ElectricModel> listElectronic = new ArrayList<>(rElectric);
        return listElectronic;
    }

    public static ArrayList<ElectricModel> selectFindNumCardElectronic(String id, String field){
        RealmResults<ElectricModel> rElectric = realm.where(ElectricModel.class).equalTo(field, id).findAll();
        ArrayList<ElectricModel> listElectronic = new ArrayList<>(rElectric);
        return listElectronic;
    }


    public List<SyncModel> findAll() {
        RealmResults<SyncModel> results = realm
                .where(SyncModel.class)
                .sort("id", Sort.ASCENDING)
                .findAll();
        return realm.copyFromRealm(results);
    }

    public SyncModel findById(int id){
        return realm
                .where(SyncModel.class)
                .equalTo("id", id)
                .findFirst();
    }

}
