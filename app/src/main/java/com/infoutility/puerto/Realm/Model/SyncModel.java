package com.infoutility.puerto.Realm.Model;

import io.realm.RealmObject;

public class SyncModel extends RealmObject {
    private int id;
    private String data;

    public SyncModel(int id, String data) {
        this.id = id;
        this.data = data;
    }

    public SyncModel() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }
}
