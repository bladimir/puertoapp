package com.infoutility.puerto.Realm;


import io.realm.Realm;


import com.infoutility.puerto.Interfaces.Response;
import com.infoutility.puerto.Keys.Key;
import com.infoutility.puerto.Realm.Model.ElectricModel;
import com.infoutility.puerto.Realm.Model.SyncModel;

public class InsertModels {

    Realm realm;

    public InsertModels(){
        realm = Realm.getDefaultInstance();
    }

    public int geId(){
        Number currentId = realm.where(SyncModel.class).max("id");
        int nextId;
        if (currentId == null){
            nextId = 1;
        } else {
            nextId = currentId.intValue() + 1;
        }
        return nextId;
    }


    public void saveElectic(final Response.OnResponse response, final int position, final ElectricModel electricModelTemp){

        realm.executeTransactionAsync(new Realm.Transaction() {
            @Override
            public void execute(Realm bgRealm) {
                ElectricModel electricModel = bgRealm.createObject(ElectricModel.class);
                electricModel.setUel_direccion(electricModelTemp.getUel_direccion());
                electricModel.setUel_contador(electricModelTemp.getUel_contador());
                electricModel.setUel_tar_codigo(electricModelTemp.getUel_tar_codigo());
                electricModel.setMus_lectura(electricModelTemp.getMus_lectura());
                electricModel.setUel_codigo(electricModelTemp.getUel_codigo());
                electricModel.setMus_consumo(electricModelTemp.getMus_consumo());
                electricModel.setMus_mes(electricModelTemp.getMus_mes());
                electricModel.setTar_tarifa_quetzal(electricModelTemp.getTar_tarifa_quetzal());
                electricModel.setUel_numero_tarjeta(electricModelTemp.getUel_numero_tarjeta());
                electricModel.setTar_tarifa_dolar(electricModelTemp.getTar_tarifa_dolar());
                electricModel.setUel_nombre(electricModelTemp.getUel_nombre());


                /*electricModel.setElectricId(position);
                electricModel.setName("Prueba" + position);*/
            }
        }, new Realm.Transaction.OnSuccess() {
            @Override
            public void onSuccess() {
                response.onResponse(Key.requestCode.RESPONSE_REALM, true, position);
            }
        }, new Realm.Transaction.OnError() {
            @Override
            public void onError(Throwable error) {
                response.onResponse(Key.requestCode.RESPONSE_REALM, false, position);
            }
        });
    }


    public void saveSync(final Response.OnResponse response, final SyncModel sync) {
//        realm.executeTransaction(new Realm.Transaction() {
//            @Override
//            public void execute(Realm realm) {
//                realm.insertOrUpdate(sync);
//            }
//        });

        realm.executeTransactionAsync(new Realm.Transaction() {
            @Override
            public void execute(Realm bgRealm) {
                bgRealm.insertOrUpdate(sync);
            }
        }, new Realm.Transaction.OnSuccess() {
            @Override
            public void onSuccess() {
                response.onResponse(Key.requestCode.RESPONSE_REALM, true, null);
            }
        }, new Realm.Transaction.OnError() {
            @Override
            public void onError(Throwable error) {
                response.onResponse(Key.requestCode.RESPONSE_REALM, false, null);
            }
        });
    }


    public boolean deleteSync(final SyncModel syncModel){
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                syncModel.deleteFromRealm();
            }
        });
        return !syncModel.isValid();
    }

}
