package com.infoutility.puerto.Events;

public class Events {

    public static class FragmentAdapterMessage{
        private boolean notifyChange;

        public boolean isNotifyChange() {
            return notifyChange;
        }

        public void setNotifyChange(boolean notifyChange) {
            this.notifyChange = notifyChange;
        }
    }


}
