package com.infoutility.puerto.Fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Spinner;

import com.google.common.collect.ImmutableMap;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.infoutility.puerto.Adapters.PinboxAdapter;
import com.infoutility.puerto.Adapters.SpinnerAdapter;
import com.infoutility.puerto.Adapters.TextAdapter;
import com.infoutility.puerto.Api.ResourceHandler;
import com.infoutility.puerto.Interfaces.Response;
import com.infoutility.puerto.Keys.Key;
import com.infoutility.puerto.MainActivity;
import com.infoutility.puerto.Models.ContainerMeasure;
import com.infoutility.puerto.Models.ContainerType;
import com.infoutility.puerto.Models.Crane;
import com.infoutility.puerto.Models.MoveType;
import com.infoutility.puerto.Models.Pinbox;
import com.infoutility.puerto.Models.PinboxType;
import com.infoutility.puerto.Models.Response.BaseResponse;
import com.infoutility.puerto.R;
import com.infoutility.puerto.Utils.AlertUtil;
import com.infoutility.puerto.Utils.PreferencesManager;
import com.infoutility.puerto.Utils.Tools;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class PinboxFragment extends Fragment implements View.OnClickListener, Response.OnResponse {
    public static final String TAG = PinboxFragment.class.getSimpleName();

    private Spinner spMove;
    private Spinner spMeasure;
    private Spinner spCrane;
    private Spinner spPinboxType;
    private RecyclerView rvPinboxes;
    private Button btnadd;

    private List<Pinbox> lstPinboxes;
    private PinboxAdapter pinboxAdapter;

    private long trip = 0;
    private int recorder = 0;
    private int workMode = 0;
    private long currentShift = 0;
    private String handheld = null;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setHasOptionsMenu(false);

        ActionBar actionBar = ((MainActivity)getActivity()).getSupportActionBar();
        if (actionBar != null)
            actionBar.setTitle(getResources().getString(R.string.fpb_title_add_pinbox));
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_pinbox, container, false);
        PreferencesManager.initializeInstance(getContext());

        trip = PreferencesManager.getInstance().getLong(Key.sharedPreferences.trip, 0L);
        recorder = PreferencesManager.getInstance().getInt(Key.sharedPreferences.recorder, 0);
        workMode = PreferencesManager.getInstance().getInt(Key.sharedPreferences.workMode, 0);
        currentShift = PreferencesManager.getInstance().getLong(Key.sharedPreferences.currentShift, 0L);
        handheld = PreferencesManager.getInstance().getString(Key.sharedPreferences.handheld, null);



        ResourceHandler.listMovementTypes(this, Key.requestCode.GET_MOVE_TYPE);
        ResourceHandler.listCranes(this, Key.requestCode.GET_CRANE);
        ResourceHandler.listPinboxTypes(this, Key.requestCode.GET_PINBOX_TYPE);
        ResourceHandler.listContainerMeasures(this, Key.requestCode.GET_CONTAINER_MEASURES);

        doDataLoad();

        spMove = v.findViewById(R.id.fpb_sp_move);
        spMeasure = v.findViewById(R.id.fpb_sp_measure);
        spCrane = v.findViewById(R.id.fpb_sp_crane);
        spPinboxType = v.findViewById(R.id.fpb_sp_pinboxType);
        rvPinboxes = v.findViewById(R.id.fpb_rv_pinboxes);
        btnadd = v.findViewById(R.id.fpb_btn_add);

        btnadd.setOnClickListener(this);


        lstPinboxes = new ArrayList<>();
        pinboxAdapter = new PinboxAdapter(getContext(), lstPinboxes, this);

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext());
        rvPinboxes.setLayoutManager(mLayoutManager);
        rvPinboxes.setAdapter(pinboxAdapter);

        return v;
    }


    private void doDataLoad(){
        ResourceHandler.listPinboxes(this, Key.requestCode.GET_PINBOX,
                ImmutableMap.of(Key.api.paramTrip, String.valueOf(trip),
                        Key.api.paramRecorder, String.valueOf(recorder),
                        Key.api.paramShift, String.valueOf(currentShift),
                        Key.api.paramMoveType, String.valueOf(workMode)));
        /*ResourceHandler.listPinboxes(this, Key.requestCode.GET_PINBOX,
                ImmutableMap.of(Key.api.paramTrip, String.valueOf(201000000073L),
                        Key.api.paramRecorder, String.valueOf(2128),
                        Key.api.paramShift, String.valueOf(1002),
                        Key.api.paramMoveType, String.valueOf(2)));*/
    }


    private void postPinbox(){
        com.infoutility.puerto.Models.Request.Pinbox pinboxRequest = new com.infoutility.puerto.Models.Request.Pinbox();

        pinboxRequest.setViaje_turno(trip);
        pinboxRequest.setMedida(Integer.valueOf(((TextAdapter)spMeasure.getSelectedItem()).getLabel()));
        pinboxRequest.setGrua(((TextAdapter)spCrane.getSelectedItem()).getId());
        pinboxRequest.setCorrelativo(0);//??
        pinboxRequest.setGrabador(String.valueOf(recorder));
        pinboxRequest.setHandheld_id(String.valueOf(handheld));
        pinboxRequest.setId_movimiento(Integer.valueOf(((TextAdapter)spMove.getSelectedItem()).getId()));
        pinboxRequest.setViaje_empornac(trip);
        pinboxRequest.setTipo_pinbox(Integer.valueOf(((TextAdapter)spPinboxType.getSelectedItem()).getId()));
        pinboxRequest.setTipo_movimiento(workMode);
        pinboxRequest.setId_turno(currentShift);
        pinboxRequest.setFecha_grabacion(Tools.formatDate(new Date(), Key.datetimePattern.YYYYMMDD_HHMMSS_DASH_SEPARATOR));
        String s = new Gson().toJson(pinboxRequest);
        ResourceHandler.postPinbox(this, Key.requestCode.POST_PINBOX, pinboxRequest);
    }

    private void putPinbox(Pinbox pinbox){
        com.infoutility.puerto.Models.Request.Pinbox pinboxRequest = new com.infoutility.puerto.Models.Request.Pinbox();
        pinboxRequest.setViaje_turno(pinbox.getViaje_turno());
        pinboxRequest.setMedida(pinbox.getMedida());
        pinboxRequest.setGrua(pinbox.getGrua());
        pinboxRequest.setCorrelativo(pinbox.getCorrelativo());
        pinboxRequest.setGrabador(pinbox.getGrabador());
        pinboxRequest.setHandheld_id(pinbox.getHandheld_id());
        pinboxRequest.setId_movimiento(pinbox.getId_movimiento());
        pinboxRequest.setViaje_empornac(pinbox.getViaje_empornac());
        pinboxRequest.setTipo_pinbox(pinbox.getTipo_pinbox());
        pinboxRequest.setTipo_movimiento(pinbox.getTipo_movimiento());
        pinboxRequest.setId_turno(pinbox.getId_turno());
        pinboxRequest.setFecha_grabacion(pinbox.getFecha_grabacion());

        ResourceHandler.putPinbox(this, Key.requestCode.PUT_PINBOX, pinboxRequest);
    }

    private void deletePinbox(Pinbox pinbox){
        com.infoutility.puerto.Models.Request.Pinbox pinboxRequest = new com.infoutility.puerto.Models.Request.Pinbox();
        pinboxRequest.setViaje_turno(pinbox.getViaje_turno());
        pinboxRequest.setMedida(pinbox.getMedida());
        pinboxRequest.setGrua(pinbox.getGrua());
        pinboxRequest.setCorrelativo(pinbox.getCorrelativo());
        pinboxRequest.setGrabador(pinbox.getGrabador());
        pinboxRequest.setHandheld_id(pinbox.getHandheld_id());
        pinboxRequest.setId_movimiento(pinbox.getId_movimiento());
        pinboxRequest.setViaje_empornac(pinbox.getViaje_empornac());
        pinboxRequest.setTipo_pinbox(pinbox.getTipo_pinbox());
        pinboxRequest.setTipo_movimiento(pinbox.getTipo_movimiento());
        pinboxRequest.setId_turno(pinbox.getId_turno());
        pinboxRequest.setFecha_grabacion(Tools.formatStrDate(pinbox.getFecha_grabacion(), Key.datetimePattern.YYYYMMDD_HHMMSS_DASH_SEPARATOR));

        ResourceHandler.deletePinbox(this, Key.requestCode.DELETE_PINBOX, pinboxRequest);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.fpb_btn_add:
                postPinbox();
                break;
        }

    }

    @Override
    public void onResponse(int requestCode, Object o, Object r) {
        switch (requestCode){
            case Key.requestCode.GET_PINBOX:
                if (o instanceof JsonObject){
                    lstPinboxes.clear();
                    JsonObject response = (JsonObject) o;
                    lstPinboxes.addAll(Tools.getListFromJson(response.getAsJsonArray(Key.jsonResponse.data).toString(), Pinbox.class));
                    pinboxAdapter.notifyDataSetChanged();
                }
            break;
            case Key.requestCode.GET_MOVE_TYPE:
                if (o instanceof JsonObject){
                    JsonObject response = (JsonObject) o;
                    List<MoveType> lstMove = Tools.getListFromJson(response.getAsJsonArray(Key.jsonResponse.data).toString(), MoveType.class);
                    List<TextAdapter> lstMoveFiltered = new ArrayList<>();
                    for (MoveType mt : lstMove) {
                        if (mt.getTipo_movimiento().equals(String.valueOf(workMode)) && !mt.getId_movimiento().equals(String.valueOf(workMode)))
                            lstMoveFiltered.add(new TextAdapter(mt.getId_movimiento(), mt.getDescripcion()));
                    }
                    SpinnerAdapter adapter = SpinnerAdapter.createArrayAdapter(getActivity(), lstMoveFiltered);
                    spMove.setAdapter(adapter);
                }
                break;
            case Key.requestCode.GET_CRANE:
                if (o instanceof JsonObject){
                    JsonObject response = (JsonObject) o;
                    SpinnerAdapter adapter = SpinnerAdapter.createArrayAdapter(getActivity(), response.getAsJsonArray(Key.jsonResponse.data).toString(), Crane.class);
                    spCrane.setAdapter(adapter);
                }
                break;
            case Key.requestCode.GET_CONTAINER_MEASURES:
                if (o instanceof JsonObject){
                    JsonObject response = (JsonObject) o;
                    SpinnerAdapter adapter = SpinnerAdapter.createArrayAdapter(getActivity(), response.getAsJsonArray(Key.jsonResponse.data).toString(), ContainerMeasure.class);
                    spMeasure.setAdapter(adapter);
                }
                break;
            case Key.requestCode.GET_PINBOX_TYPE:
                if (o instanceof JsonObject){
                    JsonObject response = (JsonObject) o;
                    SpinnerAdapter adapter = SpinnerAdapter.createArrayAdapter(getActivity(), response.getAsJsonArray(Key.jsonResponse.data).toString(), PinboxType.class);
                    spPinboxType.setAdapter(adapter);
                }
                break;
            case Key.requestCode.POST_PINBOX:
                if (o instanceof JsonObject){
                    JsonObject jObject = (JsonObject) o;
                    BaseResponse response = new Gson().fromJson(jObject.toString(), BaseResponse.class);

                    if (response.isResultado()){
                        AlertUtil.showAlertOk(getString(R.string.all_text_success), "Exito", getContext(), new AlertUtil.OnOkListener() {
                            @Override
                            public void onOk() {
                                doDataLoad();
                            }
                        }).setCancelable(false)
                                .show();
                    } else {
                        AlertUtil.showAlertOk(getString(R.string.all_text_error), "Fallo", getContext(), new AlertUtil.OnOkListener() {
                            @Override
                            public void onOk() {
                                // do nothing
                            }
                        }).show();
                    }
                }
                break;
            case Key.requestCode.PINBOX_DELETE:
                if (o instanceof com.infoutility.puerto.Models.Pinbox){
                    deletePinbox((com.infoutility.puerto.Models.Pinbox) o);
                }
                break;
            case Key.requestCode.DELETE_PINBOX:
                if (o instanceof JsonObject){
                    JsonObject jObject = (JsonObject) o;
                    BaseResponse response = new Gson().fromJson(jObject.toString(), BaseResponse.class);

                    AlertUtil.showAlertOk(response.isResultado() ? getString(R.string.all_text_success) : getString(R.string.all_text_error),
                            response.isResultado() ? "Exito Eliminado" : "Fallo Eliminar", getContext(), new AlertUtil.OnOkListener() {
                                @Override
                                public void onOk() {
                                    doDataLoad();
                                }
                            }).setCancelable(false)
                            .show();
                }
                break;
        }
    }
}
