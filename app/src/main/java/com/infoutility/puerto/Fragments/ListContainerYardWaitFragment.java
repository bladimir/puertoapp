package com.infoutility.puerto.Fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.google.common.collect.ImmutableMap;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.infoutility.puerto.Adapters.ListReportRelocation;
import com.infoutility.puerto.Adapters.ListWaitContainer;
import com.infoutility.puerto.Api.ResourceHandler;
import com.infoutility.puerto.DialogFragments.ListRequestDispatchDialogFragment;
import com.infoutility.puerto.Keys.Key;
import com.infoutility.puerto.Models.ContainerPendingList;
import com.infoutility.puerto.Models.MoveType;
import com.infoutility.puerto.R;
import com.infoutility.puerto.Utils.JsonBundle;
import com.infoutility.puerto.Utils.PreferencesManager;
import com.infoutility.puerto.Utils.Tools;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class ListContainerYardWaitFragment extends Fragment implements com.infoutility.puerto.Interfaces.Response.OnResponse, View.OnClickListener{

    public static final String TAG = ReportRelocationFragment.class.getSimpleName();
    public static boolean ACTION_TEMP = false;
    public static String JSON_FORMAT = "";
    RecyclerView recyclerWait;
    List<ContainerPendingList> listWait = null;
    private String oficial = "";

    public ListContainerYardWaitFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_list_container_yard_wait, container, false);
        initView(view);
        return view;
    }

    private void initView(View view){
        PreferencesManager.initializeInstance(getActivity());
        recyclerWait = (RecyclerView) view.findViewById(R.id.flcyw_rv_container);
        oficial = PreferencesManager.getInstance().getInt(Key.sharedPreferences.recorder, 0) + "";
        this.getListYarnWait();
    }

    public void renderRecycler(List<ContainerPendingList> list){
        ListWaitContainer listReportRelocation = new ListWaitContainer(list, this);
        recyclerWait.setAdapter(listReportRelocation);
        GridLayoutManager gridLayoutManager = new GridLayoutManager(getActivity(), 1);
        recyclerWait.setLayoutManager(gridLayoutManager);
    }

    private void getListYarnWait(){
        ResourceHandler.getContainerPending(this, Key.requestCode.GET_LIST_TEMPS,
                ImmutableMap.of(Key.api.paramUserPending, oficial ));
    }

    @Override
    public void onClick(View view) {

    }

    @Override
    public void onResponse(int requestCode, Object o, Object r) {
        if(requestCode == Key.requestCode.GET_LIST_TEMPS){
            try {

                listWait = new ArrayList<>();

                if(o instanceof JsonObject){
                    JsonObject response = (JsonObject) o;
                    List<ContainerPendingList> list  = Tools.getListFromJson(response.getAsJsonArray(Key.jsonResponse.data).toString(), ContainerPendingList.class);
                    this.listWait = list;
                    /*ContainerPendingList temp = list.get(0);
                    String oo = new Gson().toJson(list);
                    Log.i(TAG, oo);*/
                    this.renderRecycler((listWait));
                }

                /*JSONObject jsonObject = new JSONObject(o.toString());
                Bundle bundle = JsonBundle.createBundle(jsonObject);
                this.listWait = bundle.getParcelableArrayList(Key.jsonResponse.data);
                this.renderRecycler((listWait));*/

            }catch (Exception e){

            }
        }else if(requestCode == Key.requestCode.RESPONSE_CARD){
            try{
                if(o instanceof Integer){
                    int positon = (Integer)o;
                    ContainerPendingList containerPendingList = listWait.get(positon);
                    ListContainerYardWaitFragment.ACTION_TEMP = true;
                    ListContainerYardWaitFragment.JSON_FORMAT = new Gson().toJson(containerPendingList);
                    String type = containerPendingList.getTipo_movimiento();

                    switch (type){
                        case "I": //Importacion

                            PreferencesManager.getInstance().setInt(Key.sharedPreferences.workMode, Key.workMode.UNLOAD.id());
                            PreferencesManager.getInstance().setInt(Key.sharedPreferences.confirmedContainer, Key.confirmedContainer.CONFIRMED.id());
                            PreferencesManager.getInstance().setInt(Key.sharedPreferences.yardAction, Key.yardAction.IMPORT.id());
                            Tools.setFragment(getActivity().getSupportFragmentManager(), R.id.fl_main_content, new ImportExportFragment(), ImportExportFragment.TAG);
                            break;
                        case "E": //Export
                            PreferencesManager.getInstance().setInt(Key.sharedPreferences.workMode, Key.workMode.LOAD.id());
                            PreferencesManager.getInstance().setInt(Key.sharedPreferences.confirmedContainer, Key.confirmedContainer.NO_CONFIRMED.id());
                            PreferencesManager.getInstance().setInt(Key.sharedPreferences.yardAction, Key.yardAction.EXPORT.id());
                            Tools.setFragment(getActivity().getSupportFragmentManager(), R.id.fl_main_content, new ImportExportFragment(), ImportExportFragment.TAG);
                            break;
                        case "R": // Recepcion
                            Tools.setFragment(getActivity().getSupportFragmentManager(), R.id.fl_main_content, new ReceptionFragment(), ReceptionFragment.TAG);
                            break;
                        case "D": //Despacho
                            Tools.setFragment(getActivity().getSupportFragmentManager(), R.id.fl_main_content, new DispatchFragment(), DispatchFragment.TAG);
                            break;
                    }
                }

            }catch (Exception e){

            }
        }
    }
}
