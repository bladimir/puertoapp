package com.infoutility.puerto.Fragments;


import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.google.common.collect.ImmutableMap;
import com.infoutility.puerto.Adapters.ListReportRelocation;
import com.infoutility.puerto.Adapters.ListRequestElectricContainer;
import com.infoutility.puerto.Api.ResourceHandler;
import com.infoutility.puerto.DialogFragments.AddReportRelocationDialogFragment;
import com.infoutility.puerto.DialogFragments.ListRequestDialogFragment;
import com.infoutility.puerto.Keys.Key;
import com.infoutility.puerto.R;
import com.infoutility.puerto.Utils.AlertUtil;
import com.infoutility.puerto.Utils.JsonBundle;

import org.json.JSONObject;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class ContainerColdRequestFragment extends Fragment implements com.infoutility.puerto.Interfaces.Response.OnResponse, View.OnClickListener {
    public static final String TAG = ContainerColdRequestFragment.class.getSimpleName();
    ArrayList<Bundle> listRequest;
    RecyclerView recyclerViewListRequest;
    Bundle bundleRequest;
    private ProgressDialog progressDialog;

    public ContainerColdRequestFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_container_cold_request, container, false);
        initView(view);
        return view;
    }

    private void initView(View view){
        recyclerViewListRequest = (RecyclerView) view.findViewById(R.id.fccr_rv_container_request);
        ((Button)view.findViewById(R.id.dccr_btn_search)).setOnClickListener(this);
        ((Button)view.findViewById(R.id.dccr_btn_create)).setOnClickListener(this);
    }

    private void getInfoRequest(){
        try{
            this.initProgressDialog("Buscando.");
            //String request = ((TextInputEditText)getView().findViewById(R.id.dccr_tiet_request)).getText().toString();
            String number = ((TextInputEditText)getView().findViewById(R.id.dccr_tiet_num)).getText().toString();
            String ref = ((TextInputEditText)getView().findViewById(R.id.dccr_tiet_pref)).getText().toString();


            ResourceHandler.getInfoRequest(this, Key.requestCode.GET_INFO_REQUEST,
                    ImmutableMap.of(Key.api.paramPrefijo, ref, Key.api.paramNumberIDentify, number));
            ResourceHandler.getInfoRequestExist(this, Key.requestCode.GET_INFO_REQUEST_EXIST,
                    ImmutableMap.of(Key.api.paramPrefijo, ref, Key.api.paramNumberIDentify, number));
        }catch (Exception e){

        }

    }

    private void setInfoRequest(Bundle bundle){
        bundleRequest = bundle;

        String text = "Vieaje: " + bundle.getString("viaje_sistema", "") + "\n" +
                "Recepcion: " + bundle.getString("recepcion", "") + "\n" +
                "Prefijo: " + bundle.getString("prefijo", "" ) + "\n" +
                "Numero: " + bundle.getString("numero_de_identificacion", "" ) + "\n" +
                "Naviera: " + bundle.getString("naviera", "" ) + "\n" +
                "Barco: " + bundle.getString("barco", "") + "\n" +
                "Fecha conexion: " + bundle.getString("fecha_coneccion", "" ) + "\n" +
                "Fecha desconexion: " + bundle.getString("fecha_desconeccion", "") + "\n";

        ((TextView)getView().findViewById(R.id.dccr_tv_trip_pref)).setText(text);
        ((TextView)getView().findViewById(R.id.dccr_tv_trip_pref)).setVisibility(View.VISIBLE);

        ((TextView)getView().findViewById(R.id.dccr_tv_trip_num)).setText("Numereo: " + bundle.getString("numero_de_identificacion", ""));
    }

    public void renderRecycler(ArrayList<Bundle> list){
        ListRequestElectricContainer listRequestElectricContainer = new ListRequestElectricContainer(list, this);
        recyclerViewListRequest.setAdapter(listRequestElectricContainer);
        GridLayoutManager gridLayoutManager = new GridLayoutManager(getActivity(), 1);
        recyclerViewListRequest.setLayoutManager(gridLayoutManager);
    }

    private void containerDetailReceipt(){
        if(this.bundleRequest == null){
            AlertUtil.showAlertOk(getString(R.string.all_text_warning), "No tiene un item selecconado", getContext(), new AlertUtil.OnOkListener() {
                @Override
                public void onOk() {
//                                idJoined = Integer.valueOf(response.getMessage());
                }
            }).show();
            return;
        }
        DialogFragment fragments = ListRequestDialogFragment.newInstance(this, bundleRequest);
        //fragments.setArguments(Tools.fromObject(Key.extra.CONTAINER_DETAIL, container));
        fragments.show(getChildFragmentManager(), ListRequestDialogFragment.TAG);
    }

    private void initProgressDialog(String msg){
        progressDialog = new ProgressDialog(getContext());
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage(msg);
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.dccr_btn_search:
                this.getInfoRequest();
                break;
            case R.id.dccr_btn_create:
                this.containerDetailReceipt();
                break;
        }
    }

    @Override
    public void onResponse(int requestCode, Object o, Object r) {
        if(this.progressDialog != null) progressDialog.dismiss();
        if(requestCode == Key.requestCode.GET_INFO_REQUEST){
            Log.i(TAG, o.toString());

            try {


                JSONObject jsonObject = new JSONObject(o.toString());
                Bundle bundle = JsonBundle.createBundle(jsonObject);

                if(bundle == null){
                    AlertUtil.showAlertOk(getString(R.string.all_text_warning), "No se ha encontrado ningun item", getContext(), new AlertUtil.OnOkListener() {
                        @Override
                        public void onOk() {
//                                idJoined = Integer.valueOf(response.getMessage());
                        }
                    }).show();
                    return;
                }
                if(!bundle.getBoolean("Resultado", true)){
                    AlertUtil.showAlertOk(getString(R.string.all_text_warning), "No se ha encontrado ningun item", getContext(), new AlertUtil.OnOkListener() {
                        @Override
                        public void onOk() {
//                                idJoined = Integer.valueOf(response.getMessage());
                        }
                    }).show();
                }

                ArrayList<Bundle> listInfo = bundle.getParcelableArrayList(Key.jsonResponse.data);
                this.setInfoRequest(listInfo.get(0));

            }catch (Exception e){

            }
        }else if(requestCode == Key.requestCode.GET_INFO_REQUEST_EXIST){
            Log.i(TAG, o.toString());
            try{

                JSONObject jsonObject = new JSONObject(o.toString());
                Bundle bundle = JsonBundle.createBundle(jsonObject);
                if(bundle == null){
                    return;
                }
                this.listRequest = bundle.getParcelableArrayList(Key.jsonResponse.data);
                this.renderRecycler(this.listRequest);

            }catch (Exception e){

            }
        }else if(requestCode == Key.requestCode.RESPONSE_DIALOG){
            boolean status = (boolean)o;
            if(status){
                getInfoRequest();
                AlertUtil.showAlertOk(getString(R.string.all_text_success), "Creado con exito", getContext(), new AlertUtil.OnOkListener() {
                    @Override
                    public void onOk() {
//                                idJoined = Integer.valueOf(response.getMessage());
                    }
                }).show();
            }else{
                AlertUtil.showAlertOk(getString(R.string.all_text_warning), "Reporte no se puedo crear, por favor intentar luego", getContext(), new AlertUtil.OnOkListener() {
                    @Override
                    public void onOk() {
//                                idJoined = Integer.valueOf(response.getMessage());
                    }
                }).show();
            }
        }
    }
}
