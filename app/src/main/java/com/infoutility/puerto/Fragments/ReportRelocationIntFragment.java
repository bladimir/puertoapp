package com.infoutility.puerto.Fragments;


import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.google.common.collect.ImmutableMap;
import com.infoutility.puerto.Adapters.ListReportRelocation;
import com.infoutility.puerto.Api.ResourceHandler;
import com.infoutility.puerto.DialogFragments.AddReportRelocationIntDialogFragment;
import com.infoutility.puerto.Keys.Key;
import com.infoutility.puerto.R;
import com.infoutility.puerto.Utils.JsonBundle;
import com.infoutility.puerto.Utils.PreferencesManager;
import com.infoutility.puerto.Utils.Tools;

import org.json.JSONObject;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class ReportRelocationIntFragment extends Fragment implements com.infoutility.puerto.Interfaces.Response.OnResponse, View.OnClickListener {
    public static final String TAG = ReportRelocationFragment.class.getSimpleName();
    RecyclerView recyclerViewListReport;
    ArrayList<Bundle> listReportR;
    String oficial = "";

    public ReportRelocationIntFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_report_relocation_int, container, false);
        this.initView(view);
        return view;
    }

    private void initView(View view){
        PreferencesManager.initializeInstance(getActivity());
        recyclerViewListReport = (RecyclerView) view.findViewById(R.id.frri_rv_container_report);
        ((Button)view.findViewById(R.id.frri_btn_create)).setOnClickListener(this);
        oficial = PreferencesManager.getInstance().getInt(Key.sharedPreferences.recorder, 0) + "";
        this.getListReportRelocation();
    }

    private void getListReportRelocation(){
        ResourceHandler.listReportRelocationInt(this, Key.requestCode.GET_LIST_REPORT_RELOCATION,
                ImmutableMap.of(Key.api.paramOfice, oficial, Key.api.paramLast, "5" )); // TODO cambiar variables
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.frri_btn_create:
                containerDetailShow();
                break;
        }
    }

    private void containerDetailShow(){
        DialogFragment fragments = AddReportRelocationIntDialogFragment.newInstance(this);
        //fragments.setArguments(Tools.fromObject(Key.extra.CONTAINER_DETAIL, container));
        fragments.show(getChildFragmentManager(), AddReportRelocationIntDialogFragment.TAG);
    }

    public void renderRecycler(ArrayList<Bundle> list){
        ListReportRelocation listReportRelocation = new ListReportRelocation(list, this, 2);
        recyclerViewListReport.setAdapter(listReportRelocation);
        GridLayoutManager gridLayoutManager = new GridLayoutManager(getActivity(), 1);
        recyclerViewListReport.setLayoutManager(gridLayoutManager);
    }

    @Override
    public void onResponse(int requestCode, Object o, Object r) {
        if(requestCode == Key.requestCode.GET_LIST_REPORT_RELOCATION){
            Log.i(TAG, o.toString());
            try{
                JSONObject jsonObject = new JSONObject(o.toString());
                Bundle bundle = JsonBundle.createBundle(jsonObject);
                this.listReportR = bundle.getParcelableArrayList(Key.jsonResponse.data);
                this.renderRecycler(this.listReportR);
            }catch (Exception e){

            }
        }else if(requestCode == Key.requestCode.RESPONSE_DIALOG){
            boolean status = (boolean)o;
            if(status){
                PreferencesManager.getInstance().setString(Key.sharedPreferences.reportRelocation, (String)r);
                Tools.setFragment(getActivity().getSupportFragmentManager(), R.id.fl_main_content, new ContainerYardFragment(), ContainerYardFragment.TAG);

            }else{
                //TODO alert
            }
        }else if(requestCode == Key.requestCode.RESPONSE_CARD){
            try {
                Log.i(TAG, o.toString());
                int position = (int)o;
                Bundle bundle = this.listReportR.get(position);
                int num = bundle.getInt("num_reporte", 0);
                PreferencesManager.getInstance().setString(Key.sharedPreferences.reportRelocation, String.valueOf(num));
                Tools.setFragment(getActivity().getSupportFragmentManager(), R.id.fl_main_content, new ContainerYardFragment(), ContainerYardFragment.TAG);
            }catch (Exception e){

            }
        }
    }
}
