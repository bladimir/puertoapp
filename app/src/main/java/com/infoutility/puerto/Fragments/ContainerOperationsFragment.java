package com.infoutility.puerto.Fragments;

import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;

import com.google.common.base.Function;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Ordering;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.infoutility.puerto.Adapters.ContainerAdapter;
import com.infoutility.puerto.Api.ResourceHandler;
import com.infoutility.puerto.DialogFragments.AddContainerDialogFragment;
import com.infoutility.puerto.DialogFragments.ContainerDetailDialogFragment;
import com.infoutility.puerto.DialogFragments.ContainerUnjoinDialogFragment;
import com.infoutility.puerto.Events.Events;
import com.infoutility.puerto.Events.GlobalBus;
import com.infoutility.puerto.Interfaces.Response;
import com.infoutility.puerto.Keys.Key;
import com.infoutility.puerto.MainActivity;
import com.infoutility.puerto.Models.Container;
import com.infoutility.puerto.Models.ListConfirmed;
import com.infoutility.puerto.Models.Request.ContainerJoined;
import com.infoutility.puerto.Models.Response.BaseResponse;
import com.infoutility.puerto.Models.Shift;
import com.infoutility.puerto.R;
import com.infoutility.puerto.Utils.AlertUtil;
import com.infoutility.puerto.Utils.PreferencesManager;
import com.infoutility.puerto.Utils.Tools;

import org.checkerframework.checker.nullness.compatqual.NullableDecl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ContainerOperationsFragment extends Fragment implements View.OnClickListener, Response.OnResponse {
    public static final String TAG = ContainerOperationsFragment.class.getSimpleName();

//    View
    private RecyclerView rvContainers;
    private Button btSearch;
    private TextInputEditText tietPrefId;
    private Switch swOrdenFilter;
    private Spinner spFilter;
    private TextView tvConfirmed;

    private MenuItem actionJoinStart;
    private MenuItem actionJoinEnd;


    private ContainerAdapter containerAdapter;
    private List<Container>  lstContainer;
    private List<com.infoutility.puerto.Models.ContainerJoined>  lstContainerJoined;


    private long trip = 0;
    private int workMode = 0;
    private int recorder = 0;
    private long currentShift = 0L;
    private String handheld = null;

    private Bundle bShiftInfo;
    private Shift shift;

    private boolean isFirstJoined;
    private int idJoined = 0;

    private Handler handler;
    private Runnable runnable;
    private boolean restart = false;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_container_operations, container, false);



        PreferencesManager.initializeInstance(getContext());

        bShiftInfo = getArguments();

        if (bShiftInfo != null) {
            shift = Tools.fromBundle(Key.extra.WORK_SHIFT_NAVIGATE, bShiftInfo);
        }

        trip = PreferencesManager.getInstance().getLong(Key.sharedPreferences.trip, 0L);
        workMode = PreferencesManager.getInstance().getInt(Key.sharedPreferences.workMode, 0);
        recorder = PreferencesManager.getInstance().getInt(Key.sharedPreferences.recorder, 0);
        currentShift = PreferencesManager.getInstance().getLong(Key.sharedPreferences.currentShift, 0L);
        handheld = PreferencesManager.getInstance().getString(Key.sharedPreferences.handheld, null);

        if (trip != 0 && workMode != 0)
            doDataLoad();

        ActionBar actionBar = ((MainActivity)getActivity()).getSupportActionBar();
        if (actionBar != null)
            actionBar.setTitle(R.string.fco_title_container_operations);

        rvContainers = v.findViewById(R.id.fco_rv_containers);
        btSearch = v.findViewById(R.id.fco_btn_search);
        tietPrefId = v.findViewById(R.id.fco_tiet_pref_id);
        swOrdenFilter = v.findViewById(R.id.fco_sw_order_filter);
        swOrdenFilter = v.findViewById(R.id.fco_sw_order_filter);
        spFilter = v.findViewById(R.id.fco_sp_filter);
        tvConfirmed = v.findViewById(R.id.fco_tv_confirmed);

        tvConfirmed.setText("Trabajados: 0 | Total: 0");


        spFilter.setAdapter(new ArrayAdapter<>(getActivity(), R.layout.support_simple_spinner_dropdown_item, Key.containerOperationFilter.values()));

        spFilter.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                Key.containerOperationFilter s = (Key.containerOperationFilter)adapterView.getItemAtPosition(i);
                if (s.id() == Key.containerOperationFilter.ALL.id()) defaultFilter();
                else if (s.id() == Key.containerOperationFilter.ORDER.id())  orderFilter();
                else if (s.id() == Key.containerOperationFilter.JOINED.id())  joinedFilter();
                else if (s.id() == Key.containerOperationFilter.MISSING.id())  missingFilter();
                else defaultFilter();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        lstContainer = new ArrayList<>();
        lstContainerJoined = new ArrayList<>();


        containerAdapter = new ContainerAdapter(getContext(), lstContainer, this, lstContainerJoined);

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext());
        rvContainers.setLayoutManager(mLayoutManager);
        rvContainers.setAdapter(containerAdapter);


        btSearch.setOnClickListener(this);


        swOrdenFilter.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b){
                    orderFilter();
//                    confirmFilter();
                } else {
                    defaultFilter();
                }
            }
        });

        getInfoContainerConfirmed();

        startTimer();

        return v;
    }

    private void doDataLoad() {
        Tools.setFragmentLoading(getActivity(), true);
        ResourceHandler.listContainers(this, Key.requestCode.GET_CONTAINER,
                ImmutableMap.of(Key.api.paramTrip, String.valueOf(trip),
                        Key.api.paramWorkMode, String.valueOf(workMode)));
    }

    private void doJoinedLoad(){
//        ResourceHandler.listContainersJoined(this, Key.requestCode.GET_CONTAINER_JOINED, trip, workMode);
        ResourceHandler.listContainersJoined(this, Key.requestCode.GET_CONTAINER_JOINED,
                ImmutableMap.of(Key.api.paramTrip, String.valueOf(trip),
                        Key.api.paramWorkMode, String.valueOf(workMode)));
    }

    private void doSearch(){
        if (trip != 0 && workMode != 0 && tietPrefId.getText() != null){
            Tools.setFragmentLoading(getActivity(), true);
            ResourceHandler.listContainers(this, Key.requestCode.GET_CONTAINER,
                    ImmutableMap.of(Key.api.paramTrip, String.valueOf(trip),
                            Key.api.paramWorkMode, String.valueOf(workMode),
                            Key.api.paramPrefix, tietPrefId.getText().toString()));
        }
    }

    private void doJoin(Container container){
        ContainerJoined containerJoinedRequest = new ContainerJoined();
        containerJoinedRequest.setID_ATADO(idJoined);
        containerJoinedRequest.setVIAJE_NO(String.valueOf(container.getViaje_no()));
        containerJoinedRequest.setTIPO_DE_MOVIMIENTO(container.getTipo_de_movimiento());
        containerJoinedRequest.setPREFIJO(container.getPrefijo());
        containerJoinedRequest.setNUMERO_DE_IDENTIFICACION(container.getNumero_de_identificacion());
        containerJoinedRequest.setGRABADOR(recorder);
        containerJoinedRequest.setFECHA_GRABACION(Tools.formatDate(new Date(), Key.datetimePattern.YYYYMMDD_HHMMSS_DASH_SEPARATOR));
        containerJoinedRequest.setGRABADOR_ACTUALIZA(0);
        containerJoinedRequest.setFECHA_ACTUALIZA("2019-10-27 11:11:11");
        containerJoinedRequest.setHANDHELD_ID(handheld);
        containerJoinedRequest.setID_TURNO(currentShift);
        containerJoinedRequest.setHANDHELD_ID_ACTUALIZA("");

        ResourceHandler.postContainerJoined(this, Key.requestCode.POST_CONTAINER_JOINED, containerJoinedRequest);
    }

    private void doReserved(Container container){
        com.infoutility.puerto.Models.Request.Container ContainerReserved = new com.infoutility.puerto.Models.Request.Container();

        ContainerReserved.setVIAJE_NO(String.valueOf(container.getViaje_no()));
        ContainerReserved.setTIPO_DE_MOVIMIENTO(container.getTipo_de_movimiento());
        ContainerReserved.setORDEN(container.getOrden());
        ContainerReserved.setPREFIJO(container.getPrefijo());
        ContainerReserved.setNUMERO_DE_IDENTIFICACION(container.getNumero_de_identificacion());
        ContainerReserved.setCONFIRMADO("R");
        ContainerReserved.setC_O_F(container.getC_o_f());
        ContainerReserved.setCONDICION(container.getCondicion());
        ContainerReserved.setGRABADOR(recorder);
        ContainerReserved.setFECHA_GRABACION(Tools.formatDate(new Date(), Key.datetimePattern.YYYYMMDD_HHMMSS_DASH_SEPARATOR));
        ContainerReserved.setHANDHELD_ID(handheld);
        ContainerReserved.setID_TURNO(currentShift);
        ContainerReserved.setOBSERVACION_CHEQUE("Reservado");
        ContainerReserved.setID_MOVIMIENTO(workMode);


        ResourceHandler.putContainer(this, Key.requestCode.PUT_CONTAINER_RESERVED, ContainerReserved);
    }

    private void doConfirmed(Container container){
        com.infoutility.puerto.Models.Request.Container ContainerConfirmed = new com.infoutility.puerto.Models.Request.Container();

        ContainerConfirmed.setVIAJE_NO(String.valueOf(container.getViaje_no()));
        ContainerConfirmed.setTIPO_DE_MOVIMIENTO(container.getTipo_de_movimiento());
        ContainerConfirmed.setORDEN(container.getOrden());
        ContainerConfirmed.setPREFIJO(container.getPrefijo());
        ContainerConfirmed.setNUMERO_DE_IDENTIFICACION(container.getNumero_de_identificacion());
        ContainerConfirmed.setCONFIRMADO("C");
        ContainerConfirmed.setGRABADOR(recorder);
        ContainerConfirmed.setFECHA_GRABACION(Tools.formatDate(new Date(), Key.datetimePattern.YYYYMMDD_HHMMSS_DASH_SEPARATOR));
        ContainerConfirmed.setHANDHELD_ID(handheld);
        ContainerConfirmed.setID_TURNO(currentShift);
        ContainerConfirmed.setOBSERVACION_CHEQUE("Confirmado");
        ContainerConfirmed.setID_MOVIMIENTO(workMode);


        ResourceHandler.putContainer(this, Key.requestCode.PUT_CONTAINER_CONFIRMED, ContainerConfirmed);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.fco_btn_search:
                doSearch();
                break;
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        inflater.inflate(R.menu.container_menu, menu);

        actionJoinStart = menu.findItem(R.id.cm_action_join_start);
        actionJoinEnd = menu.findItem(R.id.cm_action_join_end);

        switchMenuItem(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.cm_action_add:
                addContainerShow();
                return true;
            case R.id.cm_action_join_start:
                containerAdapter.setJoinStarted(true);
                switchMenuItem(false);
                return true;
            case R.id.cm_action_join_end:
                this.idJoined = 0;
                containerAdapter.setJoinStarted(false);
                switchMenuItem(true);
                return true;
//            case R.id.cm_action_missing:
//                //
//                Tools.setFragment(getFragmentManager(), R.id.fl_main_content, new MissingContainerFragment(), MissingContainerFragment.TAG );
//                return true;
            case R.id.cm_action_pinbox:
                pinboxShow();
                return true;
            case R.id.cm_action_delay:
                delayShow();
                return true;
            case R.id.cm_action_unjoin:
                unjoinContainerShow();
                return true;
            default: return super.onOptionsItemSelected(item);
        }
    }


    private void switchMenuItem(boolean visible){
        actionJoinStart.setVisible(visible);
        actionJoinEnd.setVisible(!visible);
        doDataLoad();
        containerAdapter.notifyDataSetChanged();
        rvContainers.invalidate();
    }

    private void addContainerShow(){
        DialogFragment fragments = AddContainerDialogFragment.newInstance(this);
        fragments.show(getChildFragmentManager(), AddContainerDialogFragment.TAG);
    }

    private void containerDetailShow(Container container){
        DialogFragment fragments = ContainerDetailDialogFragment.newInstance();
        fragments.setArguments(Tools.fromObject(Key.extra.CONTAINER_DETAIL, container));
        fragments.show(getChildFragmentManager(), ContainerDetailDialogFragment.TAG);
    }

    private void pinboxShow(){
        Tools.setFragment(getFragmentManager(), R.id.fl_main_content, new PinboxFragment(), PinboxFragment.TAG );
    }


    private void delayShow(){
        Tools.setFragment(getFragmentManager(), R.id.fl_main_content, new DelayFragment(), DelayFragment.TAG );
    }

    private void unjoinContainerShow(){
        DialogFragment fragments = ContainerUnjoinDialogFragment.newInstance(this);
        fragments.show(getChildFragmentManager(), ContainerUnjoinDialogFragment.TAG);
    }

    private void confirmFilter(){
        Ordering<Container> order = Ordering.natural().onResultOf(new Function<Container, Boolean>() {
            @NullableDecl
            @Override
            public Boolean apply(@NullableDecl Container input) {
                if (input != null && input.getConfirmado() != null){
                    if (input.getConfirmado().equals("C") || input.getConfirmado().equals("SI"))
                        return true;
                }
                return false;
            }
        });
        lstContainer.sort(order);
        containerAdapter.notifyDataSetChanged();
    }

    private void orderFilter(){
        Ordering<Container> order = Ordering.natural().onResultOf(new Function<Container, Integer>() {
            @NullableDecl
            @Override
            public Integer apply(@NullableDecl Container input) {
                if (input != null)
                    return input.getOrden();
                return 0;
            }
        });
        lstContainer.sort(order);
        containerAdapter.notifyDataSetChanged();
    }



    private void joinedFilter(){
        Ordering<Container> order = Ordering.natural().onResultOf(new Function<Container, Boolean>() {
            @NullableDecl
            @Override
            public Boolean apply(@NullableDecl Container input) {
                if (input != null)
                    return (input.getAtado() != 0);
                return false;
            }
        });
        lstContainer.sort(order);
        containerAdapter.notifyDataSetChanged();
    }


    private void missingFilter(){
        Ordering<Container> order = Ordering.natural().onResultOf(new Function<Container, Boolean>() {
            @NullableDecl
            @Override
            public Boolean apply(@NullableDecl Container input) {
                if (input != null && input.getConfirmado() != null){
                    if (!input.getConfirmado().equals("C") || !input.getConfirmado().equals("SI"))
                        return true;
                }
                return false;
            }
        });
        lstContainer.sort(order);
        containerAdapter.notifyDataSetChanged();
    }

    private void confirmContainerShow(Container container){
        DialogFragment fragments = AddContainerDialogFragment.newInstance(this);
        fragments.setArguments(Tools.fromObject(Key.extra.CONTAINER_CONFIRM, container));
        fragments.show(getChildFragmentManager(), AddContainerDialogFragment.TAG);
    }


    private void defaultFilter(){
        Ordering<Container> order = Ordering.explicit("R", "C", "SI").nullsLast().onResultOf(new Function<Container, String>() {
            @NullableDecl
            @Override
            public String apply(@NullableDecl Container input) {
                if (input != null)
                    return input.getConfirmado();
                return null;
            }
        });
        lstContainer.sort(order);
        containerAdapter.notifyDataSetChanged();
    }

    private void getInfoContainerConfirmed(){
        ResourceHandler.getContainerConfirmed(this, Key.requestCode.GET_CONTAINER_CONFIRMED,
                ImmutableMap.of(Key.api.paramTrip, String.valueOf(trip),
                        Key.api.paramWorkMode, String.valueOf(workMode),
                        Key.api.paramQuantity, String.valueOf(4)));
    }

    private void startTimer(){
        Log.d("TIME--", "INICIO");
        handler = new Handler();
        runnable = new Runnable() {
            @Override
            public void run() {
                showAlert();
            }
        };
        handler.postDelayed(runnable, Key.timer.delay);
    }

    private void stopTimer(){
        if (handler != null && runnable != null){
            Log.d("TIME--", "PARO");
            handler.removeCallbacks(runnable);
        }
    }


    private void showAlert(){
//        restart = true;
        stopTimer();
        if (getContext() != null){
            Log.d("TIME--", "ALERTA");
            AlertUtil.showAlertOk(getString(R.string.all_text_warning), "Alerta de inactividad", getContext(), new AlertUtil.OnOkListener() {
                @Override
                public void onOk() {
                    startTimer();
                }
            }).setCancelable(false).show();
        }
    }

    @Override
    public void onResponse(int requestCode, Object o, Object r) {
        switch (requestCode){
            case Key.requestCode.GET_CONTAINER:
//                Tools.setFragmentLoading(getActivity(), false);
                doJoinedLoad();
                if (o instanceof JsonObject){
                    swOrdenFilter.setChecked(false);
                    lstContainer.clear();
                    JsonObject response = (JsonObject) o;
                    lstContainer.addAll(Tools.getListFromJson(response.getAsJsonArray(Key.jsonResponse.data).toString(), Container.class));
//                    Ordering<Container> containerOrdering = Ordering
//                            .natural()
//                            .nullsFirst()
//                            .onResultOf(new Function<Container, Comparable>() {
//                                @NullableDecl
//                                @Override
//                                public Comparable apply(@NullableDecl Container input) {
//                                    return input.getConfirmado();
//                                }
//                            })
//                            .reverse();

                    Ordering<Container> order = Ordering.explicit("R", "C", "SI").nullsLast().onResultOf(new Function<Container, String>() {
                        @NullableDecl
                        @Override
                        public String apply(@NullableDecl Container input) {
                            if (input != null)
                                return input.getConfirmado();
                            return null;
                        }
                    });
                    lstContainer.sort(order);
                    containerAdapter.notifyDataSetChanged();
                    rvContainers.scrollToPosition(0);
                    spFilter.setSelection(Key.containerOperationFilter.ALL.id());
                }
                break;
            case Key.requestCode.POST_CONTAINER_JOINED:
                if (o instanceof JsonObject){
                    JsonObject jObject = (JsonObject) o;
                    final BaseResponse response = new Gson().fromJson(jObject.toString(), BaseResponse.class);

                    if (response.isResultado()){
                        idJoined = Integer.valueOf(response.getMessage());
                        Events.FragmentAdapterMessage fragmentAdapterMessage = new Events.FragmentAdapterMessage();
                        fragmentAdapterMessage.setNotifyChange(true);
                        GlobalBus.getBus().post(fragmentAdapterMessage);
                        AlertUtil.showAlertOk(getString(R.string.all_text_success), "Exito", getContext(), new AlertUtil.OnOkListener() {
                            @Override
                            public void onOk() {
//                                idJoined = Integer.valueOf(response.getMessage());
                            }
                        }).show();
                    } else {
                        AlertUtil.showAlertOk(getString(R.string.all_text_error), "Fallo", getContext(), new AlertUtil.OnOkListener() {
                            @Override
                            public void onOk() {
                                // do nothing
                            }
                        }).show();
                    }
                }
                break;
            case Key.requestCode.PUT_CONTAINER_RESERVED:
            case Key.requestCode.PUT_CONTAINER_CONFIRMED:
                if (o instanceof JsonObject){
                    JsonObject jObject = (JsonObject) o;
                    final BaseResponse response = new Gson().fromJson(jObject.toString(), BaseResponse.class);
                    if (response.isResultado()){
                        AlertUtil.showAlertOk(getString(R.string.all_text_success), "Exito", getContext(), new AlertUtil.OnOkListener() {
                            @Override
                            public void onOk() {
                                doDataLoad();
                            }
                        }).show();
                    } else {
                        AlertUtil.showAlertOk(getString(R.string.all_text_error), response.getMessage(), getContext(), new AlertUtil.OnOkListener() {
                            @Override
                            public void onOk() {
                                // do nothing
                            }
                        }).show();
                    }
                }
                break;
            case Key.requestCode.CONTAINER_JOIN:
                if (o instanceof Container){
                    doJoin((Container) o);
                }
                break;
            case Key.requestCode.CONTAINER_RESERVED:
                if (o instanceof Container){
                    doReserved((Container) o);
                }
                break;
            case Key.requestCode.CONTAINER_CONFIRMED:
                if (o instanceof Container){
//                    doConfirmed((Container) o);
                    stopTimer();
                    startTimer();
                    confirmContainerShow((Container) o);
//                    final Container container = (Container) o;
//                    AlertUtil.showAlertConfirmation(getString(R.string.all_text_warning),
//                            getString(R.string.all_msg_container_confirm),
//                            getContext(),
//                            getString(R.string.all_text_confirm),
//                            new AlertUtil.OnOkListener() {
//                                @Override
//                                public void onOk() {
//                                    doConfirmed(container);
//                                }
//                            }, new AlertUtil.OnCancelListener() {
//                                @Override
//                                public void onCancel() {
//                                    //do nothing
//                                }
//                            }).show();
                }
                break;
            case Key.requestCode.CONTAINER_SHIP_DETAIL_CLOSED:
                getInfoContainerConfirmed();
                doDataLoad();
                break;
            case Key.requestCode.CONTAINER_DETAIL:
                if (o instanceof Container)
                    containerDetailShow((Container)o);
                break;
            case Key.requestCode.GET_CONTAINER_JOINED:
                Tools.setFragmentLoading(getActivity(), false);
                if (o instanceof JsonObject) {
                    lstContainerJoined.clear();
                    JsonObject response = (JsonObject) o;
                    lstContainerJoined.addAll(Tools.getListFromJson(response.getAsJsonArray(Key.jsonResponse.data).toString(), com.infoutility.puerto.Models.ContainerJoined.class));
                    containerAdapter.notifyDataSetChanged();
                }
                break;
            case Key.requestCode.GET_CONTAINER_CONFIRMED:
                if (o instanceof JsonObject){
                    JsonObject jObject = (JsonObject) o;
                    final ListConfirmed response = new Gson().fromJson(jObject.toString(), ListConfirmed.class);

                    if (response != null){
                        tvConfirmed.setText(String.format("Trabajados: %s | Total: %s", String.valueOf(response.getTrabajados()), response.getTotal()));
                    }
                }
                break;
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        stopTimer();
    }
}
