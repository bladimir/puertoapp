package com.infoutility.puerto.Fragments;


import android.os.Bundle;
import android.support.design.card.MaterialCardView;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.infoutility.puerto.Interfaces.Response;
import com.infoutility.puerto.R;
import com.infoutility.puerto.Utils.Tools;

/**
 * A simple {@link Fragment} subclass.
 */
public class ContainerColdManagerFragment extends Fragment  implements View.OnClickListener, Response.OnResponse {
    public static final String TAG = ContainerColdManagerFragment.class.getSimpleName();

    public ContainerColdManagerFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_container_cold_manager, container, false);
        initView(view);
        return view;
    }

    private void initView(View view){
        ((MaterialCardView)view.findViewById(R.id.fccm_mcv_request)).setOnClickListener(this);
        ((MaterialCardView)view.findViewById(R.id.fccm_mcv_response)).setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.fccm_mcv_request:
                Tools.setFragment(getActivity().getSupportFragmentManager(), R.id.fl_main_content, new ContainerColdRequestFragment(), ContainerColdRequestFragment.TAG);
                break;
            case R.id.fccm_mcv_response:
                Tools.setFragment(getActivity().getSupportFragmentManager(), R.id.fl_main_content, new ContainerColdDispatchFragment(), ContainerColdDispatchFragment.TAG);
                break;
        }
    }

    @Override
    public void onResponse(int requestCode, Object o, Object r) {

    }
}
