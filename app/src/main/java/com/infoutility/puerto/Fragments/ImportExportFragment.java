package com.infoutility.puerto.Fragments;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.common.collect.ImmutableMap;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.infoutility.puerto.Adapters.ContainerYardAdapter;
import com.infoutility.puerto.Adapters.SpinnerAdapter;
import com.infoutility.puerto.Adapters.TextAdapter;
import com.infoutility.puerto.Api.ResourceHandler;
import com.infoutility.puerto.DialogFragments.ImportContainerDialogFragment;
import com.infoutility.puerto.DialogFragments.RevertDialogFragment;
import com.infoutility.puerto.Interfaces.Response;
import com.infoutility.puerto.Keys.Key;
import com.infoutility.puerto.MainActivity;
import com.infoutility.puerto.Models.ContainerPending;
import com.infoutility.puerto.Models.ContainerPendingList;
import com.infoutility.puerto.Models.ContainerShip;
import com.infoutility.puerto.Models.ContainerYard;
import com.infoutility.puerto.Models.Crane;
import com.infoutility.puerto.Models.Machine;
import com.infoutility.puerto.Models.Response.BaseResponse;
import com.infoutility.puerto.R;
import com.infoutility.puerto.Utils.AlertUtil;
import com.infoutility.puerto.Utils.PreferencesManager;
import com.infoutility.puerto.Utils.Tools;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ImportExportFragment extends Fragment implements View.OnClickListener, Response.OnResponse {

    public static final String TAG = ImportExportFragment.class.getSimpleName();

    private TextInputEditText tietTrip;
    private TextInputEditText tietPrefix;
    private TextInputEditText tietId;
    private Button btnSearch;
    private RecyclerView rvContainer;
    private Button btnSearchTrip;
    private TextView tvTripData;
    private Button btnPending;

    private int workMode = 0;
    private int confirmedContainer = 0;
    private int yardAction = 0;
    private boolean isExport;
    private int recorder = 0;


    private List<ContainerYard> lstContainerYard;
    private ContainerYardAdapter containerYardAdapter;

    private ContainerShip containerShipFound;
    private boolean isPending;

    private ContainerPendingList pending;
    private SpinnerAdapter machineAdapter;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_import_export, container, false);

        PreferencesManager.initializeInstance(getContext());
//        ResourceHandler.listCranes(this, Key.requestCode.GET_CRANE);
        ResourceHandler.listMachines(this, Key.requestCode.GET_MACHINE);

        workMode = PreferencesManager.getInstance().getInt(Key.sharedPreferences.workMode, 0);
        recorder = PreferencesManager.getInstance().getInt(Key.sharedPreferences.recorder, 0);
//        workMode = 1;
        confirmedContainer = PreferencesManager.getInstance().getInt(Key.sharedPreferences.confirmedContainer, 0);
        yardAction = PreferencesManager.getInstance().getInt(Key.sharedPreferences.yardAction, 0);
        isExport = yardAction == Key.yardAction.EXPORT.id();

        ActionBar actionBar = ((MainActivity)getActivity()).getSupportActionBar();
        if (actionBar != null)
            actionBar.setTitle( isExport ? R.string.fie_title_export : R.string.fie_title_import);


        tietTrip = v.findViewById(R.id.fie_tiet_trip);
        tietPrefix = v.findViewById(R.id.fie_tiet_prefix);
        tietId = v.findViewById(R.id.fie_tiet_id);
        btnSearch = v.findViewById(R.id.fie_btn_search);
        rvContainer = v.findViewById(R.id.fie_rv_container);
        btnSearchTrip = v.findViewById(R.id.fie_btn_search_trip);
        tvTripData = v.findViewById(R.id.fie_tv_trip_data);
        btnPending = v.findViewById(R.id.fie_btn_pending);

//        if (!isExport){
            btnSearchTrip.setVisibility(View.VISIBLE);
            tvTripData.setVisibility(View.VISIBLE);
//        } else {
//            btnSearchTrip.setVisibility(View.GONE);
//            tvTripData.setVisibility(View.GONE);
//        }

        btnSearch.setOnClickListener(this);
        btnSearchTrip.setOnClickListener(this);
        btnPending.setOnClickListener(this);

        lstContainerYard = new ArrayList<>();
        containerYardAdapter = new ContainerYardAdapter(getContext(), lstContainerYard, this);

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext());
        rvContainer.setLayoutManager(mLayoutManager);
        rvContainer.setAdapter(containerYardAdapter);
        isPending = ListContainerYardWaitFragment.ACTION_TEMP;
        loadPending();

        return v;
    }



    private String validateSearch(){
//        if (tietTrip.getText() == null || tietTrip.getText().toString().trim().equals(""))
        if (containerShipFound == null)
            return "Es necesario ingresar el viaje.";
        if (tietId.getText() == null || tietId.getText().toString().trim().equals(""))
            return "Es necesario ingregar el identificador.";
        if (tietPrefix.getText() == null || tietPrefix.getText().toString().trim().equals(""))
            return "Es necesario ingregar el prefijo.";
        return null;
    }

    private String validateSearchTrip(){
        if (tietTrip.getText() == null || tietTrip.getText().toString().trim().equals(""))
            return "Es necesario ingresar el viaje.";
        return null;
    }

    private void doSearch(){
        String validation = validateSearch();
        if (validation == null){
            ResourceHandler.listContainersYard(this, Key.requestCode.GET_CONTAINER_YARD,
                    ImmutableMap.of(Key.api.paramTrip, String.valueOf(containerShipFound.getViaje_empornac()),
                            Key.api.paramPrefix, tietPrefix.getText().toString(),
                            Key.api.paramIdentification, tietId.getText().toString(),
                            Key.api.paramType, String.valueOf(workMode),
                            Key.api.paramConfirmed, String.valueOf(confirmedContainer)));
        } else {
            AlertUtil.showAlertOk(getString(R.string.all_text_warning), validation, getContext(), new AlertUtil.OnOkListener() {
                @Override
                public void onOk() {

                }
            }).show();
        }

    }

    private void doSearchTrip(){
        String validation = validateSearchTrip();
        if (validation == null){
            ResourceHandler.listTrips(this, Key.requestCode.GET_TRIP, tietTrip.getText().toString() + "%", 1);
        } else {
            AlertUtil.showAlertOk(getString(R.string.all_text_warning), validation, getContext(), new AlertUtil.OnOkListener() {
                @Override
                public void onOk() {

                }
            }).show();
        }
    }

    private void importContainerShow(ContainerYard containerYard){
        DialogFragment fragments = ImportContainerDialogFragment.newInstance(this);
        fragments.setArguments(Tools.fromObject(Key.extra.CONTAINER_IMPORT, containerYard));
        fragments.show(getChildFragmentManager(), ImportContainerDialogFragment.TAG);
    }

    private void exportContainer(ContainerYard containerYard, long selectedMachine){
        com.infoutility.puerto.Models.Request.ContainerYard containerYardRequest = new com.infoutility.puerto.Models.Request.ContainerYard();
        containerYardRequest.setVIAJE_NO(containerYard.getViaje_no());
        containerYardRequest.setTIPO_DE_MOVIMIENTO(containerYard.getTipo_de_movimiento());
        containerYardRequest.setORDEN(containerYard.getOrden());
        containerYardRequest.setPREFIJO(containerYard.getPrefijo());
        containerYardRequest.setNUMERO_DE_IDENTIFICACION(containerYard.getNumero_de_identificacion());
        containerYardRequest.setPATIO(containerYard.getPatio());
        containerYardRequest.setFILA(containerYard.getFila());
        containerYardRequest.setMODULO(containerYard.getModulo());
        containerYardRequest.setNIVEL(containerYard.getNivel());
//        containerYardRequest.setMAQUINA(containerYard.getMaquina());
        containerYardRequest.setMAQUINA(selectedMachine);
        containerYardRequest.setMEDIDA(containerYard.getMedida());
        containerYardRequest.setGRABADOR_IMPOR_PATIO(recorder);
        containerYardRequest.setFECHAHORA_IMPOR_PATIO(Tools.formatDate(new Date(), Key.datetimePattern.YYYYMMDD_HHMMSS_DASH_SEPARATOR));

        ResourceHandler.putContainerYard(this, Key.requestCode.PUT_CONTAINER_YARD, containerYardRequest);
    }

    private void revertImportContainer(ContainerYard containerYard){
        if (containerYard != null){
            if (containerYard.getPatio() != null && containerYard.getFila() != 0 && containerYard.getModulo() != 0 && containerYard.getNivel() != 0){
                com.infoutility.puerto.Models.Request.ContainerYard containerYardRequest = new com.infoutility.puerto.Models.Request.ContainerYard();
                containerYardRequest.setVIAJE_NO(containerYard.getViaje_no());
                containerYardRequest.setTIPO_DE_MOVIMIENTO(containerYard.getTipo_de_movimiento());
                containerYardRequest.setORDEN(containerYard.getOrden());
                containerYardRequest.setPREFIJO(containerYard.getPrefijo());
                containerYardRequest.setNUMERO_DE_IDENTIFICACION(containerYard.getNumero_de_identificacion());
                containerYardRequest.setPATIO(containerYard.getPatio());
                containerYardRequest.setFILA(containerYard.getFila());
                containerYardRequest.setMODULO(containerYard.getModulo());
                containerYardRequest.setNIVEL(containerYard.getNivel());
                containerYardRequest.setMAQUINA(containerYard.getMaquina());
                containerYardRequest.setGRABADOR_IMPOR_PATIO(recorder);
                containerYardRequest.setFECHAHORA_IMPOR_PATIO(Tools.formatDate(new Date(), Key.datetimePattern.YYYYMMDD_HHMMSS_DASH_SEPARATOR));
                ResourceHandler.postContainerYardRevertImport(this, Key.requestCode.POST_CONTAINER_YARD_REVERT_IMPORT, containerYardRequest);
            } else {
                AlertUtil.showAlertOk(getString(R.string.all_text_warning), "Contenedor no asignado", getContext(), new AlertUtil.OnOkListener() {
                    @Override
                    public void onOk() {

                    }
                }).show();
            }
        } else {
            AlertUtil.showAlertOk(getString(R.string.all_text_warning), "No se ha encontrado contenedor.", getContext(), new AlertUtil.OnOkListener() {
                @Override
                public void onOk() {

                }
            }).show();
        }
    }


    private void showExportRevert(ContainerYard containerYard){
        if (containerYard != null){
            DialogFragment fragments = RevertDialogFragment.newInstance(this);
            fragments.setArguments(Tools.fromObject(Key.extra.YARD_CONTAINER_REVERT_EXPORT, containerYard));
            fragments.show(getChildFragmentManager(), RevertDialogFragment.TAG);
        } else {
            AlertUtil.showAlertOk(getString(R.string.all_text_warning), "No se ha seleccionado contendor.", getContext(), new AlertUtil.OnOkListener() {
                @Override
                public void onOk() {
                    // do nothing
                }
            }).show();
        }
    }

    private void cleanForm(){
        tietId.setText(null);
        tietPrefix.setText(null);
        tietTrip.setText(null);
        tvTripData.setText("");
        lstContainerYard.clear();
        containerYardAdapter.notifyDataSetChanged();
    }


    private void doSavePending(){
        String validation = validateSearch();
        if (validation == null){
            ContainerPending containerPending = new ContainerPending();
            containerPending.setREFERENCIA(tietTrip.getText().toString());
            containerPending.setTIPO_MOVIMIENTO(isExport ? "E" : "I");
            containerPending.setPREFIJO(tietPrefix.getText().toString());
            containerPending.setNUMERO_IDENTIFIACION(tietId.getText().toString());
            containerPending.setUSUARIO_GRABADOR(recorder);
            containerPending.setFECHA_GRABACION(Tools.formatDate(new Date(), Key.datetimePattern.YYYYMMDD_HHMMSS_DASH_SEPARATOR));
            containerPending.setPARAMETROS(tietTrip.getText().toString());

            ResourceHandler.postContainerPending(this, Key.requestCode.POST_CONTAINER_PENDING, containerPending);
        } else {
            AlertUtil.showAlertOk(getString(R.string.all_text_warning), "Para guardar como pendiente " + validation.toLowerCase(), getContext(), new AlertUtil.OnOkListener() {
                @Override
                public void onOk() {

                }
            }).show();
        }
    }

    private void loadPending(){
        if (ListContainerYardWaitFragment.ACTION_TEMP && !ListContainerYardWaitFragment.JSON_FORMAT.equals("")){
            ListContainerYardWaitFragment.ACTION_TEMP = false;
            pending = new Gson().fromJson(ListContainerYardWaitFragment.JSON_FORMAT, ContainerPendingList.class);
            tietId.setText(pending.getNumero_identifiacion());
            tietPrefix.setText(pending.getPrefijo());
            tietTrip.setText(String.valueOf(pending.getReferencia()));

            doSearchTrip();

        }
//        else {
//            AlertUtil.showAlertOk(getString(R.string.all_text_warning), "No se pudo cargar la informacion.", getContext(), new AlertUtil.OnOkListener() {
//                @Override
//                public void onOk() {
//
//                }
//            }).show();
//        }
    }

    private void deletePending(){
        if (pending != null){
            ContainerPending containerPending = new ContainerPending();
            containerPending.setREFERENCIA(String.valueOf(pending.getReferencia()));
            containerPending.setTIPO_MOVIMIENTO(pending.getTipo_movimiento());
            containerPending.setPREFIJO(pending.getPrefijo());
            containerPending.setNUMERO_IDENTIFIACION(pending.getNumero_identifiacion());
            containerPending.setUSUARIO_GRABADOR(recorder);
            ResourceHandler.deleteContainerPending(this, Key.requestCode.DELETE_CONTAINER_PENDING, containerPending);
        }
    }

    private void showCrane(final ContainerYard containerYard){
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle("Seleccionar Maquina");
        final Spinner spMachine = new Spinner(getContext());
        final TextView tvMachine = new TextView(getContext());
        LinearLayout container = new LinearLayout(getContext());
        container.setOrientation(LinearLayout.VERTICAL);
        container.addView(spMachine);
        container.addView(tvMachine);
        if(machineAdapter != null){
            spMachine.setAdapter(machineAdapter);
            spMachine.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                    tvMachine.setText("\t" + ((TextAdapter)adapterView.getItemAtPosition(i)).getData());
                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {

                }
            });
            builder.setView(container);

            builder.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    exportContainer(containerYard, Long.valueOf(((TextAdapter)spMachine.getSelectedItem()).getId()));
                }
            });
            builder.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();
                }
            });

            builder.show();
        } else {
            AlertUtil.showAlertOk(getString(R.string.all_text_warning), "No se a podido cargar el listado de maquinas.", getContext(), new AlertUtil.OnOkListener() {
                @Override
                public void onOk() {
                    // do nothing
                }
            }).show();
        }

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.fie_btn_search:
                doSearch();
                break;
            case R.id.fie_btn_search_trip:
                doSearchTrip();
                break;
            case R.id.fie_btn_pending:
                doSavePending();
                break;
        }

    }

    @Override
    public void onResponse(int requestCode, Object o, Object r) {
        switch (requestCode) {
            case Key.requestCode.GET_CONTAINER_YARD:
                if (o instanceof JsonObject) {
                    lstContainerYard.clear();
                    JsonObject response = (JsonObject) o;
                    List<ContainerYard> lst = Tools.getListFromJson(response.getAsJsonArray(Key.jsonResponse.data).toString(), ContainerYard.class);
                    if (lst.size() > 0){
                        lstContainerYard.addAll(lst);
                    } else {
                        AlertUtil.showAlertOk(getString(R.string.all_text_warning), "Sin resultados", getContext(), new AlertUtil.OnOkListener() {
                            @Override
                            public void onOk() {
                                // do nothing
                            }
                        }).show();
                    }

                    containerYardAdapter.notifyDataSetChanged();
                }
                break;
            case Key.requestCode.CONTAINER_YARD_IMPORT:
                if (o instanceof ContainerYard) {
                    importContainerShow((ContainerYard)o);
                }
                break;
            case Key.requestCode.CONTAINER_YARD_EXPORT:
                if (o instanceof ContainerYard) {
                    showCrane((ContainerYard)o);
//                        exportContainer((ContainerYard)o);
                }
                break;
            case Key.requestCode.PUT_CONTAINER_YARD:
                if (o instanceof JsonObject){
                    JsonObject jObject = (JsonObject) o;
                    BaseResponse response = new Gson().fromJson(jObject.toString(), BaseResponse.class);

                    if (response.isResultado()){
                        deletePending();
                        AlertUtil.showAlertOk(getString(R.string.all_text_success), response.getMessage(), getContext(), new AlertUtil.OnOkListener() {
                            @Override
                            public void onOk() {
                                
                            }
                        }).setCancelable(false)
                                .show();
                    } else {
                        AlertUtil.showAlertOk(getString(R.string.all_text_error), response.getMessage(), getContext(), new AlertUtil.OnOkListener() {
                            @Override
                            public void onOk() {
                                // do nothing
                            }
                        }).show();
                    }
                }
                break;
            case Key.requestCode.GET_TRIP:
                Tools.setFragmentLoading(getActivity(), false);
                if (o instanceof JsonObject){
                    JsonObject response = (JsonObject) o;
                    List<ContainerShip> lst = Tools.getListFromJson(response.getAsJsonArray(Key.jsonResponse.data).toString(), ContainerShip.class);
                    if (lst.size() > 0){
                        containerShipFound = lst.get(0);
                        String data = String.format("%s\n%s\n%s\t\t%s",
                                getContext().getString(R.string.ccs_title_container_ship, containerShipFound.getBarco()).replace('\n', ' '),
                                getContext().getString(R.string.ccs_title_date, containerShipFound.getFecha_hora() == null ? "-" : " " + containerShipFound.getFecha_hora()),
                                getContext().getString(R.string.ccs_title_ship_situation, containerShipFound.getSituaciondescripcion()),
                                getContext().getString(R.string.ccs_title_trip, containerShipFound.getViaje_empornac()));
                        tvTripData.setText(data);

                        if (isPending){
                            isPending = false;
                            doSearch();
                        }
                    } else {
                        AlertUtil.showAlertOk(getString(R.string.all_text_warning), "No se encontro viaje", getContext(), new AlertUtil.OnOkListener() {
                            @Override
                            public void onOk() {
                                // do nothing
                            }
                        }).show();
                    }
                }
                break;
            case Key.requestCode.CONTAINER_YARD_REVERT_IMPORT:
                if (o instanceof ContainerYard) {
                    revertImportContainer((ContainerYard)o);
                }
                break;
            case Key.requestCode.POST_CONTAINER_YARD_REVERT_IMPORT:
            case Key.requestCode.POST_CONTAINER_PENDING:
                if (o instanceof JsonObject){
                    JsonObject jObject = (JsonObject) o;
                    BaseResponse response = new Gson().fromJson(jObject.toString(), BaseResponse.class);

                    if (response.isResultado()){
                        deletePending();
                        cleanForm();
                        AlertUtil.showAlertOk(getString(R.string.all_text_success), response.getMessage(), getContext(), new AlertUtil.OnOkListener() {
                            @Override
                            public void onOk() {

                            }
                        }).setCancelable(false)
                                .show();
                    } else {
                        AlertUtil.showAlertOk(getString(R.string.all_text_error), response.getMessage(), getContext(), new AlertUtil.OnOkListener() {
                            @Override
                            public void onOk() {

                            }
                        }).setCancelable(true).show();
                    }
                }
                break;
            case Key.requestCode.RESPONSE_DIALOG:
                deletePending();
                cleanForm();
                break;
            case Key.requestCode.CONTAINER_YARD_REVERT_EXPORT:
                if (o instanceof ContainerYard) {
                    showExportRevert((ContainerYard)o);
                }
                break;
            case Key.requestCode.DELETE_CONTAINER_PENDING:
                if (o instanceof JsonObject){
                    JsonObject jObject = (JsonObject) o;
                    BaseResponse response = new Gson().fromJson(jObject.toString(), BaseResponse.class);

                    if (response.isResultado()){
                        pending = null;
                        cleanForm();
                        AlertUtil.showAlertOk(getString(R.string.all_text_success), response.getMessage(), getContext(), new AlertUtil.OnOkListener() {
                            @Override
                            public void onOk() {

                            }
                        }).setCancelable(false)
                                .show();
                    } else {
                        AlertUtil.showAlertOk(getString(R.string.all_text_error), response.getMessage(), getContext(), new AlertUtil.OnOkListener() {
                            @Override
                            public void onOk() {

                            }
                        }).setCancelable(true).show();
                    }
                }
                break;
            case Key.requestCode.GET_CRANE:
                if (o instanceof JsonObject){
                    JsonObject response = (JsonObject) o;
                    machineAdapter = SpinnerAdapter.createArrayAdapter(getActivity(), response.getAsJsonArray(Key.jsonResponse.data).toString(), Crane.class);
                }
                break;
            case Key.requestCode.GET_MACHINE:
                if (o instanceof JsonObject) {
                    JsonObject response = (JsonObject) o;
                    machineAdapter = SpinnerAdapter.createArrayAdapter(getActivity(), response.getAsJsonArray(Key.jsonResponse.data).toString(), Machine.class);
                }
                break;
        }
    }
}
