package com.infoutility.puerto.Fragments;

import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.google.common.collect.ImmutableMap;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.infoutility.puerto.Api.ResourceHandler;
import com.infoutility.puerto.Interfaces.Response;
import com.infoutility.puerto.Keys.Key;
import com.infoutility.puerto.Models.Meter;
import com.infoutility.puerto.Models.MeterUser;
import com.infoutility.puerto.R;
import com.infoutility.puerto.Utils.AlertUtil;
import com.infoutility.puerto.Utils.PreferencesManager;
import com.infoutility.puerto.Utils.Tools;

public class ElectricPowerMeterVerifyFragment extends Fragment implements View.OnClickListener, Response.OnResponse {
    public static final String TAG = ElectricPowerMeterVerifyFragment.class.getSimpleName();

    private TextInputEditText tiet;
    private Button button;

    public static ElectricPowerMeterVerifyFragment newInstance(){
        return new ElectricPowerMeterVerifyFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_electric_power_meter_verify, container, false);
        PreferencesManager.initializeInstance(getContext());

        tiet = v.findViewById(R.id.tiet);
        button = v.findViewById(R.id.button);

        button.setOnClickListener(this);

        return v;
    }

    private String validate(){
        if (tiet.getText() == null || tiet.getText().toString().trim().equals(""))
            return "Debe ingresar el contador";
        return null;
    }


    private void doVerify(){

        String validate = validate();
        if (validate == null){
            ResourceHandler.getMeter(this, Key.requestCode.GET_METER, ImmutableMap.of(Key.api.paramToken, tiet.getText().toString()));
        } else {
            AlertUtil.showAlertOk(getString(R.string.all_text_warning), validate, getContext(), new AlertUtil.OnOkListener() {
                @Override
                public void onOk() {
                    // do nothing
                }
            }).show();
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.button:
                doVerify();
                break;
        }
    }

    @Override
    public void onResponse(int requestCode, Object o, Object r) {
        switch (requestCode) {
            case Key.requestCode.GET_METER:
                if (o instanceof JsonObject) {
                    JsonObject response = (JsonObject) o;
                    if (response.getAsJsonArray(Key.jsonResponse.data).size() > 0) {
                        Meter meter = new Gson().fromJson(response.getAsJsonArray(Key.jsonResponse.data).get(0), Meter.class);
                        PreferencesManager.getInstance().setString(Key.sharedPreferences.meter, meter.getFicha());
                        //Tools.setFragment(getFragmentManager(), R.id.fl_main_content, new ElectricPowerMeterFragmente(), ElectricPowerMeterFragmente.TAG);
                        Tools.setFragment(getFragmentManager(), R.id.fl_main_content, new ElectricPowerMeterSearchFragment(), ElectricPowerMeterSearchFragment.TAG);
                        //Tools.setFragment(getFragmentManager(), R.id.fl_main_content, new SyncElectricDataFragment(), SyncElectricDataFragment.TAG);
                    } else {
                        AlertUtil.showAlertOk(getString(R.string.all_text_warning), "No se pudo verificar el lector.", getContext(), new AlertUtil.OnOkListener() {
                            @Override
                            public void onOk() {
                                // do nothing
                            }
                        }).show();
                    }
                }
                break;
        }
    }
}
