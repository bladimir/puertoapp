package com.infoutility.puerto.Fragments;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.google.common.collect.ImmutableMap;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.infoutility.puerto.Api.ResourceHandler;
import com.infoutility.puerto.Api.Status;
import com.infoutility.puerto.DialogFragments.MeterReceiptDialogFragment;
import com.infoutility.puerto.Interfaces.Response;
import com.infoutility.puerto.Keys.Key;
import com.infoutility.puerto.MainActivity;
import com.infoutility.puerto.Models.Consumption;
import com.infoutility.puerto.Models.ConsumptionHistory;
import com.infoutility.puerto.Models.ConsumptionWrp;
import com.infoutility.puerto.Models.MeterReceipt;
import com.infoutility.puerto.Models.MeterUser;
import com.infoutility.puerto.Models.Response.BaseResponse;
import com.infoutility.puerto.R;
import com.infoutility.puerto.Realm.InsertModels;
import com.infoutility.puerto.Realm.Model.ElectricModel;
import com.infoutility.puerto.Realm.Model.SyncModel;
import com.infoutility.puerto.Utils.AlertUtil;
import com.infoutility.puerto.Utils.PreferencesManager;
import com.infoutility.puerto.Utils.Tools;

import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class ElectricPowerMeterFragmente extends Fragment implements View.OnClickListener, Response.OnResponse {
    public static final String TAG = ElectricPowerMeterFragmente.class.getSimpleName();


    private TextInputEditText tietMeterNumber;
    private TextInputEditText tietClient;
    private Button btnSearch;
    private TextView tvMeter;
    private TextView tvClient;
    private TextView tvAddress;
    private TextView tvReadPrevMeter;
    private TextView tvReadPrevAmount;
    private TextView tvReadPrev;
    private TextView tvReadMonthMeter;
    private TextView tvReadMonthAmount;
    private EditText etReadCurrent;
    private Button btnPrevMeter;
    private Button btnGenerate;
    private Button btnRecord;
    private Button btnAdd;


    private MeterUser meterUserFound;
    private ConsumptionHistory previousConsumption;
    private boolean isSave;
    private boolean isReceipt;
    private long consumption = 0;
    private double amountConsumption;
    private String meter;
    private int recorder = 0;

    private ElectricModel electricModelFound;
    private InsertModels insertModels;


    private static DecimalFormat df2 = new DecimalFormat("#.##");

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_electric_power_meter, container, false);

        PreferencesManager.initializeInstance(getContext());
        recorder = PreferencesManager.getInstance().getInt(Key.sharedPreferences.recorder, 0);
        meter = PreferencesManager.getInstance().getString(Key.sharedPreferences.meter, null);

        ActionBar actionBar = ((MainActivity)getActivity()).getSupportActionBar();
        if (actionBar != null)
            actionBar.setTitle( "Lectura de Contadores");

        tietMeterNumber = v.findViewById(R.id.fepm_tiet_meter_number);
        tietClient = v.findViewById(R.id.fepm_tiet_client);
        btnSearch = v.findViewById(R.id.fepm_btn_search);
        tvMeter = v.findViewById(R.id.fepm_tv_meter);
        tvClient = v.findViewById(R.id.fepm_tv_client);
        tvAddress = v.findViewById(R.id.fepm_tv_address);
        tvReadPrevMeter = v.findViewById(R.id.fepm_tv_read_prev_meter);
        tvReadPrevAmount = v.findViewById(R.id.fepm_tv_read_prev_amount);
        tvReadPrev = v.findViewById(R.id.fepm_tv_read_prev);
        tvReadMonthMeter = v.findViewById(R.id.fepm_tv_read_month_meter);
        tvReadMonthAmount = v.findViewById(R.id.fepm_tv_read_month_amount);
        etReadCurrent = v.findViewById(R.id.fepm_et_read_current);
        btnPrevMeter = v.findViewById(R.id.fepm_btn_prev_meter);
        btnGenerate = v.findViewById(R.id.fepm_btn_generate);
        btnRecord = v.findViewById(R.id.fepm_btn_record);
        btnAdd = v.findViewById(R.id.fepm_btn_add);

        btnSearch.setOnClickListener(this);
        btnGenerate.setOnClickListener(this);
        btnPrevMeter.setOnClickListener(this);
        btnRecord.setOnClickListener(this);
        btnAdd.setOnClickListener(this);

        etReadCurrent.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (charSequence.length() > 0){
                    setDataCurrentConsumption(Long.valueOf(charSequence.toString()));
                } else {
                    tvReadMonthMeter.setText("");
                    tvReadMonthAmount.setText("");
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        df2.setRoundingMode(RoundingMode.UP);
        loadPending();
        setData();
        setDataPreviousConsumption();

        insertModels = new InsertModels();

        return v;
    }

    private void setData(){
        if (meterUserFound != null){
            tvMeter.setText("" + meterUserFound.getUel_contador());
            tvClient.setText("" + meterUserFound.getUel_nombre());
            tvAddress.setText(meterUserFound.getUel_direccion());
        } else {
            tvMeter.setText("");
            tvClient.setText("");
            tvAddress.setText("");
        }
    }


    private void setDataPreviousConsumption(){
        if (previousConsumption != null){
            tvReadPrevMeter.setText("" + previousConsumption.getMus_consumo() + " KW/hr");
            tvReadPrevAmount.setText("Q." + df2.format(previousConsumption.getMus_consumo() * meterUserFound.getTar_tarifa_quetzal()));
            tvReadPrev.setText("" + previousConsumption.getMus_lectura());
        } else {
            tvReadPrevMeter.setText("");
            tvReadPrevAmount.setText("");
            tvReadPrev.setText("");
        }
    }


    private void setDataCurrentConsumption(long currentRead){
        if (meterUserFound != null && previousConsumption != null){
            consumption = currentRead - previousConsumption.getMus_lectura();
            amountConsumption = consumption * meterUserFound.getTar_tarifa_quetzal();
            tvReadMonthMeter.setText("" + (consumption) + " KW/hr");
            tvReadMonthAmount.setText("Q." + df2.format(amountConsumption));
        } else {
            tvReadMonthMeter.setText("");
            tvReadMonthAmount.setText("");
        }
    }

    private String validate(){
        if(!isSave) {
//            if (tietClient.getText() == null || tietClient.getText().toString().trim().equals(""))
//                return "Debe ingresar el cliente";
//            if (tietMeterNumber.getText() == null || tietMeterNumber.getText().toString().trim().equals(""))
//                return "Debe ingresar el numero de contador";
        }

        if (isSave || isReceipt){
            if (consumption < 0)
                return "El consumo no es valido";
            if (amountConsumption < 0)
                return "El monto no es valido";
            if (meterUserFound == null)
                return "El contador no es valido";
            if (previousConsumption == null)
                return "El consumo anterior no es valido";
            if (meter == null)
                return "El lector no es valido";
            if (recorder == 0)
                return "El grabador no es valido";
            if (etReadCurrent == null)
                return "Debe ingresar lectura";
        }

        return null;
    }

    private void doSearch(String metetNumber, String client) {
        String validate = validate();
        if (validate == null){
            ResourceHandler.getMeterUser(this, Key.requestCode.GET_METER_USER,
                    ImmutableMap.of(Key.api.paramMeter, tietMeterNumber.getText().toString(),
                            Key.api.paramUserCode, tietClient.getText().toString()));
        } else {
            AlertUtil.showAlertOk(getString(R.string.all_text_warning), validate, getContext(), new AlertUtil.OnOkListener() {
                @Override
                public void onOk() {
                    // do nothing
                }
            }).show();
        }
    }


    private void doGetRates(){
        ResourceHandler.getRate(this, Key.requestCode.GET_METER_RATE);
    }

    private void doPreviousConsumption() {
        Date current = new Date();
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(current);
        String year = String.valueOf(calendar.get(Calendar.YEAR));
        if (meterUserFound != null){
            ResourceHandler.getConsumptionHistory(this, Key.requestCode.GET_METER_HISTORY_CONSUMPTION,
                    ImmutableMap.of(Key.api.paramMeter, String.valueOf(meterUserFound.getUel_contador()),
                            Key.api.paramUserCode, String.valueOf(meterUserFound.getUel_codigo()),
                            Key.api.paramYear, year));
        }
    }


    private void actionShowReceipt(){
        isReceipt = true;
        String validate = validate();
        if (validate == null){
            isReceipt = false;
            MeterReceipt meterReceipt = new MeterReceipt();
            meterReceipt.setNoContador(String.valueOf(meterUserFound.getUel_contador()));
            meterReceipt.setNit("-");
            meterReceipt.setNombre(meterUserFound.getUel_nombre());
            meterReceipt.setConsumoAnterior(String.valueOf(previousConsumption.getMus_consumo()));
            meterReceipt.setLecturaAnterior(String.valueOf(previousConsumption.getMus_lectura()));
            meterReceipt.setLecturaActual(etReadCurrent.getText().toString());
            meterReceipt.setConsumo(String.valueOf(consumption));
            meterReceipt.setTotal(String.valueOf(df2.format(amountConsumption)));
            meterReceipt.setCodigo(String.valueOf(meterUserFound.getUel_codigo()));
            meterReceipt.setMesAnterior(previousConsumption.getMus_mes());
            meterReceipt.setAnioAnterior(previousConsumption.getMus_anio());

            DialogFragment fragments = MeterReceiptDialogFragment.newInstance(this);
            fragments.setArguments(Tools.fromObject(Key.extra.METER_RECEIPT, meterReceipt));
            fragments.show(getChildFragmentManager(), MeterReceiptDialogFragment.TAG);
        } else {
            isReceipt = false;
            AlertUtil.showAlertOk(getString(R.string.all_text_warning), validate, getContext(), new AlertUtil.OnOkListener() {
                @Override
                public void onOk() {
                    // do nothing
                }
            }).show();
        }


    }

    private void previusMeterRead(){
        if (previousConsumption != null){
            etReadCurrent.setText(String.valueOf(previousConsumption.getMus_consumo() + previousConsumption.getMus_lectura()));
        } else {
            AlertUtil.showAlertOk(getString(R.string.all_text_warning), "No existe consumo anterior.", getContext(), new AlertUtil.OnOkListener() {
                @Override
                public void onOk() {
                    // do nothing
                }
            }).show();
        }
    }

    private void doSaveConsumption(){
        String validate = validate();
        if (validate == null) {
            Date current = new Date();
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(current);
            Consumption consumptionRequest = new Consumption();
            consumptionRequest.setMus_uel_contador(String.valueOf(meterUserFound.getUel_contador()));
            consumptionRequest.setMus_lectura(Long.valueOf(etReadCurrent.getText().toString()));
            consumptionRequest.setMus_uel_codigo(meterUserFound.getUel_codigo());
            consumptionRequest.setMus_consumo(consumption);
            consumptionRequest.setMus_mes(calendar.get(Calendar.MONTH));
            consumptionRequest.setMus_observaciones("SIN COMENTARIOS");
            consumptionRequest.setMus_anio(String.valueOf(calendar.get(Calendar.YEAR)));
            consumptionRequest.setGrabador(recorder);
            consumptionRequest.setMus_lector(meter);
            consumptionRequest.setMus_lectura_anterior(previousConsumption.getMus_lectura());
            consumptionRequest.setFecha_grabacion(Tools.formatDate(current, Key.datetimePattern.YYYYMMDD_HHMMSS_DASH_SEPARATOR));
            consumptionRequest.setMus_exceso(0);
            consumptionRequest.setMus_total(0);

            ResourceHandler.postConsumptionHistory(this, Key.requestCode.POST_METER_HISTORY_CONSUMPTION, new ConsumptionWrp(-1, consumptionRequest));

        } else {
            AlertUtil.showAlertOk(getString(R.string.all_text_warning), validate, getContext(), new AlertUtil.OnOkListener() {
                @Override
                public void onOk() {
                    // do nothing
                }
            }).show();
        }
    }

    private void addMeterRead(){

        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle("Agregar");

        final EditText input = new EditText(getContext());
        input.setInputType(InputType.TYPE_CLASS_NUMBER);
        builder.setView(input);

        builder.setPositiveButton("Agregar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
//                if (etReadCurrent.getText() != null && !etReadCurrent.getText().toString().equals("")){
//                    int addMeterRecord = Integer.valueOf(input.getText().toString());
//                    tvReadPrevMeter.setText("" + (previousConsumption.getMus_consumo() + addMeterRecord) + " KW/hr");
//                    etReadCurrent.setText(Integer.valueOf(etReadCurrent.getText().toString()) + addMeterRecord);
//                }

                long addMeterRecord = Long.valueOf(input.getText().toString());
                if (previousConsumption != null){
                    long value = previousConsumption.getMus_lectura() + addMeterRecord;
                    previousConsumption.setMus_lectura(value);
                    tvReadPrev.setText(String.valueOf(previousConsumption.getMus_lectura()));
                    //previusMeterRead();
                } else {
                    AlertUtil.showAlertOk(getString(R.string.all_text_warning), "No se puede agregar.", getContext(), new AlertUtil.OnOkListener() {
                        @Override
                        public void onOk() {
                            // do nothing
                        }
                    }).show();
                }
            }
        });
        builder.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        builder.show();
    }


    private void loadPending(){
        if (ElectricPowerMeterSearchFragment.ACTION_TEMP && !ElectricPowerMeterSearchFragment.JSON_FORMAT.equals("")){
            ElectricPowerMeterSearchFragment.ACTION_TEMP = false;
//            meterUserFound = new Gson().fromJson(ElectricPowerMeterSearchFragment.JSON_FORMAT, MeterUser.class);
            electricModelFound = new Gson().fromJson(ElectricPowerMeterSearchFragment.JSON_FORMAT, ElectricModel.class);
            if (electricModelFound != null){
                meterUserFound = new MeterUser();
                meterUserFound.setUel_nombre(electricModelFound.getUel_nombre());
                meterUserFound.setUel_direccion(electricModelFound.getUel_direccion());
                meterUserFound.setUel_contador(electricModelFound.getUel_contador());
                meterUserFound.setUel_tar_codigo(electricModelFound.getUel_tar_codigo());
                meterUserFound.setUel_numero_tarjeta(electricModelFound.getUel_numero_tarjeta());
                meterUserFound.setUel_codigo(electricModelFound.getUel_codigo());
                meterUserFound.setTar_tarifa_quetzal(electricModelFound.getTar_tarifa_quetzal());
                meterUserFound.setTar_tarifa_dolar(electricModelFound.getTar_tarifa_dolar());

//                doPreviousConsumption();
                //Consumo Previo
                previousConsumption = new ConsumptionHistory();
                previousConsumption.setMus_lectura(electricModelFound.getMus_lectura());
                previousConsumption.setMus_consumo(electricModelFound.getMus_consumo());
                previousConsumption.setMus_mes(electricModelFound.getMus_mes());
                previousConsumption.setMus_anio(electricModelFound.getMus_anio());


            } else {
                AlertUtil.showAlertOk(getString(R.string.all_text_warning), "No se pudo cargar la informacion.", getContext(), new AlertUtil.OnOkListener() {
                    @Override
                    public void onOk() {

                    }
                }).show();
            }
        }
        else {
            AlertUtil.showAlertOk(getString(R.string.all_text_warning), "No se pudo cargar la informacion.", getContext(), new AlertUtil.OnOkListener() {
                @Override
                public void onOk() {

                }
            }).show();
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.fepm_btn_search:
//                doSearch();
                break;
            case R.id.fepm_btn_prev_meter:
//                doPreviousConsumption();
                previusMeterRead();
                break;
            case R.id.fepm_btn_generate:
                actionShowReceipt();
                break;
            case R.id.fepm_btn_record:
                isSave = true;
                doSaveConsumption();
                break;
            case R.id.fepm_btn_add:
                isSave = true;
                addMeterRead();
                break;
        }
    }



    @Override
    public void onResponse(int requestCode, Object o, Object r) {
        switch (requestCode) {
            case Key.requestCode.GET_METER_USER:
                if (o instanceof JsonObject) {
                    JsonObject response = (JsonObject) o;
                    if (response.getAsJsonArray(Key.jsonResponse.data).size() > 0) {
                        meterUserFound = new Gson().fromJson(response.getAsJsonArray(Key.jsonResponse.data).get(0), MeterUser.class);
                        doPreviousConsumption();
                    } else {
                        AlertUtil.showAlertOk(getString(R.string.all_text_warning), "No existen resultados", getContext(), new AlertUtil.OnOkListener() {
                            @Override
                            public void onOk() {
                                // do nothing
                            }
                        }).show();
                    }

                    setData();
                }
                break;
            case Key.requestCode.GET_METER_HISTORY_CONSUMPTION:
                if (o instanceof JsonObject) {
                    JsonObject response = (JsonObject) o;
                    List<ConsumptionHistory> lst = Tools.getListFromJson(response.getAsJsonArray(Key.jsonResponse.data).toString(), ConsumptionHistory.class);

                    if (lst.size() > 0){
                        previousConsumption = lst.get(0);
                    } else {
                        AlertUtil.showAlertOk(getString(R.string.all_text_warning), "No existen resultados de consumo anterior", getContext(), new AlertUtil.OnOkListener() {
                            @Override
                            public void onOk() {
                                // do nothing
                            }
                        }).show();
                    }

                    setDataPreviousConsumption();
                }
                break;
            case Key.requestCode.POST_METER_HISTORY_CONSUMPTION:
                isSave = false;
                if (o instanceof JsonObject){
                    JsonObject jObject = (JsonObject) o;
                    BaseResponse response = new Gson().fromJson(jObject.toString(), BaseResponse.class);

                    if (response.isResultado()){
                        AlertUtil.showAlertOk(getString(R.string.all_text_success), response.getMessage(), getContext(), new AlertUtil.OnOkListener() {
                            @Override
                            public void onOk() {

                            }
                        }).setCancelable(false)
                                .show();
                    } else {
                        AlertUtil.showAlertOk(getString(R.string.all_text_error), response.getMessage(), getContext(), new AlertUtil.OnOkListener() {
                            @Override
                            public void onOk() {
                                // do nothing
                            }
                        }).show();
                    }
                }  else if (o instanceof Status) {
                    Status status = (Status)o;
                    if (status.getCode() == Key.statusCode.noInternet.code){
                        if (r instanceof ConsumptionWrp) {
                            String request = new Gson().toJson(((ConsumptionWrp) r).getConsumption());
                            SyncModel sync = new SyncModel();
                            sync.setId(insertModels.geId());
                            sync.setData(request);
                            insertModels.saveSync(this, sync);

                            AlertUtil.showAlertOk(getString(R.string.all_text_warning), "No hay conexion a internet se agregara a bandeja de sincronizacion.", getContext(), new AlertUtil.OnOkListener() {
                                @Override
                                public void onOk() {
                                    // do nothing
                                }
                            }).show();
                        } else {
                            AlertUtil.showAlertOk(getString(R.string.all_text_warning), "No existe informacion para agregar a bandeja de sincronizacion.", getContext(), new AlertUtil.OnOkListener() {
                                @Override
                                public void onOk() {
                                    // do nothing
                                }
                            }).show();
                        }
                    } else {
                        AlertUtil.showAlertOk(getString(R.string.all_text_warning), "Error desconocido, no se ha podido agregar a bandeja de sincronizacion.", getContext(), new AlertUtil.OnOkListener() {
                            @Override
                            public void onOk() {
                                // do nothing
                            }
                        }).show();
                    }
                }
                break;
            case Key.requestCode.RESPONSE_REALM:
                if (o instanceof Boolean){
                    boolean status = (Boolean)o;
                    if(status){
                        AlertUtil.showAlertOk(getString(R.string.all_text_warning), "Se ha agregado a bandeja de sincronizacion.", getContext(), new AlertUtil.OnOkListener() {
                            @Override
                            public void onOk() {
                                // do nothing
                            }
                        }).show();
                    } else {
                        AlertUtil.showAlertOk(getString(R.string.all_text_error), "No se ha agregado a bandeja de sincronizacion.", getContext(), new AlertUtil.OnOkListener() {
                            @Override
                            public void onOk() {
                                // do nothing
                            }
                        }).show();
                    }
                }
                break;

        }
    }
}
