package com.infoutility.puerto.Fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.card.MaterialCardView;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Spinner;

import com.google.gson.JsonObject;
import com.infoutility.puerto.Adapters.SpinnerAdapter;
import com.infoutility.puerto.Adapters.TextAdapter;
import com.infoutility.puerto.Api.ResourceHandler;
import com.infoutility.puerto.Interfaces.Response;
import com.infoutility.puerto.Keys.Key;
import com.infoutility.puerto.Models.Yard;
import com.infoutility.puerto.R;
import com.infoutility.puerto.Utils.AlertUtil;
import com.infoutility.puerto.Utils.PreferencesManager;
import com.infoutility.puerto.Utils.Tools;

public class ContainerYardManagementFragment extends Fragment implements View.OnClickListener, Response.OnResponse {
    public static final String TAG = ContainerYardManagementFragment.class.getSimpleName();

    private MaterialCardView mcvRelocation;
    private MaterialCardView mcvImport;
    private MaterialCardView mcvExport;

    private MaterialCardView mcvRelocationIng;

    private MaterialCardView mcvReception;
    private MaterialCardView mcvDispatch;

    private Spinner spYard;
    private Button btnDefine;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.fragment_container_yard_management, container, false); //fcym_mcv_internal_moves

        spYard = v.findViewById(R.id.fcym_sp_yard);
        btnDefine = v.findViewById(R.id.fcym_btn_define);
        mcvRelocation = v.findViewById(R.id.fcym_mcv_relocation);
        mcvImport = v.findViewById(R.id.fcym_mcv_import);
        mcvExport = v.findViewById(R.id.fcym_mcv_export);
        mcvReception = v.findViewById(R.id.fcym_mcv_reception);
        mcvDispatch = v.findViewById(R.id.fcym_mcv_dispatch);
        mcvRelocationIng = v.findViewById(R.id.fcym_mcv_internal_moves);


        PreferencesManager.initializeInstance(getContext());

        ResourceHandler.listYards(this, Key.requestCode.GET_YARD);

        mcvRelocation.setOnClickListener(this);
        mcvImport.setOnClickListener(this);
        mcvExport.setOnClickListener(this);
        mcvRelocationIng.setOnClickListener(this);
        mcvReception.setOnClickListener(this);
        mcvDispatch.setOnClickListener(this);
        btnDefine.setOnClickListener(this);
        return v;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.fcym_mcv_import:
                PreferencesManager.getInstance().setInt(Key.sharedPreferences.workMode, Key.workMode.UNLOAD.id());
                PreferencesManager.getInstance().setInt(Key.sharedPreferences.confirmedContainer, Key.confirmedContainer.CONFIRMED.id());
                PreferencesManager.getInstance().setInt(Key.sharedPreferences.yardAction, Key.yardAction.IMPORT.id());
                Tools.setFragment(getActivity().getSupportFragmentManager(), R.id.fl_main_content, new ImportExportFragment(), ImportExportFragment.TAG);
                break;
            case R.id.fcym_mcv_export:
                PreferencesManager.getInstance().setInt(Key.sharedPreferences.workMode, Key.workMode.LOAD.id());
                PreferencesManager.getInstance().setInt(Key.sharedPreferences.confirmedContainer, Key.confirmedContainer.NO_CONFIRMED.id());
                PreferencesManager.getInstance().setInt(Key.sharedPreferences.yardAction, Key.yardAction.EXPORT.id());
                Tools.setFragment(getActivity().getSupportFragmentManager(), R.id.fl_main_content, new ImportExportFragment(), ImportExportFragment.TAG);
                break;
            case R.id.fcym_mcv_relocation:
                //Tools.setFragment(getActivity().getSupportFragmentManager(), R.id.fl_main_content, new ContainerYardFragment(), ContainerYardFragment.TAG);
                Tools.setFragment(getActivity().getSupportFragmentManager(), R.id.fl_main_content, new ReportRelocationFragment(), ReportRelocationFragment.TAG);
                break;
            case R.id.fcym_mcv_internal_moves:
                //Tools.setFragment(getActivity().getSupportFragmentManager(), R.id.fl_main_content, new ContainerYardFragment(), ContainerYardFragment.TAG);
                Tools.setFragment(getActivity().getSupportFragmentManager(), R.id.fl_main_content, new ReportRelocationIntFragment(), ReportRelocationIntFragment.TAG);
                break;
            case R.id.fcym_btn_define:
                PreferencesManager.getInstance().setString(Key.sharedPreferences.yard, ((TextAdapter)spYard.getSelectedItem()).getId());
                AlertUtil.showAlertOk(getString(R.string.all_text_success), "Patio definido", getContext(), new AlertUtil.OnOkListener() {
                    @Override
                    public void onOk() {
//                                idJoined = Integer.valueOf(response.getMessage());
                    }
                }).show();
                break;
            case R.id.fcym_mcv_reception:
                Tools.setFragment(getActivity().getSupportFragmentManager(), R.id.fl_main_content, new ReceptionFragment(), ReceptionFragment.TAG);
                break;
            case R.id.fcym_mcv_dispatch:
                Tools.setFragment(getActivity().getSupportFragmentManager(), R.id.fl_main_content, new DispatchFragment(), DispatchFragment.TAG);
                break;
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        inflater.inflate(R.menu.container_yarn_menu, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.cym_action_save:
                Tools.setFragment(getActivity().getSupportFragmentManager(), R.id.fl_main_content, new ListContainerYardWaitFragment(), ListContainerYardWaitFragment.TAG);
                return true;
        }
        return true;
    }


    @Override
    public void onResponse(int requestCode, Object o, Object r) {
        switch (requestCode) {
            case Key.requestCode.GET_YARD:
                if (o instanceof JsonObject) {
                    JsonObject response = (JsonObject) o;
                    SpinnerAdapter adapter = SpinnerAdapter.createArrayAdapter(getActivity(), response.getAsJsonArray(Key.jsonResponse.data).toString(), Yard.class);
                    spYard.setAdapter(adapter);
                }
                break;
        }
    }
}
