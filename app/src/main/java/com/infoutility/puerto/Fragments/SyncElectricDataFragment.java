package com.infoutility.puerto.Fragments;


import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.infoutility.puerto.Api.ResourceHandler;
import com.infoutility.puerto.Api.Status;
import com.infoutility.puerto.Interfaces.Response;
import com.infoutility.puerto.Keys.Key;
import com.infoutility.puerto.Models.Consumption;
import com.infoutility.puerto.Models.ConsumptionWrp;
import com.infoutility.puerto.Models.Response.BaseResponse;
import com.infoutility.puerto.R;
import com.infoutility.puerto.Realm.DeleteModels;
import com.infoutility.puerto.Realm.InsertModels;
import com.infoutility.puerto.Realm.Model.ElectricModel;
import com.infoutility.puerto.Realm.Model.SyncModel;
import com.infoutility.puerto.Realm.SelectModels;
import com.infoutility.puerto.Utils.AlertUtil;
import com.infoutility.puerto.Utils.Tools;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class SyncElectricDataFragment extends Fragment implements Response.OnResponse, View.OnClickListener {
    public static final String TAG = SyncElectricDataFragment.class.getSimpleName();

    List<ElectricModel>  listHistory;
    InsertModels insertModels;
    SelectModels selectModels;
    int count = 0;
    int succesCount = 0;
    int totalCount = 0;
    Button btn;
    private ProgressDialog progressDialog;
    TextView tvPending;
    private int counttotal = 0;


    public SyncElectricDataFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_sync_electric_data, container, false);
        initView(view);
        return view;
    }

    private void initView(View view){
        insertModels = new InsertModels();
        selectModels = new SelectModels();
        btn = view.findViewById(R.id.fsed_btn_show);
        tvPending = view.findViewById(R.id.fsed_tv_pending);
        ((Button)view.findViewById(R.id.fsed_btn_sync)).setOnClickListener(this);
        ((Button)view.findViewById(R.id.fsed_btn_show)).setOnClickListener(this);
        tvPending.setText("Envios Pendientes: " + selectModels.findAll().size());
    }

    private void saveInfo(int position){
        int nextPosition = position + 1;
        tvPending.setText("Almacenando " + (position + 1) + " de " + totalCount );
        if(nextPosition < this.listHistory.size()) {
            ElectricModel electricModel = this.listHistory.get(nextPosition);
            insertModels.saveElectic(this, nextPosition, electricModel);
        }else{
            tvPending.setText("Almacenado completado puede regresar a la pantalla anterior " );
        }
    }

    private void syncrequest(){
        List<SyncModel> syncList = selectModels.findAll();
        count = 0;
        totalCount = syncList.size();
        if (totalCount > 0){
//            btn.setEnabled(false);
            initProgressDialog("Enviando informacion...");
            for (SyncModel s : syncList){
                Consumption consumptionRequest = new Gson().fromJson(s.getData(), Consumption.class);
                ResourceHandler.postConsumptionHistory(this, Key.requestCode.POST_METER_HISTORY_CONSUMPTION, new ConsumptionWrp(s.getId(), consumptionRequest));
            }
//            AlertUtil.showAlertOk(getString(R.string.all_text_warning), "Se han sincronizado " + succesCount + "/" + totalCount + " registros.", getContext(), new AlertUtil.OnOkListener() {
//                @Override
//                public void onOk() {
//                    // do nothing
//                }
//            }).show();
        } else {
            AlertUtil.showAlertOk(getString(R.string.all_text_warning), "No hay informacion pendiente de sincronizacion.", getContext(), new AlertUtil.OnOkListener() {
                @Override
                public void onOk() {
                    // do nothing
                }
            }).show();
        }
    }

    private void getListOffline() {
        initProgressDialog("Descargando la informacion, por favor espere");
        ResourceHandler.getUsersOffline(this, Key.requestCode.GET_LIST_HISTORY_ELECTRONIC);
    }

    private void initProgressDialog(String msg){
        progressDialog = new ProgressDialog(getContext());
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage(msg);
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.fsed_btn_sync:
                //this.onResponse(Key.requestCode.GET_LIST_HISTORY_ELECTRONIC, null, null);
                getListOffline();
                break;
            case R.id.fsed_btn_show:

//                ArrayList<ElectricModel> elec = SelectModels.selectFindUserElectronic(0,"");
//                Log.i(TAG, elec.toString());
                syncrequest();

                break;
        }
    }

    @Override
    public void onResponse(int requestCode, Object o, Object r) {
        if(requestCode == Key.requestCode.GET_LIST_HISTORY_ELECTRONIC){
            progressDialog.dismiss();
            if(o instanceof JsonObject){
                JsonObject response = (JsonObject) o;
                List<ElectricModel> list  = Tools.getListFromJson(response.getAsJsonArray(Key.jsonResponse.data).toString(), ElectricModel.class);
                this.listHistory = list;
                this.totalCount = this.listHistory.size();
                DeleteModels.deleteAll();
                this.saveInfo(-1);
            }else{
                AlertUtil.showAlertOk(getString(R.string.all_text_warning), "Problemas de conectividad, por favor intentar mas tarde", getContext(), new AlertUtil.OnOkListener() {
                    @Override
                    public void onOk() {
                        // do nothing
                    }
                }).show();
            }



        }else if(requestCode == Key.requestCode.RESPONSE_REALM){
            if (o instanceof Boolean){
                boolean status = (Boolean)o;
                int position = (Integer)r;
                if(status){
                    this.saveInfo(position);
                }
            }
        }else if(requestCode == Key.requestCode.POST_METER_HISTORY_CONSUMPTION){
            count++;
//            if (count == totalCount){
////                btn.setEnabled(true);
//                progressDialog.dismiss();
//            }

            if (o instanceof JsonObject){
                JsonObject jObject = (JsonObject) o;
                BaseResponse response = new Gson().fromJson(jObject.toString(), BaseResponse.class);

                if (response.isResultado()){
                    succesCount++;
                    ConsumptionWrp cw = (ConsumptionWrp)r;
                    SyncModel synced = selectModels.findById(cw.getId());
                    if (synced != null){
                        insertModels.deleteSync(synced);
                    }
                    tvPending.setText("Envios Pendientes: " + (totalCount - succesCount));
                }
            }

            if (count == totalCount){
                progressDialog.dismiss();
                AlertUtil.showAlertOk(getString(R.string.all_text_warning), "Se han enviado " + succesCount + "/" + totalCount + " registros.", getContext(), new AlertUtil.OnOkListener() {
                    @Override
                    public void onOk() {
                        // do nothing
                    }
                }).show();
            }

        }else {
            AlertUtil.showAlertOk(getString(R.string.all_text_warning), "Se produjo un error, por favor intentar mas tarde", getContext(), new AlertUtil.OnOkListener() {
                @Override
                public void onOk() {
                    // do nothing
                }
            }).show();
        }




    }
}
