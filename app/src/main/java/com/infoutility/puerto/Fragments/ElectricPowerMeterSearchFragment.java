package com.infoutility.puerto.Fragments;


import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.google.common.collect.ImmutableMap;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.infoutility.puerto.Adapters.ListReportRelocation;
import com.infoutility.puerto.Adapters.ListSearchPowerMeter;
import com.infoutility.puerto.Api.ResourceHandler;
import com.infoutility.puerto.Keys.Key;
import com.infoutility.puerto.Models.ContainerPendingList;
import com.infoutility.puerto.Models.MeterUser;
import com.infoutility.puerto.R;
import com.infoutility.puerto.Realm.Model.ElectricModel;
import com.infoutility.puerto.Realm.SelectModels;
import com.infoutility.puerto.Utils.AlertUtil;
import com.infoutility.puerto.Utils.JsonBundle;
import com.infoutility.puerto.Utils.PreferencesManager;
import com.infoutility.puerto.Utils.Tools;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class ElectricPowerMeterSearchFragment extends Fragment implements com.infoutility.puerto.Interfaces.Response.OnResponse, View.OnClickListener {

    public static final String TAG = ElectricPowerMeterSearchFragment.class.getSimpleName();
    public static boolean ACTION_TEMP = false;
    public static String JSON_FORMAT = "";
    RecyclerView recyclerViewList;
    ArrayList<ElectricModel> list;
    private TextInputEditText tietSearch;

    public ElectricPowerMeterSearchFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_electric_power_meter_search, container, false);
        initView(view);
        return view;
    }

    private void initView(View view){
        PreferencesManager.initializeInstance(getActivity());
        ((Button)view.findViewById(R.id.fepms_btn_cod)).setOnClickListener(this);
        ((Button)view.findViewById(R.id.fepms_btn_cont)).setOnClickListener(this);
        ((Button)view.findViewById(R.id.fepms_btn_)).setOnClickListener(this);
        ((Button)view.findViewById(R.id.fepms_btn_sync)).setOnClickListener(this);
        tietSearch = view.findViewById(R.id.fepms_tiet_search);
        recyclerViewList= (RecyclerView) view.findViewById(R.id.fepms_rv_container_search);

        ArrayList<ElectricModel> tempList =  SelectModels.selectAllElectronic();
        if(tempList.size() < 1){
            AlertUtil.showAlertOk(getString(R.string.all_text_accept), "No tiene informacion cargada, por favor ir a sincronizar para bajar la base de datos", getContext(), new AlertUtil.OnOkListener() {
                @Override
                public void onOk() {
                    // do nothing
                }
            }).show();
        }




        /*((Button)view.findViewById(R.id.frr_btn_create)).setOnClickListener(this);
        oficial = PreferencesManager.getInstance().getInt(Key.sharedPreferences.recorder, 0) + "";
        this.getListReportRelocation();*/
    }

    private void getListReportRelocation(String type, String value){
        /*ResourceHandler.listSearchPowerMeter(this, Key.requestCode.GET_LIST_POWER_METER_SEARCH,
                ImmutableMap.of(type, value));*/


    }

    private void getListUserReportRelocation(String value){
        try {
            long lValue = Long.valueOf(value);
            this.list = SelectModels.selectFindUserElectronic(lValue,"uel_codigo");
            if(this.list.size() < 1) {
                this.alertWarning("No se encontraron datos de codigo de usuario");
            }
            this.renderRecycler(this.list);
        }catch (Exception e){

        }

    }

    private void getListContReportRelocation(String value){
        try {
            long lValue = Long.valueOf(value);
            this.list = SelectModels.selectFindUserElectronic(lValue,"uel_contador");
            if(this.list.size() < 1) {
                this.alertWarning("No se encontraron datos de codigo de contador");
            }
            this.renderRecycler(this.list);
        }catch (Exception e){

        }

    }

    private void getListNumCardReportRelocation(String value){
        try {
            this.list = SelectModels.selectFindNumCardElectronic(value.toUpperCase(),"uel_numero_tarjeta");
            if(this.list.size() < 1) {
                this.alertWarning("No se encontraron datos de numero de tarjeta");
            }
            this.renderRecycler(this.list);
        }catch (Exception e){

        }

    }

    private void alertWarning(String title){
        AlertUtil.showAlertOk(getString(R.string.all_text_warning), title, getContext(), new AlertUtil.OnOkListener() {
            @Override
            public void onOk() {
                // do nothing
            }
        }).show();
    }


    public void renderRecycler(ArrayList<ElectricModel> list){
        ListSearchPowerMeter listSearchPowerMeter = new ListSearchPowerMeter(list, this);
        recyclerViewList.setAdapter(listSearchPowerMeter);
        GridLayoutManager gridLayoutManager = new GridLayoutManager(getActivity(), 1);
        recyclerViewList.setLayoutManager(gridLayoutManager);
    }

    @Override
    public void onClick(View view) {
        String infoSerach = tietSearch.getText().toString();
        this.list = null;
        switch (view.getId()){
            case R.id.fepms_btn_cod:

                //getListReportRelocation("CodigoUsuario", infoSerach);
                getListUserReportRelocation(infoSerach);
                break;
            case  R.id.fepms_btn_cont:
                //getListReportRelocation("Contador", infoSerach);
                getListContReportRelocation(infoSerach);
                break;
            case R.id.fepms_btn_:
                //getListNumCardReportRelocation("CodigoUsuario", infoSerach);
                getListNumCardReportRelocation(infoSerach);
                break;
            case R.id.fepms_btn_sync:
                Tools.setFragment(getFragmentManager(), R.id.fl_main_content, new SyncElectricDataFragment(), SyncElectricDataFragment.TAG);
                break;
        }
    }

    @Override
    public void onResponse(int requestCode, Object o, Object r) {
        if(requestCode == Key.requestCode.GET_LIST_POWER_METER_SEARCH){
            Log.i(TAG, o.toString());
            try{

                JsonObject response = (JsonObject) o;
                List<MeterUser> list  = Tools.getListFromJson(response.getAsJsonArray(Key.jsonResponse.data).toString(), MeterUser.class);

//                this.list = list;
//                this.renderRecycler(this.list);
            }catch (Exception e){
                Log.i(TAG, e.toString());
            }
        }else if(requestCode == Key.requestCode.RESPONSE_CARD){
            try{

                if(o instanceof Integer){
                    int position = (Integer)o;
                    ElectricModel electricModel = this.list.get(position);

                    ElectricModel electricModelTemp = new ElectricModel();
                    electricModelTemp.setUel_nombre(electricModel.getUel_nombre());

                    electricModelTemp.setUel_direccion(electricModel.getUel_direccion());
                    electricModelTemp.setUel_contador(electricModel.getUel_contador());
                    electricModelTemp.setUel_tar_codigo(electricModel.getUel_tar_codigo());
                    electricModelTemp.setMus_lectura(electricModel.getMus_lectura());
                    electricModelTemp.setUel_codigo(electricModel.getUel_codigo());
                    electricModelTemp.setMus_consumo(electricModel.getMus_consumo());
                    electricModelTemp.setMus_mes(electricModel.getMus_mes());
                    electricModelTemp.setTar_tarifa_quetzal(electricModel.getTar_tarifa_quetzal());
                    electricModelTemp.setUel_numero_tarjeta(electricModel.getUel_numero_tarjeta());
                    electricModelTemp.setTar_tarifa_dolar(electricModel.getTar_tarifa_dolar());
                    electricModelTemp.setUel_nombre(electricModel.getUel_nombre());

                    Log.i(TAG, electricModel.getUel_direccion());
                    ElectricPowerMeterSearchFragment.ACTION_TEMP = true;

                    ElectricPowerMeterSearchFragment.JSON_FORMAT = new Gson().toJson(electricModelTemp);
                    Tools.setFragment(getFragmentManager(), R.id.fl_main_content, new ElectricPowerMeterFragmente(), ElectricPowerMeterFragmente.TAG);
                }

            }catch (Exception e){

            }
        }
    }
}
