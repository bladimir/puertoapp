package com.infoutility.puerto.Fragments;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Table;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.infoutility.puerto.Adapters.BlockListContainerYard;
import com.infoutility.puerto.Api.ResourceHandler;
import com.infoutility.puerto.DialogFragments.AddReportRelocationDialogFragment;
import com.infoutility.puerto.DialogFragments.SetPositionContainerDialogFragment;
import com.infoutility.puerto.Keys.Key;
import com.infoutility.puerto.R;
import com.infoutility.puerto.Utils.AlertUtil;
import com.infoutility.puerto.Utils.JsonBundle;
import com.infoutility.puerto.Utils.PreferencesManager;

import org.json.JSONObject;

import java.util.ArrayList;

public class ContainerYardFragment extends Fragment  implements View.OnClickListener, com.infoutility.puerto.Interfaces.Response.OnResponse{
    public static final String TAG = ContainerYardFragment.class.getSimpleName();
    private int tempBlock = 0;
    private int tempLevel = 0;
    public RecyclerView listContainer;
    ArrayList<Bundle> listArrayBlock = new ArrayList<>();
    ArrayList<Bundle> listArrayLevel = new ArrayList<>();
    ArrayList<Bundle> listArrayModule = new ArrayList<>();
    Bundle selected;
    Bundle selectedA;
    BlockListContainerYard blockListContainerYard;
    private int typeSelected = 0;
    private String idRelocation;
    private String patio = "";
    private ProgressDialog progressDialog;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_container_yard, container, false);
        Log.i(TAG, "Esto es una prueba de vista");
        initView(v);
        //ResourceHandler.doLogin(this, Key.requestCode.GET_LOGIN, tietUsername.getText().toString(), tietPassword.getText().toString());
        return v;

    }

    public void initView(View view){
        PreferencesManager.initializeInstance(getActivity());
        listContainer = (RecyclerView) view.findViewById(R.id.fcy_rv_container_block);
        ((Button)view.findViewById(R.id.fcy_btn_back)).setOnClickListener(this);
        ((Button)view.findViewById(R.id.fcy_btn_out)).setOnClickListener(this);
        ((Button)view.findViewById(R.id.fcy_btn_clear)).setOnClickListener(this);
        getListBlock();
        this.idRelocation = PreferencesManager.getInstance().getString(Key.sharedPreferences.reportRelocation, "");
        this.patio = PreferencesManager.getInstance().getString(Key.sharedPreferences.yard, "");
        Log.i(TAG, PreferencesManager.getInstance().getString(Key.sharedPreferences.reportRelocation, ""));
    }

    private void getListBlock(){
        ResourceHandler.listBlocks(this, Key.requestCode.GET_LIST_BLOCK,
                ImmutableMap.of(Key.api.paramYarn, patio ));
    }


    private void getListLevel(Bundle block){
        this.tempBlock = block.getInt("bloque");
        ResourceHandler.listLevelForBlock(this, Key.requestCode.GET_LIST_LEVELS,
                ImmutableMap.of(Key.api.paramYarn, patio , Key.api.paramBlock, String.valueOf( this.tempBlock )));
    }

    private void getContainers(Bundle level){
        this.tempLevel = level.getInt("nivel");

        Log.i(TAG, this.tempLevel + "");
        ResourceHandler.listContainerForLevel(this, Key.requestCode.GET_LIST_MODULES,
                ImmutableMap.of(Key.api.paramYarn, patio , Key.api.paramBlock, String.valueOf(this.tempBlock),
                        Key.api.paramLevel, String.valueOf(this.tempLevel) ));
    }

    private void getContainersReload(){
        //this.tempLevel = level.getInt("nivel");

        Log.i(TAG, this.tempLevel + "");
        ResourceHandler.listContainerForLevel(this, Key.requestCode.GET_LIST_MODULES,
                ImmutableMap.of(Key.api.paramYarn, patio , Key.api.paramBlock, String.valueOf(this.tempBlock),
                        Key.api.paramLevel, String.valueOf(this.tempLevel) ));
    }

    private void setTextSelected(int module, int row, int level, String num, String pref){
        ((TextView)getView().findViewById(R.id.fcy_txt_module)).setText(String.valueOf(module));
        ((TextView)getView().findViewById(R.id.fcy_txt_row)).setText(String.valueOf(row));
        ((TextView)getView().findViewById(R.id.fcy_txt_level)).setText(String.valueOf(level));
        ((TextView)getView().findViewById(R.id.fcy_txt_number)).setText(num);
        ((TextView)getView().findViewById(R.id.fcy_txt_pref)).setText(pref);

    }

    private void initProgressDialog(String msg){
        progressDialog = new ProgressDialog(getContext());
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage(msg);
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    private void renderRecicler(ArrayList<Bundle> list, int type, int countRow){
        blockListContainerYard = new BlockListContainerYard(list, this, type);
        listContainer.setAdapter(blockListContainerYard);
        //if(type == Key.cardType)
        GridLayoutManager gridLayoutManager = new GridLayoutManager(getActivity(), countRow);
        listContainer.setLayoutManager(gridLayoutManager);
    }

    private void containerDetailShow(int type){
        DialogFragment fragments = SetPositionContainerDialogFragment.newInstance(this, type, selected, selectedA, idRelocation);
        //fragments.setArguments(Tools.fromObject(Key.extra.CONTAINER_DETAIL, container));
        fragments.show(getChildFragmentManager(), SetPositionContainerDialogFragment.TAG);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.fcy_btn_clear:
                if(this.selected == null){
                    AlertUtil.showAlertOk(getString(R.string.all_text_warning), "No tienen item seleccionado", getContext(), new AlertUtil.OnOkListener() {
                        @Override
                        public void onOk() {
//                                idJoined = Integer.valueOf(response.getMessage());
                        }
                    }).show();
                    return;
                }
                this.selected = null;
                setTextSelected(0,0,0, "", "");
                break;

            case  R.id.fcy_btn_back:
                if(this.typeSelected == Key.cardType.typeContainer){
                    ResourceHandler.listLevelForBlock(this, Key.requestCode.GET_LIST_LEVELS,
                            ImmutableMap.of(Key.api.paramYarn, patio , Key.api.paramBlock, String.valueOf( this.tempBlock )));
                }else if(this.typeSelected == Key.cardType.typeLevel){
                    ResourceHandler.listBlocks(this, Key.requestCode.GET_LIST_BLOCK,
                            ImmutableMap.of(Key.api.paramYarn, patio ));
                }
                break;

            case R.id.fcy_btn_out:
                if(this.selected != null)
                    this.containerDetailShow(SetPositionContainerDialogFragment.TYPE_COMPLETE);
                break;
        }

    }

    @Override
    public void onResponse(int requestCode, Object o, Object r) {

        if(requestCode == Key.requestCode.GET_LIST_BLOCK){
            try {
                JSONObject jsonObject = new JSONObject(o.toString());
                Bundle bundle = JsonBundle.createBundle(jsonObject);

                this.listArrayBlock = bundle.getParcelableArrayList(Key.jsonResponse.data);

                this.typeSelected = Key.cardType.typeBlock;
                this.renderRecicler(this.listArrayBlock, Key.cardType.typeBlock, 2);


            }catch (Exception e){
                Log.i(TAG, e.toString());
            }
        }else if(requestCode == Key.requestCode.RESPONSE_CARD){
            if(o instanceof Integer){
                int i = (int)o;
                int type = (int)r;
                if(type == Key.cardType.typeBlock){
                    getListLevel(this.listArrayBlock.get(i));
                }else if(type == Key.cardType.typeLevel){
                    getContainers(this.listArrayLevel.get(i));
                }else if(type == Key.cardType.typeContainer){

                    if(this.selected != null){
                        Bundle tempSelect = this.listArrayModule.get(i);
                        String lleno = tempSelect.getString("numero_de_identificacion", "");
                        if(!lleno.equals("")){
                            AlertUtil.showAlertOk(getString(R.string.all_text_warning), "El espacio ya se encuentra ocupado", getContext(), new AlertUtil.OnOkListener() {
                                @Override
                                public void onOk() {
//                                idJoined = Integer.valueOf(response.getMessage());
                                }
                            }).show();
                            return;
                        }
                        this.selectedA = tempSelect;
                        this.selectedA.putString("patio", patio);
                        this.containerDetailShow(SetPositionContainerDialogFragment.TYPE_SIMPLE);

                    }else{
                        Bundle tempSelect = this.listArrayModule.get(i);
                        if(tempSelect.getString("muerto", "").equals("S")) return;
                        String lleno = tempSelect.getString("numero_de_identificacion", "");
                        if(lleno.equals("")){
                            AlertUtil.showAlertOk(getString(R.string.all_text_warning), "No hay contenedor para selecionar", getContext(), new AlertUtil.OnOkListener() {
                                @Override
                                public void onOk() {
//                                idJoined = Integer.valueOf(response.getMessage());
                                }
                            }).show();
                            return;
                        }

                        this.selected = tempSelect;
                        this.selected.putString("patio", patio);
                        Log.i(TAG, this.selected.toString());
                        for (Bundle bundle:
                                this.listArrayModule) {
                            if(bundle.getInt("selected", 0) == 1){
                                bundle.putInt("selected", 0);
                            }
                        }

                        this.listArrayModule.get(i).putInt("selected", 1);
                        this.blockListContainerYard.notifyDataSetChanged();
                        this.setTextSelected(this.selected.getInt("modulo"), this.selected.getInt("fila_o_carril"), this.selected.getInt("nivel"),
                                this.selected.getString("numero_de_identificacion"), this.selected.getString("prefijo"));
                    }



                }

            }
        }else if(requestCode == Key.requestCode.GET_LIST_LEVELS){
            try {


                JSONObject jsonObject = new JSONObject(o.toString());
                Bundle bundle = JsonBundle.createBundle(jsonObject);

                this.listArrayLevel = bundle.getParcelableArrayList(Key.jsonResponse.data);

                this.typeSelected = Key.cardType.typeLevel;
                this.renderRecicler(this.listArrayLevel, Key.cardType.typeLevel, 1);


            }catch (Exception e){

            }
        }else if(requestCode == Key.requestCode.GET_LIST_MODULES){
            try {
                if(progressDialog != null) progressDialog.dismiss();

                this.listArrayModule = new ArrayList<Bundle>();
                int max = 0;

                JSONObject jsonObject = new JSONObject(o.toString());
                Bundle bundle = JsonBundle.createBundle(jsonObject);
                ArrayList<Bundle> mm = bundle.getParcelableArrayList(Key.jsonResponse.data);
                java.util.Iterator<Bundle> gg = mm.iterator();

                while(gg.hasNext()) {
                    Object oo = gg.next();
                    ArrayList<Bundle> tt = (ArrayList<Bundle>)oo;
                    max = (max <= tt.size()) ? tt.size() : max;
                    this.listArrayModule.addAll(tt);
                }

                if(this.selected != null){
                    for (Bundle bundle1:
                            this.listArrayModule) {

                    }
                }

                this.typeSelected = Key.cardType.typeContainer;

                this.renderRecicler(this.listArrayModule, Key.cardType.typeContainer, max);
            }catch (Exception e){
                Log.i(TAG, e.toString());

            }
        }else if(requestCode == Key.requestCode.RESPONSE_SET_POSITION){
            //getListBlock();
            initProgressDialog("Cargando datos de los contenedores");
            this.getContainersReload();
            this.selected = null;
            setTextSelected(0,0,0, "", "");
        }
    }
}
