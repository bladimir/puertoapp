package com.infoutility.puerto.Fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.common.collect.ImmutableMap;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.infoutility.puerto.Adapters.SpinnerAdapter;
import com.infoutility.puerto.Adapters.TextAdapter;
import com.infoutility.puerto.Api.ResourceHandler;
import com.infoutility.puerto.DialogFragments.DateTimeDialogFragment;
import com.infoutility.puerto.Interfaces.Response;
import com.infoutility.puerto.Keys.Key;
import com.infoutility.puerto.MainActivity;
import com.infoutility.puerto.Models.ContainerPending;
import com.infoutility.puerto.Models.ContainerPendingList;
import com.infoutility.puerto.Models.Machine;
import com.infoutility.puerto.Models.Reception;
import com.infoutility.puerto.Models.Response.BaseResponse;
import com.infoutility.puerto.Models.Yard;
import com.infoutility.puerto.R;
import com.infoutility.puerto.Utils.AlertUtil;
import com.infoutility.puerto.Utils.PreferencesManager;
import com.infoutility.puerto.Utils.Tools;

import java.util.Date;

public class ReceptionFragment extends Fragment implements View.OnClickListener, Response.OnResponse {

    public static final String TAG = ReceptionFragment.class.getSimpleName();
    private static final int DATETIME = 1;
    private static final int DATE_RECEPTION = 2;
    private static final int DATE_DELIVERY = 3;
    private int recorder = 0;

    private TextInputEditText tietReception;
    private Button btnSearch;
    private TextView tvPrefix;
    private TextView tvId;
    private TextView tvMeasure;
    private TextView tvContainer;
    private TextInputEditText tietDatetime;
    private TextInputEditText tietYard;
    private TextInputEditText tietRow;
    private TextInputEditText tietModule;
    private TextInputEditText tietLevel;
    private TextInputEditText tietMachine;
    private TextInputEditText tietHandheld;
    private TextInputEditText tietDateDelivery;
    private TextInputEditText tietDateReception;
    private TextInputEditText tietComment;
    private Button btnAdd;
    private ImageView ivDatetime;
    private ImageView ivDateReception;
    private ImageView ivDateDelivery;
    private TextView tvMachine;

    private Spinner spYard;
    private Spinner spMachine;

    private Button btnCancel;
    private Button btnPending;

    private int dateType = -1;
    private Date selectedDatetime;
    private Date selectedDateReception;
    private Date selectedDateDelivery;


    private Reception receptionFound;

    private ContainerPendingList pending;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_reception, container, false);
        PreferencesManager.initializeInstance(getContext());
        recorder = PreferencesManager.getInstance().getInt(Key.sharedPreferences.recorder, 0);

        tietReception = v.findViewById(R.id.fr_tiet_reception);
        btnSearch = v.findViewById(R.id.fr_btn_search);
        tvPrefix = v.findViewById(R.id.fr_tv_prefix);
        tvId = v.findViewById(R.id.fr_tv_id);
        tvMeasure = v.findViewById(R.id.fr_tv_measure);
        tvContainer = v.findViewById(R.id.fr_tv_container);
        tietDatetime = v.findViewById(R.id.fr_tiet_datetime);
        tietYard = v.findViewById(R.id.fr_tiet_yard);
        tietRow = v.findViewById(R.id.fr_tiet_row);
        tietModule = v.findViewById(R.id.fr_tiet_module);
        tietLevel = v.findViewById(R.id.fr_tiet_level);
        tietMachine = v.findViewById(R.id.fr_tiet_machine);
        tietHandheld = v.findViewById(R.id.fr_tiet_handheld);
        tietDateDelivery = v.findViewById(R.id.fr_tiet_delivery);
        tietDateReception = v.findViewById(R.id.fr_tiet_date_reception);
        tietComment = v.findViewById(R.id.fr_tiet_comment);
        btnAdd = v.findViewById(R.id.fr_btn_add);
        ivDatetime = v.findViewById(R.id.fr_iv_datetime);
        ivDateReception = v.findViewById(R.id.fr_iv_date_reception);
        ivDateDelivery = v.findViewById(R.id.fr_iv_date_delivery);
        spMachine = v.findViewById(R.id.fr_sp_machine);
        spYard = v.findViewById(R.id.fr_sp_yard);
        btnCancel = v.findViewById(R.id.fr_btn_cancel);
        btnPending = v.findViewById(R.id.fr_btn_pending);
        tvMachine = v.findViewById(R.id.fr_tv_machine);

        ResourceHandler.listMachines(this, Key.requestCode.GET_MACHINE);
        ResourceHandler.listYards(this, Key.requestCode.GET_YARD);

        //default
        setData(new Reception());
        ActionBar actionBar = ((MainActivity)getActivity()).getSupportActionBar();
        if (actionBar != null)
            actionBar.setTitle( "Recepcion");

        btnAdd.setOnClickListener(this);
        btnSearch.setOnClickListener(this);
        ivDatetime.setOnClickListener(this);
        ivDateReception.setOnClickListener(this);
        ivDateDelivery.setOnClickListener(this);
        btnCancel.setOnClickListener(this);
        btnPending.setOnClickListener(this);


        spMachine.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                tvMachine.setText(((TextAdapter)adapterView.getItemAtPosition(i)).getData());
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        loadPending();

        return v;
    }


    private void setData(Reception reception) {
        String prefijo = getString(R.string.fr_title_prefix, reception.getPrefijo() == null ? "" : reception.getPrefijo());
        String noRecepcion = "No. Recepcion: " + reception.getAutorizacion_recepcion();
        tvPrefix.setText(noRecepcion + "\n" + prefijo);
        tvId.setText(getString(R.string.fr_title_id, reception.getNumero_de_identificacion() == null ? "" : reception.getNumero_de_identificacion()));
        tvMeasure.setText(getString(R.string.fr_title_measure, reception.getMedida() == 0 ? "" : String.valueOf(reception.getMedida())));
        tvContainer.setText(getString(R.string.fr_title_container, reception.getTipo_contenedor() == null ? "" : reception.getTipo_contenedor()));
    }

    private void doSearch(){
        ResourceHandler.getReception(this, Key.requestCode.GET_RECEPTION,
                ImmutableMap.of(Key.api.paramAuthReception, tietReception.getText().toString()));
    }

    private String validate(){
//        if (tietHandheld.getText() == null || tietHandheld.getText().toString().trim().equals(""))
//            return "Debe ingresar Ubicador de patio";
        if (tietLevel.getText() == null || tietLevel.getText().toString().trim().equals(""))
            return "Debe ingresar nivel";
//        if (tietMachine.getText() == null || tietMachine.getText().toString().trim().equals(""))
//            return "Debe ingresar maquina";
        if (tietModule.getText() == null || tietModule.getText().toString().trim().equals(""))
            return "Debe ingresar  modulo";
        if (tietRow.getText() == null || tietRow.getText().toString().trim().equals(""))
            return "Debe ingresar fila";
//        if (tietYard.getText() == null || tietYard.getText().toString().trim().equals(""))
//            return "Debe ingresar patio";
//        if (selectedDatetime == null)
//            return "Debe ingresar fecha y hora";
//        if (selectedDateDelivery == null)
//            return "Debe ingresar  fecha de entrega";
//        if (selectedDateReception == null)
//            return "Debe ingresar  fecha de recepcion";
        if (receptionFound == null)
            return "No se puede realizar la recepcion de un contenedor no existente";
        if (recorder == 0)
            return "No se ha podido obtener informacion del grabados";

        return null;
    }

    private void doReception(){
        String validationMessage = validate();
        if (validationMessage == null){
            Reception receptionRequest = new Reception();
            Date now = new Date();
//            receptionRequest.setGrabador_patio(2128);
            receptionRequest.setGrabador_patio(recorder);
            receptionRequest.setUsuario_de_servicio(receptionFound.getUsuario_de_servicio());
            receptionRequest.setEstado_contenedor(receptionFound.getEstado_contenedor());
            receptionRequest.setMedida(receptionFound.getMedida());
            receptionRequest.setC_o_f(receptionFound.getC_o_f());
            receptionRequest.setNumero_de_identificacion(receptionFound.getNumero_de_identificacion());
            receptionRequest.setPrefijo(receptionFound.getPrefijo());
            receptionRequest.setFila_o_carril(Integer.valueOf(tietRow.getText().toString()));
//            receptionRequest.setFecha_recibe_doc_oficial_patio(Tools.formatDate(selectedDatetime, Key.datetimePattern.YYYYMMDD_HHMMSS_DASH_SEPARATOR));
            receptionRequest.setFecha_recibe_doc_oficial_patio(Tools.formatDate(now, Key.datetimePattern.YYYYMMDD_HHMMSS_DASH_SEPARATOR));
            receptionRequest.setViaje_barco(receptionFound.getViaje_barco());
//            receptionRequest.setPatio(tietYard.getText().toString());
            receptionRequest.setPatio(((TextAdapter)spYard.getSelectedItem()).getId());
            receptionRequest.setTipo_contenedor(receptionFound.getTipo_contenedor());
//            receptionRequest.setFecha_hora_recepcion(Tools.formatDate(new Date(), Key.datetimePattern.YYYYMMDD_HHMMSS_DASH_SEPARATOR));
            receptionRequest.setFecha_hora_recepcion(Tools.formatDate(now, Key.datetimePattern.YYYYMMDD_HHMMSS_DASH_SEPARATOR));
            receptionRequest.setSeq_reg_conte(receptionFound.getSeq_reg_conte());
//            receptionRequest.setFecha_hora_ubicador(Tools.formatDate(selectedDatetime, Key.datetimePattern.YYYYMMDD_HHMMSS_DASH_SEPARATOR));
            receptionRequest.setFecha_hora_ubicador(Tools.formatDate(now, Key.datetimePattern.YYYYMMDD_HHMMSS_DASH_SEPARATOR));
            receptionRequest.setSelectivo_sat(receptionFound.getSelectivo_sat());
//            receptionRequest.setFecha_entrega_doc_piloto(Tools.formatDate(selectedDateDelivery, Key.datetimePattern.YYYYMMDD_HHMMSS_DASH_SEPARATOR));
            receptionRequest.setFecha_entrega_doc_piloto(Tools.formatDate(now, Key.datetimePattern.YYYYMMDD_HHMMSS_DASH_SEPARATOR));
            receptionRequest.setObservaciones(tietComment.getText() != null ? tietComment.getText().toString() : "");
//            receptionRequest.setMaquina(Integer.valueOf(tietMachine.getText().toString()));
            receptionRequest.setMaquina(Integer.valueOf(((TextAdapter)spMachine.getSelectedItem()).getId()));
            receptionRequest.setModulo(Integer.valueOf(tietModule.getText().toString()));
            receptionRequest.setNivel(Integer.valueOf(tietLevel.getText().toString()));
            receptionRequest.setAutorizacion_recepcion(receptionFound.getAutorizacion_recepcion());
            receptionRequest.setFecha_viaje_barco(receptionFound.getFecha_viaje_barco());
            receptionRequest.setRefer_seco_operando(receptionFound.getRefer_seco_operando());
//            receptionRequest.setTipo_contenedor("S");

            ResourceHandler.postReception(this, Key.requestCode.POST_RECEPTION, receptionRequest);
        } else {
            AlertUtil.showAlertOk(getString(R.string.all_text_warning), validationMessage, getContext(), new AlertUtil.OnOkListener() {
                @Override
                public void onOk() {
                    // do nothing
                }
            }).show();
        }
    }

    private void doReceptionRevert(){
        if (receptionFound != null){
            if (receptionFound.getPatio() != null && receptionFound.getFila_o_carril() != 0 && receptionFound.getModulo() != 0 && receptionFound.getNivel() != 0){
                Reception receptionRequest = new Reception();
                Date now = new Date();
                receptionRequest.setGrabador_patio(recorder);
                receptionRequest.setUsuario_de_servicio(receptionFound.getUsuario_de_servicio());
                receptionRequest.setEstado_contenedor(receptionFound.getEstado_contenedor());
                receptionRequest.setMedida(receptionFound.getMedida());
                receptionRequest.setC_o_f(receptionFound.getC_o_f());
                receptionRequest.setNumero_de_identificacion(receptionFound.getNumero_de_identificacion());
                receptionRequest.setPrefijo(receptionFound.getPrefijo());
                receptionRequest.setFila_o_carril(receptionFound.getFila_o_carril());
                receptionRequest.setFecha_recibe_doc_oficial_patio(Tools.formatDate(now, Key.datetimePattern.YYYYMMDD_HHMMSS_DASH_SEPARATOR));
                receptionRequest.setViaje_barco(receptionFound.getViaje_barco());
                receptionRequest.setPatio(receptionFound.getPatio());
                receptionRequest.setTipo_contenedor(receptionFound.getTipo_contenedor());
                receptionRequest.setFecha_hora_recepcion(Tools.formatDate(now, Key.datetimePattern.YYYYMMDD_HHMMSS_DASH_SEPARATOR));
                receptionRequest.setSeq_reg_conte(receptionFound.getSeq_reg_conte());
                receptionRequest.setFecha_hora_ubicador(Tools.formatDate(now, Key.datetimePattern.YYYYMMDD_HHMMSS_DASH_SEPARATOR));
                receptionRequest.setSelectivo_sat(receptionFound.getSelectivo_sat());
                receptionRequest.setFecha_entrega_doc_piloto(Tools.formatDate(now, Key.datetimePattern.YYYYMMDD_HHMMSS_DASH_SEPARATOR));
                receptionRequest.setObservaciones(receptionFound.getObservaciones());
                receptionRequest.setMaquina(receptionFound.getMaquina());
                receptionRequest.setModulo(receptionFound.getModulo());
                receptionRequest.setNivel(receptionFound.getNivel());
                receptionRequest.setAutorizacion_recepcion(receptionFound.getAutorizacion_recepcion());
                receptionRequest.setFecha_viaje_barco(receptionFound.getFecha_viaje_barco());
                receptionRequest.setRefer_seco_operando(receptionFound.getRefer_seco_operando());
                receptionRequest.setTipo_contenedor(receptionFound.getTipo_contenedor());
String s = new Gson().toJson(receptionRequest);
                ResourceHandler.putReception(this, Key.requestCode.PUT_RECEPTION, receptionRequest);
            } else {
                AlertUtil.showAlertOk(getString(R.string.all_text_warning), "No se puede revertir, faltan parametros", getContext(), new AlertUtil.OnOkListener() {
                    @Override
                    public void onOk() {
                        // do nothing
                    }
                }).show();
            }

        } else {
            AlertUtil.showAlertOk(getString(R.string.all_text_warning), "Recepcion no encontrada", getContext(), new AlertUtil.OnOkListener() {
                @Override
                public void onOk() {
                    // do nothing
                }
            }).show();
        }
    }

    private void doSavePending(){
        if (receptionFound != null){
            ContainerPending containerPending = new ContainerPending();
            containerPending.setREFERENCIA(String.valueOf(receptionFound.getAutorizacion_recepcion()));
            containerPending.setTIPO_MOVIMIENTO("R");
            containerPending.setPREFIJO(receptionFound.getPrefijo());
            containerPending.setNUMERO_IDENTIFIACION(receptionFound.getNumero_de_identificacion());
            containerPending.setUSUARIO_GRABADOR(recorder);
            containerPending.setFECHA_GRABACION(Tools.formatDate(new Date(), Key.datetimePattern.YYYYMMDD_HHMMSS_DASH_SEPARATOR));
            containerPending.setPARAMETROS(String.valueOf(receptionFound.getAutorizacion_recepcion()));

            ResourceHandler.postContainerPending(this, Key.requestCode.POST_CONTAINER_PENDING, containerPending);
        } else {
            AlertUtil.showAlertOk(getString(R.string.all_text_warning), "Recepcion no encontrada", getContext(), new AlertUtil.OnOkListener() {
                @Override
                public void onOk() {
                    // do nothing
                }
            }).show();
        }
    }

    private void loadPending(){
        if (ListContainerYardWaitFragment.ACTION_TEMP && !ListContainerYardWaitFragment.JSON_FORMAT.equals("")){
            ListContainerYardWaitFragment.ACTION_TEMP = false;
            pending = new Gson().fromJson(ListContainerYardWaitFragment.JSON_FORMAT, ContainerPendingList.class);
            tietReception.setText(String.valueOf(pending.getReferencia()));

            doSearch();

        }
//        else {
//            AlertUtil.showAlertOk(getString(R.string.all_text_warning), "No se pudo cargar la informacion.", getContext(), new AlertUtil.OnOkListener() {
//                @Override
//                public void onOk() {
//
//                }
//            }).show();
//        }
    }

    private void deletePending(){
        if (pending != null){
            ContainerPending containerPending = new ContainerPending();
            containerPending.setREFERENCIA(String.valueOf(pending.getReferencia()));
            containerPending.setTIPO_MOVIMIENTO(pending.getTipo_movimiento());
            containerPending.setPREFIJO(pending.getPrefijo());
            containerPending.setNUMERO_IDENTIFIACION(pending.getNumero_identifiacion());
            containerPending.setUSUARIO_GRABADOR(recorder);
            ResourceHandler.deleteContainerPending(this, Key.requestCode.DELETE_CONTAINER_PENDING, containerPending);
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.fr_btn_add:
                doReception();
                break;
            case R.id.fr_btn_search:
                doSearch();
                break;
            case R.id.fr_iv_datetime:
                dateType = DATETIME;
                Tools.showDialogFragment(getChildFragmentManager(), DateTimeDialogFragment.newInstance(this), DateTimeDialogFragment.TAG);
                break;
            case R.id.fr_iv_date_reception:
                dateType = DATE_RECEPTION;
                Tools.showDialogFragment(getChildFragmentManager(), DateTimeDialogFragment.newInstance(this), DateTimeDialogFragment.TAG);
                break;
            case R.id.fr_iv_date_delivery:
                dateType = DATE_DELIVERY;
                Tools.showDialogFragment(getChildFragmentManager(), DateTimeDialogFragment.newInstance(this), DateTimeDialogFragment.TAG);
                break;
            case R.id.fr_btn_cancel:
                doReceptionRevert();
                break;
            case R.id.fr_btn_pending:
                doSavePending();
                break;

        }
    }

    @Override
    public void onResponse(int requestCode, Object o, Object r) {
        switch (requestCode) {
            case Key.requestCode.GET_RECEPTION:
                if (o instanceof JsonObject) {
                    JsonObject response = (JsonObject) o;
                    if (response.getAsJsonArray(Key.jsonResponse.data).size() > 0){
                        receptionFound = new Gson().fromJson(response.getAsJsonArray(Key.jsonResponse.data).get(0), Reception.class);
                        setData(receptionFound);
                    } else {
                        setData(new Reception());
                        AlertUtil.showAlertOk(getString(R.string.all_text_warning), "No encontrado", getContext(), new AlertUtil.OnOkListener() {
                            @Override
                            public void onOk() {
                                // do nothing
                            }
                        }).show();
                    }

                }
                break;
            case Key.requestCode.DIALOG_DATETIME:
                if (o instanceof Date){
                    Date selectedDateTime = (Date)o;
                    if (dateType == DATETIME){
                        selectedDatetime = selectedDateTime;
                        tietDatetime.setText(Tools.formatDate(selectedDateTime, Key.datetimePattern.YYYYMMDD_HHMMSS_DASH_SEPARATOR));
                    } else if (dateType == DATE_RECEPTION){
                        selectedDateReception = selectedDateTime;
                        tietDateReception.setText(Tools.formatDate(selectedDateTime, Key.datetimePattern.YYYYMMDD_HHMMSS_DASH_SEPARATOR));
                    } else if (dateType == DATE_DELIVERY){
                        selectedDateDelivery = selectedDateTime;
                        tietDateDelivery.setText(Tools.formatDate(selectedDateTime, Key.datetimePattern.YYYYMMDD_HHMMSS_DASH_SEPARATOR));
                    }
                }
                break;
            case Key.requestCode.POST_RECEPTION:
                if (o instanceof JsonObject){
                    JsonObject jObject = (JsonObject) o;
                    BaseResponse response = new Gson().fromJson(jObject.toString(), BaseResponse.class);

                    if (response.isResultado()){
                        deletePending();
                        AlertUtil.showAlertOk(getString(R.string.all_text_success), "Exito", getContext(), new AlertUtil.OnOkListener() {
                            @Override
                            public void onOk() {

                            }
                        }).setCancelable(false)
                                .show();
                    } else {
                        AlertUtil.showAlertOk(getString(R.string.all_text_error), response.getMessage(), getContext(), new AlertUtil.OnOkListener() {
                            @Override
                            public void onOk() {
                                // do nothing
                            }
                        }).show();
                    }
                }
                break;
            case Key.requestCode.GET_YARD:
                if (o instanceof JsonObject) {
                    JsonObject response = (JsonObject) o;
                    SpinnerAdapter adapter = SpinnerAdapter.createArrayAdapter(getActivity(), response.getAsJsonArray(Key.jsonResponse.data).toString(), Yard.class);
                    spYard.setAdapter(adapter);
                }
                break;
            case Key.requestCode.GET_MACHINE:
                if (o instanceof JsonObject) {
                    JsonObject response = (JsonObject) o;
                    SpinnerAdapter adapter = SpinnerAdapter.createArrayAdapter(getActivity(), response.getAsJsonArray(Key.jsonResponse.data).toString(), Machine.class);
                    spMachine.setAdapter(adapter);
                }
                break;
            case Key.requestCode.PUT_RECEPTION:
            case Key.requestCode.POST_CONTAINER_PENDING:
                if (o instanceof JsonObject){
                    JsonObject jObject = (JsonObject) o;
                    BaseResponse response = new Gson().fromJson(jObject.toString(), BaseResponse.class);
                    if (response.isResultado()){
                        deletePending();
                        AlertUtil.showAlertOk(getString(R.string.all_text_success), response.getMessage(), getContext(), new AlertUtil.OnOkListener() {
                            @Override
                            public void onOk() {

                            }
                        }).setCancelable(false)
                                .show();
                    } else {
                        AlertUtil.showAlertOk(getString(R.string.all_text_error), response.getMessage(), getContext(), new AlertUtil.OnOkListener() {
                            @Override
                            public void onOk() {
                                // do nothing
                            }
                        }).show();
                    }
                }
                break;
            case Key.requestCode.DELETE_CONTAINER_PENDING:
                if (o instanceof JsonObject){
                    JsonObject jObject = (JsonObject) o;
                    BaseResponse response = new Gson().fromJson(jObject.toString(), BaseResponse.class);

                    if (response.isResultado()){
                        pending = null;
                        AlertUtil.showAlertOk(getString(R.string.all_text_success), response.getMessage(), getContext(), new AlertUtil.OnOkListener() {
                            @Override
                            public void onOk() {

                            }
                        }).setCancelable(false)
                                .show();
                    } else {
                        AlertUtil.showAlertOk(getString(R.string.all_text_error), response.getMessage(), getContext(), new AlertUtil.OnOkListener() {
                            @Override
                            public void onOk() {

                            }
                        }).setCancelable(true).show();
                    }
                }
                break;
        }
    }


}
