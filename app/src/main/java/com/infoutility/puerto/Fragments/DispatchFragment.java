package com.infoutility.puerto.Fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.common.collect.ImmutableMap;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.infoutility.puerto.Adapters.SpinnerAdapter;
import com.infoutility.puerto.Adapters.TextAdapter;
import com.infoutility.puerto.Api.ResourceHandler;
import com.infoutility.puerto.DialogFragments.DateTimeDialogFragment;
import com.infoutility.puerto.DialogFragments.RevertDialogFragment;
import com.infoutility.puerto.Interfaces.Response;
import com.infoutility.puerto.Keys.Key;
import com.infoutility.puerto.MainActivity;
import com.infoutility.puerto.Models.ContainerPending;
import com.infoutility.puerto.Models.ContainerPendingList;
import com.infoutility.puerto.Models.Dispatch;
import com.infoutility.puerto.Models.Machine;
import com.infoutility.puerto.Models.Response.BaseResponse;
import com.infoutility.puerto.R;
import com.infoutility.puerto.Utils.AlertUtil;
import com.infoutility.puerto.Utils.PreferencesManager;
import com.infoutility.puerto.Utils.Tools;

import java.util.Date;

public class DispatchFragment extends Fragment implements View.OnClickListener, Response.OnResponse {

    public static final String TAG = ReceptionFragment.class.getSimpleName();
    private static final int DATETIME = 1;
    private static final int DATE_RECEPTION = 2;
    private static final int DATE_DELIVERY = 3;

    private int recorder = 0;

    private TextInputEditText tietReception;
    private Button btnSearch;
    private TextView tvPrefix;
    private TextView tvId;
    private TextInputEditText tietDatetime;
    private TextInputEditText tietHandheld;
    private TextInputEditText tietDateDelivery;
    private TextInputEditText tietDateReception;
    private TextInputEditText tietComment;
    private Button btnAdd;
    private ImageView ivDatetime;
    private ImageView ivDateReception;
    private ImageView ivDateDelivery;
    private TextInputEditText tietMachine;
    private Button btnCancel;
    private Button btnPending;
    private TextView tvMachine;

    private int dateType = -1;
    private Date selectedDatetime;
    private Date selectedDateReception;
    private Date selectedDateDelivery;

    private Spinner spMachine;


    private Dispatch dispatchFound;
    private ContainerPendingList pending;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_dispatch, container, false);
        PreferencesManager.initializeInstance(getContext());
        recorder = PreferencesManager.getInstance().getInt(Key.sharedPreferences.recorder, 0);

        tietReception = v.findViewById(R.id.fd_tiet_reception);
        btnSearch = v.findViewById(R.id.fd_btn_search);
        tvPrefix = v.findViewById(R.id.fd_tv_prefix);
        tvId = v.findViewById(R.id.fd_tv_id);
        tietDatetime = v.findViewById(R.id.fd_tiet_datetime);
        tietHandheld = v.findViewById(R.id.fd_tiet_handheld);
        tietDateDelivery = v.findViewById(R.id.fd_tiet_delivery);
        tietDateReception = v.findViewById(R.id.fd_tiet_date_reception);
        btnAdd = v.findViewById(R.id.fd_btn_add);
        ivDatetime = v.findViewById(R.id.fd_iv_datetime);
        ivDateReception = v.findViewById(R.id.fd_iv_date_reception);
        ivDateDelivery = v.findViewById(R.id.fd_iv_date_delivery);
        spMachine = v.findViewById(R.id.fd_sp_machine);
        tietMachine = v.findViewById(R.id.fd_tiet_machine);
        btnCancel = v.findViewById(R.id.fd_btn_cancel);
        btnPending = v.findViewById(R.id.fd_btn_pending);
        tvMachine = v.findViewById(R.id.fd_tv_machine);

        ResourceHandler.listMachines(this, Key.requestCode.GET_MACHINE);

        //default
        setData(new Dispatch());
        ActionBar actionBar = ((MainActivity)getActivity()).getSupportActionBar();
        if (actionBar != null)
            actionBar.setTitle( "Despacho");

        btnAdd.setOnClickListener(this);
        btnSearch.setOnClickListener(this);
        ivDatetime.setOnClickListener(this);
        ivDateReception.setOnClickListener(this);
        ivDateDelivery.setOnClickListener(this);
        btnCancel.setOnClickListener(this);
        btnPending.setOnClickListener(this);

        spMachine.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                tvMachine.setText(((TextAdapter)adapterView.getItemAtPosition(i)).getData());
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        loadPending();

        return v;
    }

    private void setData(Dispatch dispatch) {
        String prefijo = getString(R.string.fr_title_prefix, dispatch.getPrefijo() == null ? "" : dispatch.getPrefijo());
        String noDespacho = "No. Despacho: " + dispatch.getAutorizacion_despacho();
        String prefijoChasis = "Prefijo Chasis: " + (dispatch.getPREFIJO_CHASIS() != null ? dispatch.getPREFIJO_CHASIS() : "");
        String noChasis = "No. Chasis: " + (dispatch.getNUMERO_CHASIS() != null ? dispatch.getNUMERO_CHASIS() : "");
        String placa = "Placa: " + (dispatch.getPlaca() != null ? dispatch.getPlaca() : "");
        tvPrefix.setText(noDespacho + "\n" + prefijoChasis + "\n" + noChasis + "\n" + placa + "\n" + prefijo);
        tvId.setText(getString(R.string.fr_title_id, dispatch.getNumero_de_identificacion() == null ? "" : dispatch.getNumero_de_identificacion()));
    }

    private void doSearch(){
        ResourceHandler.getDispatch(this, Key.requestCode.GET_DISPATCH,
                ImmutableMap.of(Key.api.paramAuthDispatch, tietReception.getText().toString()));
    }

    private String validate(){
//        if (tietHandheld.getText() == null || tietHandheld.getText().toString().trim().equals(""))
//            return "Debe ingresar Oficial de patio";
//        if (selectedDatetime == null)
//            return "Debe ingresar fecha y hora";
//        if (selectedDateDelivery == null)
//            return "Debe ingresar  fecha de entrega";
//        if (selectedDateReception == null)
//            return "Debe ingresar  fecha de recepcion";
        if (dispatchFound == null)
            return "No se puede realizar el despacho de un contenedor no existente";
        if (recorder == 0)
            return "No se ha podido obtener informacion del grabados";
//        if (tietMachine.getText() == null || tietMachine.getText().toString().trim().equals(""))
//            return "Debe ingresar maquina";

        return null;
    }


    private void doDispatch(){
        String validationMessage = validate();
        if (validationMessage == null){
            Dispatch dispatchRequest = new Dispatch();
            Date now = new Date();

            dispatchRequest.setAutorizacion_despacho(dispatchFound.getAutorizacion_despacho());
//            dispatchRequest.setFecha_hora_ubicador(Tools.formatDate(selectedDatetime, Key.datetimePattern.YYYYMMDD_HHMMSS_DASH_SEPARATOR));
            dispatchRequest.setFecha_hora_ubicador(Tools.formatDate(now, Key.datetimePattern.YYYYMMDD_HHMMSS_DASH_SEPARATOR));
            dispatchRequest.setFila_o_carril(dispatchFound.getFila_o_carril());
//            dispatchRequest.setGrabador_patio(Long.valueOf(tietHandheld.getText().toString()));
            dispatchRequest.setGrabador_patio(recorder);
            dispatchRequest.setHora_grabacion_patio(Tools.formatDate(now, Key.datetimePattern.YYYYMMDD_HHMMSS_DASH_SEPARATOR));
            dispatchRequest.setMaquina(Long.valueOf(((TextAdapter)spMachine.getSelectedItem()).getId()));
//            dispatchRequest.setMaquina(Integer.valueOf(tietMachine.getText().toString()));
            dispatchRequest.setModulo(dispatchFound.getModulo());
            dispatchRequest.setNivel(dispatchFound.getNivel());
            dispatchRequest.setNumero_de_identificacion(dispatchFound.getNumero_de_identificacion());
            dispatchRequest.setPatio(dispatchFound.getPatio());
            dispatchRequest.setPrefijo(dispatchFound.getPrefijo());
            String s = new Gson().toJson(dispatchRequest);
            ResourceHandler.postDispatch(this, Key.requestCode.POST_DISPATCH, dispatchRequest);
        } else {
            AlertUtil.showAlertOk(getString(R.string.all_text_warning), validationMessage, getContext(), new AlertUtil.OnOkListener() {
                @Override
                public void onOk() {
                    // do nothing
                }
            }).show();
        }
    }


    private void doDispatchRevert(){
        if (dispatchFound != null){
            Dispatch dispatchRequest = new Dispatch();
            Date now = new Date();

            dispatchRequest.setAutorizacion_despacho(dispatchFound.getAutorizacion_despacho());
            dispatchRequest.setFecha_hora_ubicador(Tools.formatDate(now, Key.datetimePattern.YYYYMMDD_HHMMSS_DASH_SEPARATOR));
            dispatchRequest.setFila_o_carril(dispatchFound.getFila_o_carril());
            dispatchRequest.setGrabador_patio(recorder);
            dispatchRequest.setHora_grabacion_patio(Tools.formatDate(now, Key.datetimePattern.YYYYMMDD_HHMMSS_DASH_SEPARATOR));
            dispatchRequest.setMaquina(dispatchFound.getMaquina());
            dispatchRequest.setModulo(dispatchFound.getModulo());
            dispatchRequest.setNivel(dispatchFound.getNivel());
            dispatchRequest.setNumero_de_identificacion(dispatchFound.getNumero_de_identificacion());
            dispatchRequest.setPatio(dispatchFound.getPatio());
            dispatchRequest.setPrefijo(dispatchFound.getPrefijo());
            String s = new Gson().toJson(dispatchRequest);
            ResourceHandler.putDispatch(this, Key.requestCode.PUT_DISPATCH, dispatchRequest);
        } else {
            AlertUtil.showAlertOk(getString(R.string.all_text_warning), "No se ha seleccionado despacho.", getContext(), new AlertUtil.OnOkListener() {
                @Override
                public void onOk() {
                    // do nothing
                }
            }).show();
        }
    }


    private void showDispatchRevert() {
        if (dispatchFound != null){
            DialogFragment fragments = RevertDialogFragment.newInstance(this);
            fragments.setArguments(Tools.fromObject(Key.extra.YARD_CONTAINER_REVERT_DISPATCH, dispatchFound));
            fragments.show(getChildFragmentManager(), RevertDialogFragment.TAG);
        } else {
            AlertUtil.showAlertOk(getString(R.string.all_text_warning), "No se ha seleccionado despacho.", getContext(), new AlertUtil.OnOkListener() {
                @Override
                public void onOk() {
                    // do nothing
                }
            }).show();
        }
    }

    private void cleanForm(){
        tietMachine.setText(null);
        tietReception.setText(null);
        tvPrefix.setText("");
        tvId.setText("");
    }

    private void doSavePending(){
        if (dispatchFound != null){
            ContainerPending containerPending = new ContainerPending();
            containerPending.setREFERENCIA(String.valueOf(dispatchFound.getAutorizacion_despacho()));
            containerPending.setTIPO_MOVIMIENTO("D");
            containerPending.setPREFIJO(dispatchFound.getPrefijo());
            containerPending.setNUMERO_IDENTIFIACION(dispatchFound.getNumero_de_identificacion());
            containerPending.setUSUARIO_GRABADOR(recorder);
            containerPending.setFECHA_GRABACION(Tools.formatDate(new Date(), Key.datetimePattern.YYYYMMDD_HHMMSS_DASH_SEPARATOR));
            containerPending.setPARAMETROS(String.valueOf(dispatchFound.getAutorizacion_despacho()));

            ResourceHandler.postContainerPending(this, Key.requestCode.POST_CONTAINER_PENDING, containerPending);
        } else {
            AlertUtil.showAlertOk(getString(R.string.all_text_warning), "Despacho no encontrada", getContext(), new AlertUtil.OnOkListener() {
                @Override
                public void onOk() {
                    // do nothing
                }
            }).show();
        }
    }


    private void loadPending(){
        if (ListContainerYardWaitFragment.ACTION_TEMP && !ListContainerYardWaitFragment.JSON_FORMAT.equals("")){
            ListContainerYardWaitFragment.ACTION_TEMP = false;
            pending = new Gson().fromJson(ListContainerYardWaitFragment.JSON_FORMAT, ContainerPendingList.class);
            tietReception.setText(String.valueOf(pending.getReferencia()));

            doSearch();

        }
//        else {
//            AlertUtil.showAlertOk(getString(R.string.all_text_warning), "No se pudo cargar la informacion.", getContext(), new AlertUtil.OnOkListener() {
//                @Override
//                public void onOk() {
//
//                }
//            }).show();
//        }
    }

    private void deletePending(){
        if (pending != null){
            ContainerPending containerPending = new ContainerPending();
            containerPending.setREFERENCIA(String.valueOf(pending.getReferencia()));
            containerPending.setTIPO_MOVIMIENTO(pending.getTipo_movimiento());
            containerPending.setPREFIJO(pending.getPrefijo());
            containerPending.setNUMERO_IDENTIFIACION(pending.getNumero_identifiacion());
            containerPending.setUSUARIO_GRABADOR(recorder);
            ResourceHandler.deleteContainerPending(this, Key.requestCode.DELETE_CONTAINER_PENDING, containerPending);
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.fd_btn_add:
                doDispatch();
                break;
            case R.id.fd_btn_search:
                doSearch();
                break;
            case R.id.fd_iv_datetime:
                dateType = DATETIME;
                Tools.showDialogFragment(getChildFragmentManager(), DateTimeDialogFragment.newInstance(this), DateTimeDialogFragment.TAG);
                break;
            case R.id.fd_iv_date_reception:
                dateType = DATE_RECEPTION;
                Tools.showDialogFragment(getChildFragmentManager(), DateTimeDialogFragment.newInstance(this), DateTimeDialogFragment.TAG);
                break;
            case R.id.fd_iv_date_delivery:
                dateType = DATE_DELIVERY;
                Tools.showDialogFragment(getChildFragmentManager(), DateTimeDialogFragment.newInstance(this), DateTimeDialogFragment.TAG);
                break;
            case R.id.fd_btn_cancel:
//                doDispatchRevert();
//                Tools.showDialogFragment(getChildFragmentManager(), RevertDialogFragment.newInstance(this), RevertDialogFragment.TAG);
                showDispatchRevert();
                break;
            case R.id.fd_btn_pending:
                doSavePending();
                break;

        }
    }

    @Override
    public void onResponse(int requestCode, Object o, Object r) {
        switch (requestCode) {
            case Key.requestCode.GET_DISPATCH:
                if (o instanceof JsonObject) {
                    JsonObject response = (JsonObject) o;
                    if (response.getAsJsonArray(Key.jsonResponse.data).size() > 0){
                        dispatchFound = new Gson().fromJson(response.getAsJsonArray(Key.jsonResponse.data).get(0), Dispatch.class);
                        setData(dispatchFound);
                    } else {
                        setData(new Dispatch());
                        AlertUtil.showAlertOk(getString(R.string.all_text_warning), "No encontrado", getContext(), new AlertUtil.OnOkListener() {
                            @Override
                            public void onOk() {
                                // do nothing
                            }
                        }).show();
                    }

                }
                break;
            case Key.requestCode.DIALOG_DATETIME:
                if (o instanceof Date){
                    Date selectedDateTime = (Date)o;
                    if (dateType == DATETIME){
                        selectedDatetime = selectedDateTime;
                        tietDatetime.setText(Tools.formatDate(selectedDateTime, Key.datetimePattern.YYYYMMDD_HHMMSS_DASH_SEPARATOR));
                    } else if (dateType == DATE_RECEPTION){
                        selectedDateReception = selectedDateTime;
                        tietDateReception.setText(Tools.formatDate(selectedDateTime, Key.datetimePattern.YYYYMMDD_HHMMSS_DASH_SEPARATOR));
                    } else if (dateType == DATE_DELIVERY){
                        selectedDateDelivery = selectedDateTime;
                        tietDateDelivery.setText(Tools.formatDate(selectedDateTime, Key.datetimePattern.YYYYMMDD_HHMMSS_DASH_SEPARATOR));
                    }
                }
                break;
            case Key.requestCode.POST_DISPATCH:
            case Key.requestCode.POST_CONTAINER_PENDING:
                if (o instanceof JsonObject){
                    JsonObject jObject = (JsonObject) o;
                    BaseResponse response = new Gson().fromJson(jObject.toString(), BaseResponse.class);

                    if (response.isResultado()){
                        cleanForm();
                        AlertUtil.showAlertOk(getString(R.string.all_text_success), response.getMessage(), getContext(), new AlertUtil.OnOkListener() {
                            @Override
                            public void onOk() {

                            }
                        }).setCancelable(false)
                                .show();
                    } else {
                        AlertUtil.showAlertOk(getString(R.string.all_text_error), response.getMessage(), getContext(), new AlertUtil.OnOkListener() {
                            @Override
                            public void onOk() {
                                // do nothing
                            }
                        }).show();
                    }
                }
                break;
            case Key.requestCode.GET_MACHINE:
                if (o instanceof JsonObject) {
                    JsonObject response = (JsonObject) o;
                    SpinnerAdapter adapter = SpinnerAdapter.createArrayAdapter(getActivity(), response.getAsJsonArray(Key.jsonResponse.data).toString(), Machine.class);
                    spMachine.setAdapter(adapter);
                }
                break;
            case Key.requestCode.PUT_DISPATCH:
                if (o instanceof JsonObject){
                    JsonObject jObject = (JsonObject) o;
                    BaseResponse response = new Gson().fromJson(jObject.toString(), BaseResponse.class);

                    if (response.isResultado()){
                        AlertUtil.showAlertOk(getString(R.string.all_text_success), response.getMessage(), getContext(), new AlertUtil.OnOkListener() {
                            @Override
                            public void onOk() {

                            }
                        }).setCancelable(false)
                                .show();
                    } else {
                        AlertUtil.showAlertOk(getString(R.string.all_text_error), response.getMessage(), getContext(), new AlertUtil.OnOkListener() {
                            @Override
                            public void onOk() {
                                // do nothing
                            }
                        }).show();
                    }
                }
                break;
            case Key.requestCode.RESPONSE_DIALOG:
                cleanForm();
                break;
            case Key.requestCode.DELETE_CONTAINER_PENDING:
                if (o instanceof JsonObject){
                    JsonObject jObject = (JsonObject) o;
                    BaseResponse response = new Gson().fromJson(jObject.toString(), BaseResponse.class);

                    if (response.isResultado()){
                        pending = null;
                        cleanForm();
                        AlertUtil.showAlertOk(getString(R.string.all_text_success), response.getMessage(), getContext(), new AlertUtil.OnOkListener() {
                            @Override
                            public void onOk() {

                            }
                        }).setCancelable(false)
                                .show();
                    } else {
                        AlertUtil.showAlertOk(getString(R.string.all_text_error), response.getMessage(), getContext(), new AlertUtil.OnOkListener() {
                            @Override
                            public void onOk() {

                            }
                        }).setCancelable(true).show();
                    }
                }
                break;
        }
    }
}
