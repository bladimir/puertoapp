package com.infoutility.puerto.Fragments;


import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.google.common.collect.ImmutableMap;
import com.infoutility.puerto.Api.ResourceHandler;
import com.infoutility.puerto.DialogFragments.ListRequestDispatchDialogFragment;
import com.infoutility.puerto.Keys.Key;
import com.infoutility.puerto.R;
import com.infoutility.puerto.Utils.AlertUtil;
import com.infoutility.puerto.Utils.JsonBundle;
import com.infoutility.puerto.Utils.Tools;

import org.json.JSONObject;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class ContainerColdDispatchFragment extends Fragment implements com.infoutility.puerto.Interfaces.Response.OnResponse, View.OnClickListener {
    public static final String TAG = ContainerColdDispatchFragment.class.getSimpleName();
    private TextView txtInfo;
    private Bundle bundleRequest;
    private ProgressDialog progressDialog;


    public ContainerColdDispatchFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_container_cold_dispatch, container, false);
        initView(view);
        return view;
    }

    private void initView(View view){
        //recyclerViewListRequest = (RecyclerView) view.findViewById(R.id.fccr_rv_container_request);
        ((Button)view.findViewById(R.id.fccd_btn_search)).setOnClickListener(this);
        ((Button)view.findViewById(R.id.fccd_btn_create)).setOnClickListener(this);
        txtInfo = view.findViewById(R.id.fccd_tv_trip_texts);
    }

    private void getInfoRequest(){
        try{
            this.initProgressDialog("Buscando.");
            String num = ((TextInputEditText)getView().findViewById(R.id.fccd_tiet_num)).getText().toString();
            String ref = ((TextInputEditText)getView().findViewById(R.id.fccd_tiet_pref)).getText().toString();

            ResourceHandler.getInfoRequestDispatch(this, Key.requestCode.GET_LIST_DISPATCH,
                    ImmutableMap.of(Key.api.paramPrefijo, ref, Key.api.paramNumberIDentify, num));
        }catch (Exception e){

        }

    }

    private void initProgressDialog(String msg){
        progressDialog = new ProgressDialog(getContext());
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage(msg);
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    private void containerDetailReceipt(){
        if(this.bundleRequest == null){
            AlertUtil.showAlertOk(getString(R.string.all_text_warning), "No tiene un item selecconado", getContext(), new AlertUtil.OnOkListener() {
                @Override
                public void onOk() {
//                                idJoined = Integer.valueOf(response.getMessage());
                }
            }).show();
            return;
        }
        DialogFragment fragments = ListRequestDispatchDialogFragment.newInstance(this, bundleRequest);
        //fragments.setArguments(Tools.fromObject(Key.extra.CONTAINER_DETAIL, container));
        fragments.show(getChildFragmentManager(), ListRequestDispatchDialogFragment.TAG);
    }



    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.fccd_btn_create:
                this.containerDetailReceipt();
                break;
            case R.id.fccd_btn_search:
                getInfoRequest();
                break;
        }
    }

    @Override
    public void onResponse(int requestCode, Object o, Object r) {
        if(this.progressDialog != null) progressDialog.dismiss();
        if(requestCode == Key.requestCode.GET_LIST_DISPATCH){
            Log.i(TAG, o.toString());
            try{
                JSONObject jsonObject = new JSONObject(o.toString());
                Bundle bundle = JsonBundle.createBundle(jsonObject);

                if(bundle == null){
                    AlertUtil.showAlertOk(getString(R.string.all_text_warning), "No se ha encontrado ningun item", getContext(), new AlertUtil.OnOkListener() {
                        @Override
                        public void onOk() {
//                                idJoined = Integer.valueOf(response.getMessage());
                        }
                    }).show();
                    return;
                }
                ArrayList<Bundle> listInfo = bundle.getParcelableArrayList(Key.jsonResponse.data);
                Bundle temp = listInfo.get(0);
                bundleRequest = temp;
                String dateC = "----";
                String dateD = "----";
                if(temp.getString("fecha_coneccion", null) !=null){
                    dateC =  Tools.formatStrDate( temp.getString("fecha_coneccion", null), Key.datetimePattern.YYYYMMDD_HHMMSS_DASH_SEPARATOR);
                }

                if(temp.getString("fecha_desconexion", null) !=null){
                    dateD =  Tools.formatStrDate( temp.getString("fecha_desconexion", null), Key.datetimePattern.YYYYMMDD_HHMMSS_DASH_SEPARATOR);
                }

                String infoText = "Viaje:" + temp.getString("viaje_no", "") + "\n\r" +
                        "Medida: " + temp.getInt("medida", 0) + "\n\r" +
                        "CoF: " + temp.getString("c_o_f", "") + "\n\r" +
                        "Numero de Identificacion: " + temp.getString("numero_de_identificacion", "") + "\n\r" +
                        "Prefijo: " + temp.getString("prefijo", "") + "\n\r" +
                        "Tara: " + temp.getInt("tara", 0) + "\n\r" +
                        "Peso Manifiesto: " + temp.getInt("peso_manifestado", 0) + "\n\r" +
                        "Naviera: " + temp.getString("naviera", "") + "\n\r" +
                        "Barco: " + temp.getString("barco", "") + "\n\r" +
                        "Fecha Conexion: " + dateC + "\n\r" +
                        "Fecha Desconexion: " + dateD + "\n\r";
                txtInfo.setText(infoText);


            }catch (Exception e){

            }
        }else if(requestCode == Key.requestCode.RESPONSE_DIALOG){
            boolean status = (boolean)o;
            if(status){
                AlertUtil.showAlertOk(getString(R.string.all_text_warning), "Reporte creado con exito", getContext(), new AlertUtil.OnOkListener() {
                    @Override
                    public void onOk() {
//                                idJoined = Integer.valueOf(response.getMessage());
                    }
                }).show();
                getInfoRequest();
            }else{
                AlertUtil.showAlertOk(getString(R.string.all_text_warning), "Reporte no se puedo crear, por favor intentar luego", getContext(), new AlertUtil.OnOkListener() {
                    @Override
                    public void onOk() {
//                                idJoined = Integer.valueOf(response.getMessage());
                    }
                }).show();
            }
        }
    }
}
