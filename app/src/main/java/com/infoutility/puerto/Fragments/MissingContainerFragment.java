package com.infoutility.puerto.Fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.infoutility.puerto.Adapters.ContainerAdapter;
import com.infoutility.puerto.Interfaces.Response;
import com.infoutility.puerto.MainActivity;
import com.infoutility.puerto.Models.Container;
import com.infoutility.puerto.Models.ContainerJoined;
import com.infoutility.puerto.R;

import java.util.ArrayList;
import java.util.List;

public class MissingContainerFragment extends Fragment implements Response.OnResponse {
    public static final String TAG = MissingContainerFragment.class.getSimpleName();

    private RecyclerView rvMissingContainer;
    private ContainerAdapter containerAdapter;
    private List<Container> lstContainer;
    private List<ContainerJoined> lstContainerJoined;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setHasOptionsMenu(false);

        ActionBar actionBar = ((MainActivity)getActivity()).getSupportActionBar();
        if (actionBar != null)
            actionBar.setTitle(getResources().getString(R.string.fmc_title_missing_container));
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_missing_container, container, false);

        rvMissingContainer = v.findViewById(R.id.fmc_rv_missing_container);


        lstContainer = new ArrayList<>();
        lstContainerJoined = new ArrayList<>();


        containerAdapter = new ContainerAdapter(getContext(), lstContainer, this, lstContainerJoined);

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext());
        rvMissingContainer.setLayoutManager(mLayoutManager);
        rvMissingContainer.setAdapter(containerAdapter);


        return v;
    }


    @Override
    public void onResponse(int requestCode, Object o, Object r) {

    }
}
