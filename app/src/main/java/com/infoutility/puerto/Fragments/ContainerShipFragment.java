package com.infoutility.puerto.Fragments;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;

import com.google.gson.JsonObject;
import com.infoutility.puerto.Adapters.ContainerShipAdapter;
import com.infoutility.puerto.Api.ResourceHandler;
import com.infoutility.puerto.DialogFragments.ContainerShipDetailDialogFragment;
import com.infoutility.puerto.Interfaces.Response;
import com.infoutility.puerto.Keys.Key;
import com.infoutility.puerto.MainActivity;
import com.infoutility.puerto.Models.ContainerShip;
import com.infoutility.puerto.R;
import com.infoutility.puerto.Utils.PreferencesManager;
import com.infoutility.puerto.Utils.Tools;

import java.util.ArrayList;
import java.util.List;

public class ContainerShipFragment extends Fragment implements Response.OnResponse, View.OnClickListener {

    public static final String TAG = ContainerShipFragment.class.getSimpleName();
    //    View
    private RecyclerView rvContainerShips;
    private Button btnSearch;
    private TextInputEditText tietTrip;
    private Spinner spFilter;

    private MenuItem actionInfo;
    private MenuItem actionDetail;


    private ContainerShipAdapter containerShipAdapter;
    private List<ContainerShip> lstContainerShips;

    private boolean isSearch;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_container_ship, container, false);

        setHasOptionsMenu(false);

        PreferencesManager.initializeInstance(getContext());

        ResourceHandler.listTrips(this, Key.requestCode.GET_TRIP, "%", 50);

        ActionBar actionBar = ((MainActivity)getActivity()).getSupportActionBar();
        if (actionBar != null)
            actionBar.setTitle("BÚSQUEDA DE BUQUE");

        rvContainerShips = v.findViewById(R.id.fcs_rv_container_ships);
        btnSearch = v.findViewById(R.id.fcs_btn_search);
        tietTrip = v.findViewById(R.id.fcs_tiet_trip);
        spFilter = v.findViewById(R.id.fcs_sp_filter);

        spFilter.setAdapter(new ArrayAdapter<>(getActivity(), R.layout.support_simple_spinner_dropdown_item, Key.containerSituation.values()));

        spFilter.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                Key.containerSituation s = (Key.containerSituation)adapterView.getItemAtPosition(i);
                if (s.id() == 0)
                    doFilterBy("");
                else
                    doFilterBy(s.description());
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        Tools.setFragmentLoading(getActivity(), true);

        btnSearch.setOnClickListener(this);

        lstContainerShips = new ArrayList<>();

        containerShipAdapter = new ContainerShipAdapter(getContext(), lstContainerShips, this);

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext());
        rvContainerShips.setLayoutManager(mLayoutManager);
        rvContainerShips.setAdapter(containerShipAdapter);

        return v;
    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        inflater.inflate(R.menu.container_ship_menu, menu);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.csm_action_info:
//                containerShipDetailAction();
                return true;
//            case R.id.scm_action_detail:
//                //
//                return true;
            default: return super.onOptionsItemSelected(item);
        }
    }


    private void doFilterBy(String situation){
        containerShipAdapter.getFilter().filter(situation);
        containerShipAdapter.notifyDataSetChanged();
    }

    private void containerShipDetailAction(ContainerShip containerShip){
        DialogFragment fragments = ContainerShipDetailDialogFragment.newInstance();
        fragments.setArguments(Tools.fromObject(Key.extra.CONTAINER_SHIP_DETAIL, containerShip));
        fragments.show(getChildFragmentManager(), ContainerShipDetailDialogFragment.TAG);
    }


    private void navigateToShift(final ContainerShip containerShip){
        final ArrayAdapter<Key.workMode> workmodes =new ArrayAdapter<>(getContext(), R.layout.support_simple_spinner_dropdown_item_custom, Key.workMode.values());
        final AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle(R.string.title_work_mode)
                .setAdapter(workmodes, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Key.workMode wMode = workmodes.getItem(which);
                        if (wMode != null && getFragmentManager() != null){
                            PreferencesManager.getInstance().setInt(Key.sharedPreferences.workMode, wMode.id());
                            PreferencesManager.getInstance().setLong(Key.sharedPreferences.trip, containerShip.getViaje_empornac());
                            PreferencesManager.getInstance().setString(Key.sharedPreferences.tripName, containerShip.getBarco());
                            WorkShiftFragment workShiftFragment = new WorkShiftFragment();
                            workShiftFragment.setArguments(Tools.fromObject(Key.extra.CONTAINER_SHIP_NAVIGATE, containerShip));
                            Tools.setFragment(getFragmentManager(),  R.id.fl_main_content, workShiftFragment, WorkShiftFragment.TAG);
                        }
                    }
                })
                .setNegativeButton(R.string.all_text_cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // do nothing
                    }
                });
        builder.show();

    }

    @Override
    public void onResponse(int requestCode, Object o, Object r) {
        switch (requestCode){
            case Key.requestCode.CONTAINER_SHIP_DETAIL:
                if (o instanceof ContainerShip){
                    containerShipDetailAction((ContainerShip)o);
                }
                break;
            case Key.requestCode.CONTAINER_SHIFT:
                if (o instanceof ContainerShip){
                    navigateToShift((ContainerShip)o);
                }
                break;
            case Key.requestCode.GET_TRIP:
                Tools.setFragmentLoading(getActivity(), false);
                if (o instanceof JsonObject){
                    lstContainerShips.clear();
                    JsonObject response = (JsonObject) o;
                    lstContainerShips.addAll(Tools.getListFromJson(response.getAsJsonArray(Key.jsonResponse.data).toString(), ContainerShip.class));
                    containerShipAdapter.notifyDataSetChanged();
                    if (isSearch)
                        spFilter.setSelection(Key.containerSituation.ALL.id());
                    else
                        spFilter.setSelection(Key.containerSituation.ATRAQUE.id()); //Default
                }
                break;
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.fcs_btn_search:
                isSearch = true;
                searchContainerShip(tietTrip.getText().toString(), 50);
                break;
            default:
                break;
        }
    }

    private void searchContainerShip(String trip, int max) {
        ResourceHandler.listTrips(this, Key.requestCode.GET_TRIP, trip + "%", max);
    }
}
