package com.infoutility.puerto.Fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.common.collect.ImmutableMap;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.infoutility.puerto.Adapters.DelayAdapter;
import com.infoutility.puerto.Adapters.SpinnerAdapter;
import com.infoutility.puerto.Adapters.TextAdapter;
import com.infoutility.puerto.Api.ResourceHandler;
import com.infoutility.puerto.DialogFragments.DateTimeDialogFragment;
import com.infoutility.puerto.Interfaces.Response;
import com.infoutility.puerto.Keys.Key;
import com.infoutility.puerto.MainActivity;
import com.infoutility.puerto.Models.DelayType;
import com.infoutility.puerto.Models.LoadType;
import com.infoutility.puerto.Models.Request.Delay;
import com.infoutility.puerto.Models.Response.BaseResponse;
import com.infoutility.puerto.R;
import com.infoutility.puerto.Utils.AlertUtil;
import com.infoutility.puerto.Utils.PreferencesManager;
import com.infoutility.puerto.Utils.Tools;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class DelayFragment extends Fragment implements View.OnClickListener, Response.OnResponse {
    public static final String TAG = DelayFragment.class.getSimpleName();


    private Spinner spType;
    private TextInputEditText tietDescription;
    private TextView tvDateTimeStart;
    private ImageView ivDateTimeStart;
    private TextView tvDateTimeEnd;
    private ImageView ivDateTimeEnd;
    private Spinner spLoad;
    private TextInputEditText tietComment;
    private RecyclerView rvDelays;
    private Button btnAdd;
    private TextInputEditText tietType;

    private List<com.infoutility.puerto.Models.Delay> lstDelays;
    private DelayAdapter delayAdapter;

    private long trip = 0;
    private int recorder = 0;
    private int workMode = 0;
    private long currentShift = 0;
    private String handheld = null;


    private Date selectedTimeStart;
    private Date selectedTimeEnd;
    private boolean isStartDateTime;
    private String strDateTimeStart;
    private String strDateTimeEnd;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setHasOptionsMenu(false);

        ActionBar actionBar = ((MainActivity)getActivity()).getSupportActionBar();
        if (actionBar != null)
            actionBar.setTitle(getResources().getString(R.string.df_title_add_delay));
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_delay, container, false);
        PreferencesManager.initializeInstance(getContext());

        trip = PreferencesManager.getInstance().getLong(Key.sharedPreferences.trip, 0L);
        recorder = PreferencesManager.getInstance().getInt(Key.sharedPreferences.recorder, 0);
        workMode = PreferencesManager.getInstance().getInt(Key.sharedPreferences.workMode, 0);
        currentShift = PreferencesManager.getInstance().getLong(Key.sharedPreferences.currentShift, 0L);
        handheld = PreferencesManager.getInstance().getString(Key.sharedPreferences.handheld, null);

        ResourceHandler.listLoadTypes(this, Key.requestCode.GET_LOAD_TYPE);
        ResourceHandler.listDelayTypes(this, Key.requestCode.GET_DELAY_TYPE);


        spType = v.findViewById(R.id.df_sp_type);
        tietDescription = v.findViewById(R.id.df_tiet_description);
        tvDateTimeStart = v.findViewById(R.id.df_tv_datetime_start);
        ivDateTimeStart = v.findViewById(R.id.df_iv_datetime_start);
        tvDateTimeEnd = v.findViewById(R.id.df_tv_datetime_end);
        ivDateTimeEnd = v.findViewById(R.id.df_iv_datetime_end);
        spLoad = v.findViewById(R.id.df_sp_load);
        tietComment = v.findViewById(R.id.df_tiet_comment);
        rvDelays = v.findViewById(R.id.df_rv_delays);
        btnAdd = v.findViewById(R.id.df_btn_add);
        tietType = v.findViewById(R.id.df_tiet_type);


        ivDateTimeStart.setOnClickListener(this);
        ivDateTimeEnd.setOnClickListener(this);
        btnAdd.setOnClickListener(this);


        lstDelays = new ArrayList<>();
        delayAdapter = new DelayAdapter(getContext(), lstDelays, this);

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext());
        rvDelays.setLayoutManager(mLayoutManager);
        rvDelays.setAdapter(delayAdapter);


        spType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                TextAdapter selected = (TextAdapter) adapterView.getItemAtPosition(i);
                if (selected.getId().equals("115")
                        || selected.getId().equals("209")
                        || selected.getId().equals("500")
                        || selected.getId().equals("400")){
                    tietDescription.setText("");
                    tietDescription.setEnabled(true);
                }

                else {
                    tietDescription.setText(selected.getData());
                    tietDescription.setEnabled(false);
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        return v;
    }

    private void doDataLoad(){
                    ResourceHandler.listDelays(this, Key.requestCode.GET_DELAY,
                            ImmutableMap.of(Key.api.paramTrip, String.valueOf(trip),
                                    Key.api.paramMoveTypeAlt, String.valueOf(workMode),
                                    Key.api.paramRecorder, String.valueOf(recorder)));
        /*ResourceHandler.listDelays(this, Key.requestCode.GET_DELAY,
                ImmutableMap.of(Key.api.paramTrip,"201000000073",
                        Key.api.paramMoveTypeAlt, String.valueOf(2),2019
                        Key.api.paramRecorder, String.valueOf(2128)));*/
    }

    private String validate(){
        if (recorder == 0)
            return "No existe recorder al cual asginar turno.";
        if (workMode == 0)
            return "Debe seleccionar una opcion de trabajo";
        if (strDateTimeStart == null)
            return "Debe selecionar la fecha de inicio de turno";
        if (strDateTimeEnd == null)
            return "Debe selecionar la fecha de fin de turno";
        if (selectedTimeStart != null && selectedTimeEnd != null){
            if (selectedTimeStart.compareTo(selectedTimeEnd) > 0){
                return "La fecha de fin de turno debe ser mayor a la fecha de inicio.";
            }
        }
        if (tietComment.getText() == null)
            return "Debe ingresar observacion.";
        if (tietDescription.getText() == null)
            return "Debe ingresar descripcion";
//        if (tietType.getText() == null)
//            return "Debe ingresar el tipo demora";
        return null;
    }

    private void addDelay(){
        String validationMessage = validate();
        if (validationMessage == null){
            Delay delayRequest = new Delay();
            delayRequest.setNum_rep_demora(0);//?
            delayRequest.setDemora(Integer.valueOf(((TextAdapter)spType.getSelectedItem()).getId()));
//            delayRequest.setDemora(Integer.valueOf(tietType.getText().toString()));
            delayRequest.setPers_persona(Integer.valueOf(recorder));
            delayRequest.setInicio(strDateTimeStart);
            delayRequest.setViaje(trip);
            delayRequest.setFin(strDateTimeEnd);
            delayRequest.setGrabador(String.valueOf(recorder));
            delayRequest.setHandheld_id(String.valueOf(handheld));
            delayRequest.setTurno(currentShift);
            delayRequest.setTipo_movimiento(String.valueOf(workMode));
            delayRequest.setTipo_carga(((TextAdapter)spLoad.getSelectedItem()).getId());
            delayRequest.setFecha_grabacion(Tools.formatDate(new Date(), Key.datetimePattern.YYYYMMDD_HHMMSS_DASH_SEPARATOR));
            delayRequest.setDESCRIPCION(tietDescription.getText() == null ? "" : tietDescription.getText().toString());

            ResourceHandler.postDelay(this, Key.requestCode.POST_DELAY, delayRequest);
        } else {
            AlertUtil.showAlertOk(getString(R.string.all_text_warning), validationMessage, getContext(), new AlertUtil.OnOkListener() {
                @Override
                public void onOk() {
                    // do nothing
                }
            }).show();
        }
    }

    private void deleteDelay(com.infoutility.puerto.Models.Delay delay){
        Delay delayRequest = new Delay();
        delayRequest.setNum_rep_demora(delay.getNum_rep_demora());
        delayRequest.setDemora(delay.getDemora());
        delayRequest.setPers_persona(delay.getPers_persona());
        delayRequest.setInicio(Tools.formatStrDate(delay.getInicio(), Key.datetimePattern.YYYYMMDD_HHMMSS_DASH_SEPARATOR));
        delayRequest.setViaje(delay.getViaje());
        delayRequest.setFin(Tools.formatStrDate(delay.getFin(), Key.datetimePattern.YYYYMMDD_HHMMSS_DASH_SEPARATOR));
        delayRequest.setGrabador(delay.getGrabador());
        delayRequest.setHandheld_id(delay.getHandheld_id());
        delayRequest.setTurno(delay.getTurno());
        delayRequest.setTipo_movimiento(delay.getTipo_movimiento());
        delayRequest.setTipo_carga(delay.getTipo_carga());
        delayRequest.setFecha_grabacion(Tools.formatStrDate(delay.getFecha_grabacion(), Key.datetimePattern.YYYYMMDD_HHMMSS_DASH_SEPARATOR));

        String s = new Gson().toJson(delayRequest);
        ResourceHandler.deleteDelay(this, Key.requestCode.DELETE_DELAY, delayRequest);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.df_btn_add:
                addDelay();
                break;
            case R.id.df_iv_datetime_start:
                this.isStartDateTime = true;
                Tools.showDialogFragment(getChildFragmentManager(), DateTimeDialogFragment.newInstance(this), DateTimeDialogFragment.TAG);
                break;
            case R.id.df_iv_datetime_end:
                this.isStartDateTime = false;
                Tools.showDialogFragment(getChildFragmentManager(), DateTimeDialogFragment.newInstance(this), DateTimeDialogFragment.TAG);
                break;
        }
    }

    @Override
    public void onResponse(int requestCode, Object o, Object r) {
        switch (requestCode) {
            case Key.requestCode.GET_LOAD_TYPE:
                if (o instanceof JsonObject) {
                    JsonObject response = (JsonObject) o;
                    SpinnerAdapter adapter = SpinnerAdapter.createArrayAdapter(getActivity(), response.getAsJsonArray(Key.jsonResponse.data).toString(), LoadType.class);
                    spLoad.setAdapter(adapter);
                }
            break;
            case Key.requestCode.GET_DELAY_TYPE:
                if (o instanceof JsonObject) {
                    JsonObject response = (JsonObject) o;
                    PreferencesManager.getInstance().setString(Key.catalog.lstDelayType, response.getAsJsonArray(Key.jsonResponse.data).toString());
                    SpinnerAdapter adapter = SpinnerAdapter.createArrayAdapter(getActivity(), response.getAsJsonArray(Key.jsonResponse.data).toString(), DelayType.class);
                    spType.setAdapter(adapter);

                    doDataLoad();
                }
            break;
            case Key.requestCode.DIALOG_DATETIME:
                if (o instanceof Date){
                    Date selectedDateTime = (Date)o;
                    if (isStartDateTime){
                        selectedTimeStart = selectedDateTime;
                        strDateTimeStart = Tools.formatDate(selectedDateTime, Key.datetimePattern.YYYYMMDD_HHMMSS_DASH_SEPARATOR);
                        tvDateTimeStart.setText(strDateTimeStart);
                    } else {
                        selectedTimeEnd = selectedDateTime;
                        strDateTimeEnd = Tools.formatDate(selectedDateTime, Key.datetimePattern.YYYYMMDD_HHMMSS_DASH_SEPARATOR);
                        tvDateTimeEnd.setText(strDateTimeEnd);
                    }
                }
            break;
            case Key.requestCode.POST_DELAY:
                if (o instanceof JsonObject){
                    JsonObject jObject = (JsonObject) o;
                    BaseResponse response = new Gson().fromJson(jObject.toString(), BaseResponse.class);

                    if (response.isResultado()){
                        AlertUtil.showAlertOk(getString(R.string.all_text_success), response.getMessage(), getContext(), new AlertUtil.OnOkListener() {
                            @Override
                            public void onOk() {
                                    doDataLoad();
                            }
                        }).setCancelable(false)
                                .show();
                    } else {
                        AlertUtil.showAlertOk(getString(R.string.all_text_error), response.getMessage(), getContext(), new AlertUtil.OnOkListener() {
                            @Override
                            public void onOk() {
                                // do nothing
                            }
                        }).show();
                    }
                }
                break;
            case Key.requestCode.GET_DELAY:
                if (o instanceof JsonObject){
                    lstDelays.clear();
                    JsonObject response = (JsonObject) o;
                    lstDelays.addAll(Tools.getListFromJson(response.getAsJsonArray(Key.jsonResponse.data).toString(), com.infoutility.puerto.Models.Delay.class));
                    delayAdapter.loadCatalog();
                    delayAdapter.notifyDataSetChanged();
                }
                break;
            case Key.requestCode.DELAY_DELETE:
                if (o instanceof com.infoutility.puerto.Models.Delay){
                    deleteDelay((com.infoutility.puerto.Models.Delay) o);
                }
                break;
            case Key.requestCode.DELETE_DELAY:
                if (o instanceof JsonObject){
                    JsonObject jObject = (JsonObject) o;
                    BaseResponse response = new Gson().fromJson(jObject.toString(), BaseResponse.class);

                    AlertUtil.showAlertOk(response.isResultado() ? getString(R.string.all_text_success) : getString(R.string.all_text_error),
                            response.getMessage(), getContext(), new AlertUtil.OnOkListener() {
                                @Override
                                public void onOk() {
                                    doDataLoad();
                                }
                            }).setCancelable(false)
                            .show();
                }
                break;
        }
    }
}
