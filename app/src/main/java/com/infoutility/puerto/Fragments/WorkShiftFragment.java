package com.infoutility.puerto.Fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.infoutility.puerto.Adapters.CurrentShiftAdapter;
import com.infoutility.puerto.Adapters.SpinnerAdapter;
import com.infoutility.puerto.Adapters.TextAdapter;
import com.infoutility.puerto.Api.ResourceHandler;
import com.infoutility.puerto.DialogFragments.DateTimeDialogFragment;
import com.infoutility.puerto.Interfaces.Response;
import com.infoutility.puerto.Keys.Key;
import com.infoutility.puerto.MainActivity;
import com.infoutility.puerto.Models.ContainerShip;
import com.infoutility.puerto.Models.Response.BaseResponse;
import com.infoutility.puerto.Models.Shift;
import com.infoutility.puerto.Models.ShiftType;
import com.infoutility.puerto.R;
import com.infoutility.puerto.Utils.AlertUtil;
import com.infoutility.puerto.Utils.PreferencesManager;
import com.infoutility.puerto.Utils.Tools;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class WorkShiftFragment extends Fragment implements View.OnClickListener, Response.OnResponse {
    public static final String TAG = WorkShiftFragment.class.getSimpleName();

    //    View
    private RecyclerView rvCurrentShifts;
    private Button btnAdd;
    private ImageView ivDateTimeStart;
    private ImageView ivDateTimeEnd;
    private Spinner spShiftType;
    private TextView tvDateTimeStart;
    private TextView tvDateTimeEnd;
    private TextInputEditText tietComment;
    private TextInputEditText tietCuadrilla;



    private CurrentShiftAdapter currentShiftAdapter;
    private List<Shift> lstShifts;

    private boolean isStartDateTime;


    private Bundle bContainerShipInfo;
    private ContainerShip containerShip;

    private int recorder = 0;
    private int workMode = 0;
    private String handheld;

    private String strDateTimeStart;
    private String strDateTimeEnd;

    private Date selectedTimeStart;
    private Date selectedTimeEnd;

    private Shift editShift;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(false);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_work_shift, container, false);

        PreferencesManager.initializeInstance(getContext());
        bContainerShipInfo = getArguments();

        ActionBar actionBar = ((MainActivity)getActivity()).getSupportActionBar();
        if (actionBar != null)
            actionBar.setTitle("TURNO");

        recorder = PreferencesManager.getInstance().getInt(Key.sharedPreferences.recorder, 0);
        workMode = PreferencesManager.getInstance().getInt(Key.sharedPreferences.workMode, 0);
        handheld = PreferencesManager.getInstance().getString(Key.sharedPreferences.handheld, null);

        if (bContainerShipInfo != null && recorder != 0) {
            containerShip = Tools.fromBundle(Key.extra.CONTAINER_SHIP_NAVIGATE, bContainerShipInfo);
            ResourceHandler.listShifts(this, Key.requestCode.GET_SHIFT, recorder, containerShip.getViaje_empornac());
        }


        ResourceHandler.listShiftTypes(this, Key.requestCode.GET_SHIFT_TYPE);

        rvCurrentShifts = v.findViewById(R.id.fws_rv_current_shifts);
        ivDateTimeStart = v.findViewById(R.id.fws_iv_datetime_start);
        ivDateTimeEnd = v.findViewById(R.id.fws_iv_datetime_end);

        spShiftType = v.findViewById(R.id.fws_sp_shift_type);

        tvDateTimeStart  = v.findViewById(R.id.fws_tv_datetime_start);
        tvDateTimeEnd  = v.findViewById(R.id.fws_tv_datetime_end);

        btnAdd = v.findViewById(R.id.fws_btn_add);

        tietComment = v.findViewById(R.id.fws_tiet_comment);
        tietCuadrilla = v.findViewById(R.id.fws_tiet_group);

        Tools.setFragmentLoading(getActivity(), true);

        lstShifts = new ArrayList<>();



        currentShiftAdapter = new CurrentShiftAdapter(getContext(), lstShifts, this);

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext());
        rvCurrentShifts.setLayoutManager(mLayoutManager);
        rvCurrentShifts.setAdapter(currentShiftAdapter);


        ivDateTimeStart.setOnClickListener(this);
        ivDateTimeEnd.setOnClickListener(this);

        btnAdd.setOnClickListener(this);

        return v;
    }

    private String validate(){
        if (handheld == null)
            return "Handheld no especificado.";
        if (recorder == 0)
            return "No existe recorder al cual asginar turno.";
        if (workMode == 0)
            return "Debe seleccionar una opcion de trabajo";
        if (strDateTimeStart == null)
            return "Debe selecionar la fecha de inicio de turno";
        if (strDateTimeEnd == null)
            return "Debe selecionar la fecha de fin de turno";
        if (containerShip == null)
            return "No existe viaje.";
        if (selectedTimeStart != null && selectedTimeEnd != null){
            if (selectedTimeStart.compareTo(selectedTimeEnd) > 0){
                return "La fecha de fin de turno debe ser mayor a la fecha de inicio.";
            }
        }
        return null;
    }

    private void clear(){
        strDateTimeStart = null;
        strDateTimeEnd = null;
    }

    private void addShift(){
        boolean isEdit = (editShift != null);
        String validationMessage = validate();
        if (validationMessage == null){
            com.infoutility.puerto.Models.Request.Shift shiftRequest = new com.infoutility.puerto.Models.Request.Shift();
            shiftRequest.setID_TURNO(isEdit ? editShift.getId_turno() : 1);//0
            shiftRequest.setFECHA_HORA_INICIO(strDateTimeStart);
            shiftRequest.setFECHA_HORA_FINAL(strDateTimeEnd);
            shiftRequest.setOBSERVACIONES(tietComment.getText() != null ? tietComment.getText().toString() : "");
            shiftRequest.setGRABADOR(recorder);
            shiftRequest.setFECHA_GRABACION(Tools.formatDate(new Date(), Key.datetimePattern.YYYYMMDD_HHMMSS_DASH_SEPARATOR));
            shiftRequest.setID_TIPO_TURNO(Integer.valueOf(((TextAdapter)spShiftType.getSelectedItem()).getId()));
            shiftRequest.setVIAJE_NO(String.valueOf(containerShip.getViaje_empornac()));
            shiftRequest.setTIPO_DE_MOVIMIENTO(workMode);
            shiftRequest.setHANDHELD_ID(handheld);
            if (tietCuadrilla.getText() != null && !tietCuadrilla.getText().toString().isEmpty())
                shiftRequest.setID_CUADRILLA(tietCuadrilla.getText().toString());//1
            shiftRequest.setACTUALIZACION(isEdit ? 1 : 0);

            ResourceHandler.postShift(this, Key.requestCode.POST_SHIFT, shiftRequest);
        } else {
            AlertUtil.showAlertOk(getString(R.string.all_text_warning), validationMessage, getContext(), new AlertUtil.OnOkListener() {
                @Override
                public void onOk() {
                    // do nothing
                }
            }).show();
        }
    }

    private void editShift(){
//        String validationMessage = validate();
//        if (validationMessage == null){
//            com.infoutility.puerto.Models.Request.Shift shiftRequest = new com.infoutility.puerto.Models.Request.Shift();
//            shiftRequest.setID_TURNO(shift.getId_turno());//0
//            shiftRequest.setFECHA_HORA_INICIO(strDateTimeStart);
//            shiftRequest.setFECHA_HORA_FINAL(strDateTimeEnd);
//            shiftRequest.setOBSERVACIONES(tietComment.getText() != null ? tietComment.getText().toString() : "");
//            shiftRequest.setGRABADOR(recorder);
//            shiftRequest.setFECHA_GRABACION(Tools.formatDate(new Date(), Key.datetimePattern.YYYYMMDD_HHMMSS_DASH_SEPARATOR));
//            shiftRequest.setID_TIPO_TURNO(Integer.valueOf(((TextAdapter)spShiftType.getSelectedItem()).getId()));
//            shiftRequest.setVIAJE_NO(String.valueOf(containerShip.getViaje_empornac()));
//            shiftRequest.setTIPO_DE_MOVIMIENTO(workMode);
//            shiftRequest.setHANDHELD_ID(handheld);
//            if (tietCuadrilla.getText() != null && !tietCuadrilla.getText().toString().isEmpty())
//                shiftRequest.setID_CUADRILLA(tietCuadrilla.getText().toString());//1
//            shiftRequest.setACTUALIZACION(1);
//            String s = new Gson().toJson(shiftRequest);
//            ResourceHandler.postShift(this, Key.requestCode.POST_SHIFT, shiftRequest);
//        } else {
//            AlertUtil.showAlertOk(getString(R.string.all_text_warning), validationMessage, getContext(), new AlertUtil.OnOkListener() {
//                @Override
//                public void onOk() {
//                    // do nothing
//                }
//            }).show();
//        }
        if (editShift != null){
            SpinnerAdapter.selectSpinnerItemById(spShiftType, String.valueOf(editShift.getId_tipo_turno()));
            strDateTimeStart = Tools.formatStrDate(editShift.getFecha_hora_inicio(), Key.datetimePattern.YYYYMMDD_HHMMSS_DASH_SEPARATOR);
            strDateTimeEnd = Tools.formatStrDate(editShift.getFecha_hora_final(), Key.datetimePattern.YYYYMMDD_HHMMSS_DASH_SEPARATOR);
            selectedTimeStart = Tools.toDate(strDateTimeStart, Key.datetimePattern.YYYYMMDD_HHMMSS_DASH_SEPARATOR);
            selectedTimeEnd = Tools.toDate(strDateTimeEnd, Key.datetimePattern.YYYYMMDD_HHMMSS_DASH_SEPARATOR);
            tvDateTimeStart.setText(strDateTimeStart);
            tvDateTimeEnd.setText(strDateTimeEnd);
            tietComment.setText(editShift.getObservaciones());
            tietCuadrilla.setText(editShift.getId_cuadrilla());
        } else {
            AlertUtil.showAlertOk(getString(R.string.all_text_warning), "No ha seleccionado un turno valido.", getContext(), new AlertUtil.OnOkListener() {
                @Override
                public void onOk() {
                    // do nothing
                }
            }).show();
        }
    }

    private void deleteShift(Shift shift){
        com.infoutility.puerto.Models.Request.Shift shiftRequest = new com.infoutility.puerto.Models.Request.Shift();
        shiftRequest.setID_TURNO(shift.getId_turno());
        shiftRequest.setGRABADOR(recorder);
        shiftRequest.setID_TIPO_TURNO(Integer.valueOf(shift.getId_tipo_turno()));
        shiftRequest.setVIAJE_NO(String.valueOf(containerShip.getViaje_empornac()));
        shiftRequest.setTIPO_DE_MOVIMIENTO(Integer.valueOf(shift.getTipo_de_movimiento()));

        ResourceHandler.deleteShift(this, Key.requestCode.DELETE_SHIFT, shiftRequest);
    }

    private void navigateToContainer(Shift shift){
        PreferencesManager.getInstance().setLong(Key.sharedPreferences.currentShift, shift.getId_turno());
        ContainerOperationsFragment containerOperationsFragment = new ContainerOperationsFragment();
        containerOperationsFragment.setArguments(Tools.fromObject(Key.extra.WORK_SHIFT_NAVIGATE, shift));
        Tools.setFragment(getFragmentManager(), R.id.fl_main_content, new ContainerOperationsFragment(), ContainerOperationsFragment.TAG);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.fws_iv_datetime_start:
                this.isStartDateTime = true;
                Tools.showDialogFragment(getChildFragmentManager(), DateTimeDialogFragment.newInstance(this), DateTimeDialogFragment.TAG);
                break;
            case R.id.fws_iv_datetime_end:
                this.isStartDateTime = false;
                Tools.showDialogFragment(getChildFragmentManager(), DateTimeDialogFragment.newInstance(this), DateTimeDialogFragment.TAG);
                break;
            case R.id.fws_btn_add:
                addShift();
                break;
            default:
                break;
        }
    }

    @Override
    public void onResponse(int requestCode, Object o, Object r) {
        switch (requestCode){
            case Key.requestCode.GET_SHIFT_TYPE:
                if (o instanceof JsonObject){
                    JsonObject response = (JsonObject) o;
                    SpinnerAdapter adapter = SpinnerAdapter.createArrayAdapter(getActivity(), response.getAsJsonArray(Key.jsonResponse.data).toString(), ShiftType.class);
                    spShiftType.setAdapter(adapter);
                }
                break;
            case Key.requestCode.GET_SHIFT:
                Tools.setFragmentLoading(getActivity(), false);
                if (o instanceof JsonObject){
                    lstShifts.clear();
                    JsonObject response = (JsonObject) o;
                    lstShifts.addAll(Tools.getListFromJson(response.getAsJsonArray(Key.jsonResponse.data).toString(), Shift.class));
                    currentShiftAdapter.notifyDataSetChanged();
                }
                break;
            case Key.requestCode.POST_SHIFT:
                if (o instanceof JsonObject){
                    JsonObject jObject = (JsonObject) o;
                    BaseResponse response = new Gson().fromJson(jObject.toString(), BaseResponse.class);

                    if (response.isResultado()){
                        editShift = null;
                        ResourceHandler.listShifts(WorkShiftFragment.this, Key.requestCode.GET_SHIFT, recorder, containerShip.getViaje_empornac());
                        AlertUtil.showAlertOk(getString(R.string.all_text_success), response.getMessage(), getContext(), new AlertUtil.OnOkListener() {
                            @Override
                            public void onOk() {
//                                ResourceHandler.listShifts(WorkShiftFragment.this, Key.requestCode.GET_SHIFT, recorder, containerShip.getViaje_empornac());
                            }
                        }).setCancelable(false)
                                .show();
                    } else {
                        AlertUtil.showAlertOk(getString(R.string.all_text_error), response.getMessage(), getContext(), new AlertUtil.OnOkListener() {
                            @Override
                            public void onOk() {
                                // do nothing
                            }
                        }).show();
                    }
                }
                break;
            case Key.requestCode.DELETE_SHIFT:
                if (o instanceof JsonObject){
                    JsonObject jObject = (JsonObject) o;
                    BaseResponse response = new Gson().fromJson(jObject.toString(), BaseResponse.class);

                    AlertUtil.showAlertOk(response.isResultado() ? getString(R.string.all_text_success) : getString(R.string.all_text_error),
                            response.getMessage(), getContext(), new AlertUtil.OnOkListener() {
                                @Override
                                public void onOk() {
                                    ResourceHandler.listShifts(WorkShiftFragment.this, Key.requestCode.GET_SHIFT, recorder, containerShip.getViaje_empornac());
                                }
                            }).setCancelable(false)
                            .show();
                }
                break;
            case Key.requestCode.CONTAINER_OPERATIONS:
                if (o instanceof Shift){
                    navigateToContainer((Shift)o);
                }
                break;
            case Key.requestCode.SHIFT_DELETE:
                if (o instanceof Shift){
                    deleteShift((Shift)o);
                }
                break;
            case Key.requestCode.SHIFT_EDIT:
                if (o instanceof Shift){
//                    editShift((Shift)o);
                    editShift = (Shift)o;
                    editShift();
                }
                break;
            case Key.requestCode.DIALOG_DATETIME:
                if (o instanceof Date){
                    Date selectedDateTime = (Date)o;
                    if (isStartDateTime){
                        selectedTimeStart = selectedDateTime;
                        strDateTimeStart = Tools.formatDate(selectedDateTime, Key.datetimePattern.YYYYMMDD_HHMMSS_DASH_SEPARATOR);
                        tvDateTimeStart.setText(strDateTimeStart);
                    } else {
                        selectedTimeEnd = selectedDateTime;
                        strDateTimeEnd = Tools.formatDate(selectedDateTime, Key.datetimePattern.YYYYMMDD_HHMMSS_DASH_SEPARATOR);
                        tvDateTimeEnd.setText(strDateTimeEnd);
                    }
                }
                break;
        }

    }
}
