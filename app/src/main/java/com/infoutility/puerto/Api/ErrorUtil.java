package com.infoutility.puerto.Api;

import java.io.IOException;
import java.lang.annotation.Annotation;

import okhttp3.ResponseBody;
import retrofit2.Converter;
import retrofit2.Response;

public class ErrorUtil {

    public static Error parseError(Response<?> response){
        Error error;

        Converter<ResponseBody, Error> converter =
                Client.getInstance()
                .responseBodyConverter(Error.class, new Annotation[0]);
        try {
            error = converter.convert(response.errorBody());
        } catch (IOException e){
            return new Error();
        }

        return error;
    }
}
