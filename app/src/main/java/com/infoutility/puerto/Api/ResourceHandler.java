package com.infoutility.puerto.Api;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.infoutility.puerto.Interfaces.Response;
import com.infoutility.puerto.Models.Consumption;
import com.infoutility.puerto.Models.ConsumptionWrp;
import com.infoutility.puerto.Models.ContainerPending;
import com.infoutility.puerto.Models.Dispatch;
import com.infoutility.puerto.Models.MeterUser;
import com.infoutility.puerto.Models.Reception;
import com.infoutility.puerto.Models.Request.ColedContainer;
import com.infoutility.puerto.Models.Request.ColedContainerDispatch;
import com.infoutility.puerto.Models.Request.Container;
import com.infoutility.puerto.Models.Request.ContainerJoined;
import com.infoutility.puerto.Models.Request.ContainerYard;
import com.infoutility.puerto.Models.Request.Delay;
import com.infoutility.puerto.Models.Request.Pinbox;
import com.infoutility.puerto.Models.Request.RelocationContainer;
import com.infoutility.puerto.Models.Request.ReportRelocation;
import com.infoutility.puerto.Models.Request.ReportRelocationInt;
import com.infoutility.puerto.Models.Request.Shift;
import com.infoutility.puerto.Models.YardMap;

import java.util.Map;

import retrofit2.Call;

public class ResourceHandler {

    private static EndpointInterface apiClient = Client.createService(EndpointInterface.class);

    public static void listMovementTypes(Response.OnResponse response, int statusCode){
        Call<JsonObject> call = apiClient.doGetMovementTypes();
        call.enqueue(new ResourceCallback<JsonObject>(response, statusCode));
    }

    public static void listShiftTypes(Response.OnResponse response, int statusCode){
        Call<JsonObject> call = apiClient.doGetShiftTypes();
        call.enqueue(new ResourceCallback<JsonObject>(response, statusCode));
    }

    public static void listTrips(Response.OnResponse response, int statusCode, String viaje_empornac, int max){
        Call<JsonObject> call = apiClient.doGetTrips(viaje_empornac, max);
        call.enqueue(new ResourceCallback<JsonObject>(response, statusCode));
    }

    public static void listContainerConditions(Response.OnResponse response, int statusCode){
        Call<JsonObject> call = apiClient.doGetContainerConditions();
        call.enqueue(new ResourceCallback<JsonObject>(response, statusCode));
    }

    public static void listCranes(Response.OnResponse response, int statusCode){
        Call<JsonObject> call = apiClient.doGetCranes();
        call.enqueue(new ResourceCallback<JsonObject>(response, statusCode));
    }

    public static void listContainerTypes(Response.OnResponse response, int statusCode){
        Call<JsonObject> call = apiClient.doGetContainerTypes();
        call.enqueue(new ResourceCallback<JsonObject>(response, statusCode));
    }

    public static void listContainerMeasures(Response.OnResponse response, int statusCode){
        Call<JsonObject> call = apiClient.doGetContainerMeasures();
        call.enqueue(new ResourceCallback<JsonObject>(response, statusCode));
    }

    public static void doLogin(Response.OnResponse response, int statusCode, String usr, String pass){
        Call<JsonObject> call = apiClient.doLogin(usr, pass);
        call.enqueue(new ResourceCallback<JsonObject>(response, statusCode));
    }

    public static void listPinboxTypes(Response.OnResponse response, int statusCode){
        Call<JsonObject> call = apiClient.doGetpinboxTypes();
        call.enqueue(new ResourceCallback<JsonObject>(response, statusCode));
    }

    public static void listDelayTypesDetail(Response.OnResponse response, int statusCode){
        Call<JsonObject> call = apiClient.doGetDelayTypesDetail();
        call.enqueue(new ResourceCallback<JsonObject>(response, statusCode));
    }

    public static void listSequences(Response.OnResponse response, int statusCode){
        Call<JsonObject> call = apiClient.doGetSequences();
        call.enqueue(new ResourceCallback<JsonObject>(response, statusCode));
    }

    public static void listActivities(Response.OnResponse response, int statusCode){
        Call<JsonObject> call = apiClient.doGetActivities();
        call.enqueue(new ResourceCallback<JsonObject>(response, statusCode));
    }

//    public static void listHandhelds(Response.OnResponse response, int statusCode){
//        Call<JsonObject> call = apiClient.doGetHandhelds();
//        call.enqueue(new ResourceCallback<JsonObject>(response, statusCode));
//    }

    public static void getHandhelds(Response.OnResponse response, int statusCode, Map<String, String> queries){
        Call<JsonObject> call = apiClient.doGetHandhelds(queries);
        call.enqueue(new ResourceCallback<JsonObject>(response, statusCode));
    }

    public static void listWorkModes(Response.OnResponse response, int statusCode){
        Call<JsonObject> call = apiClient.doGetWorkModes();
        call.enqueue(new ResourceCallback<JsonObject>(response, statusCode));
    }

    public static void listLoadTypes(Response.OnResponse response, int statusCode){
        Call<JsonObject> call = apiClient.doGetLoadTypes();
        call.enqueue(new ResourceCallback<JsonObject>(response, statusCode));
    }

    public static void listDelays(Response.OnResponse response, int statusCode, Map<String, String> queries){
        Call<JsonObject> call = apiClient.doGetDelays(queries);
        call.enqueue(new ResourceCallback<JsonObject>(response, statusCode));
    }

    public static void listShifts(Response.OnResponse response, int statusCode, int grabador, long viaje){
        Call<JsonObject> call = apiClient.doGetShifts(grabador, viaje);
        call.enqueue(new ResourceCallback<JsonObject>(response, statusCode));
    }

    public static void listContainers(Response.OnResponse response, int statusCode, Map<String, String> queries){
        Call<JsonObject> call = apiClient.doGetContainers(queries);
        call.enqueue(new ResourceCallback<JsonObject>(response, statusCode));
    }

    public static void listContainersJoined(Response.OnResponse response, int statusCode, Map<String, String> queries){
        Call<JsonObject> call = apiClient.doGetContainersJoined(queries);
        call.enqueue(new ResourceCallback<JsonObject>(response, statusCode));
    }

    public static void postShift(Response.OnResponse response, int statusCode, Shift shift){
        Call<JsonObject> call = apiClient.doPostShift(shift);
        call.enqueue(new ResourceCallback<JsonObject>(response, statusCode));
    }

    public static void deleteShift(Response.OnResponse response, int statusCode, Shift shift){
        Call<JsonObject> call = apiClient.doDeleteShift(shift);
        call.enqueue(new ResourceCallback<JsonObject>(response, statusCode));
    }

    public static void putContainer(Response.OnResponse response, int statusCode, Container container){
        Call<JsonObject> call = apiClient.doPutContainers(container);
        call.enqueue(new ResourceCallback<JsonObject>(response, statusCode));
    }

    public static void postContainer(Response.OnResponse response, int statusCode, Container container){
        Call<JsonObject> call = apiClient.doPostContainers(container);
        call.enqueue(new ResourceCallback<JsonObject>(response, statusCode));
    }

    public static void postContainerJoined(Response.OnResponse response, int statusCode, ContainerJoined containerJoined){
        Call<JsonObject> call = apiClient.doPostContainersJoined(containerJoined);
        call.enqueue(new ResourceCallback<JsonObject>(response, statusCode));
    }

    public static void deleteContainerJoined(Response.OnResponse response, int statusCode, ContainerJoined containerJoined){
        Call<JsonObject> call = apiClient.doDeleteContainersJoined(containerJoined);
        call.enqueue(new ResourceCallback<JsonObject>(response, statusCode));
    }

    public static void listPinboxes(Response.OnResponse response, int statusCode, Map<String, String> queries){
        Call<JsonObject> call = apiClient.doGetPinboxes(queries);
        call.enqueue(new ResourceCallback<JsonObject>(response, statusCode));
    }

    public static void postPinbox(Response.OnResponse response, int statusCode, Pinbox pinbox){
        Call<JsonObject> call = apiClient.doPostPinbox(pinbox);
        call.enqueue(new ResourceCallback<JsonObject>(response, statusCode));
    }

    public static void putPinbox(Response.OnResponse response, int statusCode, Pinbox pinbox){
        Call<JsonObject> call = apiClient.doPutPinbox(pinbox);
        call.enqueue(new ResourceCallback<JsonObject>(response, statusCode));
    }

    public static void deletePinbox(Response.OnResponse response, int statusCode, Pinbox pinbox){
        Call<JsonObject> call = apiClient.doDeletePinbox(pinbox);
        call.enqueue(new ResourceCallback<JsonObject>(response, statusCode));
    }

    public static void listDelayTypes(Response.OnResponse response, int statusCode){
        Call<JsonObject> call = apiClient.doGetDelayTypes();
        call.enqueue(new ResourceCallback<JsonObject>(response, statusCode));
    }

    public static void postDelay(Response.OnResponse response, int statusCode, Delay delay){
        Call<JsonObject> call = apiClient.doPostDelay(delay);
        call.enqueue(new ResourceCallback<JsonObject>(response, statusCode));
    }

    public static void deleteDelay(Response.OnResponse response, int statusCode, Delay delay){
        Call<JsonObject> call = apiClient.doDeleteDelay(delay);
        call.enqueue(new ResourceCallback<JsonObject>(response, statusCode));
    }

    public static void listContainersYard(Response.OnResponse response, int statusCode, Map<String, String> queries){
        Call<JsonObject> call = apiClient.doGetContainersYard(queries);
        call.enqueue(new ResourceCallback<JsonObject>(response, statusCode));
    }

    public static void listMachines(Response.OnResponse response, int statusCode){
        Call<JsonObject> call = apiClient.doGetMachines();
        call.enqueue(new ResourceCallback<JsonObject>(response, statusCode));
    }

    public static void listYards(Response.OnResponse response, int statusCode){
        Call<JsonObject> call = apiClient.doGetYards();
        call.enqueue(new ResourceCallback<JsonObject>(response, statusCode));
    }


    public static void postContainerYard(Response.OnResponse response, int statusCode, ContainerYard containerYard){
        Call<JsonObject> call = apiClient.doPostContainerYard(containerYard);
        call.enqueue(new ResourceCallback<JsonObject>(response, statusCode));
    }

    public static void putContainerYard(Response.OnResponse response, int statusCode, ContainerYard containerYard){
        Call<JsonObject> call = apiClient.doPutContainerYard(containerYard);
        call.enqueue(new ResourceCallback<JsonObject>(response, statusCode));
    }


    public static void listBlocks(Response.OnResponse response, int statusCode, Map<String, String> queries){
        Call<JsonObject> call = apiClient.doGetListBlock(queries);
        call.enqueue(new ResourceCallback<JsonObject>(response, statusCode));
    }

    public static void listLevelForBlock(Response.OnResponse response, int statusCode, Map<String, String> queries){
        Call<JsonObject> call = apiClient.doGetListLevelForBlock(queries);
        call.enqueue(new ResourceCallback<JsonObject>(response, statusCode));
    }

    public static void listContainerForLevel(Response.OnResponse response, int statusCode, Map<String, String> queries){
        Call<JsonObject> call = apiClient.doGetListContainerForLevel(queries);
        call.enqueue(new ResourceCallback<JsonObject>(response, statusCode));
    }

    public static void listReportRelocation(Response.OnResponse response, int statusCode, Map<String, String> queries){
        Call<JsonObject> call = apiClient.doGetReportRelocation(queries);
        call.enqueue(new ResourceCallback<JsonObject>(response, statusCode));
    }


    public static void postCreateReportRelocation(Response.OnResponse response, int statusCode, ReportRelocation reportRelocation){
        Call<JsonObject> call = apiClient.doPostReportRelocation(reportRelocation);
        call.enqueue(new ResourceCallback<JsonObject>(response, statusCode));
    }

    public static void listReportRelocationInt(Response.OnResponse response, int statusCode, Map<String, String> queries){
        Call<JsonObject> call = apiClient.doGetReportRelocationInt(queries);
        call.enqueue(new ResourceCallback<JsonObject>(response, statusCode));
    }

    public static void postCreateReportRelocationImt(Response.OnResponse response, int statusCode, ReportRelocationInt reportRelocationInt){
        Call<JsonObject> call = apiClient.doPostReportRelocationInt(reportRelocationInt);
        call.enqueue(new ResourceCallback<JsonObject>(response, statusCode));
    }

    public static void postCreateRelocationContainer(Response.OnResponse response, int statusCode, RelocationContainer relocationContainer){
        Call<JsonObject> call = apiClient.doPostRelocationContainer(relocationContainer);
        call.enqueue(new ResourceCallback<JsonObject>(response, statusCode));
    }

    public static void infoRelocation(Response.OnResponse response, int statusCode, Map<String, String> queries){
        Call<JsonObject> call = apiClient.doGetInfoRelocation(queries);
        call.enqueue(new ResourceCallback<JsonObject>(response, statusCode));
    }

    public static void getReception(Response.OnResponse response, int statusCode, Map<String, String> queries){
        Call<JsonObject> call = apiClient.doGetReception(queries);
        call.enqueue(new ResourceCallback<JsonObject>(response, statusCode));
    }

    public static void postReception(Response.OnResponse response, int statusCode, Reception reception){
        Call<JsonObject> call = apiClient.doPostReception(reception);
        call.enqueue(new ResourceCallback<JsonObject>(response, statusCode));
    }

    public static void getDispatch(Response.OnResponse response, int statusCode, Map<String, String> queries){
        Call<JsonObject> call = apiClient.doGetDispatch(queries);
        call.enqueue(new ResourceCallback<JsonObject>(response, statusCode));
    }

    public static void postDispatch(Response.OnResponse response, int statusCode, Dispatch dispatch){
        Call<JsonObject> call = apiClient.doPostDispatch(dispatch);
        call.enqueue(new ResourceCallback<JsonObject>(response, statusCode));
    }

    public static void getMeterUser(Response.OnResponse response, int statusCode, Map<String, String> queries){
        Call<JsonObject> call = apiClient.doGetMeterUser(queries);
        call.enqueue(new ResourceCallback<JsonObject>(response, statusCode));
    }

    public static void getMeter(Response.OnResponse response, int statusCode, Map<String, String> queries){
        Call<JsonObject> call = apiClient.doGetMeter(queries);
        call.enqueue(new ResourceCallback<JsonObject>(response, statusCode));
    }

    public static void getConsumptionHistory(Response.OnResponse response, int statusCode, Map<String, String> queries){
        Call<JsonObject> call = apiClient.doGetConsumptionHistory(queries);
        call.enqueue(new ResourceCallback<JsonObject>(response, statusCode));
    }

    public static void postConsumptionHistory(Response.OnResponse response, int statusCode, ConsumptionWrp consumptionWrp){
        Call<JsonObject> call = apiClient.doPostConsumptionHistory(consumptionWrp.getConsumption());
        call.enqueue(new ResourceCallback<JsonObject>(response, statusCode, consumptionWrp));
    }

    public static void getRate(Response.OnResponse response, int statusCode){
        Call<JsonObject> call = apiClient.doGetRate();
        call.enqueue(new ResourceCallback<JsonObject>(response, statusCode));
    }

    public static void getTypeRelocatio (Response.OnResponse response, int statusCode){
        Call<JsonObject> call = apiClient.doGetListTypeRelocation();
        call.enqueue(new ResourceCallback<JsonObject>(response, statusCode));
    }

    public static void getInfoRequest(Response.OnResponse response, int statusCode, Map<String, String> queries){
        Call<JsonObject> call = apiClient.doGetInfoRequest(queries);
        call.enqueue(new ResourceCallback<JsonObject>(response, statusCode));
    }

    public static void getInfoRequestExist(Response.OnResponse response, int statusCode, Map<String, String> queries){
        Call<JsonObject> call = apiClient.doGetInfoRequestExist(queries);
        call.enqueue(new ResourceCallback<JsonObject>(response, statusCode));
    }

    public static void postCreateRequertColed(Response.OnResponse response, int statusCode, ColedContainer coledContainer){
        Call<JsonObject> call = apiClient.doPostRequestColed(coledContainer);
        call.enqueue(new ResourceCallback<JsonObject>(response, statusCode));
    }

    public static void getInfoRequestDispatch(Response.OnResponse response, int statusCode, Map<String, String> queries){
        Call<JsonObject> call = apiClient.doGetInfoRequestDispatch(queries);
        call.enqueue(new ResourceCallback<JsonObject>(response, statusCode));
    }

    public static void postCreateRequertColedDispatch(Response.OnResponse response, int statusCode, ColedContainerDispatch coledContainerDispatch){
        Call<JsonObject> call = apiClient.doPostRequestColedDispatch(coledContainerDispatch);
        call.enqueue(new ResourceCallback<JsonObject>(response, statusCode));
    }

    public static void listSearchPowerMeter(Response.OnResponse response, int statusCode, Map<String, String> queries){
        Call<JsonObject> call = apiClient.doGetListSearchPowerMeter(queries);
        call.enqueue(new ResourceCallback<JsonObject>(response, statusCode));
    }

    public static void postContainerYardRevertImport(Response.OnResponse response, int statusCode, ContainerYard containerYard){
        Call<JsonObject> call = apiClient.doPostContainerYardRevertImport(containerYard);
        call.enqueue(new ResourceCallback<JsonObject>(response, statusCode));
    }

    public static void putDispatch(Response.OnResponse response, int statusCode, Dispatch dispatch){
        Call<JsonObject> call = apiClient.doPutDispatch(dispatch);
        call.enqueue(new ResourceCallback<JsonObject>(response, statusCode));
    }

    public static void putReception(Response.OnResponse response, int statusCode, Reception reception){
        Call<JsonObject> call = apiClient.doPutReception(reception);
        call.enqueue(new ResourceCallback<JsonObject>(response, statusCode));
    }

    public static void postYardMap(Response.OnResponse response, int statusCode, YardMap yardMap){
        Call<JsonObject> call = apiClient.doPostYardMap(yardMap);
        call.enqueue(new ResourceCallback<JsonObject>(response, statusCode));
    }

    public static void postContainerYardRevertExport(Response.OnResponse response, int statusCode, ContainerYard containerYard){
        Call<JsonObject> call = apiClient.doPostContainerYardRevertExport(containerYard);
        call.enqueue(new ResourceCallback<JsonObject>(response, statusCode));
    }

    public static void getContainerPending(Response.OnResponse response, int statusCode, Map<String, String> queries){
        Call<JsonObject> call = apiClient.doGetContainerPending(queries);
        call.enqueue(new ResourceCallback<JsonObject>(response, statusCode));
    }

    public static void postContainerPending(Response.OnResponse response, int statusCode, ContainerPending containerPending){
        Call<JsonObject> call = apiClient.doPostContainerPending(containerPending);
        call.enqueue(new ResourceCallback<JsonObject>(response, statusCode));
    }

    public static void deleteContainerPending(Response.OnResponse response, int statusCode, ContainerPending containerPending){
        Call<JsonObject> call = apiClient.doDeleteContainerPending(containerPending);
        call.enqueue(new ResourceCallback<JsonObject>(response, statusCode));
    }

    public static void getUsersOffline (Response.OnResponse response, int statusCode){
        Call<JsonObject> call = apiClient.doGetUsersOffline();
        call.enqueue(new ResourceCallback<JsonObject>(response, statusCode));
    }

    public static void getContainerConfirmed (Response.OnResponse response, int statusCode, Map<String, String> queries){
        Call<JsonObject> call = apiClient.doGetContainerConfirmed(queries);
        call.enqueue(new ResourceCallback<JsonObject>(response, statusCode));
    }

}
