package com.infoutility.puerto.Api;

import android.text.TextUtils;

import com.infoutility.puerto.Activities.LoginActivity;
import com.infoutility.puerto.Keys.Key;
import com.infoutility.puerto.Models.Activity;
import com.infoutility.puerto.Models.Login;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class Client {

    private static Retrofit retrofit = null;

//    public static Retrofit getInstance(){
//        if (retrofit == null){
//            retrofit = new Retrofit.Builder()
//                    .baseUrl(Key.api.baseUrl)
//                    .addConverterFactory(GsonConverterFactory.create())
//                    .build();
//        }
//        return retrofit;
//    }


    private static OkHttpClient.Builder httpClient = new OkHttpClient.Builder();

    private static Retrofit.Builder builder =
            new Retrofit.Builder()
                    .baseUrl(LoginActivity.IPADDRESS)
                    .addConverterFactory(GsonConverterFactory.create());

    public static <S> S createService(Class<S> serviceClass) {
        return createService(serviceClass, null);
    }

    public static <S> S createService(Class<S> serviceClass, final String authToken) {
        httpClient.connectTimeout(1, TimeUnit.MINUTES)
                .readTimeout(1, TimeUnit.MINUTES)
                .writeTimeout(30, TimeUnit.SECONDS);
        builder.client(httpClient.build());
        if (!TextUtils.isEmpty(authToken)) {
            AuthenticationInterceptor interceptor =
                    new AuthenticationInterceptor(authToken);

            if (!httpClient.interceptors().contains(interceptor)) {
                httpClient.addInterceptor(interceptor);
                builder.client(httpClient.build());
            }
        }
        retrofit = builder.build();

        return retrofit.create(serviceClass);
    }

    public static Retrofit getInstance(){
        return retrofit;
    }
}
