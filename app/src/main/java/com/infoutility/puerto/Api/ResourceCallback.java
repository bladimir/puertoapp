package com.infoutility.puerto.Api;

import android.util.Log;

import com.google.gson.JsonObject;
import com.infoutility.puerto.Keys.Key;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ResourceCallback<T> implements Callback<T> {

    private com.infoutility.puerto.Interfaces.Response.OnResponse onResponse;
    private int statusCode;
    private Object request;

    public ResourceCallback(com.infoutility.puerto.Interfaces.Response.OnResponse onResponse, int statusCode) {
        this.onResponse = onResponse;
        this.statusCode = statusCode;
    }

    public ResourceCallback(com.infoutility.puerto.Interfaces.Response.OnResponse onResponse, int statusCode, Object request) {
        this.onResponse = onResponse;
        this.statusCode = statusCode;
        this.request = request;
    }

    @Override
    public void onResponse(Call<T> call, Response<T> response) {
        if (response.isSuccessful() || response.body() != null){
            onResponse.onResponse(statusCode, response.body(), request);
        } else {
//            Error error = ErrorUtil.parseError(response);
//            Log.d("ResourceCallback", "Error");
            onResponse.onResponse(statusCode, new Status(response.code()), null);
            Log.d("ResourceCallback", "Error");
        }
    }

    @Override
    public void onFailure(Call<T> call, Throwable t) {
        if (t instanceof IOException){
            Log.d("ResourceCallback", "Error Conexion");
            onResponse.onResponse(statusCode, new Status(Key.statusCode.noInternet.code), request);
        } else {
            Log.d("ResourceCallback", "Error Desconocido");
            onResponse.onResponse(statusCode, new Status(Key.statusCode.unknown.code), null);
        }
    }

    private static <T> Throwable convertUnsuccessfulResponseToException(Response<T> response) {
        response.errorBody();
        response.code();
        response.headers();
        return null;
    }
}
