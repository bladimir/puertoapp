package com.infoutility.puerto.Api;

import com.google.gson.JsonObject;
import com.infoutility.puerto.Keys.Key;
import com.infoutility.puerto.Models.Consumption;
import com.infoutility.puerto.Models.ContainerPending;
import com.infoutility.puerto.Models.Dispatch;
import com.infoutility.puerto.Models.MeterUser;
import com.infoutility.puerto.Models.Reception;
import com.infoutility.puerto.Models.Request.ColedContainer;
import com.infoutility.puerto.Models.Request.ColedContainerDispatch;
import com.infoutility.puerto.Models.Request.ContainerYard;
import com.infoutility.puerto.Models.Request.Pinbox;
import com.infoutility.puerto.Models.Request.Container;
import com.infoutility.puerto.Models.Request.ContainerJoined;
import com.infoutility.puerto.Models.Request.Delay;
import com.infoutility.puerto.Models.Request.RelocationContainer;
import com.infoutility.puerto.Models.Request.ReportRelocation;
import com.infoutility.puerto.Models.Request.ReportRelocationInt;
import com.infoutility.puerto.Models.Request.Shift;
import com.infoutility.puerto.Models.YardMap;

import java.util.Map;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.HTTP;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Query;
import retrofit2.http.QueryMap;

public interface EndpointInterface {

    @GET(Key.api.moveType)
    Call<JsonObject>  doGetMovementTypes();

    @GET(Key.api.containerType)
    Call<JsonObject>  doGetContainerTypes();

    @GET(Key.api.shiftType)
    Call<JsonObject>  doGetShiftTypes();

    @GET(Key.api.pinboxType)
    Call<JsonObject>  doGetpinboxTypes();

    @GET(Key.api.delayTypesDetail)
    Call<JsonObject> doGetDelayTypesDetail();

    @GET(Key.api.sequence)
    Call<JsonObject>  doGetSequences();

    @GET(Key.api.activities)
    Call<JsonObject>  doGetActivities();

    @GET(Key.api.containerCondition)
    Call<JsonObject>  doGetContainerConditions();

    @GET(Key.api.crane)
    Call<JsonObject>  doGetCranes();

//    @GET(Key.api.handheld)
//    Call<JsonObject>  doGetHandhelds();
    @GET(Key.api.handheld)
    Call<JsonObject>  doGetHandhelds(@QueryMap Map<String, String> queries);

    @GET(Key.api.containerMeasures)
    Call<JsonObject>  doGetContainerMeasures();

    @GET(Key.api.workMode)
    Call<JsonObject>  doGetWorkModes();

    @GET(Key.api.loadType)
    Call<JsonObject>  doGetLoadTypes();

    @GET(Key.api.delay)
    Call<JsonObject>  doGetDelays(@QueryMap Map<String, String> queries);

    @GET(Key.api.trip)
    Call<JsonObject>  doGetTrips(@Query("viaje_empornac") String viaje_empornac, @Query("max") int max);

    @GET(Key.api.login)
    Call<JsonObject>  doLogin(@Query("usr") String usr, @Query("pass") String pass);

    @GET(Key.api.shift)
    Call<JsonObject>  doGetShifts(@Query("grabador") int grabador, @Query("viaje") long viaje);

//    @GET(Key.api.container)
//    Call<JsonObject>  doGetContainers(@Query("viaje") long viaje, @Query("tipo") int tipo);
    @GET(Key.api.container)
    Call<JsonObject>  doGetContainers(@QueryMap Map<String, String> queries);

    @GET(Key.api.containerJoined)
    Call<JsonObject>  doGetContainersJoined(@QueryMap Map<String, String> queries);

    @POST(Key.api.containerJoined)
    Call<JsonObject>  doPostContainersJoined(@Body ContainerJoined containerJoined);

    @HTTP(method = "DELETE", path = Key.api.containerJoined, hasBody = true)
    Call<JsonObject>  doDeleteContainersJoined(@Body ContainerJoined containerJoined);

    @PUT(Key.api.container)
    Call<JsonObject>  doPutContainers(@Body Container container);

    @POST(Key.api.container)
    Call<JsonObject>  doPostContainers(@Body Container container);

    @POST(Key.api.shift)
    Call<JsonObject>  doPostShift(@Body Shift shift);

//    @DELETE(Key.api.shift)
    @HTTP(method = "DELETE", path = Key.api.shift, hasBody = true)
    Call<JsonObject>  doDeleteShift(@Body Shift shift);

    @GET(Key.api.pinbox)
    Call<JsonObject>  doGetPinboxes(@QueryMap Map<String, String> queries);

    @POST(Key.api.pinbox)
    Call<JsonObject>  doPostPinbox(@Body Pinbox pinbox);

    @PUT(Key.api.pinbox)
    Call<JsonObject>  doPutPinbox(@Body Pinbox pinbox);

    @HTTP(method = "DELETE", path = Key.api.pinbox, hasBody = true)
    Call<JsonObject>  doDeletePinbox(@Body Pinbox pinbox);

    @GET(Key.api.delayType)
    Call<JsonObject>  doGetDelayTypes();

    @POST(Key.api.delay)
    Call<JsonObject>  doPostDelay(@Body Delay delay);

    @HTTP(method = "DELETE", path = Key.api.delay, hasBody = true)
    Call<JsonObject>  doDeleteDelay(@Body Delay delay);

    @GET(Key.api.containerYard)
    Call<JsonObject>  doGetContainersYard(@QueryMap Map<String, String> queries);

    @GET(Key.api.machine)
    Call<JsonObject>  doGetMachines();

    @GET(Key.api.yard)
    Call<JsonObject>  doGetYards();

    @POST(Key.api.exportImport)
    Call<JsonObject> doPostContainerYard(@Body ContainerYard containerYard);

    @PUT(Key.api.exportImport)
    Call<JsonObject> doPutContainerYard(@Body ContainerYard containerYard);

    @GET(Key.api.listBlock)
    Call<JsonObject>  doGetListBlock(@QueryMap Map<String, String> queries);

    @GET(Key.api.listLevelForBlock)
    Call<JsonObject>  doGetListLevelForBlock(@QueryMap Map<String, String> queries);

    @GET(Key.api.listContainerForLevel)
    Call<JsonObject>  doGetListContainerForLevel(@QueryMap Map<String, String> queries);

    @GET(Key.api.listReportRelocation)
    Call<JsonObject>  doGetReportRelocation(@QueryMap Map<String, String> queries);

    @POST(Key.api.listReportRelocation)
    Call<JsonObject>  doPostReportRelocation(@Body ReportRelocation reportRelocation);

    @GET(Key.api.listReportRelocationInt)
    Call<JsonObject>  doGetReportRelocationInt(@QueryMap Map<String, String> queries);

    @POST(Key.api.listReportRelocationInt)
    Call<JsonObject>  doPostReportRelocationInt(@Body ReportRelocationInt reportRelocationInt);

    @POST(Key.api.listRelocationContainer)
    Call<JsonObject>  doPostRelocationContainer(@Body RelocationContainer relocationContainer);

    @GET(Key.api.InfoRelocation)
    Call<JsonObject>  doGetInfoRelocation(@QueryMap Map<String, String> queries);

    @GET(Key.api.reception)
    Call<JsonObject>  doGetReception(@QueryMap Map<String, String> queries);

    @POST(Key.api.reception)
    Call<JsonObject>  doPostReception(@Body Reception reception);

    @GET(Key.api.dispatch)
    Call<JsonObject>  doGetDispatch(@QueryMap Map<String, String> queries);

    @POST(Key.api.dispatch)
    Call<JsonObject>  doPostDispatch(@Body Dispatch dispatch);

    @GET(Key.api.meterUser)
    Call<JsonObject>  doGetMeterUser(@QueryMap Map<String, String> queries);

    @GET(Key.api.meter)
    Call<JsonObject>  doGetMeter(@QueryMap Map<String, String> queries);

    @GET(Key.api.consumptionHistory)
    Call<JsonObject>  doGetConsumptionHistory(@QueryMap Map<String, String> queries);

    @POST(Key.api.consumptionHistory)
    Call<JsonObject>  doPostConsumptionHistory(@Body Consumption consumption);

    @GET(Key.api.rate)
    Call<JsonObject>  doGetRate();

    @GET(Key.api.listTypeRelocation)
    Call<JsonObject>  doGetListTypeRelocation();

    @GET(Key.api.infoRequest)
    Call<JsonObject>  doGetInfoRequest(@QueryMap Map<String, String> queries);

    @GET(Key.api.infoRequestExist)
    Call<JsonObject>  doGetInfoRequestExist(@QueryMap Map<String, String> queries);

    @POST(Key.api.requestColed)
    Call<JsonObject>  doPostRequestColed(@Body ColedContainer coledContainer);

    @GET(Key.api.infoRequestDispatch)
    Call<JsonObject>  doGetInfoRequestDispatch(@QueryMap Map<String, String> queries);

    @POST(Key.api.requestColedDispatch)
    Call<JsonObject>  doPostRequestColedDispatch(@Body ColedContainerDispatch coledContainerDispatch);

    @GET(Key.api.listSearchPowerMeter)
    Call<JsonObject>  doGetListSearchPowerMeter(@QueryMap Map<String, String> queries);

    @POST(Key.api.revertImport)
    Call<JsonObject> doPostContainerYardRevertImport(@Body ContainerYard containerYard);

    @PUT(Key.api.dispatch)
    Call<JsonObject>  doPutDispatch(@Body Dispatch dispatch);

    @PUT(Key.api.reception)
    Call<JsonObject>  doPutReception(@Body Reception reception);

    @POST(Key.api.yardMap)
    Call<JsonObject> doPostYardMap(@Body YardMap yardMap);

    @POST(Key.api.revertExport)
    Call<JsonObject> doPostContainerYardRevertExport(@Body ContainerYard containerYard);

    @GET(Key.api.containerPending)
    Call<JsonObject>  doGetContainerPending(@QueryMap Map<String, String> queries);

    @POST(Key.api.containerPending)
    Call<JsonObject>  doPostContainerPending(@Body ContainerPending containerPending);

    @HTTP(method = "DELETE", path = Key.api.containerPending, hasBody = true)
    Call<JsonObject>  doDeleteContainerPending(@Body ContainerPending containerPending);

    @GET(Key.api.usersOffline)
    Call<JsonObject>  doGetUsersOffline();

    @GET(Key.api.containerConfirmed)
    Call<JsonObject>  doGetContainerConfirmed(@QueryMap Map<String, String> queries);

}
