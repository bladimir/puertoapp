package com.infoutility.puerto.Api;

import com.infoutility.puerto.Keys.Key;

import java.util.Locale;

public class Status {
    private int code;
    private String message;

    public Status(int code) {
        this.code = code;
        this.setMessage();
    }

    public int getCode() {
        return code;
    }

    public String getMessage() {
        return String.format(Locale.getDefault(), "[%d] - %s", code, message);
    }

    private void setMessage() {
        switch (code){
            case Key.statusCode.Continue.code:
                message = Key.statusCode.Continue.message;
                break;
            case Key.statusCode.success.code:
                message = Key.statusCode.success.message;
                break;
            case Key.statusCode.multipleChoice.code:
                message = Key.statusCode.multipleChoice.message;
                break;
            case Key.statusCode.badRequest.code:
                message = Key.statusCode.badRequest.message;
                break;
            case Key.statusCode.unauthorized.code:
                message = Key.statusCode.unauthorized.message;
                break;
            case Key.statusCode.forbidden.code:
                message = Key.statusCode.forbidden.message;
                break;
            case Key.statusCode.notFound.code:
                message = Key.statusCode.notFound.message;
                break;
            case Key.statusCode.internalServerError.code:
                message = Key.statusCode.internalServerError.message;
                break;
            case Key.statusCode.noInternet.code:
                message = Key.statusCode.noInternet.message;
                break;
            case Key.statusCode.unknown.code:
                message = Key.statusCode.unknown.message;
                break;
        }
    }
}
